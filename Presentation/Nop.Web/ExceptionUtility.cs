using Nop.Core.Domain.Messages;
using Nop.Services.Messages;
using System;
using System.Configuration;
using System.IO;
using System.Net.Mail;
using System.Text;
using System.Web;
using System.Linq;
using System.Data.SqlClient;

// Create our own utility for exceptions
public sealed class ExceptionUtility
{
    // All methods are static, so this can be private

    private ExceptionUtility()
    {
    }

    static string Email = string.Empty;
    static string Host = string.Empty;
    static string Password = string.Empty;
    static int Port = 0;
    static bool EnableSSL = false;

    // Log an Exception
    public static void LogException(Exception exc, string source)
    {
       //// // Include enterprise logic for logging exceptions
       //// // Get the absolute path to the log file
       //// //string logFile = "~/App_Data/ErrorLog.txt";
        //string pathString =Convert.ToString(ConfigurationManager.AppSettings["ErrorFilePath"]);
        string logFile = Convert.ToString(ConfigurationManager.AppSettings["ErrorFilePath"]);


        // logFile = HttpContext.Current.Server.MapPath(logFile);

        // Open the log file for append and write the log
        StreamWriter sw = new StreamWriter(logFile, true);
        sw.WriteLine("********** {0} **********", DateTime.Now);
        if (exc.InnerException != null)
        {
            sw.Write("Inner Exception Type: ");
            sw.WriteLine(exc.InnerException.GetType().ToString());
            sw.Write("Inner Exception: ");
            sw.WriteLine(exc.InnerException.Message);
            sw.Write("Inner Source: ");
            sw.WriteLine(exc.InnerException.Source);
            if (exc.InnerException.StackTrace != null)
            {
                sw.WriteLine("Inner Stack Trace: ");
                sw.WriteLine(exc.InnerException.StackTrace);
            }
        }
        sw.Write("Exception Type: ");
        sw.WriteLine(exc.GetType().ToString());
        sw.WriteLine("Exception: " + exc.Message);
        sw.WriteLine("Source: " + source);
        sw.WriteLine("Stack Trace: ");
        if (exc.StackTrace != null)
        {
            sw.WriteLine(exc.StackTrace);
            sw.WriteLine();
        }
        sw.Close();

    }

    public static int GetDefaultEmailID()
    {
        string query = "select value from setting where name ='emailaccountsettings.defaultemailaccountid'";
        string connString = System.IO.File.ReadAllText(HttpContext.Current.Server.MapPath("/App_Data/Settings.txt"));
        connString = connString.Substring(connString.LastIndexOf(":") + 1).Trim();
        using (SqlConnection conn = new SqlConnection(connString))
        {
            SqlCommand cmd = new SqlCommand(query, conn);
            try
            {
                conn.Open();
                return Convert.ToInt32(cmd.ExecuteScalar());
            }
            catch (Exception ex)
            {
                Console.WriteLine(ex.Message);
                return 0;
            }
        }
    }

    public static void GetEmailAccountDetails()
    {
        string query = "select * from EmailAccount where Id =" + GetDefaultEmailID();
        string connString = System.IO.File.ReadAllText(HttpContext.Current.Server.MapPath("/App_Data/Settings.txt"));
        connString = connString.Substring(connString.LastIndexOf(":") + 1).Trim();
        using (SqlConnection conn = new SqlConnection(connString))
        {
            SqlCommand cmd = new SqlCommand(query, conn);
            try
            {
                conn.Open();
                SqlDataReader reader = cmd.ExecuteReader();
                while (reader.Read())
                {
                    // get the results of each column
                    Email = (string)reader["Email"];
                    Password = (string)reader["Password"];
                    Host = (string)reader["Host"];
                    Port = (int)reader["Port"];
                    EnableSSL = (bool)reader["EnableSsl"];
                }
            }
            catch (Exception ex)
            {
                Console.WriteLine(ex.Message);
            }
        }
    }

    // Notify System Operators about an exception
    public static void NotifySystemOps(Exception exc)
    {
        //EmailAccountSettings _emailAccountSettings = Type.GetType("Nop.Core.Domain.Messages.EmailAccountSettings").GetMethod("GetInstance").Invoke(null, null) as EmailAccountSettings;
        //IEmailAccountService _emailAccountService = null;
        // Include code for notifying IT system operators
        //var emailAccount = _emailAccountService.GetEmailAccountById(GetDefaultEmailID());
        GetEmailAccountDetails();

        string emailFrom = Email;
        string emailTo = Convert.ToString(ConfigurationManager.AppSettings["emailReportErrorsTo"]);
        string subject = "Site Error  From " + Host;
        StringBuilder errorString = new StringBuilder();

        if (exc.InnerException != null)
        {
            errorString.Append("Inner Exception Type: ").Append(exc.InnerException.GetType().ToString()).Append(System.Environment.NewLine);
            errorString.Append("Inner Exception: ").Append(exc.InnerException.Message).Append(System.Environment.NewLine);
            errorString.Append("Inner Source: ").Append(exc.InnerException.Source).Append(System.Environment.NewLine);
            if (exc.InnerException.StackTrace != null)
            {
                errorString.Append("Inner Stack Trace: ").Append(exc.InnerException.StackTrace).Append(System.Environment.NewLine);
            }
        }
        errorString.Append("Exception Type: ").Append(exc.GetType().ToString()).Append(System.Environment.NewLine);
        errorString.Append("Exception: " + exc.Message).Append("Source: " + exc.Source);
        errorString.Append("Stack Trace: ");
        if (exc.StackTrace != null)
        {
            errorString.Append(exc.StackTrace).Append(System.Environment.NewLine);            
        }
        SendEmail(emailFrom, emailTo, subject, errorString.ToString());

    }

	// Notify System Operators about an exception
    public void NotifySystemOps(Exception exc, string path, string originalurl, string username)
    {
        //EmailAccountSettings _emailAccountSettings = Type.GetType("Nop.Core.Domain.Messages.EmailAccountSettings").GetMethod("GetInstance").Invoke(null, null) as EmailAccountSettings;
        //IEmailAccountService _emailAccountService = null;
        //var emailAccount = _emailAccountService.GetEmailAccountById(GetDefaultEmailID());
        //string emailFrom = emailAccount.Email;
        //string emailTo = Convert.ToString(ConfigurationManager.AppSettings["emailReportErrorsTo"]);
        //string subject = "Site Error  From " + emailAccount.Host;
        //StringBuilder errorString = new StringBuilder();
        //string websiterootpath = Convert.ToString(ConfigurationManager.AppSettings["WebSiteRootPath"]);		
       
        //if (exc.InnerException != null)
        //{
        //    errorString.Append("Logged In UserName: ").Append(username.ToString()).Append(System.Environment.NewLine);
        //    errorString.Append("PageName : ").Append(System.IO.Path.GetFileName(path).ToString()).Append(System.Environment.NewLine);
        //    errorString.Append("Path for Page on the Disk  :").Append(websiterootpath).Append(path).Append(System.Environment.NewLine);
        //    errorString.Append("Original URL with Query String : ").Append(originalurl.ToString()).Append(System.Environment.NewLine);
        //    errorString.Append(System.Environment.NewLine);
			
			
        //    errorString.Append("Inner Exception Type: ").Append(exc.InnerException.GetType().ToString()).Append(System.Environment.NewLine);
        //    errorString.Append("Inner Exception: ").Append(exc.InnerException.Message).Append(System.Environment.NewLine);
        //    errorString.Append("Inner Source: ").Append(exc.InnerException.Source).Append(System.Environment.NewLine);
        //    if (exc.InnerException.StackTrace != null)
        //    {
        //        errorString.Append("Inner Stack Trace: ").Append(exc.InnerException.StackTrace).Append(System.Environment.NewLine);
        //    }
        //}
        //errorString.Append("Exception Type: ").Append(exc.GetType().ToString()).Append(System.Environment.NewLine);
        //errorString.Append("Exception: " + exc.Message).Append("Source: " + exc.Source);
        //errorString.Append("Stack Trace: ");
        //if (exc.StackTrace != null)
        //{
        //    errorString.Append(exc.StackTrace).Append(System.Environment.NewLine);            
        //}
		
        //// Database db = CustomDBFactory.CreateDatabase("ofixConString");
        //// string sqlCommand = "usp_log_error";
        //// DbCommand dbCommand = db.GetStoredProcCommand(sqlCommand);
        //// db.AddInParameter(dbCommand, "error", DbType.String, errorString.ToString());
        //// db.ExecuteNonQuery(dbCommand);

        //SendEmail(emailAccount, emailTo, subject, errorString.ToString());

    }
    private static void SendEmail(string emailFrom, string emailTo, string subject, string body)
    {
        SmtpClient smtpClient = new SmtpClient();
        MailMessage message = new MailMessage();
        try
        {
            MailAddress fromAddress = new MailAddress(emailFrom);
            
            string smtpServer = Host;
            if (smtpServer.Length > 0)
                smtpClient.Host = smtpServer;

            message.From = fromAddress;
            smtpClient.Port = Port;

            // To address collection of MailAddress
            string[] addressArray = emailTo.Split(new char[1] { ';' });
            foreach( string address in  addressArray)
                message.To.Add(address);

            message.Subject = subject;
            message.IsBodyHtml = false;

            smtpClient.Credentials = new System.Net.NetworkCredential(emailFrom, Password);
            smtpClient.EnableSsl = EnableSSL;

            // Message body content
            message.Body = body;

            // Send SMTP mail
            smtpClient.Send(message);
        }
        catch (Exception ex)
        {
            
            throw;
        }
    }



}
