﻿<%@ Page Language="C#" AutoEventWireup="True" Inherits="RefereeStoreQuery.WHInventory" CodeBehind="WHInventory.aspx.cs" %>

<!DOCTYPE html>

<html xmlns="http://www.w3.org/1999/xhtml">
<head runat="server">
    <title></title>
    <style type="text/css">
        div {
            padding: 10px;
        }

        body {
            font-family: Arial;
            font-size: 12px;
        }
    </style>
</head>
<body>
    <form id="form1" runat="server">
        <div id="mainDiv" runat="server">
            <fieldset>
                <legend>
                    <span style="font-size: 15px;"><b>Manage Inventory:</b>
                        <asp:Label ID="lbProductName" runat="server" Text=""></asp:Label></span>
                </legend>
                <asp:HiddenField ID="hdnProductID" runat="server" />
                <asp:HiddenField ID="hdnProductQty" runat="server" />
                <div id="adjustDiv" runat="server" visible="true">
                    <div>
                        <i>Note: Enter Quantity and Remarks to add/subtract stock quantity. Put blank both fields if you do not want to update to a particular Warehouse.</i>
                        <br>
                        <asp:Label ID="lbMessage" runat="server" Text=""></asp:Label>
                    </div>
                    <asp:GridView ID="grdWarehouse" runat="server" AutoGenerateColumns="False" DataKeyNames="Id"
                        OnRowCommand="grdWarehouse_RowCommand" CellPadding="10">
                        <Columns>
                            <asp:BoundField DataField="Name"
                                HeaderText="Warehouse" SortExpression="Name" />
                            <asp:TemplateField HeaderText="Adjustment Type">
                                <ItemTemplate>
                                    <asp:DropDownList ID="ddlAdjType"
                                        runat="server">
                                        <asp:ListItem Text="Add" Value="Add"></asp:ListItem>
                                        <asp:ListItem Text="Subtract" Value="Subtract"></asp:ListItem>
                                    </asp:DropDownList>
                                    <asp:HiddenField ID="hdnWHID"
                                        runat="server" Value='<%#Eval("Id") %>' />
                                </ItemTemplate>
                            </asp:TemplateField>
                            <asp:TemplateField HeaderText="Quantity">
                                <ItemTemplate>
                                    <asp:TextBox ID="txtQty" onkeypress="return isNumberKey(event)" MaxLength="4"
                                        runat="server" Width="50px"></asp:TextBox>
                                </ItemTemplate>
                            </asp:TemplateField>
                            <asp:TemplateField HeaderText="Remarks">
                                <ItemTemplate>
                                    <asp:TextBox ID="txtRemarks"
                                        runat="server" Width="200px"></asp:TextBox>
                                </ItemTemplate>
                            </asp:TemplateField>
                            <asp:TemplateField HeaderText="View Transaction History">
                                <ItemTemplate>
                                    <asp:LinkButton ID="lnkHisotry" CommandArgument='<%# Eval("Id" )%>' CommandName="history" runat="server">View</asp:LinkButton>
                                </ItemTemplate>
                            </asp:TemplateField>
                        </Columns>
                    </asp:GridView>
                    <div>
                        <asp:Button ID="btnSubmit" runat="server"
                            Text="Update Quanity" OnClick="btnSubmit_Click" />
                    </div>
                    <div>
                        <asp:HyperLink ID="hlAdminHome"
                            runat="server">Go to Admin Home Page</asp:HyperLink>
                        &nbsp;|&nbsp;<asp:HyperLink ID="hlProduct"
                            runat="server">Go to Product Page</asp:HyperLink>
                    </div>
                </div>
                <div id="historyDiv" runat="server" visible="false">
                    <div>
                        <asp:Label ID="lbClosingQty" runat="server" Text=""></asp:Label></div>
                    <asp:GridView ID="grdHistory" runat="server"
                        AutoGenerateColumns="False" CellPadding="10" EmptyDataText="No Transaction History Found!!">
                        <Columns>
                            <asp:BoundField DataField="Qty"
                                HeaderText="Quantity" SortExpression="Qty" />
                            <asp:BoundField DataField="AdjustementType"
                                HeaderText="Adjustment Type" SortExpression="AdjustementType" />
                            <asp:BoundField DataField="RunningTotal" HeaderText="Closing Qty" SortExpression="RunningTotal" />
                            <asp:BoundField DataField="AdjustmentDateUtc"
                                HeaderText="Adjustment Date" SortExpression="AdjustmentDateUtc" DataFormatString="{0:MM/dd/yyyy HH:mm}" />
                            <asp:BoundField DataField="Username"
                                HeaderText="Adjusted By" SortExpression="Username" />
                            <asp:BoundField DataField="Remarks"
                                HeaderText="Remarks" SortExpression="Remarks" />
                        </Columns>
                    </asp:GridView>
                    
                    <div>
                        <asp:Button ID="lnkBack" runat="server" Text="Go Back" OnClick="lnkBack_Click" />
                    </div>
                </div>
            </fieldset>
        </div>
        <script language="Javascript" type="text/javascript">
    function isNumberKey(evt) {
        var charCode = (evt.which) ? evt.which : evt.keyCode;
        if (charCode != 46 && charCode > 31
          && (charCode < 48 || charCode > 57))
            return false;

        return true;
    }
        </script>
    </form>
</body>
</html>
