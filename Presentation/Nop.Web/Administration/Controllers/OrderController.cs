﻿using Nop.Admin.Extensions;
using Nop.Admin.Models.Order;
using Nop.Admin.Models.Orders;
using Nop.Core;
using Nop.Core.Domain.Catalog;
using Nop.Core.Domain.Common;
using Nop.Core.Domain.Customers;
using Nop.Core.Domain.Directory;
using Nop.Core.Domain.Discounts;
using Nop.Core.Domain.Media;
using Nop.Core.Domain.Orders;
using Nop.Core.Domain.Payments;
using Nop.Core.Domain.Shipping;
using Nop.Core.Domain.Tax;
using Nop.Services.Affiliates;
using Nop.Services.Catalog;
using Nop.Services.Common;
using Nop.Services.Customers;
using Nop.Services.Directory;
using Nop.Services.Discounts;
using Nop.Services.ExportImport;
using Nop.Services.Helpers;
using Nop.Services.Localization;
using Nop.Services.Logging;
using Nop.Services.Media;
using Nop.Services.Messages;
using Nop.Services.Orders;
using Nop.Services.Payments;
using Nop.Services.Security;
using Nop.Services.Shipping;
using Nop.Services.Stores;
using Nop.Services.Tax;
using Nop.Services.Vendors;
using Nop.Web.Framework;
using Nop.Web.Framework.Controllers;
using Nop.Web.Framework.Kendoui;
using Nop.Web.Framework.Mvc;
using System;
using System.Collections;
using System.Collections.Generic;
using System.Configuration;
using System.Data;
using System.Data.SqlClient;
using System.Globalization;
using System.IO;
using System.Linq;
using System.Text;
using System.Web;
using System.Web.Configuration;
using System.Web.Mvc;
using System.Web.Services;
using System.Xml;

namespace Nop.Admin.Controllers
{
    public partial class OrderController : BaseAdminController
    {
        #region Fields

        private readonly IOrderService _orderService;
        private readonly IOrderReportService _orderReportService;
        private readonly IOrderProcessingService _orderProcessingService;
        private readonly IPriceCalculationService _priceCalculationService;
        private readonly IDateTimeHelper _dateTimeHelper;
        private readonly IPriceFormatter _priceFormatter;
        private readonly IDiscountService _discountService;
        private readonly ILocalizationService _localizationService;
        private readonly IWorkContext _workContext;
        private readonly ICurrencyService _currencyService;
        private readonly IEncryptionService _encryptionService;
        private readonly IPaymentService _paymentService;
        private readonly IMeasureService _measureService;
        private readonly IPdfService _pdfService;
        private readonly IAddressService _addressService;
        private readonly ICountryService _countryService;
        private readonly IStateProvinceService _stateProvinceService;
        private readonly IProductService _productService;
        private readonly IExportManager _exportManager;
        private readonly IPermissionService _permissionService;
        private readonly IWorkflowMessageService _workflowMessageService;
        private readonly ICategoryService _categoryService;
        private readonly IManufacturerService _manufacturerService;
        private readonly IProductAttributeService _productAttributeService;
        private readonly IProductAttributeParser _productAttributeParser;
        private readonly IProductAttributeFormatter _productAttributeFormatter;
        private readonly IShoppingCartService _shoppingCartService;
        private readonly IGiftCardService _giftCardService;
        private readonly IDownloadService _downloadService;
        private readonly IShipmentService _shipmentService;
        private readonly IShippingService _shippingService;
        private readonly IStoreService _storeService;
        private readonly IVendorService _vendorService;
        private readonly IAddressAttributeParser _addressAttributeParser;
        private readonly IAddressAttributeService _addressAttributeService;
        private readonly IAddressAttributeFormatter _addressAttributeFormatter;
        private readonly IAffiliateService _affiliateService;
        private readonly IPictureService _pictureService;

        private readonly CurrencySettings _currencySettings;
        private readonly TaxSettings _taxSettings;
        private readonly MeasureSettings _measureSettings;
        private readonly AddressSettings _addressSettings;
        private readonly ShippingSettings _shippingSettings;
        //prachi - 11/25/2015
        private readonly IGenericAttributeService _genericAttributeService;
        private readonly ICustomerActivityService _customerActivityService;
        //Added By Faisal on 03 Dec, 2015
        private readonly PaymentSettings _paymentSettings;

        private readonly OrderSettings _orderSettings;
        //prachi - 02/22/2016
        private readonly ICustomerService _customerService;
        //prachi - 03/10/2016
        private readonly ITaxService _taxService;

        //Added By Prachi on 29 June, 2016
        private readonly IOrderTotalCalculationService _orderTotalCalculationService;
        #endregion

        #region Ctor

        public OrderController(IOrderService orderService,
            IOrderReportService orderReportService,
            IOrderProcessingService orderProcessingService,
            IPriceCalculationService priceCalculationService,
            IDateTimeHelper dateTimeHelper,
            IPriceFormatter priceFormatter,
            IDiscountService discountService,
            ILocalizationService localizationService,
            IWorkContext workContext,
            ICurrencyService currencyService,
            IEncryptionService encryptionService,
            IPaymentService paymentService,
            IMeasureService measureService,
            IPdfService pdfService,
            IAddressService addressService,
            ICountryService countryService,
            IStateProvinceService stateProvinceService,
            IProductService productService,
            IExportManager exportManager,
            IPermissionService permissionService,
            IWorkflowMessageService workflowMessageService,
            ICategoryService categoryService,
            IManufacturerService manufacturerService,
            IProductAttributeService productAttributeService,
            IProductAttributeParser productAttributeParser,
            IProductAttributeFormatter productAttributeFormatter,
            IShoppingCartService shoppingCartService,
            IGiftCardService giftCardService,
            IDownloadService downloadService,
            IShipmentService shipmentService,
            IShippingService shippingService,
            IStoreService storeService,
            IVendorService vendorService,
            IAddressAttributeParser addressAttributeParser,
            IAddressAttributeService addressAttributeService,
            IAddressAttributeFormatter addressAttributeFormatter,
            IAffiliateService affiliateService,
            IPictureService pictureService,
            CurrencySettings currencySettings,
            TaxSettings taxSettings,
            MeasureSettings measureSettings,
            AddressSettings addressSettings,
            ShippingSettings shippingSettings,
            //prachi - 11/25/2015
            IGenericAttributeService genericAttributeService,

            //Faisal
            PaymentSettings _paymentSettings,
            ICustomerActivityService customerActivityService,
            OrderSettings orderSettings,
            //prachi - 02/22/2016
            ICustomerService customerService,
            //prachi - 03/10/2016
            ITaxService taxService,
            //Added By Prachi on 29 June, 2016
            IOrderTotalCalculationService orderTotalCalculationService)
        {
            this._orderService = orderService;
            this._orderReportService = orderReportService;
            this._orderProcessingService = orderProcessingService;
            this._priceCalculationService = priceCalculationService;
            this._dateTimeHelper = dateTimeHelper;
            this._priceFormatter = priceFormatter;
            this._discountService = discountService;
            this._localizationService = localizationService;
            this._workContext = workContext;
            this._currencyService = currencyService;
            this._encryptionService = encryptionService;
            this._paymentService = paymentService;
            this._measureService = measureService;
            this._pdfService = pdfService;
            this._addressService = addressService;
            this._countryService = countryService;
            this._stateProvinceService = stateProvinceService;
            this._productService = productService;
            this._exportManager = exportManager;
            this._permissionService = permissionService;
            this._workflowMessageService = workflowMessageService;
            this._categoryService = categoryService;
            this._manufacturerService = manufacturerService;
            this._productAttributeService = productAttributeService;
            this._productAttributeParser = productAttributeParser;
            this._productAttributeFormatter = productAttributeFormatter;
            this._shoppingCartService = shoppingCartService;
            this._giftCardService = giftCardService;
            this._downloadService = downloadService;
            this._shipmentService = shipmentService;
            this._shippingService = shippingService;
            this._storeService = storeService;
            this._vendorService = vendorService;
            this._addressAttributeParser = addressAttributeParser;
            this._addressAttributeService = addressAttributeService;
            this._addressAttributeFormatter = addressAttributeFormatter;
            this._affiliateService = affiliateService;
            this._pictureService = pictureService;

            this._currencySettings = currencySettings;
            this._taxSettings = taxSettings;
            this._measureSettings = measureSettings;
            this._addressSettings = addressSettings;
            this._shippingSettings = shippingSettings;
            //prachi - 11/25/2015
            this._genericAttributeService = genericAttributeService;
            //Faisal
            this._paymentSettings = _paymentSettings;
            this._customerActivityService = customerActivityService;
            this._orderSettings = orderSettings;
            //prachi - 02/22/2016
            this._customerService = customerService;
            //prachi - 03/10/2016
            this._taxService = taxService;

            //Added By Prachi on 29 June, 2016
            this._orderTotalCalculationService = orderTotalCalculationService;
        }

        #endregion

        #region Utilities

        [NonAction]
        protected virtual bool HasAccessToOrder(Order order)
        {
            if (order == null)
                throw new ArgumentNullException("order");

            if (_workContext.CurrentVendor == null)
                //not a vendor; has access
                return true;

            var vendorId = _workContext.CurrentVendor.Id;
            var hasVendorProducts = order.OrderItems.Any(orderItem => orderItem.Product.VendorId == vendorId);
            return hasVendorProducts;
        }

        [NonAction]
        protected virtual bool HasAccessToOrderItem(OrderItem orderItem)
        {
            if (orderItem == null)
                throw new ArgumentNullException("orderItem");

            if (_workContext.CurrentVendor == null)
                //not a vendor; has access
                return true;

            var vendorId = _workContext.CurrentVendor.Id;
            return orderItem.Product.VendorId == vendorId;
        }

        [NonAction]
        protected virtual bool HasAccessToProduct(Product product)
        {
            if (product == null)
                throw new ArgumentNullException("product");

            if (_workContext.CurrentVendor == null)
                //not a vendor; has access
                return true;

            var vendorId = _workContext.CurrentVendor.Id;
            return product.VendorId == vendorId;
        }

        [NonAction]
        protected virtual bool HasAccessToShipment(Shipment shipment)
        {
            if (shipment == null)
                throw new ArgumentNullException("shipment");

            if (_workContext.CurrentVendor == null)
                //not a vendor; has access
                return true;

            var hasVendorProducts = false;
            var vendorId = _workContext.CurrentVendor.Id;
            foreach (var shipmentItem in shipment.ShipmentItems)
            {
                var orderItem = _orderService.GetOrderItemById(shipmentItem.OrderItemId);
                if (orderItem != null)
                {
                    if (orderItem.Product.VendorId == vendorId)
                    {
                        hasVendorProducts = true;
                        break;
                    }
                }
            }
            return hasVendorProducts;
        }

        [NonAction]
        protected virtual void PrepareOrderDetailsModel(OrderModel model, Order order)
        {
            if (order == null)
                throw new ArgumentNullException("order");

            if (model == null)
                throw new ArgumentNullException("model");

            model.Id = order.Id;
            model.OrderStatus = order.OrderStatus.GetLocalizedEnum(_localizationService, _workContext);
            model.OrderStatusId = order.OrderStatusId;
            model.OrderGuid = order.OrderGuid;
            var store = _storeService.GetStoreById(order.StoreId);
            model.StoreName = store != null ? store.Name : "Unknown";
            model.CustomerId = order.CustomerId;
            var customer = order.Customer;
            model.CustomerInfo = customer.IsRegistered() ? customer.Email : _localizationService.GetResource("Admin.Customers.Guest");
            model.CustomerIp = order.CustomerIp;
            model.VatNumber = order.VatNumber;
            model.CreatedOn = _dateTimeHelper.ConvertToUserTime(order.CreatedOnUtc, DateTimeKind.Utc);
            model.AllowCustomersToSelectTaxDisplayType = _taxSettings.AllowCustomersToSelectTaxDisplayType;
            model.TaxDisplayType = _taxSettings.TaxDisplayType;
            model.TaxDisplayType = _taxSettings.TaxDisplayType;
            model.PaymentType = order.PaymentType;
            model.PaymentRemarks = order.PaymentRemarks;

            var affiliate = _affiliateService.GetAffiliateById(order.AffiliateId);
            if (affiliate != null)
            {
                model.AffiliateId = affiliate.Id;
                model.AffiliateName = affiliate.GetFullName();
            }

            //a vendor should have access only to his products
            model.IsLoggedInAsVendor = _workContext.CurrentVendor != null;
            //custom values
            model.CustomValues = order.DeserializeCustomValues();

            #region Order totals

            var primaryStoreCurrency = _currencyService.GetCurrencyById(_currencySettings.PrimaryStoreCurrencyId);
            if (primaryStoreCurrency == null)
                throw new Exception("Cannot load primary store currency");

            //subtotal
            model.OrderSubtotalInclTax = _priceFormatter.FormatPrice(order.OrderSubtotalInclTax, true, primaryStoreCurrency, _workContext.WorkingLanguage, true);
            model.OrderSubtotalExclTax = _priceFormatter.FormatPrice(order.OrderSubtotalExclTax, true, primaryStoreCurrency, _workContext.WorkingLanguage, false);
            model.OrderSubtotalInclTaxValue = order.OrderSubtotalInclTax;
            model.OrderSubtotalExclTaxValue = order.OrderSubtotalExclTax;
            //discount (applied to order subtotal)
            string orderSubtotalDiscountInclTaxStr = _priceFormatter.FormatPrice(order.OrderSubTotalDiscountInclTax, true, primaryStoreCurrency, _workContext.WorkingLanguage, true);
            string orderSubtotalDiscountExclTaxStr = _priceFormatter.FormatPrice(order.OrderSubTotalDiscountExclTax, true, primaryStoreCurrency, _workContext.WorkingLanguage, false);
            if (order.OrderSubTotalDiscountInclTax > decimal.Zero)
                model.OrderSubTotalDiscountInclTax = orderSubtotalDiscountInclTaxStr;
            if (order.OrderSubTotalDiscountExclTax > decimal.Zero)
                model.OrderSubTotalDiscountExclTax = orderSubtotalDiscountExclTaxStr;
            model.OrderSubTotalDiscountInclTaxValue = order.OrderSubTotalDiscountInclTax;
            model.OrderSubTotalDiscountExclTaxValue = order.OrderSubTotalDiscountExclTax;

            //shipping
            model.OrderShippingInclTax = _priceFormatter.FormatShippingPrice(order.OrderShippingInclTax, true, primaryStoreCurrency, _workContext.WorkingLanguage, true);
            model.OrderShippingExclTax = _priceFormatter.FormatShippingPrice(order.OrderShippingExclTax, true, primaryStoreCurrency, _workContext.WorkingLanguage, false);
            model.OrderShippingInclTaxValue = order.OrderShippingInclTax;
            model.OrderShippingExclTaxValue = order.OrderShippingExclTax;

            //payment method additional fee
            if (order.PaymentMethodAdditionalFeeInclTax > decimal.Zero)
            {
                model.PaymentMethodAdditionalFeeInclTax = _priceFormatter.FormatPaymentMethodAdditionalFee(order.PaymentMethodAdditionalFeeInclTax, true, primaryStoreCurrency, _workContext.WorkingLanguage, true);
                model.PaymentMethodAdditionalFeeExclTax = _priceFormatter.FormatPaymentMethodAdditionalFee(order.PaymentMethodAdditionalFeeExclTax, true, primaryStoreCurrency, _workContext.WorkingLanguage, false);
            }
            model.PaymentMethodAdditionalFeeInclTaxValue = order.PaymentMethodAdditionalFeeInclTax;
            model.PaymentMethodAdditionalFeeExclTaxValue = order.PaymentMethodAdditionalFeeExclTax;


            //tax
            model.Tax = _priceFormatter.FormatPrice(order.OrderTax, true, false);
            SortedDictionary<decimal, decimal> taxRates = order.TaxRatesDictionary;
            bool displayTaxRates = _taxSettings.DisplayTaxRates && taxRates.Count > 0;
            bool displayTax = !displayTaxRates;
            foreach (var tr in order.TaxRatesDictionary)
            {
                model.TaxRates.Add(new OrderModel.TaxRate
                {
                    Rate = _priceFormatter.FormatTaxRate(tr.Key),
                    Value = _priceFormatter.FormatPrice(tr.Value, true, false),
                });
            }
            model.DisplayTaxRates = displayTaxRates;
            model.DisplayTax = displayTax;
            model.TaxValue = order.OrderTax;
            model.TaxRatesValue = order.TaxRates;

            //discount
            if (order.OrderDiscount > 0)
                model.OrderTotalDiscount = _priceFormatter.FormatPrice(-order.OrderDiscount, true, false);
            model.OrderTotalDiscountValue = order.OrderDiscount;

            //gift cards
            foreach (var gcuh in order.GiftCardUsageHistory)
            {
                model.GiftCards.Add(new OrderModel.GiftCard
                {
                    CouponCode = gcuh.GiftCard.GiftCardCouponCode,
                    Amount = _priceFormatter.FormatPrice(-gcuh.UsedValue, true, false),
                });
            }

            //reward points
            if (order.RedeemedRewardPointsEntry != null)
            {
                model.RedeemedRewardPoints = -order.RedeemedRewardPointsEntry.Points;
                model.RedeemedRewardPointsAmount = _priceFormatter.FormatPrice(-order.RedeemedRewardPointsEntry.UsedAmount, true, false);
            }

            //total
            model.OrderTotal = _priceFormatter.FormatPrice(order.OrderTotal, true, false);
            model.OrderTotalValue = order.OrderTotal;

            //refunded amount
            if (order.RefundedAmount > decimal.Zero)
                model.RefundedAmount = _priceFormatter.FormatPrice(order.RefundedAmount, true, false);

            //used discounts
            var duh = _discountService.GetAllDiscountUsageHistory(orderId: order.Id);
            foreach (var d in duh)
            {
                model.UsedDiscounts.Add(new OrderModel.UsedDiscountModel
                {
                    DiscountId = d.DiscountId,
                    DiscountName = d.Discount.Name
                });
            }

            //profit (hide for vendors)
            if (_workContext.CurrentVendor == null)
            {
                var profit = _orderReportService.ProfitReport(orderId: order.Id);
                model.Profit = _priceFormatter.FormatPrice(profit, true, false);
            }

            #endregion

            #region Payment info

            //Changed by Faisal On 02 Dec, 2015
            string strFriendlyName = "";
            var paymentMethod = _paymentService.LoadPaymentMethodBySystemName(order.PaymentMethodSystemName);
            if (paymentMethod != null)
                strFriendlyName = paymentMethod.GetLocalizedFriendlyName(_localizationService, _workContext.WorkingLanguage.Id);

            if (order.AllowStoringCreditCardNumber || (strFriendlyName.Trim() != string.Empty
                    && strFriendlyName.ToUpper().Trim() != "CREDIT CARD" && strFriendlyName.ToUpper().Trim() != "CREDITCARD")
                )
            {
                //card type
                model.CardType = _encryptionService.DecryptText(order.CardType);
                //cardholder name
                model.CardName = _encryptionService.DecryptText(order.CardName);
                //card number
                model.CardNumber = _encryptionService.DecryptText(order.CardNumber);
                //cvv
                model.CardCvv2 = _encryptionService.DecryptText(order.CardCvv2);
                //expiry date
                string cardExpirationMonthDecrypted = _encryptionService.DecryptText(order.CardExpirationMonth);
                if (!String.IsNullOrEmpty(cardExpirationMonthDecrypted) && cardExpirationMonthDecrypted != "0")
                    model.CardExpirationMonth = cardExpirationMonthDecrypted;
                string cardExpirationYearDecrypted = _encryptionService.DecryptText(order.CardExpirationYear);
                if (!String.IsNullOrEmpty(cardExpirationYearDecrypted) && cardExpirationYearDecrypted != "0")
                    model.CardExpirationYear = cardExpirationYearDecrypted;

                model.AllowStoringCreditCardNumber = true;
            }
            else
            {
                string maskedCreditCardNumberDecrypted = _encryptionService.DecryptText(order.MaskedCreditCardNumber);
                if (!String.IsNullOrEmpty(maskedCreditCardNumberDecrypted))
                    model.CardNumber = maskedCreditCardNumberDecrypted;
            }


            //payment transaction info
            model.AuthorizationTransactionId = order.AuthorizationTransactionId;
            model.CaptureTransactionId = order.CaptureTransactionId;
            model.SubscriptionTransactionId = order.SubscriptionTransactionId;

            //payment method info
            var pm = _paymentService.LoadPaymentMethodBySystemName(order.PaymentMethodSystemName);
            model.PaymentMethod = pm != null ? pm.PluginDescriptor.FriendlyName : order.PaymentMethodSystemName;
            model.PaymentStatus = order.PaymentStatus.GetLocalizedEnum(_localizationService, _workContext);

            //payment method buttons
            model.CanCancelOrder = _orderProcessingService.CanCancelOrder(order);
            model.CanCapture = _orderProcessingService.CanCapture(order);
            model.CanMarkOrderAsPaid = _orderProcessingService.CanMarkOrderAsPaid(order);
            model.CanRefund = _orderProcessingService.CanRefund(order);
            model.CanRefundOffline = _orderProcessingService.CanRefundOffline(order);
            model.CanPartiallyRefund = _orderProcessingService.CanPartiallyRefund(order, decimal.Zero);
            model.CanPartiallyRefundOffline = _orderProcessingService.CanPartiallyRefundOffline(order, decimal.Zero);
            model.CanVoid = _orderProcessingService.CanVoid(order);
            model.CanVoidOffline = _orderProcessingService.CanVoidOffline(order);

            model.PrimaryStoreCurrencyCode = _currencyService.GetCurrencyById(_currencySettings.PrimaryStoreCurrencyId).CurrencyCode;
            model.MaxAmountToRefund = order.OrderTotal - order.RefundedAmount;

            //recurring payment record
            var recurringPayment = _orderService.SearchRecurringPayments(initialOrderId: order.Id, showHidden: true).FirstOrDefault();
            if (recurringPayment != null)
            {
                model.RecurringPaymentId = recurringPayment.Id;
            }
            #endregion

            #region Billing & shipping info

            model.BillingAddress = order.BillingAddress.ToModel();
            model.BillingAddress.FormattedCustomAddressAttributes = _addressAttributeFormatter.FormatAttributes(order.BillingAddress.CustomAttributes);
            model.BillingAddress.FirstNameEnabled = true;
            model.BillingAddress.FirstNameRequired = true;
            model.BillingAddress.LastNameEnabled = true;
            model.BillingAddress.LastNameRequired = true;
            model.BillingAddress.EmailEnabled = true;
            model.BillingAddress.EmailRequired = true;
            model.BillingAddress.CompanyEnabled = _addressSettings.CompanyEnabled;
            model.BillingAddress.CompanyRequired = _addressSettings.CompanyRequired;
            model.BillingAddress.CountryEnabled = _addressSettings.CountryEnabled;
            model.BillingAddress.StateProvinceEnabled = _addressSettings.StateProvinceEnabled;
            model.BillingAddress.CityEnabled = _addressSettings.CityEnabled;
            model.BillingAddress.CityRequired = _addressSettings.CityRequired;
            model.BillingAddress.StreetAddressEnabled = _addressSettings.StreetAddressEnabled;
            model.BillingAddress.StreetAddressRequired = _addressSettings.StreetAddressRequired;
            model.BillingAddress.StreetAddress2Enabled = _addressSettings.StreetAddress2Enabled;
            model.BillingAddress.StreetAddress2Required = _addressSettings.StreetAddress2Required;
            model.BillingAddress.ZipPostalCodeEnabled = _addressSettings.ZipPostalCodeEnabled;
            model.BillingAddress.ZipPostalCodeRequired = _addressSettings.ZipPostalCodeRequired;
            model.BillingAddress.PhoneEnabled = _addressSettings.PhoneEnabled;
            model.BillingAddress.PhoneRequired = _addressSettings.PhoneRequired;
            model.BillingAddress.FaxEnabled = _addressSettings.FaxEnabled;
            model.BillingAddress.FaxRequired = _addressSettings.FaxRequired;

            model.ShippingStatus = order.ShippingStatus.GetLocalizedEnum(_localizationService, _workContext); ;
            if (order.ShippingStatus != ShippingStatus.ShippingNotRequired)
            {
                model.IsShippable = true;

                model.PickUpInStore = order.PickUpInStore;
                if (!order.PickUpInStore)
                {
                    model.ShippingAddress = order.ShippingAddress.ToModel();
                    model.ShippingAddress.FormattedCustomAddressAttributes = _addressAttributeFormatter.FormatAttributes(order.ShippingAddress.CustomAttributes);
                    model.ShippingAddress.FirstNameEnabled = true;
                    model.ShippingAddress.FirstNameRequired = true;
                    model.ShippingAddress.LastNameEnabled = true;
                    model.ShippingAddress.LastNameRequired = true;
                    model.ShippingAddress.EmailEnabled = true;
                    model.ShippingAddress.EmailRequired = true;
                    model.ShippingAddress.CompanyEnabled = _addressSettings.CompanyEnabled;
                    model.ShippingAddress.CompanyRequired = _addressSettings.CompanyRequired;
                    model.ShippingAddress.CountryEnabled = _addressSettings.CountryEnabled;
                    model.ShippingAddress.StateProvinceEnabled = _addressSettings.StateProvinceEnabled;
                    model.ShippingAddress.CityEnabled = _addressSettings.CityEnabled;
                    model.ShippingAddress.CityRequired = _addressSettings.CityRequired;
                    model.ShippingAddress.StreetAddressEnabled = _addressSettings.StreetAddressEnabled;
                    model.ShippingAddress.StreetAddressRequired = _addressSettings.StreetAddressRequired;
                    model.ShippingAddress.StreetAddress2Enabled = _addressSettings.StreetAddress2Enabled;
                    model.ShippingAddress.StreetAddress2Required = _addressSettings.StreetAddress2Required;
                    model.ShippingAddress.ZipPostalCodeEnabled = _addressSettings.ZipPostalCodeEnabled;
                    model.ShippingAddress.ZipPostalCodeRequired = _addressSettings.ZipPostalCodeRequired;
                    model.ShippingAddress.PhoneEnabled = _addressSettings.PhoneEnabled;
                    model.ShippingAddress.PhoneRequired = _addressSettings.PhoneRequired;
                    model.ShippingAddress.FaxEnabled = _addressSettings.FaxEnabled;
                    model.ShippingAddress.FaxRequired = _addressSettings.FaxRequired;

                    model.ShippingAddressGoogleMapsUrl = string.Format("http://maps.google.com/maps?f=q&hl=en&ie=UTF8&oe=UTF8&geocode=&q={0}", Server.UrlEncode(order.ShippingAddress.Address1 + " " + order.ShippingAddress.ZipPostalCode + " " + order.ShippingAddress.City + " " + (order.ShippingAddress.Country != null ? order.ShippingAddress.Country.Name : "")));
                }
                model.ShippingMethod = order.ShippingMethod;

                model.CanAddNewShipments = order.HasItemsToAddToShipment();
            }

            #endregion

            #region Products

            model.CheckoutAttributeInfo = order.CheckoutAttributeDescription;
            bool hasDownloadableItems = false;
            var products = order.OrderItems;
            //a vendor should have access only to his products
            if (_workContext.CurrentVendor != null)
            {
                products = products
                    .Where(orderItem => orderItem.Product.VendorId == _workContext.CurrentVendor.Id)
                    .ToList();
            }

            model.ReturnRequest = PrepareReturnRequestModel(order);

            foreach (var orderItem in products)
            {
                if (orderItem.Product.IsDownload)
                    hasDownloadableItems = true;

                var orderItemModel = new OrderModel.OrderItemModel
                {
                    Id = orderItem.Id,
                    ProductId = orderItem.ProductId,
                    ProductName = orderItem.Product.Name,
                    Sku = orderItem.Product.FormatSku(orderItem.AttributesXml, _productAttributeParser),
                    Quantity = orderItem.Quantity,
                    IsDownload = orderItem.Product.IsDownload,
                    DownloadCount = orderItem.DownloadCount,
                    DownloadActivationType = orderItem.Product.DownloadActivationType,
                    IsDownloadActivated = orderItem.IsDownloadActivated
                };
                //picture
                var orderItemPicture = orderItem.Product.GetProductPicture(orderItem.AttributesXml, _pictureService, _productAttributeParser);
                orderItemModel.PictureThumbnailUrl = _pictureService.GetPictureUrl(orderItemPicture, 75, true);

                //license file
                if (orderItem.LicenseDownloadId.HasValue)
                {
                    var licenseDownload = _downloadService.GetDownloadById(orderItem.LicenseDownloadId.Value);
                    if (licenseDownload != null)
                    {
                        orderItemModel.LicenseDownloadGuid = licenseDownload.DownloadGuid;
                    }
                }
                //vendor
                var vendor = _vendorService.GetVendorById(orderItem.Product.VendorId);
                orderItemModel.VendorName = vendor != null ? vendor.Name : "";

                //unit price
                orderItemModel.UnitPriceInclTaxValue = orderItem.UnitPriceInclTax;
                orderItemModel.UnitPriceExclTaxValue = orderItem.UnitPriceExclTax;
                orderItemModel.UnitPriceInclTax = _priceFormatter.FormatPrice(orderItem.UnitPriceInclTax, true, primaryStoreCurrency, _workContext.WorkingLanguage, true, true);
                orderItemModel.UnitPriceExclTax = _priceFormatter.FormatPrice(orderItem.UnitPriceExclTax, true, primaryStoreCurrency, _workContext.WorkingLanguage, false, true);
                //discounts
                orderItemModel.DiscountInclTaxValue = orderItem.DiscountAmountInclTax;
                orderItemModel.DiscountExclTaxValue = orderItem.DiscountAmountExclTax;
                orderItemModel.DiscountInclTax = _priceFormatter.FormatPrice(orderItem.DiscountAmountInclTax, true, primaryStoreCurrency, _workContext.WorkingLanguage, true, true);
                orderItemModel.DiscountExclTax = _priceFormatter.FormatPrice(orderItem.DiscountAmountExclTax, true, primaryStoreCurrency, _workContext.WorkingLanguage, false, true);
                //subtotal
                orderItemModel.SubTotalInclTaxValue = orderItem.PriceInclTax;
                orderItemModel.SubTotalExclTaxValue = orderItem.PriceExclTax;
                orderItemModel.SubTotalInclTax = _priceFormatter.FormatPrice(orderItem.PriceInclTax, true, primaryStoreCurrency, _workContext.WorkingLanguage, true, true);
                orderItemModel.SubTotalExclTax = _priceFormatter.FormatPrice(orderItem.PriceExclTax, true, primaryStoreCurrency, _workContext.WorkingLanguage, false, true);

                orderItemModel.AttributeInfo = orderItem.AttributeDescription;
                if (orderItem.Product.IsRecurring)
                    orderItemModel.RecurringInfo = string.Format(_localizationService.GetResource("Admin.Orders.Products.RecurringPeriod"), orderItem.Product.RecurringCycleLength, orderItem.Product.RecurringCyclePeriod.GetLocalizedEnum(_localizationService, _workContext));
                //rental info
                if (orderItem.Product.IsRental)
                {
                    var rentalStartDate = orderItem.RentalStartDateUtc.HasValue ? orderItem.Product.FormatRentalDate(orderItem.RentalStartDateUtc.Value) : "";
                    var rentalEndDate = orderItem.RentalEndDateUtc.HasValue ? orderItem.Product.FormatRentalDate(orderItem.RentalEndDateUtc.Value) : "";
                    orderItemModel.RentalInfo = string.Format(_localizationService.GetResource("Order.Rental.FormattedDate"),
                        rentalStartDate, rentalEndDate);
                }

                //return requests
                orderItemModel.ReturnRequestIds = _orderService.SearchReturnRequests(orderItemId: orderItem.Id)
                    .Select(rr => rr.Id).ToList();
                //gift cards
                orderItemModel.PurchasedGiftCardIds = _giftCardService.GetGiftCardsByPurchasedWithOrderItemId(orderItem.Id)
                    .Select(gc => gc.Id).ToList();

                //Added By Prachi on 24 Feb, 2016 - Preparing return request model and assigning value to "ReturnRequest" property of order model
                orderItemModel.ReturnedQuantity = 0;
                foreach (var id in orderItemModel.ReturnRequestIds)
                {
                    ReturnRequest returnRequest = _orderService.GetReturnRequestById(id);
                    model.ReturnRequest.Items.Where(x => x.Id == returnRequest.OrderItemId).First().Quantity -= returnRequest.Quantity;
                    orderItemModel.ReturnedQuantity += returnRequest.Quantity;
                }

                //Added By Prachi on 04 March, 2016
                orderItemModel.ShippedQuantity = orderItem.GetTotalNumberOfShippedItems();

                //Added By Faisal 03 Mar, 2016
                //orderItemModel.RefundedProduct = _priceFormatter.FormatPrice(orderItem.RefundedProduct, true, false);
                //End 03 Mar, 2016

                //Added By Faisal on 06 Apr, 2016
                orderItem.RefundedProduct = Math.Truncate(orderItem.RefundedProduct * 100) / 100;

                if (!String.IsNullOrEmpty(_workContext.WorkingCurrency.DisplayLocale))
                    orderItemModel.RefundedProduct = orderItem.RefundedProduct.ToString("C", new CultureInfo(_workContext.WorkingCurrency.DisplayLocale));
                else
                    orderItemModel.RefundedProduct = String.Format("{0} ({1})", orderItem.RefundedProduct.ToString("N"), _workContext.WorkingCurrency.CurrencyCode);

                //Added By Faisal on 29 Apr, 2016
                orderItemModel.RefundedProductValue = Math.Truncate(orderItem.RefundedProduct * 100) / 100;
                orderItemModel.RefundAfterShippingValue = Math.Truncate(orderItem.RefundAfterShipping * 100) / 100;
                decimal maxRefundValue = orderItem.TotalChargedAmount - orderItem.RefundAfterShipping;

                if (!String.IsNullOrEmpty(_workContext.WorkingCurrency.DisplayLocale))
                    orderItemModel.RefundAfterShipping = maxRefundValue.ToString("C", new CultureInfo(_workContext.WorkingCurrency.DisplayLocale));
                else
                    orderItemModel.RefundAfterShipping = String.Format("{0} ({1})", maxRefundValue.ToString("N"), _workContext.WorkingCurrency.CurrencyCode);

                //Added By Faisal 31 Mar, 2016
                orderItemModel.TotalChargedAmount = orderItem.TotalChargedAmount;
                //End 31 Mar, 2016

                var product = _productService.GetProductById(orderItemModel.ProductId);
                //Changed By Prachi on 30 June, 2016
                orderItemModel.IsQuantityAvailable = IsQuantityAvailable(orderItemModel.Quantity, orderItemModel.ShippedQuantity, product, orderItemModel.Id);

                //Added By Prachi on 05 April, 2016
                if (product.TaxCategoryId > 0)
                    orderItemModel.IsProductTaxable = true;
                else
                    orderItemModel.IsProductTaxable = false;
                model.Items.Add(orderItemModel);

            }
            model.HasDownloadableProducts = hasDownloadableItems;

            //Added By Prachi on 03 May, 2016
            //model.ActualDateShipped = order.ActualDateShipped;
            //model.ActualShipCost = order.ActualShipCost;

            #endregion

            //Added By Faisal 03 Mar, 2016
            model.TotalChargedAmount = _priceFormatter.FormatPrice(order.TotalChargedAmount, true, false);
            decimal amountDue = 0;

            //Changed By Faisal on 11 Apr, 2016
            //if (Convert.ToString(order.CardNumber ?? "").Trim() != "")
            //{
            //    //if (order.OrderTotal < order.TotalChargedAmount)
            //    //    amountDue = order.OrderTotal - (order.TotalChargedAmount - (order.RefundedAmount - order.RefundedShipping));
            //    //else
            //        amountDue = order.OrderTotal - order.TotalChargedAmount;
            //}
            ////Added on 31 Mar, 2016
            //else if (order.PaymentStatus == PaymentStatus.Pending)
            //    amountDue = order.OrderTotal;
            ////End - 31 Mar, 2016

            amountDue = order.OrderTotal - order.TotalChargedAmount;
            //End 11 Apr, 2016

            model.AmountDue = _priceFormatter.FormatPrice(amountDue, true, false);
            model.RefundedShipping = _priceFormatter.FormatPrice(order.RefundedShipping, true, false);

            //End 03 Mar, 2016

            //Added By Faisal on 04 Dec, 2015
            BindDDLs(model);
        }

        [NonAction]
        protected virtual OrderModel.AddOrderProductModel.ProductDetailsModel PrepareAddProductToOrderModel(int orderId, int productId)
        {
            Product product = _productService.GetProductById(productId);
            if (product == null)
                throw new ArgumentException("No product found with the specified id");

            decimal discountAmount = 0;
            decimal taxRate = 0;
            Discount appliedDiscount = null;
            Order order = _orderService.GetOrderById(orderId);
            Customer customer = order.Customer;
            bool isNasoMember = VerifyNASOMember(customer);
            decimal price = Math.Round(_priceCalculationService.GetFinalPrice(product, customer, 0, true, 1, out discountAmount, out appliedDiscount), 2);
            decimal productPriceInclTax = Math.Round(_taxService.GetProductPrice(product, price, true, order.Customer, out taxRate), 2);

            var model = new OrderModel.AddOrderProductModel.ProductDetailsModel
            {
                ProductId = productId,
                OrderId = orderId,
                Name = product.Name,
                ProductType = product.ProductType,
                //Changed & Added By Faisal on 15 Jan, 2016
                //UnitPriceExclTax = decimal.Zero,
                //UnitPriceInclTax = decimal.Zero,
                //Quantity = 1,
                //SubTotalExclTax = decimal.Zero,
                //SubTotalInclTax = decimal.Zero

                UnitPriceExclTax = price,
                UnitPriceInclTax = productPriceInclTax,
                Quantity = 1,
                SubTotalExclTax = price,
                SubTotalInclTax = productPriceInclTax
               
                //End
            };

            //attributes
            var attributes = _productAttributeService.GetProductAttributeMappingsByProductId(product.Id);
            foreach (var attribute in attributes)
            {
                var attributeModel = new OrderModel.AddOrderProductModel.ProductAttributeModel
                {
                    Id = attribute.Id,
                    ProductAttributeId = attribute.ProductAttributeId,
                    Name = attribute.ProductAttribute.Name,
                    TextPrompt = attribute.TextPrompt,
                    IsRequired = attribute.IsRequired,
                    AttributeControlType = attribute.AttributeControlType
                };

                if (attribute.ShouldHaveValues())
                {
                    //values
                    var attributeValues = _productAttributeService.GetProductAttributeValues(attribute.Id);
                    foreach (var attributeValue in attributeValues)
                    {
                        var attributeValueModel = new OrderModel.AddOrderProductModel.ProductAttributeValueModel
                        {
                            Id = attributeValue.Id,
                            Name = attributeValue.Name,
                            IsPreSelected = attributeValue.IsPreSelected
                        };
                        attributeModel.Values.Add(attributeValueModel);
                    }
                }

                model.ProductAttributes.Add(attributeModel);
            }
            //gift card
            model.GiftCard.IsGiftCard = product.IsGiftCard;
            if (model.GiftCard.IsGiftCard)
            {
                model.GiftCard.GiftCardType = product.GiftCardType;
            }
            //rental
            model.IsRental = product.IsRental;
            
            return model;
        }

        [NonAction]
        protected virtual ShipmentModel PrepareShipmentModel(Shipment shipment, bool prepareProducts, bool prepareShipmentEvent = false)
        {
            //measures
            var baseWeight = _measureService.GetMeasureWeightById(_measureSettings.BaseWeightId);
            var baseWeightIn = baseWeight != null ? baseWeight.Name : "";
            var baseDimension = _measureService.GetMeasureDimensionById(_measureSettings.BaseDimensionId);
            var baseDimensionIn = baseDimension != null ? baseDimension.Name : "";

            var model = new ShipmentModel
            {
                Id = shipment.Id,
                OrderId = shipment.OrderId,
                TrackingNumber = shipment.TrackingNumber,
                TotalWeight = shipment.TotalWeight.HasValue ? string.Format("{0:F2} [{1}]", shipment.TotalWeight, baseWeightIn) : "",
                ShippedDate = shipment.ShippedDateUtc.HasValue ? _dateTimeHelper.ConvertToUserTime(shipment.ShippedDateUtc.Value, DateTimeKind.Utc).ToString() : _localizationService.GetResource("Admin.Orders.Shipments.ShippedDate.NotYet"),
                ShippedDateUtc = shipment.ShippedDateUtc,
                CanShip = !shipment.ShippedDateUtc.HasValue,
                DeliveryDate = shipment.DeliveryDateUtc.HasValue ? _dateTimeHelper.ConvertToUserTime(shipment.DeliveryDateUtc.Value, DateTimeKind.Utc).ToString() : _localizationService.GetResource("Admin.Orders.Shipments.DeliveryDate.NotYet"),
                DeliveryDateUtc = shipment.DeliveryDateUtc,
                CanDeliver = shipment.ShippedDateUtc.HasValue && !shipment.DeliveryDateUtc.HasValue,
                AdminComment = shipment.AdminComment,
                //Added By Prachi on 03 May, 2016
                ActualDateShipped = shipment.ActualDateShipped,
                ActualShipCost = shipment.ActualShipCost
            };

            if (prepareProducts)
            {
                foreach (var shipmentItem in shipment.ShipmentItems)
                {
                    var orderItem = _orderService.GetOrderItemById(shipmentItem.OrderItemId);
                    if (orderItem == null)
                        continue;

                    //quantities
                    var qtyInThisShipment = shipmentItem.Quantity;
                    var maxQtyToAdd = orderItem.GetTotalNumberOfItemsCanBeAddedToShipment();
                    var qtyOrdered = orderItem.Quantity;
                    var qtyInAllShipments = orderItem.GetTotalNumberOfItemsInAllShipment();

                    var warehouse = _shippingService.GetWarehouseById(shipmentItem.WarehouseId);
                    var shipmentItemModel = new ShipmentModel.ShipmentItemModel
                    {
                        Id = shipmentItem.Id,
                        OrderItemId = orderItem.Id,
                        ProductId = orderItem.ProductId,
                        ProductName = orderItem.Product.Name,
                        Sku = orderItem.Product.FormatSku(orderItem.AttributesXml, _productAttributeParser),
                        AttributeInfo = orderItem.AttributeDescription,
                        ShippedFromWarehouse = warehouse != null ? warehouse.Name : null,
                        ShipSeparately = orderItem.Product.ShipSeparately,
                        ItemWeight = orderItem.ItemWeight.HasValue ? string.Format("{0:F2} [{1}]", orderItem.ItemWeight, baseWeightIn) : "",
                        ItemDimensions = string.Format("{0:F2} x {1:F2} x {2:F2} [{3}]", orderItem.Product.Length, orderItem.Product.Width, orderItem.Product.Height, baseDimensionIn),
                        QuantityOrdered = qtyOrdered,
                        QuantityInThisShipment = qtyInThisShipment,
                        QuantityInAllShipments = qtyInAllShipments,
                        QuantityToAdd = maxQtyToAdd,
                        SelectedWareHouse = orderItem.SelectedWarehouse,
                        //Changed By Prachi on 30 June, 2016
                        IsQuantityAvailable = IsQuantityAvailable(orderItem.Quantity, orderItem.GetTotalNumberOfShippedItems(), orderItem.Product, orderItem.Id)
                    };
                    //rental info
                    if (orderItem.Product.IsRental)
                    {
                        var rentalStartDate = orderItem.RentalStartDateUtc.HasValue ? orderItem.Product.FormatRentalDate(orderItem.RentalStartDateUtc.Value) : "";
                        var rentalEndDate = orderItem.RentalEndDateUtc.HasValue ? orderItem.Product.FormatRentalDate(orderItem.RentalEndDateUtc.Value) : "";
                        shipmentItemModel.RentalInfo = string.Format(_localizationService.GetResource("Order.Rental.FormattedDate"),
                            rentalStartDate, rentalEndDate);
                    }

                    model.Items.Add(shipmentItemModel);
                }
            }

            if (prepareShipmentEvent && !String.IsNullOrEmpty(shipment.TrackingNumber))
            {
                var order = shipment.Order;
                var srcm = _shippingService.LoadShippingRateComputationMethodBySystemName(order.ShippingRateComputationMethodSystemName);
                if (srcm != null &&
                    srcm.PluginDescriptor.Installed &&
                    srcm.IsShippingRateComputationMethodActive(_shippingSettings))
                {
                    var shipmentTracker = srcm.ShipmentTracker;
                    if (shipmentTracker != null)
                    {
                        model.TrackingNumberUrl = shipmentTracker.GetUrl(shipment.TrackingNumber);
                        if (_shippingSettings.DisplayShipmentEventsToStoreOwner)
                        {
                            var shipmentEvents = shipmentTracker.GetShipmentEvents(shipment.TrackingNumber);
                            if (shipmentEvents != null)
                            {
                                foreach (var shipmentEvent in shipmentEvents)
                                {
                                    var shipmentStatusEventModel = new ShipmentModel.ShipmentStatusEventModel();
                                    var shipmentEventCountry = _countryService.GetCountryByTwoLetterIsoCode(shipmentEvent.CountryCode);
                                    shipmentStatusEventModel.Country = shipmentEventCountry != null
                                        ? shipmentEventCountry.GetLocalized(x => x.Name)
                                        : shipmentEvent.CountryCode;
                                    shipmentStatusEventModel.Date = shipmentEvent.Date;
                                    shipmentStatusEventModel.EventName = shipmentEvent.EventName;
                                    shipmentStatusEventModel.Location = shipmentEvent.Location;
                                    model.ShipmentStatusEvents.Add(shipmentStatusEventModel);
                                }
                            }
                        }
                    }
                }
            }

            return model;
        }

        #endregion

        #region Order list

        public ActionResult Index()
        {
            return RedirectToAction("List");
        }

        public ActionResult List(int? orderStatusId = null,
            int? paymentStatusId = null, int? shippingStatusId = null)
        {
            if (!_permissionService.Authorize(StandardPermissionProvider.ManageOrders))
                return AccessDeniedView();

            //order statuses
            var model = new OrderListModel();
            model.AvailableOrderStatuses = OrderStatus.Pending.ToSelectList(false).ToList();
            model.AvailableOrderStatuses.Insert(0, new SelectListItem { Text = _localizationService.GetResource("Admin.Common.All"), Value = "0" });
            if (orderStatusId.HasValue)
            {
                //pre-select value?
                var item = model.AvailableOrderStatuses.FirstOrDefault(x => x.Value == orderStatusId.Value.ToString());
                if (item != null)
                    item.Selected = true;
            }

            //payment statuses
            model.AvailablePaymentStatuses = PaymentStatus.Pending.ToSelectList(false).ToList();
            model.AvailablePaymentStatuses.Insert(0, new SelectListItem { Text = _localizationService.GetResource("Admin.Common.All"), Value = "0" });
            if (paymentStatusId.HasValue)
            {
                //pre-select value?
                var item = model.AvailablePaymentStatuses.FirstOrDefault(x => x.Value == paymentStatusId.Value.ToString());
                if (item != null)
                    item.Selected = true;
            }

            //shipping statuses
            model.AvailableShippingStatuses = ShippingStatus.NotYetShipped.ToSelectList(false).ToList();
            model.AvailableShippingStatuses.Insert(0, new SelectListItem { Text = _localizationService.GetResource("Admin.Common.All"), Value = "0" });
            if (shippingStatusId.HasValue)
            {
                //pre-select value?
                var item = model.AvailableShippingStatuses.FirstOrDefault(x => x.Value == shippingStatusId.Value.ToString());
                if (item != null)
                    item.Selected = true;
            }

            //stores
            model.AvailableStores.Add(new SelectListItem { Text = _localizationService.GetResource("Admin.Common.All"), Value = "0" });
            foreach (var s in _storeService.GetAllStores())
                model.AvailableStores.Add(new SelectListItem { Text = s.Name, Value = s.Id.ToString() });

            //vendors
            model.AvailableVendors.Add(new SelectListItem { Text = _localizationService.GetResource("Admin.Common.All"), Value = "0" });
            foreach (var v in _vendorService.GetAllVendors(showHidden: true))
                model.AvailableVendors.Add(new SelectListItem { Text = v.Name, Value = v.Id.ToString() });

            //warehouses
            model.AvailableWarehouses.Add(new SelectListItem { Text = _localizationService.GetResource("Admin.Common.All"), Value = "0" });
            foreach (var w in _shippingService.GetAllWarehouses())
                model.AvailableWarehouses.Add(new SelectListItem { Text = w.Name, Value = w.Id.ToString() });

            //payment methods
            model.AvailablePaymentMethods.Add(new SelectListItem { Text = _localizationService.GetResource("Admin.Common.All"), Value = "" });
            foreach (var pm in _paymentService.LoadAllPaymentMethods())
                model.AvailablePaymentMethods.Add(new SelectListItem { Text = pm.PluginDescriptor.FriendlyName, Value = pm.PluginDescriptor.SystemName });

            //billing countries
            foreach (var c in _countryService.GetAllCountriesForBilling(true))
            {
                model.AvailableCountries.Add(new SelectListItem { Text = c.Name, Value = c.Id.ToString() });
            }
            model.AvailableCountries.Insert(0, new SelectListItem { Text = _localizationService.GetResource("Admin.Common.All"), Value = "0" });

            //a vendor should have access only to orders with his products
            model.IsLoggedInAsVendor = _workContext.CurrentVendor != null;

            return View(model);
        }

        [HttpPost]
        public ActionResult OrderList(DataSourceRequest command, OrderListModel model)
        {
            if (!_permissionService.Authorize(StandardPermissionProvider.ManageOrders))
                return AccessDeniedView();

            //a vendor should have access only to his products
            if (_workContext.CurrentVendor != null)
            {
                model.VendorId = _workContext.CurrentVendor.Id;
            }

            //Changes made by Prachi on 21 May, 2016
            DateTime? startDateValue = (model.StartDate == null ? null : (DateTime?)_dateTimeHelper.ConvertToUtcTime(model.StartDate.Value, _dateTimeHelper.CurrentTimeZone));

            DateTime? endDateValue = (model.EndDate == null ? null : (DateTime?)_dateTimeHelper.ConvertToUtcTime(model.EndDate.Value, _dateTimeHelper.CurrentTimeZone).AddDays(1));

            OrderStatus? orderStatus = model.OrderStatusId > 0 ? (OrderStatus?)(model.OrderStatusId) : null;
            PaymentStatus? paymentStatus = model.PaymentStatusId > 0 ? (PaymentStatus?)(model.PaymentStatusId) : null;
            ShippingStatus? shippingStatus = model.ShippingStatusId > 0 ? (ShippingStatus?)(model.ShippingStatusId) : null;

            var filterByProductId = 0;
            var product = _productService.GetProductById(model.ProductId);
            if (product != null && HasAccessToProduct(product))
                filterByProductId = model.ProductId;

            //load orders
            var orders = _orderService.SearchOrders(storeId: model.StoreId,
                vendorId: model.VendorId,
                productId: filterByProductId,
                warehouseId: model.WarehouseId,
                billingCountryId: model.BillingCountryId,
                paymentMethodSystemName: model.PaymentMethodSystemName,
                createdFromUtc: startDateValue,
                createdToUtc: endDateValue,
                os: orderStatus,
                ps: paymentStatus,
                ss: shippingStatus,
                billingEmail: model.CustomerEmail,
                orderNotes: model.OrderNotes,
                orderGuid: model.OrderGuid,
                pageIndex: command.Page - 1,
                pageSize: command.PageSize);
            var gridModel = new DataSourceResult
            {
                Data = orders.Select(x =>
                {
                    var store = _storeService.GetStoreById(x.StoreId);
                    return new OrderModel
                    {
                        Id = x.Id,
                        StoreName = store != null ? store.Name : "Unknown",
                        OrderTotal = _priceFormatter.FormatPrice(x.OrderTotal, true, false),
                        OrderStatus = x.OrderStatus.GetLocalizedEnum(_localizationService, _workContext),
                        PaymentStatus = x.PaymentStatus.GetLocalizedEnum(_localizationService, _workContext),
                        ShippingStatus = x.ShippingStatus.GetLocalizedEnum(_localizationService, _workContext),
                        CustomerEmail = x.BillingAddress.Email,
                        CustomerFullName = string.Format("{0} {1}", x.BillingAddress.FirstName, x.BillingAddress.LastName),
                        //Changes made by Prachi on 23 May, 2016
                        CreatedOn = _dateTimeHelper.ConvertToUserTime(x.CreatedOnUtc, DateTimeKind.Utc)
                    };
                }),
                Total = orders.TotalCount
            };

            //summary report
            //currently we do not support productId and warehouseId parameters for this report
            var reportSummary = _orderReportService.GetOrderAverageReportLine(
                storeId: model.StoreId,
                vendorId: model.VendorId,
                billingCountryId: model.BillingCountryId,
                orderId: 0,
                paymentMethodSystemName: model.PaymentMethodSystemName,
                os: orderStatus,
                ps: paymentStatus,
                ss: shippingStatus,
                startTimeUtc: startDateValue,
                endTimeUtc: endDateValue,
                billingEmail: model.CustomerEmail,
                orderNotes: model.OrderNotes);
            var profit = _orderReportService.ProfitReport(
                storeId: model.StoreId,
                vendorId: model.VendorId,
                billingCountryId: model.BillingCountryId,
                paymentMethodSystemName: model.PaymentMethodSystemName,
                os: orderStatus,
                ps: paymentStatus,
                ss: shippingStatus,
                startTimeUtc: startDateValue,
                endTimeUtc: endDateValue,
                billingEmail: model.CustomerEmail,
                orderNotes: model.OrderNotes);
            var primaryStoreCurrency = _currencyService.GetCurrencyById(_currencySettings.PrimaryStoreCurrencyId);
            if (primaryStoreCurrency == null)
                throw new Exception("Cannot load primary store currency");

            gridModel.ExtraData = new OrderAggreratorModel
            {
                aggregatorprofit = _priceFormatter.FormatPrice(profit, true, false),
                aggregatorshipping = _priceFormatter.FormatShippingPrice(reportSummary.SumShippingExclTax, true, primaryStoreCurrency, _workContext.WorkingLanguage, false),
                aggregatortax = _priceFormatter.FormatPrice(reportSummary.SumTax, true, false),
                aggregatortotal = _priceFormatter.FormatPrice(reportSummary.SumOrders, true, false)
            };
            return new JsonResult
            {
                Data = gridModel
            };
        }

        [HttpPost, ActionName("List")]
        [FormValueRequired("go-to-order-by-number")]
        public ActionResult GoToOrderId(OrderListModel model)
        {
            var order = _orderService.GetOrderById(model.GoDirectlyToNumber);
            if (order == null)
                return List();

            return RedirectToAction("Edit", "Order", new { id = order.Id });
        }

        public ActionResult ProductSearchAutoComplete(string term)
        {
            const int searchTermMinimumLength = 3;
            if (String.IsNullOrWhiteSpace(term) || term.Length < searchTermMinimumLength)
                return Content("");

            //a vendor should have access only to his products
            var vendorId = 0;
            if (_workContext.CurrentVendor != null)
            {
                vendorId = _workContext.CurrentVendor.Id;
            }

            //products
            const int productNumber = 15;
            var products = _productService.SearchProducts(
                vendorId: vendorId,
                keywords: term,
                pageSize: productNumber,
                showHidden: true);

            var result = (from p in products
                          select new
                          {
                              label = p.Name,
                              productid = p.Id
                          })
                          .ToList();
            return Json(result, JsonRequestBehavior.AllowGet);
        }

        #endregion

        #region Export / Import

        [HttpPost, ActionName("List")]
        [FormValueRequired("exportxml-all")]
        public ActionResult ExportXmlAll(OrderListModel model)
        {
            if (!_permissionService.Authorize(StandardPermissionProvider.ManageOrders))
                return AccessDeniedView();

            //a vendor cannot export orders
            if (_workContext.CurrentVendor != null)
                return AccessDeniedView();

            DateTime? startDateValue = (model.StartDate == null) ? null
                            : (DateTime?)_dateTimeHelper.ConvertToUtcTime(model.StartDate.Value, _dateTimeHelper.CurrentTimeZone);

            DateTime? endDateValue = (model.EndDate == null) ? null
                            : (DateTime?)_dateTimeHelper.ConvertToUtcTime(model.EndDate.Value, _dateTimeHelper.CurrentTimeZone).AddDays(1);

            OrderStatus? orderStatus = model.OrderStatusId > 0 ? (OrderStatus?)(model.OrderStatusId) : null;
            PaymentStatus? paymentStatus = model.PaymentStatusId > 0 ? (PaymentStatus?)(model.PaymentStatusId) : null;
            ShippingStatus? shippingStatus = model.ShippingStatusId > 0 ? (ShippingStatus?)(model.ShippingStatusId) : null;

            var filterByProductId = 0;
            var product = _productService.GetProductById(model.ProductId);
            if (product != null && HasAccessToProduct(product))
                filterByProductId = model.ProductId;

            //load orders
            var orders = _orderService.SearchOrders(storeId: model.StoreId,
                vendorId: model.VendorId,
                productId: filterByProductId,
                warehouseId: model.WarehouseId,
                billingCountryId: model.BillingCountryId,
                paymentMethodSystemName: model.PaymentMethodSystemName,
                createdFromUtc: startDateValue,
                createdToUtc: endDateValue,
                os: orderStatus,
                ps: paymentStatus,
                ss: shippingStatus,
                billingEmail: model.CustomerEmail,
                orderNotes: model.OrderNotes,
                orderGuid: model.OrderGuid);

            try
            {
                var xml = _exportManager.ExportOrdersToXml(orders);
                return new XmlDownloadResult(xml, "orders.xml");
            }
            catch (Exception exc)
            {
                ErrorNotification(exc);
                return RedirectToAction("List");
            }
        }

        [HttpPost]
        public ActionResult ExportXmlSelected(string selectedIds)
        {
            if (!_permissionService.Authorize(StandardPermissionProvider.ManageOrders))
                return AccessDeniedView();

            //a vendor cannot export orders
            if (_workContext.CurrentVendor != null)
                return AccessDeniedView();

            var orders = new List<Order>();
            if (selectedIds != null)
            {
                var ids = selectedIds
                    .Split(new[] { ',' }, StringSplitOptions.RemoveEmptyEntries)
                    .Select(x => Convert.ToInt32(x))
                    .ToArray();
                orders.AddRange(_orderService.GetOrdersByIds(ids));
            }

            var xml = _exportManager.ExportOrdersToXml(orders);
            return new XmlDownloadResult(xml, "orders.xml");
        }

        [HttpPost, ActionName("List")]
        [FormValueRequired("exportexcel-all")]
        public ActionResult ExportExcelAll(OrderListModel model)
        {
            if (!_permissionService.Authorize(StandardPermissionProvider.ManageOrders))
                return AccessDeniedView();

            //a vendor cannot export orders
            if (_workContext.CurrentVendor != null)
                return AccessDeniedView();

            DateTime? startDateValue = (model.StartDate == null) ? null
                            : (DateTime?)_dateTimeHelper.ConvertToUtcTime(model.StartDate.Value, _dateTimeHelper.CurrentTimeZone);

            DateTime? endDateValue = (model.EndDate == null) ? null
                            : (DateTime?)_dateTimeHelper.ConvertToUtcTime(model.EndDate.Value, _dateTimeHelper.CurrentTimeZone).AddDays(1);

            OrderStatus? orderStatus = model.OrderStatusId > 0 ? (OrderStatus?)(model.OrderStatusId) : null;
            PaymentStatus? paymentStatus = model.PaymentStatusId > 0 ? (PaymentStatus?)(model.PaymentStatusId) : null;
            ShippingStatus? shippingStatus = model.ShippingStatusId > 0 ? (ShippingStatus?)(model.ShippingStatusId) : null;

            var filterByProductId = 0;
            var product = _productService.GetProductById(model.ProductId);
            if (product != null && HasAccessToProduct(product))
                filterByProductId = model.ProductId;

            //load orders
            var orders = _orderService.SearchOrders(storeId: model.StoreId,
                vendorId: model.VendorId,
                productId: filterByProductId,
                warehouseId: model.WarehouseId,
                billingCountryId: model.BillingCountryId,
                paymentMethodSystemName: model.PaymentMethodSystemName,
                createdFromUtc: startDateValue,
                createdToUtc: endDateValue,
                os: orderStatus,
                ps: paymentStatus,
                ss: shippingStatus,
                billingEmail: model.CustomerEmail,
                orderNotes: model.OrderNotes,
                orderGuid: model.OrderGuid);

            try
            {
                byte[] bytes;
                using (var stream = new MemoryStream())
                {
                    _exportManager.ExportOrdersToXlsx(stream, orders);
                    bytes = stream.ToArray();
                }
                return File(bytes, "text/xls", "orders.xlsx");
                //Response.AddHeader("content-disposition", "attachment;filename=orders.xlsx");
                //Response.ContentType = "application/vnd.openxmlformats-officedocument.spreadsheetml.sheet";
                //Response.BinaryWrite(bytes);
                //Response.End();
                //return new EmptyResult();
            }
            catch (Exception exc)
            {
                ErrorNotification(exc);
                return RedirectToAction("List");
            }
        }

        [HttpPost]
        public ActionResult ExportExcelSelected(string selectedIds)
        {
            if (!_permissionService.Authorize(StandardPermissionProvider.ManageOrders))
                return AccessDeniedView();

            //a vendor cannot export orders
            if (_workContext.CurrentVendor != null)
                return AccessDeniedView();

            var orders = new List<Order>();
            if (selectedIds != null)
            {
                var ids = selectedIds
                    .Split(new[] { ',' }, StringSplitOptions.RemoveEmptyEntries)
                    .Select(x => Convert.ToInt32(x))
                    .ToArray();
                orders.AddRange(_orderService.GetOrdersByIds(ids));
            }

            byte[] bytes;
            using (var stream = new MemoryStream())
            {
                _exportManager.ExportOrdersToXlsx(stream, orders);
                bytes = stream.ToArray();
            }
            return File(bytes, "text/xls", "orders.xlsx");
            //Response.AddHeader("content-disposition", "attachment;filename=orders.xlsx");
            //Response.ContentType = "application/vnd.openxmlformats-officedocument.spreadsheetml.sheet";
            //Response.BinaryWrite(bytes);
            //Response.End();
            //return new EmptyResult();
        }

        #endregion

        #region Order details

        #region Payments and other order workflow

        [HttpPost, ActionName("Edit")]
        [FormValueRequired("cancelorder")]
        public ActionResult CancelOrder(int id)
        {
            if (!_permissionService.Authorize(StandardPermissionProvider.ManageOrders))
                return AccessDeniedView();

            var order = _orderService.GetOrderById(id);
            if (order == null)
                //No order found with the specified id
                return RedirectToAction("List");

            //a vendor does not have access to this functionality
            if (_workContext.CurrentVendor != null)
                return RedirectToAction("Edit", "Order", new { id = id });

            try
            {
                _orderProcessingService.CancelOrder(order, true);
                var model = new OrderModel();
                PrepareOrderDetailsModel(model, order);
                return View(model);
            }
            catch (Exception exc)
            {
                //error
                var model = new OrderModel();
                PrepareOrderDetailsModel(model, order);
                ErrorNotification(exc, false);
                return View(model);
            }
        }

        [HttpPost, ActionName("Edit")]
        [FormValueRequired("captureorder")]
        public ActionResult CaptureOrder(int id)
        {
            if (!_permissionService.Authorize(StandardPermissionProvider.ManageOrders))
                return AccessDeniedView();

            var order = _orderService.GetOrderById(id);
            if (order == null)
                //No order found with the specified id
                return RedirectToAction("List");

            //a vendor does not have access to this functionality
            if (_workContext.CurrentVendor != null)
                return RedirectToAction("Edit", "Order", new { id = id });

            try
            {
                var errors = _orderProcessingService.Capture(order);
                var model = new OrderModel();
                PrepareOrderDetailsModel(model, order);
                foreach (var error in errors)
                    ErrorNotification(error, false);
                return View(model);
            }
            catch (Exception exc)
            {
                //error
                var model = new OrderModel();
                PrepareOrderDetailsModel(model, order);
                ErrorNotification(exc, false);
                return View(model);
            }

        }

        [HttpPost, ActionName("Edit")]
        [FormValueRequired("markorderaspaid")]
        public ActionResult MarkOrderAsPaid(int id, string PaymentType = "", string PaymentRemarks = "")
        {
            if (!_permissionService.Authorize(StandardPermissionProvider.ManageOrders))
                return AccessDeniedView();

            var order = _orderService.GetOrderById(id);
            if (order == null)
                //No order found with the specified id
                return RedirectToAction("List");

            //a vendor does not have access to this functionality
            if (_workContext.CurrentVendor != null)
                return RedirectToAction("Edit", "Order", new { id = id });

            try
            {
                //Added By Faisal on 03 Dec, 2015
                var model = new OrderModel();
                string strFriendlyName = "";

                var chkpaymentMethod = _paymentService.LoadPaymentMethodBySystemName(order.PaymentMethodSystemName);
                if (chkpaymentMethod != null)
                    strFriendlyName = chkpaymentMethod.GetLocalizedFriendlyName(_localizationService, _workContext.WorkingLanguage.Id);

                if (!order.AllowStoringCreditCardNumber)
                {
                    order.CardType = null;
                    order.CardName = null;
                    order.CardNumber = null;
                    order.CardCvv2 = null;
                    order.CardExpirationMonth = null;
                    order.CardExpirationYear = null;
                    //Added By Faisal on 11 Apr, 2016
                    order.TotalChargedAmount = order.OrderTotal;
                    order.RefundedShipping = decimal.Zero;

                    foreach (OrderItem item in order.OrderItems)
                    {
                        item.TotalChargedAmount = item.PriceInclTax;
                        item.RefundedProduct = decimal.Zero;
                        item.RefundAfterShipping = decimal.Zero;
                    }

                    //End 11 Apr, 2016
                }
                else if (strFriendlyName.Trim() != string.Empty && strFriendlyName.ToUpper().Trim() != "CREDIT CARD" && strFriendlyName.ToUpper().Trim() != "CREDITCARD")
                {
                    ProcessPaymentResult processPaymentResult = null;
                    ProcessPaymentRequest paymentInfo = new ProcessPaymentRequest();
                    paymentInfo.CreditCardCvv2 = _encryptionService.DecryptText(order.CardCvv2);
                    paymentInfo.CreditCardExpireMonth = Convert.ToInt32(_encryptionService.DecryptText(order.CardExpirationMonth));
                    paymentInfo.CreditCardExpireYear = Convert.ToInt32(_encryptionService.DecryptText(order.CardExpirationYear));
                    paymentInfo.CreditCardName = _encryptionService.DecryptText(order.CardName);
                    paymentInfo.CreditCardNumber = _encryptionService.DecryptText(order.CardNumber);
                    paymentInfo.CreditCardType = _encryptionService.DecryptText(order.CardType);
                    paymentInfo.CustomerId = order.CustomerId;
                    paymentInfo.StoreId = order.StoreId;
                    paymentInfo.OrderGuid = order.OrderGuid;
                    paymentInfo.OrderTotal = order.OrderTotal;
                    paymentInfo.InitialOrderId = order.Id;
                    paymentInfo.PaymentMethodSystemName = "Payments.Cybersource";

                    int filterByCountryId = 0;
                    if (_addressSettings.CountryEnabled &&
                        _workContext.CurrentCustomer.BillingAddress != null &&
                        _workContext.CurrentCustomer.BillingAddress.Country != null)
                    {
                        filterByCountryId = order.BillingAddress.Country.Id;
                    }

                    var paymentMethod = _paymentService.LoadPaymentMethodBySystemName(paymentInfo.PaymentMethodSystemName);
                    var paymentMethods = _paymentService
                        .LoadActivePaymentMethods(_workContext.CurrentCustomer.Id, order.StoreId, filterByCountryId)
                        .Where(pm => pm.PaymentMethodType == PaymentMethodType.Standard || pm.PaymentMethodType == PaymentMethodType.Redirection)
                        .ToList();

                    foreach (IPaymentMethod obj in paymentMethods)
                    {
                        if (obj.IsPaymentMethodActive(_paymentSettings))
                        {
                            string name = obj.GetLocalizedFriendlyName(_localizationService, _workContext.WorkingLanguage.Id);
                            if (name.ToUpper().Trim() == "CREDIT CARD" || name.ToUpper().Trim() == "CREDITCARD")
                            {
                                paymentMethod = _paymentService.LoadPaymentMethodBySystemName(obj.ToModel().SystemName);
                                paymentInfo.PaymentMethodSystemName = obj.ToModel().SystemName.Trim();
                                break;
                            }
                        }

                    }

                    if (paymentMethod == null)
                        return RedirectToRoute("CheckoutPaymentMethod");

                    var paymentControllerType = paymentMethod.GetControllerType();
                    var paymentController = DependencyResolver.Current.GetService(paymentControllerType) as BasePaymentController;
                    if (paymentController == null)
                        throw new Exception("Payment controller cannot be loaded");

                    FormCollection frmCollection = new FormCollection();
                    frmCollection.Add("CardholderName", paymentInfo.CreditCardName);
                    frmCollection.Add("CardNumber", paymentInfo.CreditCardNumber);
                    frmCollection.Add("CardCode", paymentInfo.CreditCardCvv2);
                    frmCollection.Add("ExpireMonth", paymentInfo.CreditCardExpireMonth.ToString());
                    frmCollection.Add("ExpireYear", paymentInfo.CreditCardExpireYear.ToString());

                    var warnings = paymentController.ValidatePaymentForm(frmCollection);


                    foreach (var warning in warnings)
                        ModelState.AddModelError("", warning);

                    if (warnings.Count > 0)
                        throw new Exception(warnings[0]);

                    if (ModelState.IsValid)
                    {
                        processPaymentResult = _paymentService.ProcessPayment(paymentInfo);

                        //Added By Faisal on 03 Mar, 2016
                        if (processPaymentResult.Success)
                        {
                            order.TotalChargedAmount = order.OrderTotal;
                            order.RefundedShipping = decimal.Zero;

                            //Added By Prachi on 28 June, 2016
                            foreach(OrderItem oi in order.OrderItems)
                            {
                                oi.TotalChargedAmount = oi.PriceInclTax;
                            }
                            //End 28 June, 2016
                        }
                        //End 03 Mar, 2016
                    }
                    else
                    {
                        ModelState.AddModelError("", processPaymentResult.AuthorizationTransactionResult);
                        PrepareOrderDetailsModel(model, order);
                        return View(model);
                    }
                }
                //End -- Faisal

                //Added By Faisal On 11 Jun, 2016
                order.TotalShippingCharged = order.OrderShippingInclTax;
                //End 11 Jun, 2016

                order.PaymentRemarks = PaymentRemarks;
                order.PaymentType = PaymentType;
                _orderProcessingService.MarkOrderAsPaid(order);
                PrepareOrderDetailsModel(model, order);
                return View(model);
            }
            catch (Exception exc)
            {
                //error
                var model = new OrderModel();
                PrepareOrderDetailsModel(model, order);

                try
                {
                    ErrorNotification(exc, false);
                }
                catch (Exception) { }

                return View(model);
            }
        }

        [HttpPost, ActionName("Edit")]
        [FormValueRequired("refundorder")]
        public ActionResult RefundOrder(int id)
        {
            if (!_permissionService.Authorize(StandardPermissionProvider.ManageOrders))
                return AccessDeniedView();

            var order = _orderService.GetOrderById(id);
            if (order == null)
                //No order found with the specified id
                return RedirectToAction("List");

            //a vendor does not have access to this functionality
            if (_workContext.CurrentVendor != null)
                return RedirectToAction("Edit", "Order", new { id = id });

            try
            {
                var errors = _orderProcessingService.Refund(order);
                var model = new OrderModel();
                PrepareOrderDetailsModel(model, order);
                foreach (var error in errors)
                    ErrorNotification(error, false);
                return View(model);
            }
            catch (Exception exc)
            {
                //error
                var model = new OrderModel();
                PrepareOrderDetailsModel(model, order);
                ErrorNotification(exc, false);
                return View(model);
            }
        }

        [HttpPost, ActionName("Edit")]
        [FormValueRequired("refundorderoffline")]
        public ActionResult RefundOrderOffline(int id)
        {
            if (!_permissionService.Authorize(StandardPermissionProvider.ManageOrders))
                return AccessDeniedView();

            var order = _orderService.GetOrderById(id);
            if (order == null)
                //No order found with the specified id
                return RedirectToAction("List");

            //a vendor does not have access to this functionality
            if (_workContext.CurrentVendor != null)
                return RedirectToAction("Edit", "Order", new { id = id });

            try
            {
                _orderProcessingService.RefundOffline(order);
                var model = new OrderModel();
                PrepareOrderDetailsModel(model, order);
                return View(model);
            }
            catch (Exception exc)
            {
                //error
                var model = new OrderModel();
                PrepareOrderDetailsModel(model, order);
                ErrorNotification(exc, false);
                return View(model);
            }
        }

        [HttpPost, ActionName("Edit")]
        [FormValueRequired("voidorder")]
        public ActionResult VoidOrder(int id)
        {
            if (!_permissionService.Authorize(StandardPermissionProvider.ManageOrders))
                return AccessDeniedView();

            var order = _orderService.GetOrderById(id);
            if (order == null)
                //No order found with the specified id
                return RedirectToAction("List");

            //a vendor does not have access to this functionality
            if (_workContext.CurrentVendor != null)
                return RedirectToAction("Edit", "Order", new { id = id });

            try
            {
                var errors = _orderProcessingService.Void(order);
                var model = new OrderModel();
                PrepareOrderDetailsModel(model, order);
                foreach (var error in errors)
                    ErrorNotification(error, false);
                return View(model);
            }
            catch (Exception exc)
            {
                //error
                var model = new OrderModel();
                PrepareOrderDetailsModel(model, order);
                ErrorNotification(exc, false);
                return View(model);
            }
        }

        [HttpPost, ActionName("Edit")]
        [FormValueRequired("voidorderoffline")]
        public ActionResult VoidOrderOffline(int id)
        {
            if (!_permissionService.Authorize(StandardPermissionProvider.ManageOrders))
                return AccessDeniedView();

            var order = _orderService.GetOrderById(id);
            if (order == null)
                //No order found with the specified id
                return RedirectToAction("List");

            //a vendor does not have access to this functionality
            if (_workContext.CurrentVendor != null)
                return RedirectToAction("Edit", "Order", new { id = id });

            try
            {
                _orderProcessingService.VoidOffline(order);
                var model = new OrderModel();
                PrepareOrderDetailsModel(model, order);
                return View(model);
            }
            catch (Exception exc)
            {
                //error
                var model = new OrderModel();
                PrepareOrderDetailsModel(model, order);
                ErrorNotification(exc, false);
                return View(model);
            }
        }

        public ActionResult PartiallyRefundOrderPopup(int id, bool online)
        {
            if (!_permissionService.Authorize(StandardPermissionProvider.ManageOrders))
                return AccessDeniedView();

            var order = _orderService.GetOrderById(id);
            if (order == null)
                //No order found with the specified id
                return RedirectToAction("List");

            //a vendor does not have access to this functionality
            if (_workContext.CurrentVendor != null)
                return RedirectToAction("Edit", "Order", new { id = id });

            var model = new OrderModel();
            PrepareOrderDetailsModel(model, order);

            return View(model);
        }

        [HttpPost]
        [FormValueRequired("partialrefundorder")]
        public ActionResult PartiallyRefundOrderPopup(string btnId, string formId, int id, bool online, OrderModel model)
        {
            if (!_permissionService.Authorize(StandardPermissionProvider.ManageOrders))
                return AccessDeniedView();

            var order = _orderService.GetOrderById(id);
            if (order == null)
                //No order found with the specified id
                return RedirectToAction("List");

            //a vendor does not have access to this functionality
            if (_workContext.CurrentVendor != null)
                return RedirectToAction("Edit", "Order", new { id = id });

            try
            {
                decimal amountToRefund = model.AmountToRefund;
                if (amountToRefund <= decimal.Zero)
                    throw new NopException("Enter amount to refund");

                decimal maxAmountToRefund = order.OrderTotal - order.RefundedAmount;
                if (amountToRefund > maxAmountToRefund)
                    amountToRefund = maxAmountToRefund;

                var errors = new List<string>();
                if (online)
                    errors = _orderProcessingService.PartiallyRefund(order, amountToRefund).ToList();
                else
                    _orderProcessingService.PartiallyRefundOffline(order, amountToRefund);

                if (errors.Count == 0)
                {
                    //success
                    ViewBag.RefreshPage = true;
                    ViewBag.btnId = btnId;
                    ViewBag.formId = formId;

                    PrepareOrderDetailsModel(model, order);
                    return View(model);
                }

                //error
                PrepareOrderDetailsModel(model, order);
                foreach (var error in errors)
                    ErrorNotification(error, false);
                return View(model);
            }
            catch (Exception exc)
            {
                //error
                PrepareOrderDetailsModel(model, order);
                ErrorNotification(exc, false);
                return View(model);
            }
        }

        [HttpPost, ActionName("Edit")]
        [FormValueRequired("btnSaveOrderStatus")]
        public ActionResult ChangeOrderStatus(int id, OrderModel model)
        {
            if (!_permissionService.Authorize(StandardPermissionProvider.ManageOrders))
                return AccessDeniedView();

            var order = _orderService.GetOrderById(id);
            if (order == null)
                //No order found with the specified id
                return RedirectToAction("List");

            //a vendor does not have access to this functionality
            if (_workContext.CurrentVendor != null)
                return RedirectToAction("Edit", "Order", new { id = id });

            try
            {
                order.OrderStatusId = model.OrderStatusId;
                _orderService.UpdateOrder(order);

                //add a note
                order.OrderNotes.Add(new OrderNote
                {
                    Note = string.Format("Order status has been edited. New status: {0}", order.OrderStatus.GetLocalizedEnum(_localizationService, _workContext)),
                    DisplayToCustomer = false,
                    CreatedOnUtc = DateTime.UtcNow
                });
                _orderService.UpdateOrder(order);

                model = new OrderModel();
                PrepareOrderDetailsModel(model, order);
                return View(model);
            }
            catch (Exception exc)
            {
                //error
                model = new OrderModel();
                PrepareOrderDetailsModel(model, order);
                ErrorNotification(exc, false);
                return View(model);
            }
        }

        #endregion

        #region Edit, delete

        public ActionResult Edit(int id)
        {
            if (!_permissionService.Authorize(StandardPermissionProvider.ManageOrders))
                return AccessDeniedView();

            var order = _orderService.GetOrderById(id);
            if (order == null || order.Deleted)
                //No order found with the specified id
                return RedirectToAction("List");

            //a vendor does not have access to this functionality
            if (_workContext.CurrentVendor != null && !HasAccessToOrder(order))
                return RedirectToAction("List");

            var model = new OrderModel();
            PrepareOrderDetailsModel(model, order);

            return View(model);
        }

        [HttpPost]
        public ActionResult Delete(int id)
        {
            if (!_permissionService.Authorize(StandardPermissionProvider.ManageOrders))
                return AccessDeniedView();

            var order = _orderService.GetOrderById(id);
            if (order == null)
                //No order found with the specified id
                return RedirectToAction("List");

            //a vendor does not have access to this functionality
            if (_workContext.CurrentVendor != null)
                return RedirectToAction("Edit", "Order", new { id = id });

            _orderProcessingService.DeleteOrder(order);
            return RedirectToAction("List");
        }

        public ActionResult PdfInvoice(int orderId)
        {
            if (!_permissionService.Authorize(StandardPermissionProvider.ManageOrders))
                return AccessDeniedView();

            //a vendor does not have access to this functionality
            if (_workContext.CurrentVendor != null)
                return RedirectToAction("Edit", "Order", new { id = orderId });

            var order = _orderService.GetOrderById(orderId);
            var orders = new List<Order>();
            orders.Add(order);
            byte[] bytes;
            using (var stream = new MemoryStream())
            {
                _pdfService.PrintOrdersToPdf(stream, orders, _workContext.WorkingLanguage.Id);
                bytes = stream.ToArray();
            }
            return File(bytes, "application/pdf", string.Format("order_{0}.pdf", order.Id));
        }

        //Added By Prachi on 3 March, 2016 - to send invoice for an order by email
        public ActionResult EmailInvoice(int orderId)
        {
            if (!_permissionService.Authorize(StandardPermissionProvider.ManageOrders))
                return AccessDeniedView();

            //a vendor does not have access to this functionality
            if (_workContext.CurrentVendor != null)
                return RedirectToAction("Edit", "Order", new { id = orderId });

            var order = _orderService.GetOrderById(orderId);

            var orderPlacedAttachmentFilePath = _orderSettings.AttachPdfInvoiceToOrderPlacedEmail ?
                _pdfService.PrintOrderToPdf(order, order.CustomerLanguageId) : null;
            var orderPlacedAttachmentFileName = _orderSettings.AttachPdfInvoiceToOrderPlacedEmail ?
                "order" + order.Id + ".pdf" : null;

            int queuedEmailId = _workflowMessageService.SendOrderInvoice(order, order.CustomerLanguageId, orderPlacedAttachmentFilePath, orderPlacedAttachmentFileName);
            if (queuedEmailId > 0)
                SuccessNotification(_localizationService.GetResource("Admin.Orders.PdfInvoice.Email"));
            return RedirectToAction("Edit", "Order", new { id = orderId });
        }

        [HttpPost, ActionName("List")]
        [FormValueRequired("pdf-invoice-all")]
        public ActionResult PdfInvoiceAll(OrderListModel model)
        {
            if (!_permissionService.Authorize(StandardPermissionProvider.ManageOrders))
                return AccessDeniedView();

            //a vendor should have access only to his products
            if (_workContext.CurrentVendor != null)
            {
                model.VendorId = _workContext.CurrentVendor.Id;
            }

            DateTime? startDateValue = (model.StartDate == null) ? null
                            : (DateTime?)_dateTimeHelper.ConvertToUtcTime(model.StartDate.Value, _dateTimeHelper.CurrentTimeZone);

            DateTime? endDateValue = (model.EndDate == null) ? null
                            : (DateTime?)_dateTimeHelper.ConvertToUtcTime(model.EndDate.Value, _dateTimeHelper.CurrentTimeZone).AddDays(1);

            OrderStatus? orderStatus = model.OrderStatusId > 0 ? (OrderStatus?)(model.OrderStatusId) : null;
            PaymentStatus? paymentStatus = model.PaymentStatusId > 0 ? (PaymentStatus?)(model.PaymentStatusId) : null;
            ShippingStatus? shippingStatus = model.ShippingStatusId > 0 ? (ShippingStatus?)(model.ShippingStatusId) : null;

            var filterByProductId = 0;
            var product = _productService.GetProductById(model.ProductId);
            if (product != null && HasAccessToProduct(product))
                filterByProductId = model.ProductId;

            //load orders
            var orders = _orderService.SearchOrders(storeId: model.StoreId,
                vendorId: model.VendorId,
                productId: filterByProductId,
                warehouseId: model.WarehouseId,
                billingCountryId: model.BillingCountryId,
                paymentMethodSystemName: model.PaymentMethodSystemName,
                createdFromUtc: startDateValue,
                createdToUtc: endDateValue,
                os: orderStatus,
                ps: paymentStatus,
                ss: shippingStatus,
                billingEmail: model.CustomerEmail,
                orderNotes: model.OrderNotes,
                orderGuid: model.OrderGuid);

            byte[] bytes;
            using (var stream = new MemoryStream())
            {
                _pdfService.PrintOrdersToPdf(stream, orders, _workContext.WorkingLanguage.Id);
                bytes = stream.ToArray();
            }
            return File(bytes, "application/pdf", "orders.pdf");
        }

        [HttpPost]
        public ActionResult PdfInvoiceSelected(string selectedIds)
        {
            if (!_permissionService.Authorize(StandardPermissionProvider.ManageOrders))
                return AccessDeniedView();

            var orders = new List<Order>();
            if (selectedIds != null)
            {
                var ids = selectedIds
                    .Split(new[] { ',' }, StringSplitOptions.RemoveEmptyEntries)
                    .Select(x => Convert.ToInt32(x))
                    .ToArray();
                orders.AddRange(_orderService.GetOrdersByIds(ids));
            }

            //a vendor should have access only to his products
            if (_workContext.CurrentVendor != null)
            {
                orders = orders.Where(HasAccessToOrder).ToList();
            }

            //ensure that we at least one order selected
            if (orders.Count == 0)
            {
                ErrorNotification(_localizationService.GetResource("Admin.Orders.PdfInvoice.NoOrders"));
                return RedirectToAction("List");
            }

            byte[] bytes;
            using (var stream = new MemoryStream())
            {
                _pdfService.PrintOrdersToPdf(stream, orders, _workContext.WorkingLanguage.Id);
                bytes = stream.ToArray();
            }
            return File(bytes, "application/pdf", "orders.pdf");
        }

        [HttpPost, ActionName("Edit")]
        [FormValueRequired("btnSaveCC")]
        public ActionResult EditCreditCardInfo(int id, OrderModel model)
        {
            if (!_permissionService.Authorize(StandardPermissionProvider.ManageOrders))
                return AccessDeniedView();

            var order = _orderService.GetOrderById(id);
            if (order == null)
                //No order found with the specified id
                return RedirectToAction("List");

            //a vendor does not have access to this functionality
            if (_workContext.CurrentVendor != null)
                return RedirectToAction("Edit", "Order", new { id = id });

            if (order.AllowStoringCreditCardNumber)
            {
                //Added By Faisal on 04 Dec, 2015
                {
                    string strFriendlyName = "";
                    var chkpaymentMethod = _paymentService.LoadPaymentMethodBySystemName(order.PaymentMethodSystemName);
                    if (chkpaymentMethod != null)
                        strFriendlyName = chkpaymentMethod.GetLocalizedFriendlyName(_localizationService, _workContext.WorkingLanguage.Id);

                    ProcessPaymentRequest paymentInfo = new ProcessPaymentRequest();
                    paymentInfo.CreditCardCvv2 = model.CardCvv2;
                    paymentInfo.CreditCardExpireMonth = Convert.ToInt32(model.CardExpirationMonth);
                    paymentInfo.CreditCardExpireYear = Convert.ToInt32(model.CardExpirationYear);
                    paymentInfo.CreditCardName = model.CardName;
                    paymentInfo.CreditCardNumber = model.CardNumber;
                    paymentInfo.CreditCardType = model.CardType;
                    paymentInfo.CustomerId = order.CustomerId;
                    paymentInfo.StoreId = order.StoreId;
                    paymentInfo.OrderGuid = order.OrderGuid;
                    paymentInfo.OrderTotal = order.OrderTotal;
                    paymentInfo.InitialOrderId = order.Id;

                    if (paymentInfo.CreditCardCvv2 == null)
                        paymentInfo.CreditCardCvv2 = " ";

                    int filterByCountryId = 0;
                    if (_addressSettings.CountryEnabled &&
                        _workContext.CurrentCustomer.BillingAddress != null &&
                        _workContext.CurrentCustomer.BillingAddress.Country != null)
                    {
                        filterByCountryId = order.BillingAddress.Country.Id;
                    }

                    var paymentMethod = _paymentService.LoadPaymentMethodBySystemName(paymentInfo.PaymentMethodSystemName);
                    var paymentMethods = _paymentService
                        .LoadActivePaymentMethods(_workContext.CurrentCustomer.Id, order.StoreId, filterByCountryId)
                        .Where(pm => pm.PaymentMethodType == PaymentMethodType.Standard || pm.PaymentMethodType == PaymentMethodType.Redirection)
                        .ToList();

                    foreach (IPaymentMethod obj in paymentMethods)
                    {
                        if (obj.IsPaymentMethodActive(_paymentSettings))
                        {
                            string name = obj.GetLocalizedFriendlyName(_localizationService, _workContext.WorkingLanguage.Id);
                            if (name.ToUpper().Trim() == "CREDIT CARD" || name.ToUpper().Trim() == "CREDITCARD")
                            {
                                paymentMethod = _paymentService.LoadPaymentMethodBySystemName(obj.ToModel().SystemName);
                                paymentInfo.PaymentMethodSystemName = obj.ToModel().SystemName.Trim();
                                break;
                            }
                        }
                    }

                    if (paymentMethod != null)
                    {
                        var paymentControllerType = paymentMethod.GetControllerType();
                        var paymentController = DependencyResolver.Current.GetService(paymentControllerType) as BasePaymentController;
                        if (paymentController != null)
                        {
                            FormCollection frmCollection = new FormCollection();
                            frmCollection.Add("CardholderName", paymentInfo.CreditCardName);
                            frmCollection.Add("CardNumber", paymentInfo.CreditCardNumber);
                            frmCollection.Add("CardCode", paymentInfo.CreditCardCvv2);
                            frmCollection.Add("ExpireMonth", paymentInfo.CreditCardExpireMonth.ToString());
                            frmCollection.Add("ExpireYear", paymentInfo.CreditCardExpireYear.ToString());

                            var warnings = paymentController.ValidatePaymentForm(frmCollection);
                            foreach (var warning in warnings)
                                ModelState.AddModelError("", warning);

                            if (warnings.Count > 0 || !ModelState.IsValid)
                            {
                                ViewBag.IsErr = "Y";
                                PrepareOrderDetailsModel(model, order);
                                return View(model);
                            }
                            //Added By Faisal On 17 Feb, 2016
                            order.PaymentType = "Credit Card";
                            //End
                        }
                    }
                }

                string cardType = model.CardType;
                string cardName = model.CardName;
                string cardNumber = model.CardNumber;
                string cardCvv2 = model.CardCvv2;
                string cardExpirationMonth = model.CardExpirationMonth;
                string cardExpirationYear = model.CardExpirationYear;

                order.CardType = _encryptionService.EncryptText(cardType);
                order.CardName = _encryptionService.EncryptText(cardName);
                order.CardNumber = _encryptionService.EncryptText(cardNumber);
                order.MaskedCreditCardNumber = _encryptionService.EncryptText(_paymentService.GetMaskedCreditCardNumber(cardNumber));
                order.CardCvv2 = _encryptionService.EncryptText(cardCvv2);
                order.CardExpirationMonth = _encryptionService.EncryptText(cardExpirationMonth);
                order.CardExpirationYear = _encryptionService.EncryptText(cardExpirationYear);
                _orderService.UpdateOrder(order);
            }

            //add a note
            order.OrderNotes.Add(new OrderNote
            {
                Note = "Credit card info has been edited",
                DisplayToCustomer = false,
                CreatedOnUtc = DateTime.UtcNow
            });
            _orderService.UpdateOrder(order);

            PrepareOrderDetailsModel(model, order);
            return View(model);
        }

        [HttpPost, ActionName("Edit")]
        [FormValueRequired("btnSaveOrderTotals")]
        public ActionResult EditOrderTotals(int id, OrderModel model)
        {
            if (!_permissionService.Authorize(StandardPermissionProvider.ManageOrders))
                return AccessDeniedView();

            var order = _orderService.GetOrderById(id);
            if (order == null)
                //No order found with the specified id
                return RedirectToAction("List");

            //a vendor does not have access to this functionality
            if (_workContext.CurrentVendor != null)
                return RedirectToAction("Edit", "Order", new { id = id });
            //Changs by Prachi on 17 Feb, 2016 - Order total re-calculated after order subtotal, shipping cost, discount has been manually updated
            order.OrderSubtotalInclTax = Math.Round(model.OrderSubtotalInclTaxValue, 2);
            order.OrderSubtotalExclTax = Math.Round(model.OrderSubtotalExclTaxValue, 2);
            order.OrderSubTotalDiscountInclTax = Math.Round(model.OrderSubTotalDiscountInclTaxValue, 2);
            order.OrderSubTotalDiscountExclTax = Math.Round(model.OrderSubTotalDiscountExclTaxValue, 2);
            order.OrderShippingInclTax = Math.Round(model.OrderShippingInclTaxValue, 2);
            order.OrderShippingExclTax = Math.Round(model.OrderShippingExclTaxValue, 2);
            order.PaymentMethodAdditionalFeeInclTax = Math.Round(model.PaymentMethodAdditionalFeeInclTaxValue, 2);
            order.PaymentMethodAdditionalFeeExclTax = Math.Round(model.PaymentMethodAdditionalFeeExclTaxValue, 2);
            order.TaxRates = model.TaxRatesValue;
            order.OrderTax = Math.Round(model.TaxValue, 2);
            order.OrderDiscount = Math.Round(model.OrderTotalDiscountValue, 2);
            //Prachi 01/12/2015 - changes made to re-calculate the order total after making changes in product prices

            //order.OrderTotal = model.OrderTotalValue - order.OrderDiscount;
            order.OrderTotal = Math.Round(model.OrderTotalValue, 2);
            _orderService.UpdateOrder(order);

            //add a note
            order.OrderNotes.Add(new OrderNote
            {
                Note = "Order totals have been edited",
                DisplayToCustomer = false,
                CreatedOnUtc = DateTime.UtcNow
            });
            _orderService.UpdateOrder(order);

            PrepareOrderDetailsModel(model, order);
            return View(model);
        }

        [HttpPost, ActionName("Edit")]
        [FormValueRequired("save-shipping-method")]
        public ActionResult EditShippingMethod(int id, OrderModel model)
        {
            if (!_permissionService.Authorize(StandardPermissionProvider.ManageOrders))
                return AccessDeniedView();

            var order = _orderService.GetOrderById(id);
            if (order == null)
                //No order found with the specified id
                return RedirectToAction("List");

            //a vendor does not have access to this functionality
            if (_workContext.CurrentVendor != null)
                return RedirectToAction("Edit", "Order", new { id = id });

            order.ShippingMethod = model.ShippingMethod;
            _orderService.UpdateOrder(order);

            //add a note
            order.OrderNotes.Add(new OrderNote
            {
                Note = "Shipping method has been edited",
                DisplayToCustomer = false,
                CreatedOnUtc = DateTime.UtcNow
            });
            _orderService.UpdateOrder(order);

            PrepareOrderDetailsModel(model, order);

            //selected tab
            SaveSelectedTabIndex(persistForTheNextRequest: false);

            return View(model);
        }

        [HttpPost, ActionName("Edit")]
        [FormValueRequired(FormValueRequirement.StartsWith, "btnSaveOrderItem")]
        [ValidateInput(false)]
        public ActionResult EditOrderItem(int id, FormCollection form)
        {
            if (!_permissionService.Authorize(StandardPermissionProvider.ManageOrders))
                return AccessDeniedView();

            var order = _orderService.GetOrderById(id);
            if (order == null)
                //No order found with the specified id
                return RedirectToAction("List");

            //a vendor does not have access to this functionality
            if (_workContext.CurrentVendor != null)
                return RedirectToAction("Edit", "Order", new { id = id });

            //get order item identifier
            int orderItemId = 0;
            foreach (var formValue in form.AllKeys)
                if (formValue.StartsWith("btnSaveOrderItem", StringComparison.InvariantCultureIgnoreCase))
                    orderItemId = Convert.ToInt32(formValue.Substring("btnSaveOrderItem".Length));

            var orderItem = order.OrderItems.FirstOrDefault(x => x.Id == orderItemId);
            if (orderItem == null)
                throw new ArgumentException("No order item found with the specified id");

            decimal unitPriceInclTax, unitPriceExclTax, discountInclTax, discountExclTax, priceInclTax, priceExclTax;
            int quantity;
            //if (!decimal.TryParse(form["pvUnitPriceInclTax" + orderItemId], out unitPriceInclTax))
            //    unitPriceInclTax = orderItem.UnitPriceInclTax;
            //if (!decimal.TryParse(form["pvUnitPriceExclTax" + orderItemId], out unitPriceExclTax))
            //    unitPriceExclTax = orderItem.UnitPriceExclTax;
            //if (!int.TryParse(form["pvQuantity" + orderItemId], out quantity))
            //    quantity = orderItem.Quantity;
            //if (!decimal.TryParse(form["pvDiscountInclTax" + orderItemId], out discountInclTax))
            //    discountInclTax = orderItem.DiscountAmountInclTax;
            //if (!decimal.TryParse(form["pvDiscountExclTax" + orderItemId], out discountExclTax))
            //    discountExclTax = orderItem.DiscountAmountExclTax;
            //if (!decimal.TryParse(form["pvPriceInclTax" + orderItemId], out priceInclTax))
            //    priceInclTax = orderItem.PriceInclTax;
            //    //Prachi 01/12/2015 - changes made to re-calculate the order total after making changes in product prices
            //    //priceInclTax = (unitPriceInclTax * quantity) + discountInclTax;

            //if (!decimal.TryParse(form["pvPriceExclTax" + orderItemId], out priceExclTax))
            //    //Prachi 01/12/2015 - changes made to re-calculate the order total after making changes in product prices
            //    //priceExclTax = (unitPriceExclTax * quantity) + discountExclTax;
            //    priceExclTax = orderItem.PriceExclTax;

            if (!decimal.TryParse(form["pvUnitPriceInclTax" + orderItemId], out unitPriceInclTax))
                unitPriceInclTax = Math.Round(orderItem.UnitPriceInclTax, 2);
            if (!decimal.TryParse(form["pvUnitPriceExclTax" + orderItemId], out unitPriceExclTax))
                unitPriceExclTax = Math.Round(orderItem.UnitPriceExclTax, 2);
            if (!int.TryParse(form["pvQuantity" + orderItemId], out quantity))
                quantity = orderItem.Quantity;
            if (!decimal.TryParse(form["pvDiscountInclTax" + orderItemId], out discountInclTax))
                discountInclTax = Math.Round(orderItem.DiscountAmountInclTax, 2);
            if (!decimal.TryParse(form["pvDiscountExclTax" + orderItemId], out discountExclTax))
                discountExclTax = Math.Round(orderItem.DiscountAmountExclTax, 2);
            if (!decimal.TryParse(form["pvPriceInclTax" + orderItemId], out priceInclTax))
                priceInclTax = Math.Round(orderItem.PriceInclTax, 2);
            //Prachi 01/12/2015 - changes made to re-calculate the order total after making changes in product prices
            //priceInclTax = (unitPriceInclTax * quantity) + discountInclTax;

            if (!decimal.TryParse(form["pvPriceExclTax" + orderItemId], out priceExclTax))
                //Prachi 01/12/2015 - changes made to re-calculate the order total after making changes in product prices
                //priceExclTax = (unitPriceExclTax * quantity) + discountExclTax;
                priceExclTax = Math.Round(orderItem.PriceExclTax, 2);

            int QuantityToUpdateInStock = quantity - orderItem.Quantity;

            if (QuantityToUpdateInStock != 0)
                UpdateStock(orderItemId, QuantityToUpdateInStock);

            if (quantity >= 0)
            {
                orderItem.UnitPriceInclTax = unitPriceInclTax;
                orderItem.UnitPriceExclTax = unitPriceExclTax;
                orderItem.Quantity = quantity;
                orderItem.DiscountAmountInclTax = discountInclTax;
                orderItem.DiscountAmountExclTax = discountExclTax;
                orderItem.PriceInclTax = priceInclTax;
                orderItem.PriceExclTax = priceExclTax;
                _orderService.UpdateOrder(order);
            }
            //else
            //{
            //    _orderService.DeleteOrderItem(orderItem);
            //}

            //Prachi 01/12/2015 - changes made to re-calculate the order total after making changes in product prices
            order.OrderSubtotalInclTax = 0;
            order.OrderSubtotalExclTax = 0;
            order.OrderTotal = 0;

            //Changes by Prachi on 16 Feb, 2016 - Method added to calculate order totals after editing of particular order item
            CalculateOrderTotals(order);

            //add a note
            order.OrderNotes.Add(new OrderNote
            {
                //Changed By Faisal On 26 May, 2016
                Note = "Order item \"" + orderItem.Product.Name + "\" has been edited by " + _workContext.CurrentCustomer.GetFullName(),
                DisplayToCustomer = false,
                CreatedOnUtc = DateTime.UtcNow
            });
            _orderService.UpdateOrder(order);

            var model = new OrderModel();
            PrepareOrderDetailsModel(model, order);

            //selected tab
            SaveSelectedTabIndex(persistForTheNextRequest: false);

            return View(model);
        }

        [HttpPost, ActionName("Edit")]
        [FormValueRequired(FormValueRequirement.StartsWith, "btnDeleteOrderItem")]
        [ValidateInput(false)]
        public ActionResult DeleteOrderItem(int id, FormCollection form)
        {
            if (!_permissionService.Authorize(StandardPermissionProvider.ManageOrders))
                return AccessDeniedView();

            var order = _orderService.GetOrderById(id);
            if (order == null)
                //No order found with the specified id
                return RedirectToAction("List");

            //a vendor does not have access to this functionality
            if (_workContext.CurrentVendor != null)
                return RedirectToAction("Edit", "Order", new { id = id });

            //get order item identifier
            int orderItemId = 0;
            foreach (var formValue in form.AllKeys)
                if (formValue.StartsWith("btnDeleteOrderItem", StringComparison.InvariantCultureIgnoreCase))
                    orderItemId = Convert.ToInt32(formValue.Substring("btnDeleteOrderItem".Length));

            var orderItem = order.OrderItems.FirstOrDefault(x => x.Id == orderItemId);
            if (orderItem == null)
                throw new ArgumentException("No order item found with the specified id");

            //Added by Prachi On 1 July, 2016
            if (orderItem.GetTotalNumberOfShippedItems() > 0)
            {
                var model = new OrderModel();
                PrepareOrderDetailsModel(model, order);

                ErrorNotification("This order item has an associated shipment record. Please delete it first.", false);

                //selected tab
                SaveSelectedTabIndex(persistForTheNextRequest: false);

                return View(model);
            }
            if (_giftCardService.GetGiftCardsByPurchasedWithOrderItemId(orderItem.Id).Count > 0)
            {
                //we cannot delete an order item with associated gift cards
                //a store owner should delete them first

                var model = new OrderModel();
                PrepareOrderDetailsModel(model, order);

                ErrorNotification("This order item has an associated gift card record. Please delete it first.", false);

                //selected tab
                SaveSelectedTabIndex(persistForTheNextRequest: false);

                return View(model);

            }
            else
            {
                //Added by Faisal On 26 May, 2016
                
                //Changed By Faisal on 06 Jul, 2016 --Total charged amount cannot go negative (was going in case when qty was increased & then item was deleted without charging)
                //decimal itemAmount = orderItem.PriceInclTax;
                //decimal productTax = orderItem.PriceInclTax - orderItem.PriceExclTax;
                
                decimal itemAmount = orderItem.TotalChargedAmount;
                decimal taxRate = 0;
                Math.Round(_taxService.GetProductPrice(orderItem.Product, itemAmount, true, order.Customer, out taxRate), 2);
                decimal productTax = Math.Round(itemAmount * taxRate / (100 + taxRate), 2);
                //End 06 Jul, 2016

                int productId = orderItem.ProductId;
                string strNote = "Order item has been deleted and " + _priceFormatter.FormatPrice(orderItem.PriceInclTax, true, false) + " has been refunded from \"" + orderItem.Product.Name + "\" by " + _workContext.CurrentCustomer.GetFullName();
                decimal totalChargedAmount = orderItem.TotalChargedAmount;
                string prodName = orderItem.Product.Name;

                UpdateStock(orderItem.Id, orderItem.Quantity, true);

                _orderService.DeleteOrderItem(orderItem);

                //Changed By Faisal On 26 May, 2016
                //add a note
                if (totalChargedAmount <= 0)
                {
                    order.OrderNotes.Add(new OrderNote
                    {
                        Note = "Order item \"" + prodName + "\" has been deleted by " + _workContext.CurrentCustomer.GetFullName(),
                        DisplayToCustomer = false,
                        CreatedOnUtc = DateTime.UtcNow
                    });
                }
                else
                {
                    order.TotalChargedAmount -= itemAmount;

                    if (order.CardType != null)
                    {
                        //Process Payment Start
                        ProcessPaymentRequest paymentInfo = new ProcessPaymentRequest();
                        paymentInfo.CreditCardCvv2 = _encryptionService.DecryptText(order.CardCvv2);
                        paymentInfo.CreditCardExpireMonth = Convert.ToInt32(_encryptionService.DecryptText(order.CardExpirationMonth));
                        paymentInfo.CreditCardExpireYear = Convert.ToInt32(_encryptionService.DecryptText(order.CardExpirationYear));
                        paymentInfo.CreditCardName = _encryptionService.DecryptText(order.CardName);
                        paymentInfo.CreditCardNumber = _encryptionService.DecryptText(order.CardNumber);
                        paymentInfo.CreditCardType = _encryptionService.DecryptText(order.CardType);
                        paymentInfo.CustomerId = order.CustomerId;
                        paymentInfo.StoreId = order.StoreId;
                        paymentInfo.OrderGuid = order.OrderGuid;
                        paymentInfo.OrderTotal = itemAmount;//(amountToCharge - amountToRefund);
                        paymentInfo.InitialOrderId = order.Id;
                        paymentInfo.PaymentMethodSystemName = order.PaymentMethodSystemName;

                        try
                        {
                            var errors = _orderProcessingService.RefundOrCharge(order, itemAmount, paymentInfo, true);
                            foreach (var error in errors)
                                ErrorNotification(error, false);
                        }
                        catch (Exception exc)
                        {
                            ErrorNotification(exc, false);
                        }
                    }
                    else
                    {
                        order.PaymentStatus = PaymentStatus.PartiallyRefunded;
                    }

                    order.OrderNotes.Add(new OrderNote
                    {
                        Note = strNote,
                        DisplayToCustomer = false,
                        CreatedOnUtc = DateTime.UtcNow,
                        Amount = itemAmount,
                        ProductId = productId,
                        ProductTax = productTax,
                    });
                }
                //End 26 May, 2016

                //Changs by Prachi on 16 Feb, 2016 - Order total re-calculated after product is deleted
                //decimal orderSubtotalInclTax = 0;

                //Changes by Prachi on 16 Feb, 2016 - Method added to calculate order totals after deleting of particular order item
                CalculateOrderTotals(order);

                if (order.OrderTotal <= 0)
                    order.PaymentStatus = PaymentStatus.Refunded;

                _orderService.UpdateOrder(order);

                var model = new OrderModel();
                PrepareOrderDetailsModel(model, order);

                //selected tab
                SaveSelectedTabIndex(persistForTheNextRequest: false);

                return View(model);
            }
        }

        [HttpPost, ActionName("Edit")]
        [FormValueRequired(FormValueRequirement.StartsWith, "btnResetDownloadCount")]
        [ValidateInput(false)]
        public ActionResult ResetDownloadCount(int id, FormCollection form)
        {
            if (!_permissionService.Authorize(StandardPermissionProvider.ManageOrders))
                return AccessDeniedView();

            var order = _orderService.GetOrderById(id);
            if (order == null)
                //No order found with the specified id
                return RedirectToAction("List");

            //get order item identifier
            int orderItemId = 0;
            foreach (var formValue in form.AllKeys)
                if (formValue.StartsWith("btnResetDownloadCount", StringComparison.InvariantCultureIgnoreCase))
                    orderItemId = Convert.ToInt32(formValue.Substring("btnResetDownloadCount".Length));

            var orderItem = order.OrderItems.FirstOrDefault(x => x.Id == orderItemId);
            if (orderItem == null)
                throw new ArgumentException("No order item found with the specified id");

            //ensure a vendor has access only to his products 
            if (_workContext.CurrentVendor != null && !HasAccessToOrderItem(orderItem))
                return RedirectToAction("List");

            orderItem.DownloadCount = 0;
            _orderService.UpdateOrder(order);

            var model = new OrderModel();
            PrepareOrderDetailsModel(model, order);

            //selected tab
            SaveSelectedTabIndex(persistForTheNextRequest: false);

            return View(model);
        }

        [HttpPost, ActionName("Edit")]
        [FormValueRequired(FormValueRequirement.StartsWith, "btnPvActivateDownload")]
        [ValidateInput(false)]
        public ActionResult ActivateDownloadItem(int id, FormCollection form)
        {
            if (!_permissionService.Authorize(StandardPermissionProvider.ManageOrders))
                return AccessDeniedView();

            var order = _orderService.GetOrderById(id);
            if (order == null)
                //No order found with the specified id
                return RedirectToAction("List");

            //get order item identifier
            int orderItemId = 0;
            foreach (var formValue in form.AllKeys)
                if (formValue.StartsWith("btnPvActivateDownload", StringComparison.InvariantCultureIgnoreCase))
                    orderItemId = Convert.ToInt32(formValue.Substring("btnPvActivateDownload".Length));

            var orderItem = order.OrderItems.FirstOrDefault(x => x.Id == orderItemId);
            if (orderItem == null)
                throw new ArgumentException("No order item found with the specified id");

            //ensure a vendor has access only to his products 
            if (_workContext.CurrentVendor != null && !HasAccessToOrderItem(orderItem))
                return RedirectToAction("List");

            orderItem.IsDownloadActivated = !orderItem.IsDownloadActivated;
            _orderService.UpdateOrder(order);

            var model = new OrderModel();
            PrepareOrderDetailsModel(model, order);

            //selected tab
            SaveSelectedTabIndex(persistForTheNextRequest: false);

            return View(model);
        }

        public ActionResult UploadLicenseFilePopup(int id, int orderItemId)
        {
            if (!_permissionService.Authorize(StandardPermissionProvider.ManageOrders))
                return AccessDeniedView();

            var order = _orderService.GetOrderById(id);
            if (order == null)
                //No order found with the specified id
                return RedirectToAction("List");

            var orderItem = order.OrderItems.FirstOrDefault(x => x.Id == orderItemId);
            if (orderItem == null)
                throw new ArgumentException("No order item found with the specified id");

            if (!orderItem.Product.IsDownload)
                throw new ArgumentException("Product is not downloadable");

            //ensure a vendor has access only to his products 
            if (_workContext.CurrentVendor != null && !HasAccessToOrderItem(orderItem))
                return RedirectToAction("List");

            var model = new OrderModel.UploadLicenseModel
            {
                LicenseDownloadId = orderItem.LicenseDownloadId.HasValue ? orderItem.LicenseDownloadId.Value : 0,
                OrderId = order.Id,
                OrderItemId = orderItem.Id
            };

            return View(model);
        }

        [HttpPost]
        [FormValueRequired("uploadlicense")]
        public ActionResult UploadLicenseFilePopup(string btnId, string formId, OrderModel.UploadLicenseModel model)
        {
            if (!_permissionService.Authorize(StandardPermissionProvider.ManageOrders))
                return AccessDeniedView();

            var order = _orderService.GetOrderById(model.OrderId);
            if (order == null)
                //No order found with the specified id
                return RedirectToAction("List");

            var orderItem = order.OrderItems.FirstOrDefault(x => x.Id == model.OrderItemId);
            if (orderItem == null)
                throw new ArgumentException("No order item found with the specified id");

            //ensure a vendor has access only to his products 
            if (_workContext.CurrentVendor != null && !HasAccessToOrderItem(orderItem))
                return RedirectToAction("List");

            //attach license
            if (model.LicenseDownloadId > 0)
                orderItem.LicenseDownloadId = model.LicenseDownloadId;
            else
                orderItem.LicenseDownloadId = null;
            _orderService.UpdateOrder(order);

            //success
            ViewBag.RefreshPage = true;
            ViewBag.btnId = btnId;
            ViewBag.formId = formId;

            return View(model);
        }

        [HttpPost, ActionName("UploadLicenseFilePopup")]
        [FormValueRequired("deletelicense")]
        public ActionResult DeleteLicenseFilePopup(string btnId, string formId, OrderModel.UploadLicenseModel model)
        {
            if (!_permissionService.Authorize(StandardPermissionProvider.ManageOrders))
                return AccessDeniedView();

            var order = _orderService.GetOrderById(model.OrderId);
            if (order == null)
                //No order found with the specified id
                return RedirectToAction("List");

            var orderItem = order.OrderItems.FirstOrDefault(x => x.Id == model.OrderItemId);
            if (orderItem == null)
                throw new ArgumentException("No order item found with the specified id");

            //ensure a vendor has access only to his products 
            if (_workContext.CurrentVendor != null && !HasAccessToOrderItem(orderItem))
                return RedirectToAction("List");

            //attach license
            orderItem.LicenseDownloadId = null;
            _orderService.UpdateOrder(order);

            //success
            ViewBag.RefreshPage = true;
            ViewBag.btnId = btnId;
            ViewBag.formId = formId;

            return View(model);
        }

        public ActionResult AddProductToOrder(int orderId)
        {
            if (!_permissionService.Authorize(StandardPermissionProvider.ManageOrders))
                return AccessDeniedView();

            //a vendor does not have access to this functionality
            if (_workContext.CurrentVendor != null)
                return RedirectToAction("Edit", "Order", new { id = orderId });

            var model = new OrderModel.AddOrderProductModel();
            model.OrderId = orderId;
            //categories
            model.AvailableCategories.Add(new SelectListItem { Text = _localizationService.GetResource("Admin.Common.All"), Value = "0" });
            var categories = _categoryService.GetAllCategories(showHidden: true);
            foreach (var c in categories)
                model.AvailableCategories.Add(new SelectListItem { Text = c.GetFormattedBreadCrumb(categories), Value = c.Id.ToString() });

            //manufacturers
            model.AvailableManufacturers.Add(new SelectListItem { Text = _localizationService.GetResource("Admin.Common.All"), Value = "0" });
            foreach (var m in _manufacturerService.GetAllManufacturers(showHidden: true))
                model.AvailableManufacturers.Add(new SelectListItem { Text = m.Name, Value = m.Id.ToString() });

            //product types
            model.AvailableProductTypes = ProductType.SimpleProduct.ToSelectList(false).ToList();
            model.AvailableProductTypes.Insert(0, new SelectListItem { Text = _localizationService.GetResource("Admin.Common.All"), Value = "0" });

            return View(model);
        }

        [HttpPost]
        public ActionResult AddProductToOrder(DataSourceRequest command, OrderModel.AddOrderProductModel model)
        {
            if (!_permissionService.Authorize(StandardPermissionProvider.ManageOrders))
                return AccessDeniedView();

            //a vendor does not have access to this functionality
            if (_workContext.CurrentVendor != null)
                return Content("");

            var gridModel = new DataSourceResult();
            var products = _productService.SearchProducts(categoryIds: new List<int> { model.SearchCategoryId },
                manufacturerId: model.SearchManufacturerId,
                productType: model.SearchProductTypeId > 0 ? (ProductType?)model.SearchProductTypeId : null,
                keywords: model.SearchProductName,
                pageIndex: command.Page - 1,
                pageSize: command.PageSize,
                showHidden: true);
            gridModel.Data = products.Select(x =>
            {
                var productModel = new OrderModel.AddOrderProductModel.ProductModel
                {
                    Id = x.Id,
                    Name = x.Name,
                    Sku = x.Sku,
                };

                return productModel;
            });
            gridModel.Total = products.TotalCount;

            return Json(gridModel);
        }

        public ActionResult AddProductToOrderDetails(int orderId, int productId)
        {
            if (!_permissionService.Authorize(StandardPermissionProvider.ManageOrders))
                return AccessDeniedView();

            //a vendor does not have access to this functionality
            if (_workContext.CurrentVendor != null)
                return RedirectToAction("Edit", "Order", new { id = orderId });

            var model = PrepareAddProductToOrderModel(orderId, productId);
            return View(model);
        }

        [HttpPost]
        public ActionResult AddProductToOrderDetails(int orderId, int productId, FormCollection form)
        {
            if (!_permissionService.Authorize(StandardPermissionProvider.ManageOrders))
                return AccessDeniedView();

            //a vendor does not have access to this functionality
            if (_workContext.CurrentVendor != null)
                return RedirectToAction("Edit", "Order", new { id = orderId });

            var order = _orderService.GetOrderById(orderId);
            var product = _productService.GetProductById(productId);
            //save order item

            //basic properties
            decimal unitPriceInclTax;
            decimal.TryParse(form["UnitPriceInclTax"], out unitPriceInclTax);
            decimal unitPriceExclTax;
            decimal.TryParse(form["UnitPriceExclTax"], out unitPriceExclTax);
            int quantity;
            int.TryParse(form["Quantity"], out quantity);
            decimal priceInclTax;
            decimal.TryParse(form["SubTotalInclTax"], out priceInclTax);
            decimal priceExclTax;
            decimal.TryParse(form["SubTotalExclTax"], out priceExclTax);

            //attributes
            //warnings
            var warnings = new List<string>();
            string attributesXml = "";

            #region Product attributes

            var attributes = _productAttributeService.GetProductAttributeMappingsByProductId(product.Id);
            foreach (var attribute in attributes)
            {
                string controlId = string.Format("product_attribute_{0}_{1}", attribute.ProductAttributeId, attribute.Id);
                switch (attribute.AttributeControlType)
                {
                    case AttributeControlType.DropdownList:
                    case AttributeControlType.RadioList:
                    case AttributeControlType.ColorSquares:
                        {
                            var ctrlAttributes = form[controlId];
                            if (!String.IsNullOrEmpty(ctrlAttributes))
                            {
                                int selectedAttributeId = int.Parse(ctrlAttributes);
                                if (selectedAttributeId > 0)
                                    attributesXml = _productAttributeParser.AddProductAttribute(attributesXml,
                                        attribute, selectedAttributeId.ToString());
                            }
                        }
                        break;
                    case AttributeControlType.Checkboxes:
                        {
                            var ctrlAttributes = form[controlId];
                            if (!String.IsNullOrEmpty(ctrlAttributes))
                            {
                                foreach (var item in ctrlAttributes.Split(new[] { ',' }, StringSplitOptions.RemoveEmptyEntries))
                                {
                                    int selectedAttributeId = int.Parse(item);
                                    if (selectedAttributeId > 0)
                                        attributesXml = _productAttributeParser.AddProductAttribute(attributesXml,
                                            attribute, selectedAttributeId.ToString());
                                }
                            }
                        }
                        break;
                    case AttributeControlType.ReadonlyCheckboxes:
                        {
                            //load read-only (already server-side selected) values
                            var attributeValues = _productAttributeService.GetProductAttributeValues(attribute.Id);
                            foreach (var selectedAttributeId in attributeValues
                                .Where(v => v.IsPreSelected)
                                .Select(v => v.Id)
                                .ToList())
                            {
                                attributesXml = _productAttributeParser.AddProductAttribute(attributesXml,
                                    attribute, selectedAttributeId.ToString());
                            }
                        }
                        break;
                    case AttributeControlType.TextBox:
                    case AttributeControlType.MultilineTextbox:
                        {
                            var ctrlAttributes = form[controlId];
                            if (!String.IsNullOrEmpty(ctrlAttributes))
                            {
                                string enteredText = ctrlAttributes.Trim();
                                attributesXml = _productAttributeParser.AddProductAttribute(attributesXml,
                                    attribute, enteredText);
                            }
                        }
                        break;
                    case AttributeControlType.Datepicker:
                        {
                            var day = form[controlId + "_day"];
                            var month = form[controlId + "_month"];
                            var year = form[controlId + "_year"];
                            DateTime? selectedDate = null;
                            try
                            {
                                selectedDate = new DateTime(Int32.Parse(year), Int32.Parse(month), Int32.Parse(day));
                            }
                            catch { }
                            if (selectedDate.HasValue)
                            {
                                attributesXml = _productAttributeParser.AddProductAttribute(attributesXml,
                                    attribute, selectedDate.Value.ToString("D"));
                            }
                        }
                        break;
                    case AttributeControlType.FileUpload:
                        {
                            var httpPostedFile = this.Request.Files[controlId];
                            if ((httpPostedFile != null) && (!String.IsNullOrEmpty(httpPostedFile.FileName)))
                            {
                                var fileSizeOk = true;
                                if (attribute.ValidationFileMaximumSize.HasValue)
                                {
                                    //compare in bytes
                                    var maxFileSizeBytes = attribute.ValidationFileMaximumSize.Value * 1024;
                                    if (httpPostedFile.ContentLength > maxFileSizeBytes)
                                    {
                                        warnings.Add(string.Format(_localizationService.GetResource("ShoppingCart.MaximumUploadedFileSize"), attribute.ValidationFileMaximumSize.Value));
                                        fileSizeOk = false;
                                    }
                                }
                                if (fileSizeOk)
                                {
                                    //save an uploaded file
                                    var download = new Download
                                    {
                                        DownloadGuid = Guid.NewGuid(),
                                        UseDownloadUrl = false,
                                        DownloadUrl = "",
                                        DownloadBinary = httpPostedFile.GetDownloadBits(),
                                        ContentType = httpPostedFile.ContentType,
                                        Filename = Path.GetFileNameWithoutExtension(httpPostedFile.FileName),
                                        Extension = Path.GetExtension(httpPostedFile.FileName),
                                        IsNew = true
                                    };
                                    _downloadService.InsertDownload(download);
                                    //save attribute
                                    attributesXml = _productAttributeParser.AddProductAttribute(attributesXml,
                                        attribute, download.DownloadGuid.ToString());
                                }
                            }
                        }
                        break;
                    default:
                        break;
                }
            }

            #endregion

            #region Gift cards

            string recipientName = "";
            string recipientEmail = "";
            string senderName = "";
            string senderEmail = "";
            string giftCardMessage = "";
            if (product.IsGiftCard)
            {
                foreach (string formKey in form.AllKeys)
                {
                    if (formKey.Equals("giftcard.RecipientName", StringComparison.InvariantCultureIgnoreCase))
                    {
                        recipientName = form[formKey];
                        continue;
                    }
                    if (formKey.Equals("giftcard.RecipientEmail", StringComparison.InvariantCultureIgnoreCase))
                    {
                        recipientEmail = form[formKey];
                        continue;
                    }
                    if (formKey.Equals("giftcard.SenderName", StringComparison.InvariantCultureIgnoreCase))
                    {
                        senderName = form[formKey];
                        continue;
                    }
                    if (formKey.Equals("giftcard.SenderEmail", StringComparison.InvariantCultureIgnoreCase))
                    {
                        senderEmail = form[formKey];
                        continue;
                    }
                    if (formKey.Equals("giftcard.Message", StringComparison.InvariantCultureIgnoreCase))
                    {
                        giftCardMessage = form[formKey];
                        continue;
                    }
                }

                attributesXml = _productAttributeParser.AddGiftCardAttribute(attributesXml,
                    recipientName, recipientEmail, senderName, senderEmail, giftCardMessage);
            }

            #endregion

            #region Rental product

            DateTime? rentalStartDate = null;
            DateTime? rentalEndDate = null;
            if (product.IsRental)
            {
                var ctrlStartDate = form["rental_start_date"];
                var ctrlEndDate = form["rental_end_date"];
                try
                {
                    //currenly we support only this format (as in the \Views\Order\_ProductAddRentalInfo.cshtml file)
                    const string datePickerFormat = "MM/dd/yyyy";
                    rentalStartDate = DateTime.ParseExact(ctrlStartDate, datePickerFormat, CultureInfo.InvariantCulture);
                    rentalEndDate = DateTime.ParseExact(ctrlEndDate, datePickerFormat, CultureInfo.InvariantCulture);
                }
                catch
                {
                }
            }

            #endregion

            //warnings
            warnings.AddRange(_shoppingCartService.GetShoppingCartItemAttributeWarnings(order.Customer, ShoppingCartType.ShoppingCart, product, quantity, attributesXml));
            warnings.AddRange(_shoppingCartService.GetShoppingCartItemGiftCardWarnings(ShoppingCartType.ShoppingCart, product, attributesXml));
            warnings.AddRange(_shoppingCartService.GetRentalProductWarnings(product, rentalStartDate, rentalEndDate));
            if (warnings.Count == 0)
            {
                //no errors

                //attributes
                string attributeDescription = _productAttributeFormatter.FormatAttributes(product, attributesXml, order.Customer);

                ////Added by Prachi On 28 June, 2016 - Changes to save "SelectedWarehouse" while adding new product to existing order
                //int warehouseId = 0;
                //if (product.ManageInventoryMethod == ManageInventoryMethod.ManageStock)
                //{
                //    if (product.UseMultipleWarehouses)
                //    {
                //        var inventory = product.ProductWarehouseInventory.FirstOrDefault(pwi => pwi.Warehouse.Name.Contains("ESP"));
                //        if (inventory == null)
                //        {
                //            inventory = product.ProductWarehouseInventory.FirstOrDefault();
                //        }
                //        warehouseId = inventory.WarehouseId;
                //    }
                //    else
                //        warehouseId = product.WarehouseId;
                //}
                ////End - 28 June, 2016

                var guid = Guid.NewGuid();

                //save item
                var orderItem = new OrderItem
                {
                    OrderItemGuid = guid,
                    Order = order,
                    ProductId = product.Id,
                    UnitPriceInclTax = unitPriceInclTax,
                    UnitPriceExclTax = unitPriceExclTax,
                    PriceInclTax = priceInclTax,
                    PriceExclTax = priceExclTax,
                    OriginalProductCost = _priceCalculationService.GetProductCost(product, attributesXml),
                    AttributeDescription = attributeDescription,
                    AttributesXml = attributesXml,
                    Quantity = quantity,
                    DiscountAmountInclTax = decimal.Zero,
                    DiscountAmountExclTax = decimal.Zero,
                    DownloadCount = 0,
                    IsDownloadActivated = false,
                    LicenseDownloadId = 0,
                    RentalStartDateUtc = rentalStartDate,
                    RentalEndDateUtc = rentalEndDate,
                    ItemWeight = product.Weight
                };
                order.OrderItems.Add(orderItem);

            
                CalculateOrderTotals(order);
             
                _orderService.UpdateOrder(order);

                //Added by Prachi On 28 June, 2016
                var oi = _orderService.GetOrderItemByGuid(guid);
                int warehouseId = UpdateStock(oi.Id, quantity);
                oi.SelectedWarehouse = warehouseId;
                _orderService.UpdateOrder(order);

                //add a note
                order.OrderNotes.Add(new OrderNote
                {
                    //Changed By faisal On 26 May, 2016
                    Note = "A new order item \"" + orderItem.Product.Name + "\" has been added by " + _workContext.CurrentCustomer.GetFullName(),
                    DisplayToCustomer = false,
                    CreatedOnUtc = DateTime.UtcNow
                });
                _orderService.UpdateOrder(order);

                //gift cards
                if (product.IsGiftCard)
                {
                    for (int i = 0; i < orderItem.Quantity; i++)
                    {
                        var gc = new GiftCard
                        {
                            GiftCardType = product.GiftCardType,
                            PurchasedWithOrderItem = orderItem,
                            Amount = unitPriceExclTax,
                            IsGiftCardActivated = false,
                            GiftCardCouponCode = _giftCardService.GenerateGiftCardCode(),
                            RecipientName = recipientName,
                            RecipientEmail = recipientEmail,
                            SenderName = senderName,
                            SenderEmail = senderEmail,
                            Message = giftCardMessage,
                            IsRecipientNotified = false,
                            CreatedOnUtc = DateTime.UtcNow
                        };
                        _giftCardService.InsertGiftCard(gc);
                    }
                }

                //redirect to order details page
                return RedirectToAction("Edit", "Order", new { id = order.Id });
            }

            //errors
            var model = PrepareAddProductToOrderModel(order.Id, product.Id);
            model.Warnings.AddRange(warnings);
            return View(model);
        }

        #endregion

        #endregion

        #region Addresses

        public ActionResult AddressEdit(int addressId, int orderId)
        {
            if (!_permissionService.Authorize(StandardPermissionProvider.ManageOrders))
                return AccessDeniedView();

            var order = _orderService.GetOrderById(orderId);
            if (order == null)
                //No order found with the specified id
                return RedirectToAction("List");

            //a vendor does not have access to this functionality
            if (_workContext.CurrentVendor != null)
                return RedirectToAction("Edit", "Order", new { id = orderId });

            var address = _addressService.GetAddressById(addressId);
            if (address == null)
                throw new ArgumentException("No address found with the specified id", "addressId");

            var model = new OrderAddressModel();
            model.OrderId = orderId;
            model.Address = address.ToModel();
            model.Address.FirstNameEnabled = true;
            model.Address.FirstNameRequired = true;
            model.Address.LastNameEnabled = true;
            model.Address.LastNameRequired = true;
            model.Address.EmailEnabled = true;
            model.Address.EmailRequired = true;
            model.Address.CompanyEnabled = _addressSettings.CompanyEnabled;
            model.Address.CompanyRequired = _addressSettings.CompanyRequired;
            model.Address.CountryEnabled = _addressSettings.CountryEnabled;
            model.Address.StateProvinceEnabled = _addressSettings.StateProvinceEnabled;
            model.Address.CityEnabled = _addressSettings.CityEnabled;
            model.Address.CityRequired = _addressSettings.CityRequired;
            model.Address.StreetAddressEnabled = _addressSettings.StreetAddressEnabled;
            model.Address.StreetAddressRequired = _addressSettings.StreetAddressRequired;
            model.Address.StreetAddress2Enabled = _addressSettings.StreetAddress2Enabled;
            model.Address.StreetAddress2Required = _addressSettings.StreetAddress2Required;
            model.Address.ZipPostalCodeEnabled = _addressSettings.ZipPostalCodeEnabled;
            model.Address.ZipPostalCodeRequired = _addressSettings.ZipPostalCodeRequired;
            model.Address.PhoneEnabled = _addressSettings.PhoneEnabled;
            model.Address.PhoneRequired = _addressSettings.PhoneRequired;
            model.Address.FaxEnabled = _addressSettings.FaxEnabled;
            model.Address.FaxRequired = _addressSettings.FaxRequired;

            //countries
            model.Address.AvailableCountries.Add(new SelectListItem { Text = _localizationService.GetResource("Admin.Address.SelectCountry"), Value = "0" });
            foreach (var c in _countryService.GetAllCountries(true))
                model.Address.AvailableCountries.Add(new SelectListItem { Text = c.Name, Value = c.Id.ToString(), Selected = (c.Id == address.CountryId) });
            //states
            var states = address.Country != null ? _stateProvinceService.GetStateProvincesByCountryId(address.Country.Id, true).ToList() : new List<StateProvince>();
            if (states.Count > 0)
            {
                foreach (var s in states)
                    model.Address.AvailableStates.Add(new SelectListItem { Text = s.Name, Value = s.Id.ToString(), Selected = (s.Id == address.StateProvinceId) });
            }
            else
                model.Address.AvailableStates.Add(new SelectListItem { Text = _localizationService.GetResource("Admin.Address.OtherNonUS"), Value = "0" });
            //customer attribute services
            model.Address.PrepareCustomAddressAttributes(address, _addressAttributeService, _addressAttributeParser);

            return View(model);
        }

        [HttpPost]
        [ValidateInput(false)]
        public ActionResult AddressEdit(OrderAddressModel model, FormCollection form)
        {
            if (!_permissionService.Authorize(StandardPermissionProvider.ManageOrders))
                return AccessDeniedView();

            var order = _orderService.GetOrderById(model.OrderId);
            if (order == null)
                //No order found with the specified id
                return RedirectToAction("List");

            //a vendor does not have access to this functionality
            if (_workContext.CurrentVendor != null)
                return RedirectToAction("Edit", "Order", new { id = order.Id });

            //Changed By Prachi on 29 June, 2016
            var originalAddress = _addressService.GetAddressById(model.Address.Id);
            if (originalAddress == null)
                throw new ArgumentException("No address found with the specified id");
            //End 29 June, 2016

            //custom address attributes
            var customAttributes = form.ParseCustomAddressAttributes(_addressAttributeParser, _addressAttributeService);
            var customAttributeWarnings = _addressAttributeParser.GetAttributeWarnings(customAttributes);
            foreach (var error in customAttributeWarnings)
            {
                ModelState.AddModelError("", error);
            }

            if (ModelState.IsValid)
            {
                var address = model.Address.ToEntity();
                //Added By Prachi on 30 June, 2016
                int stateId = (int)(model.Address.StateProvinceId == null ? 0 : model.Address.StateProvinceId);
                int countryId = (int)(model.Address.CountryId == null ? 0 : model.Address.CountryId);
                address.StateProvince = _stateProvinceService.GetStateProvinceById(stateId);
                address.Country = _countryService.GetCountryById(countryId);
                address.CreatedOnUtc = originalAddress.CreatedOnUtc;
                //End 30 June, 2016
                address.CustomAttributes = customAttributes;
                _addressService.UpdateAddress(address);
                

                //Added By Prachi on 29 June, 2016
                if (order.ShippingAddressId == model.Address.Id && (order.ShippingStatus != ShippingStatus.Shipped || order.ShippingStatus != ShippingStatus.Delivered))
                {
                    IList<ShoppingCartItem> shoppingCartItems = order.Customer.ShoppingCartItems.ToList();
                    for (int i = 0; i < shoppingCartItems.Count; i++)
                    {
                        _shoppingCartService.DeleteShoppingCartItem(shoppingCartItems[i]);
                    }
                    foreach (var orderItem in order.OrderItems)
                    {
                        _shoppingCartService.AddToCart(order.Customer,
                            orderItem.Product, ShoppingCartType.ShoppingCart, order.StoreId,
                            orderItem.AttributesXml, 0, null, null, orderItem.Quantity);
                    }

                    var cart = order.Customer.ShoppingCartItems
                        .Where(sci => sci.ShoppingCartType == ShoppingCartType.ShoppingCart)
                        .LimitPerStore(order.StoreId)
                        .ToList();

                    Discount appliedDiscount = null;
                    GetShippingOptionResponse getShippingOptionResponse = _shippingService
                     .GetShippingOptions(cart, address, order.ShippingRateComputationMethodSystemName,
                    order.StoreId);

                    decimal rate = 0;
                    string price = "$0.00";
                    if (getShippingOptionResponse.Success)
                    {
                        foreach (var shippingOption in getShippingOptionResponse.ShippingOptions)
                        {
                            if (shippingOption.Name.ToLower() == order.ShippingMethod.ToLower())
                            {
                                var shippingTotal = _orderTotalCalculationService.AdjustShippingRate(
                                             shippingOption.Rate, cart, out appliedDiscount,
                                             shippingOption.Name + "~" + shippingOption.ShippingRateComputationMethodSystemName);

                                decimal rateBase = _taxService.GetShippingPrice(shippingTotal, _workContext.CurrentCustomer);
                                rate = _currencyService.ConvertFromPrimaryStoreCurrency(rateBase, _workContext.WorkingCurrency);
                                if (rate > 0)
                                {
                                    price = _priceFormatter.FormatPrice(rate);
                                }
                                else
                                    price = "$0.00";
                            }
                        }
                    }

                    if (rate != order.OrderShippingExclTax)
                    {
                        decimal diff = order.OrderShippingExclTax - rate;
                        order.OrderNotes.Add(new OrderNote
                        {
                            Note = "Order Shipping Amount has been changed from " + _priceFormatter.FormatPrice(order.OrderShippingExclTax) + " to " + price,
                            DisplayToCustomer = false,
                            CreatedOnUtc = DateTime.UtcNow
                        });

                        order.OrderTotal -= order.OrderShippingInclTax;

                        decimal shippingTax = order.OrderShippingInclTax - order.OrderShippingExclTax;

                        order.OrderShippingExclTax = rate;
                        order.OrderShippingInclTax = rate + shippingTax;
                        order.OrderTotal += rate + shippingTax;

                        _orderService.UpdateOrder(order);
                    }
                    //for (int i = 0; i < shoppingCartItems.Count; i++)
                    //{
                    //    _shoppingCartService.DeleteShoppingCartItem(shoppingCartItems[i], false);
                    //}
                }
                //End 29 June, 2016

                //Added By Prachi on 30 June, 2016
                StringBuilder sbOriginalAddress = new StringBuilder();
                sbOriginalAddress.Append(originalAddress.Company).Append(CheckNullOrEmpty(originalAddress.FirstName)).Append(" ").Append(originalAddress.LastName)
                    .Append(CheckNullOrEmpty(originalAddress.Email)).Append(CheckNullOrEmpty(originalAddress.Address1)).Append(CheckNullOrEmpty(originalAddress.Address2))
                    .Append(CheckNullOrEmpty(originalAddress.City)).Append((originalAddress.StateProvince != null ? CheckNullOrEmpty(originalAddress.StateProvince.Name) : string.Empty))
                        .Append((originalAddress.Country != null ? CheckNullOrEmpty(originalAddress.Country.Name) : string.Empty)).Append(CheckNullOrEmpty(originalAddress.ZipPostalCode))
                    .Append(CheckNullOrEmpty(originalAddress.PhoneNumber)).Append(CheckNullOrEmpty(originalAddress.FaxNumber));

                StringBuilder sbAddress = new StringBuilder();
                sbAddress.Append(address.Company).Append(CheckNullOrEmpty(address.FirstName)).Append(" ").Append(address.LastName)
                  .Append(CheckNullOrEmpty(address.Email)).Append(CheckNullOrEmpty(address.Address1)).Append(CheckNullOrEmpty(address.Address2))
                  .Append(CheckNullOrEmpty(address.City)).Append((address.StateProvince != null ? CheckNullOrEmpty(address.StateProvince.Name) : string.Empty))
                      .Append((address.Country != null ? CheckNullOrEmpty(address.Country.Name) : string.Empty)).Append(CheckNullOrEmpty(address.ZipPostalCode))
                  .Append(CheckNullOrEmpty(address.PhoneNumber)).Append(CheckNullOrEmpty(address.FaxNumber));

                StringBuilder sb = new StringBuilder();
                if (order.ShippingAddressId == address.Id)
                {
                    order.ShippingAddress.FirstName = address.FirstName;
                    order.ShippingAddress.LastName = address.LastName;
                    order.ShippingAddress.Email = address.Email;
                    order.ShippingAddress.Company = address.Company;
                    order.ShippingAddress.Country = address.Country;
                    order.ShippingAddress.CountryId = address.CountryId;
                    order.ShippingAddress.StateProvince = address.StateProvince;
                    order.ShippingAddress.StateProvinceId = address.StateProvinceId;
                    order.ShippingAddress.City = address.City;
                    order.ShippingAddress.Address1 = address.Address1;
                    order.ShippingAddress.Address2 = address.Address2;
                    order.ShippingAddress.ZipPostalCode = address.ZipPostalCode;
                    order.ShippingAddress.PhoneNumber = address.PhoneNumber;
                    order.ShippingAddress.FaxNumber = address.FaxNumber;
                    order.ShippingAddress.CustomAttributes = address.CustomAttributes;
                    
                    sb.Append("Shipping Address has been changed.").Append(Environment.NewLine).Append("Previous Address: " + sbOriginalAddress.ToString()).
                       Append(Environment.NewLine).Append("New Address: " + sbAddress.ToString());
                }
                else if (order.BillingAddressId == address.Id)
                {
                    order.BillingAddress.FirstName = address.FirstName;
                    order.BillingAddress.LastName = address.LastName;
                    order.BillingAddress.Email = address.Email;
                    order.BillingAddress.Company = address.Company;
                    order.BillingAddress.Country = address.Country;
                    order.BillingAddress.CountryId = address.CountryId;
                    order.BillingAddress.StateProvince = address.StateProvince;
                    order.BillingAddress.StateProvinceId = address.StateProvinceId;
                    order.BillingAddress.City = address.City;
                    order.BillingAddress.Address1 = address.Address1;
                    order.BillingAddress.Address2 = address.Address2;
                    order.BillingAddress.ZipPostalCode = address.ZipPostalCode;
                    order.BillingAddress.PhoneNumber = address.PhoneNumber;
                    order.BillingAddress.FaxNumber = address.FaxNumber;
                    order.BillingAddress.CustomAttributes = address.CustomAttributes;

                    sb.Append("Billing Address has been changed.").Append(Environment.NewLine).Append("Previous Address: " + sbOriginalAddress.ToString()).
                      Append(Environment.NewLine).Append("New Address: " + sbAddress.ToString());
                }
                //End 30 June, 2016

                //add a note
                order.OrderNotes.Add(new OrderNote
                {
                    //Added By Prachi on 30 June, 2016
                    Note = sb.ToString(),
                    DisplayToCustomer = false,
                    CreatedOnUtc = DateTime.UtcNow
                });
                _orderService.UpdateOrder(order);

                //Changes by Prachi on 31 March, 2016 - Success Notification added 
                SuccessNotification("Address has been updated.");
                return RedirectToAction("AddressEdit", new { addressId = model.Address.Id, orderId = model.OrderId });
            }

            //If we got this far, something failed, redisplay form
            //Changed By Prachi on 30 June, 2016
            model.OrderId = order.Id;
            model.Address = originalAddress.ToModel();
            model.Address.FirstNameEnabled = true;
            model.Address.FirstNameRequired = true;
            model.Address.LastNameEnabled = true;
            model.Address.LastNameRequired = true;
            model.Address.EmailEnabled = true;
            model.Address.EmailRequired = true;
            model.Address.CompanyEnabled = _addressSettings.CompanyEnabled;
            model.Address.CompanyRequired = _addressSettings.CompanyRequired;
            model.Address.CountryEnabled = _addressSettings.CountryEnabled;
            model.Address.StateProvinceEnabled = _addressSettings.StateProvinceEnabled;
            model.Address.CityEnabled = _addressSettings.CityEnabled;
            model.Address.CityRequired = _addressSettings.CityRequired;
            model.Address.StreetAddressEnabled = _addressSettings.StreetAddressEnabled;
            model.Address.StreetAddressRequired = _addressSettings.StreetAddressRequired;
            model.Address.StreetAddress2Enabled = _addressSettings.StreetAddress2Enabled;
            model.Address.StreetAddress2Required = _addressSettings.StreetAddress2Required;
            model.Address.ZipPostalCodeEnabled = _addressSettings.ZipPostalCodeEnabled;
            model.Address.ZipPostalCodeRequired = _addressSettings.ZipPostalCodeRequired;
            model.Address.PhoneEnabled = _addressSettings.PhoneEnabled;
            model.Address.PhoneRequired = _addressSettings.PhoneRequired;
            model.Address.FaxEnabled = _addressSettings.FaxEnabled;
            model.Address.FaxRequired = _addressSettings.FaxRequired;
            //countries
            model.Address.AvailableCountries.Add(new SelectListItem { Text = _localizationService.GetResource("Admin.Address.SelectCountry"), Value = "0" });
            foreach (var c in _countryService.GetAllCountries(true))
                model.Address.AvailableCountries.Add(new SelectListItem { Text = c.Name, Value = c.Id.ToString(), Selected = (c.Id == originalAddress.CountryId) });
            //states
            var states = originalAddress.Country != null ? _stateProvinceService.GetStateProvincesByCountryId(originalAddress.Country.Id, true).ToList() : new List<StateProvince>();
            if (states.Count > 0)
            {
                foreach (var s in states)
                    model.Address.AvailableStates.Add(new SelectListItem { Text = s.Name, Value = s.Id.ToString(), Selected = (s.Id == originalAddress.StateProvinceId) });
            }
            else
                model.Address.AvailableStates.Add(new SelectListItem { Text = _localizationService.GetResource("Admin.Address.OtherNonUS"), Value = "0" });
            //customer attribute services
            model.Address.PrepareCustomAddressAttributes(originalAddress, _addressAttributeService, _addressAttributeParser);
            //End 30 June, 2016

            return View(model);
        }

        #endregion

        #region Shipments

        public ActionResult ShipmentList()
        {
            if (!_permissionService.Authorize(StandardPermissionProvider.ManageOrders))
                return AccessDeniedView();

            var model = new ShipmentListModel();
            //countries
            model.AvailableCountries.Add(new SelectListItem { Text = "*", Value = "0" });
            foreach (var c in _countryService.GetAllCountries(true))
                model.AvailableCountries.Add(new SelectListItem { Text = c.Name, Value = c.Id.ToString() });
            //states
            model.AvailableStates.Add(new SelectListItem { Text = "*", Value = "0" });

            //warehouses
            model.AvailableWarehouses.Add(new SelectListItem { Text = _localizationService.GetResource("Admin.Common.All"), Value = "0" });
            foreach (var w in _shippingService.GetAllWarehouses())
                model.AvailableWarehouses.Add(new SelectListItem { Text = w.Name, Value = w.Id.ToString() });

            return View(model);
        }

        [HttpPost]
        public ActionResult ShipmentListSelect(DataSourceRequest command, ShipmentListModel model)
        {
            if (!_permissionService.Authorize(StandardPermissionProvider.ManageOrders))
                return AccessDeniedView();

            DateTime? startDateValue = (model.StartDate == null) ? null
                            : (DateTime?)_dateTimeHelper.ConvertToUtcTime(model.StartDate.Value, _dateTimeHelper.CurrentTimeZone);

            DateTime? endDateValue = (model.EndDate == null) ? null
                            : (DateTime?)_dateTimeHelper.ConvertToUtcTime(model.EndDate.Value, _dateTimeHelper.CurrentTimeZone).AddDays(1);

            //a vendor should have access only to his products
            int vendorId = 0;
            if (_workContext.CurrentVendor != null)
                vendorId = _workContext.CurrentVendor.Id;

            //load shipments
            var shipments = _shipmentService.GetAllShipments(vendorId: vendorId,
                warehouseId: model.WarehouseId,
                shippingCountryId: model.CountryId,
                shippingStateId: model.StateProvinceId,
                shippingCity: model.City,
                trackingNumber: model.TrackingNumber,
                loadNotShipped: model.LoadNotShipped,
                createdFromUtc: startDateValue,
                createdToUtc: endDateValue,
                pageIndex: command.Page - 1,
                pageSize: command.PageSize);
            var gridModel = new DataSourceResult
            {
                Data = shipments.Select(shipment => PrepareShipmentModel(shipment, false)),
                Total = shipments.TotalCount
            };
            return new JsonResult
            {
                Data = gridModel
            };
        }

        [HttpPost]
        public ActionResult ShipmentsByOrder(int orderId, DataSourceRequest command)
        {
            if (!_permissionService.Authorize(StandardPermissionProvider.ManageOrders))
                return AccessDeniedView();

            var order = _orderService.GetOrderById(orderId);
            if (order == null)
                throw new ArgumentException("No order found with the specified id");

            //a vendor should have access only to his products
            if (_workContext.CurrentVendor != null && !HasAccessToOrder(order))
                return Content("");

            //shipments
            var shipmentModels = new List<ShipmentModel>();
            var shipments = order.Shipments
                //a vendor should have access only to his products
                .Where(s => _workContext.CurrentVendor == null || HasAccessToShipment(s))
                .OrderBy(s => s.CreatedOnUtc)
                .ToList();
            foreach (var shipment in shipments)
                shipmentModels.Add(PrepareShipmentModel(shipment, false));

            var gridModel = new DataSourceResult
            {
                Data = shipmentModels,
                Total = shipmentModels.Count
            };


            return Json(gridModel);
        }

        [HttpPost]
        public ActionResult ShipmentsItemsByShipmentId(int shipmentId, DataSourceRequest command)
        {
            if (!_permissionService.Authorize(StandardPermissionProvider.ManageOrders))
                return AccessDeniedView();

            var shipment = _shipmentService.GetShipmentById(shipmentId);
            if (shipment == null)
                throw new ArgumentException("No shipment found with the specified id");

            //a vendor should have access only to his products
            if (_workContext.CurrentVendor != null && !HasAccessToShipment(shipment))
                return Content("");

            var order = _orderService.GetOrderById(shipment.OrderId);
            if (order == null)
                throw new ArgumentException("No order found with the specified id");

            //a vendor should have access only to his products
            if (_workContext.CurrentVendor != null && !HasAccessToOrder(order))
                return Content("");

            //shipments
            var shipmentModel = PrepareShipmentModel(shipment, true);
            var gridModel = new DataSourceResult
            {
                Data = shipmentModel.Items,
                Total = shipmentModel.Items.Count
            };

            return Json(gridModel);
        }

        public ActionResult AddShipment(int orderId)
        {
            if (!_permissionService.Authorize(StandardPermissionProvider.ManageOrders))
                return AccessDeniedView();

            var order = _orderService.GetOrderById(orderId);
            if (order == null)
                //No order found with the specified id
                return RedirectToAction("List");

            //a vendor should have access only to his products
            if (_workContext.CurrentVendor != null && !HasAccessToOrder(order))
                return RedirectToAction("List");

            var model = new ShipmentModel
            {
                OrderId = order.Id,
            };

            //measures
            var baseWeight = _measureService.GetMeasureWeightById(_measureSettings.BaseWeightId);
            var baseWeightIn = baseWeight != null ? baseWeight.Name : "";
            var baseDimension = _measureService.GetMeasureDimensionById(_measureSettings.BaseDimensionId);
            var baseDimensionIn = baseDimension != null ? baseDimension.Name : "";

            var orderItems = order.OrderItems;
            //a vendor should have access only to his products
            if (_workContext.CurrentVendor != null)
            {
                orderItems = orderItems.Where(HasAccessToOrderItem).ToList();
            }

            foreach (var orderItem in orderItems)
            {
                //we can ship only shippable products
                if (!orderItem.Product.IsShipEnabled)
                    continue;

                //quantities
                var qtyInThisShipment = 0;
                var maxQtyToAdd = orderItem.GetTotalNumberOfItemsCanBeAddedToShipment();
                var qtyOrdered = orderItem.Quantity;
                var qtyInAllShipments = orderItem.GetTotalNumberOfItemsInAllShipment();

                //ensure that this product can be added to a shipment
                if (maxQtyToAdd <= 0)
                    continue;

                var shipmentItemModel = new ShipmentModel.ShipmentItemModel
                {
                    OrderItemId = orderItem.Id,
                    ProductId = orderItem.ProductId,
                    ProductName = orderItem.Product.Name,
                    Sku = orderItem.Product.FormatSku(orderItem.AttributesXml, _productAttributeParser),
                    AttributeInfo = orderItem.AttributeDescription,
                    ShipSeparately = orderItem.Product.ShipSeparately,
                    ItemWeight = orderItem.ItemWeight.HasValue ? string.Format("{0:F2} [{1}]", orderItem.ItemWeight, baseWeightIn) : "",
                    ItemDimensions = string.Format("{0:F2} x {1:F2} x {2:F2} [{3}]", orderItem.Product.Length, orderItem.Product.Width, orderItem.Product.Height, baseDimensionIn),
                    QuantityOrdered = qtyOrdered,
                    QuantityInThisShipment = qtyInThisShipment,
                    QuantityInAllShipments = qtyInAllShipments,
                    QuantityToAdd = maxQtyToAdd,
                    SelectedWareHouse = orderItem.SelectedWarehouse,
                    //Changed By Prachi on 30 June, 2016
                    IsQuantityAvailable = IsQuantityAvailable(orderItem.Quantity, orderItem.GetTotalNumberOfShippedItems(), orderItem.Product, orderItem.Id)
                };
                //rental info
                if (orderItem.Product.IsRental)
                {
                    var rentalStartDate = orderItem.RentalStartDateUtc.HasValue ? orderItem.Product.FormatRentalDate(orderItem.RentalStartDateUtc.Value) : "";
                    var rentalEndDate = orderItem.RentalEndDateUtc.HasValue ? orderItem.Product.FormatRentalDate(orderItem.RentalEndDateUtc.Value) : "";
                    shipmentItemModel.RentalInfo = string.Format(_localizationService.GetResource("Order.Rental.FormattedDate"),
                        rentalStartDate, rentalEndDate);
                }

                if (orderItem.Product.ManageInventoryMethod == ManageInventoryMethod.ManageStock &&
                    orderItem.Product.UseMultipleWarehouses)
                {
                    //multiple warehouses supported
                    shipmentItemModel.AllowToChooseWarehouse = true;
                    foreach (var pwi in orderItem.Product.ProductWarehouseInventory
                        .OrderBy(w => w.WarehouseId).ToList())
                    {
                        var warehouse = pwi.Warehouse;
                        if (warehouse != null)
                        {
                            //Added By Faisal on 29 Jun, 2016
                            var pwiadju = orderItem.Product.ProductWarehouseInventory_OpeningAdjustment.FirstOrDefault(x => x.WarehouseId == warehouse.Id);
                            if (pwiadju != null)
                                pwi.StockQuantity += pwiadju.AdjustmentQuantity;
                            //End 29 Jun, 2016

                            shipmentItemModel.AvailableWarehouses.Add(new ShipmentModel.ShipmentItemModel.WarehouseInfo
                            {
                                WarehouseId = warehouse.Id,
                                WarehouseName = warehouse.Name,
                                StockQuantity = pwi.StockQuantity,
                                ReservedQuantity = pwi.ReservedQuantity,
                                PlannedQuantity = _shipmentService.GetQuantityInShipments(orderItem.Product, warehouse.Id, true, true)
                            });
                        }
                    }
                }
                else
                {
                    //multiple warehouses are not supported
                    var warehouse = _shippingService.GetWarehouseById(orderItem.Product.WarehouseId);
                    if (warehouse != null)
                    {
                        shipmentItemModel.AvailableWarehouses.Add(new ShipmentModel.ShipmentItemModel.WarehouseInfo
                        {
                            WarehouseId = warehouse.Id,
                            WarehouseName = warehouse.Name,
                            StockQuantity = orderItem.Product.StockQuantity
                        });
                    }
                }

                model.Items.Add(shipmentItemModel);
            }

            return View(model);
        }

        [HttpPost, ParameterBasedOnFormName("save-continue", "continueEditing")]
        [FormValueRequired("save", "save-continue")]
        public ActionResult AddShipment(int orderId, FormCollection form, bool continueEditing)
        {
            if (!_permissionService.Authorize(StandardPermissionProvider.ManageOrders))
                return AccessDeniedView();

            var order = _orderService.GetOrderById(orderId);
            if (order == null)
                //No order found with the specified id
                return RedirectToAction("List");

            //a vendor should have access only to his products
            if (_workContext.CurrentVendor != null && !HasAccessToOrder(order))
                return RedirectToAction("List");

            var orderItems = order.OrderItems;
            //a vendor should have access only to his products
            if (_workContext.CurrentVendor != null)
            {
                orderItems = orderItems.Where(HasAccessToOrderItem).ToList();
            }

            Shipment shipment = null;
            decimal? totalWeight = null;
            foreach (var orderItem in orderItems)
            {
                //is shippable
                if (!orderItem.Product.IsShipEnabled)
                    continue;

                //ensure that this product can be shipped (have at least one item to ship)
                var maxQtyToAdd = orderItem.GetTotalNumberOfItemsCanBeAddedToShipment();
                if (maxQtyToAdd <= 0)
                    continue;

                int qtyToAdd = 0; //parse quantity
                foreach (string formKey in form.AllKeys)
                    if (formKey.Equals(string.Format("qtyToAdd{0}", orderItem.Id), StringComparison.InvariantCultureIgnoreCase))
                    {
                        int.TryParse(form[formKey], out qtyToAdd);
                        break;
                    }

                int warehouseId = 0;
                //Added By Faisal on 11 Decc, 2015
                if (orderItem.SelectedWarehouse > 0)
                    warehouseId = orderItem.SelectedWarehouse;
                //End 11 Decc, 2015
                else if (orderItem.Product.ManageInventoryMethod == ManageInventoryMethod.ManageStock &&
                    orderItem.Product.UseMultipleWarehouses)
                {
                    //multiple warehouses supported
                    //warehouse is chosen by a store owner
                    foreach (string formKey in form.AllKeys)
                        if (formKey.Equals(string.Format("warehouse_{0}", orderItem.Id), StringComparison.InvariantCultureIgnoreCase))
                        {
                            int.TryParse(form[formKey], out warehouseId);
                            break;
                        }
                }
                else
                {
                    //multiple warehouses are not supported
                    warehouseId = orderItem.Product.WarehouseId;
                }

                foreach (string formKey in form.AllKeys)
                    if (formKey.Equals(string.Format("qtyToAdd{0}", orderItem.Id), StringComparison.InvariantCultureIgnoreCase))
                    {
                        int.TryParse(form[formKey], out qtyToAdd);
                        break;
                    }

                //validate quantity
                if (qtyToAdd <= 0)
                    continue;
                if (qtyToAdd > maxQtyToAdd)
                    qtyToAdd = maxQtyToAdd;

                //ok. we have at least one item. let's create a shipment (if it does not exist)

                var orderItemTotalWeight = orderItem.ItemWeight.HasValue ? orderItem.ItemWeight * qtyToAdd : null;
                if (orderItemTotalWeight.HasValue)
                {
                    if (!totalWeight.HasValue)
                        totalWeight = 0;
                    totalWeight += orderItemTotalWeight.Value;
                }
                if (shipment == null)
                {
                    var trackingNumber = form["TrackingNumber"];
                    DateTime? ActualDateShipped;
                    if (form["ActualDateShipped"] == "")
                        ActualDateShipped = null;
                    else
                        ActualDateShipped = Convert.ToDateTime(form["ActualDateShipped"]);

                    decimal ActualShipCost = (form["ActualShipCost"] == "" ? 0 : Convert.ToDecimal(form["ActualShipCost"]));
                    var adminComment = form["AdminComment"];
                    shipment = new Shipment
                    {
                        OrderId = order.Id,
                        TrackingNumber = trackingNumber,
                        ActualDateShipped = ActualDateShipped,
                        ActualShipCost = ActualShipCost,
                        TotalWeight = null,
                        ShippedDateUtc = null,
                        DeliveryDateUtc = null,
                        AdminComment = adminComment,
                        CreatedOnUtc = DateTime.UtcNow,
                    };
                }
                //create a shipment item
                var shipmentItem = new ShipmentItem
                {
                    OrderItemId = orderItem.Id,
                    Quantity = qtyToAdd,
                    WarehouseId = warehouseId
                };
                shipment.ShipmentItems.Add(shipmentItem);
            }

            //if we have at least one item in the shipment, then save it
            if (shipment != null && shipment.ShipmentItems.Count > 0)
            {
                shipment.TotalWeight = totalWeight;
                _shipmentService.InsertShipment(shipment);

                //add a note
                order.OrderNotes.Add(new OrderNote
                {
                    Note = "A shipment has been added",
                    DisplayToCustomer = false,
                    CreatedOnUtc = DateTime.UtcNow
                });
                _orderService.UpdateOrder(order);

                SuccessNotification(_localizationService.GetResource("Admin.Orders.Shipments.Added"));
                return continueEditing
                           ? RedirectToAction("ShipmentDetails", new { id = shipment.Id })
                           : RedirectToAction("Edit", new { id = orderId });
            }

            ErrorNotification(_localizationService.GetResource("Admin.Orders.Shipments.NoProductsSelected"));
            return RedirectToAction("AddShipment", new { orderId = orderId });
        }

        public ActionResult ShipmentDetails(int id)
        {
            if (!_permissionService.Authorize(StandardPermissionProvider.ManageOrders))
                return AccessDeniedView();

            var shipment = _shipmentService.GetShipmentById(id);
            if (shipment == null)
                //No shipment found with the specified id
                return RedirectToAction("List");

            //a vendor should have access only to his products
            if (_workContext.CurrentVendor != null && !HasAccessToShipment(shipment))
                return RedirectToAction("List");

            var model = PrepareShipmentModel(shipment, true, true);
            return View(model);
        }

        [HttpPost]
        public ActionResult DeleteShipment(int id)
        {
            if (!_permissionService.Authorize(StandardPermissionProvider.ManageOrders))
                return AccessDeniedView();

            var shipment = _shipmentService.GetShipmentById(id);
            if (shipment == null)
                //No shipment found with the specified id
                return RedirectToAction("List");

            //a vendor should have access only to his products
            if (_workContext.CurrentVendor != null && !HasAccessToShipment(shipment))
                return RedirectToAction("List");

            foreach (var shipmentItem in shipment.ShipmentItems)
            {
                var orderItem = _orderService.GetOrderItemById(shipmentItem.OrderItemId);
                if (orderItem == null)
                    continue;

                _productService.ReverseBookedInventory(orderItem.Product, shipmentItem);
            }

            var orderId = shipment.OrderId;
            _shipmentService.DeleteShipment(shipment);

            var order = _orderService.GetOrderById(shipment.OrderId);
            //add a note
            order.OrderNotes.Add(new OrderNote
            {
                Note = "A shipment has been deleted",
                DisplayToCustomer = false,
                CreatedOnUtc = DateTime.UtcNow
            });
            _orderService.UpdateOrder(order);

            SuccessNotification(_localizationService.GetResource("Admin.Orders.Shipments.Deleted"));
            return RedirectToAction("Edit", new { id = orderId });
        }

        [HttpPost, ActionName("ShipmentDetails")]
        [FormValueRequired("settrackingnumber")]
        public ActionResult SetTrackingNumber(ShipmentModel model)
        {
            if (!_permissionService.Authorize(StandardPermissionProvider.ManageOrders))
                return AccessDeniedView();

            var shipment = _shipmentService.GetShipmentById(model.Id);
            if (shipment == null)
                //No shipment found with the specified id
                return RedirectToAction("List");

            //a vendor should have access only to his products
            if (_workContext.CurrentVendor != null && !HasAccessToShipment(shipment))
                return RedirectToAction("List");

            shipment.TrackingNumber = model.TrackingNumber;
            _shipmentService.UpdateShipment(shipment);

            return RedirectToAction("ShipmentDetails", new { id = shipment.Id });
        }

        [HttpPost, ActionName("ShipmentDetails")]
        [FormValueRequired("setadmincomment")]
        public ActionResult SetShipmentAdminComment(ShipmentModel model)
        {
            if (!_permissionService.Authorize(StandardPermissionProvider.ManageOrders))
                return AccessDeniedView();

            var shipment = _shipmentService.GetShipmentById(model.Id);
            if (shipment == null)
                //No shipment found with the specified id
                return RedirectToAction("List");

            //a vendor should have access only to his products
            if (_workContext.CurrentVendor != null && !HasAccessToShipment(shipment))
                return RedirectToAction("List");

            shipment.AdminComment = model.AdminComment;
            _shipmentService.UpdateShipment(shipment);

            return RedirectToAction("ShipmentDetails", new { id = shipment.Id });
        }

        [HttpPost, ActionName("ShipmentDetails")]
        [FormValueRequired("setasshipped")]
        public ActionResult SetAsShipped(int id)
        {
            if (!_permissionService.Authorize(StandardPermissionProvider.ManageOrders))
                return AccessDeniedView();

            var shipment = _shipmentService.GetShipmentById(id);
            if (shipment == null)
                //No shipment found with the specified id
                return RedirectToAction("List");

            //a vendor should have access only to his products
            if (_workContext.CurrentVendor != null && !HasAccessToShipment(shipment))
                return RedirectToAction("List");

            try
            {

                _orderProcessingService.Ship(shipment, true);
                return RedirectToAction("ShipmentDetails", new { id = shipment.Id });
            }
            catch (Exception exc)
            {
                //error
                ErrorNotification(exc, true);
                return RedirectToAction("ShipmentDetails", new { id = shipment.Id });
            }
        }

        [HttpPost, ActionName("ShipmentDetails")]
        [FormValueRequired("saveshippeddate")]
        public ActionResult EditShippedDate(ShipmentModel model)
        {
            if (!_permissionService.Authorize(StandardPermissionProvider.ManageOrders))
                return AccessDeniedView();

            var shipment = _shipmentService.GetShipmentById(model.Id);
            if (shipment == null)
                //No shipment found with the specified id
                return RedirectToAction("List");

            //a vendor should have access only to his products
            if (_workContext.CurrentVendor != null && !HasAccessToShipment(shipment))
                return RedirectToAction("List");

            try
            {
                if (!model.ShippedDateUtc.HasValue)
                {
                    throw new Exception("Enter shipped date");
                }
                shipment.ShippedDateUtc = model.ShippedDateUtc;
                _shipmentService.UpdateShipment(shipment);
                return RedirectToAction("ShipmentDetails", new { id = shipment.Id });
            }
            catch (Exception exc)
            {
                //error
                ErrorNotification(exc, true);
                return RedirectToAction("ShipmentDetails", new { id = shipment.Id });
            }
        }

        [HttpPost, ActionName("ShipmentDetails")]
        [FormValueRequired("setasdelivered")]
        public ActionResult SetAsDelivered(int id)
        {
            if (!_permissionService.Authorize(StandardPermissionProvider.ManageOrders))
                return AccessDeniedView();

            var shipment = _shipmentService.GetShipmentById(id);
            if (shipment == null)
                //No shipment found with the specified id
                return RedirectToAction("List");

            //a vendor should have access only to his products
            if (_workContext.CurrentVendor != null && !HasAccessToShipment(shipment))
                return RedirectToAction("List");

            try
            {
                _orderProcessingService.Deliver(shipment, true);
                return RedirectToAction("ShipmentDetails", new { id = shipment.Id });
            }
            catch (Exception exc)
            {
                //error
                ErrorNotification(exc, true);
                return RedirectToAction("ShipmentDetails", new { id = shipment.Id });
            }
        }


        [HttpPost, ActionName("ShipmentDetails")]
        [FormValueRequired("savedeliverydate")]
        public ActionResult EditDeliveryDate(ShipmentModel model)
        {
            if (!_permissionService.Authorize(StandardPermissionProvider.ManageOrders))
                return AccessDeniedView();

            var shipment = _shipmentService.GetShipmentById(model.Id);
            if (shipment == null)
                //No shipment found with the specified id
                return RedirectToAction("List");

            //a vendor should have access only to his products
            if (_workContext.CurrentVendor != null && !HasAccessToShipment(shipment))
                return RedirectToAction("List");

            try
            {
                if (!model.DeliveryDateUtc.HasValue)
                {
                    throw new Exception("Enter delivery date");
                }
                shipment.DeliveryDateUtc = model.DeliveryDateUtc;
                _shipmentService.UpdateShipment(shipment);
                return RedirectToAction("ShipmentDetails", new { id = shipment.Id });
            }
            catch (Exception exc)
            {
                //error
                ErrorNotification(exc, true);
                return RedirectToAction("ShipmentDetails", new { id = shipment.Id });
            }
        }

        public ActionResult PdfPackagingSlip(int shipmentId)
        {
            if (!_permissionService.Authorize(StandardPermissionProvider.ManageOrders))
                return AccessDeniedView();

            var shipment = _shipmentService.GetShipmentById(shipmentId);
            if (shipment == null)
                //no shipment found with the specified id
                return RedirectToAction("List");

            //a vendor should have access only to his products
            if (_workContext.CurrentVendor != null && !HasAccessToShipment(shipment))
                return RedirectToAction("List");

            var shipments = new List<Shipment>();
            shipments.Add(shipment);

            byte[] bytes;
            using (var stream = new MemoryStream())
            {
                _pdfService.PrintPackagingSlipsToPdf(stream, shipments, _workContext.WorkingLanguage.Id);
                bytes = stream.ToArray();
            }
            return File(bytes, "application/pdf", string.Format("packagingslip_{0}.pdf", shipment.Id));
        }

        [HttpPost, ActionName("ShipmentList")]
        [FormValueRequired("exportpackagingslips-all")]
        public ActionResult PdfPackagingSlipAll(ShipmentListModel model)
        {
            if (!_permissionService.Authorize(StandardPermissionProvider.ManageOrders))
                return AccessDeniedView();

            DateTime? startDateValue = (model.StartDate == null) ? null
                            : (DateTime?)_dateTimeHelper.ConvertToUtcTime(model.StartDate.Value, _dateTimeHelper.CurrentTimeZone);

            DateTime? endDateValue = (model.EndDate == null) ? null
                            : (DateTime?)_dateTimeHelper.ConvertToUtcTime(model.EndDate.Value, _dateTimeHelper.CurrentTimeZone).AddDays(1);

            //a vendor should have access only to his products
            int vendorId = 0;
            if (_workContext.CurrentVendor != null)
                vendorId = _workContext.CurrentVendor.Id;

            //load shipments
            var shipments = _shipmentService.GetAllShipments(vendorId: vendorId,
                warehouseId: model.WarehouseId,
                shippingCountryId: model.CountryId,
                shippingStateId: model.StateProvinceId,
                shippingCity: model.City,
                trackingNumber: model.TrackingNumber,
                loadNotShipped: model.LoadNotShipped,
                createdFromUtc: startDateValue,
                createdToUtc: endDateValue);

            //ensure that we at least one shipment selected
            if (shipments.Count == 0)
            {
                ErrorNotification(_localizationService.GetResource("Admin.Orders.Shipments.NoShipmentsSelected"));
                return RedirectToAction("ShipmentList");
            }

            byte[] bytes;
            using (var stream = new MemoryStream())
            {
                _pdfService.PrintPackagingSlipsToPdf(stream, shipments, _workContext.WorkingLanguage.Id);
                bytes = stream.ToArray();
            }
            return File(bytes, "application/pdf", "packagingslips.pdf");
        }

        [HttpPost]
        public ActionResult PdfPackagingSlipSelected(string selectedIds)
        {
            if (!_permissionService.Authorize(StandardPermissionProvider.ManageOrders))
                return AccessDeniedView();

            var shipments = new List<Shipment>();
            if (selectedIds != null)
            {
                var ids = selectedIds
                    .Split(new[] { ',' }, StringSplitOptions.RemoveEmptyEntries)
                    .Select(x => Convert.ToInt32(x))
                    .ToArray();
                shipments.AddRange(_shipmentService.GetShipmentsByIds(ids));
            }
            //a vendor should have access only to his products
            if (_workContext.CurrentVendor != null)
            {
                shipments = shipments.Where(HasAccessToShipment).ToList();
            }

            //ensure that we at least one shipment selected
            if (shipments.Count == 0)
            {
                ErrorNotification(_localizationService.GetResource("Admin.Orders.Shipments.NoShipmentsSelected"));
                return RedirectToAction("ShipmentList");
            }

            byte[] bytes;
            using (var stream = new MemoryStream())
            {
                _pdfService.PrintPackagingSlipsToPdf(stream, shipments, _workContext.WorkingLanguage.Id);
                bytes = stream.ToArray();
            }
            return File(bytes, "application/pdf", "packagingslips.pdf");
        }

        [HttpPost]
        public ActionResult SetAsShippedSelected(ICollection<int> selectedIds)
        {
            if (!_permissionService.Authorize(StandardPermissionProvider.ManageOrders))
                return AccessDeniedView();

            var shipments = new List<Shipment>();
            if (selectedIds != null)
            {
                shipments.AddRange(_shipmentService.GetShipmentsByIds(selectedIds.ToArray()));
            }
            //a vendor should have access only to his products
            if (_workContext.CurrentVendor != null)
            {
                shipments = shipments.Where(HasAccessToShipment).ToList();
            }

            foreach (var shipment in shipments)
            {
                try
                {
                    _orderProcessingService.Ship(shipment, true);
                }
                catch
                {
                    //ignore any exception
                }
            }

            return Json(new { Result = true });
        }

        [HttpPost]
        public ActionResult SetAsDeliveredSelected(ICollection<int> selectedIds)
        {
            if (!_permissionService.Authorize(StandardPermissionProvider.ManageOrders))
                return AccessDeniedView();

            var shipments = new List<Shipment>();
            if (selectedIds != null)
            {
                shipments.AddRange(_shipmentService.GetShipmentsByIds(selectedIds.ToArray()));
            }
            //a vendor should have access only to his products
            if (_workContext.CurrentVendor != null)
            {
                shipments = shipments.Where(HasAccessToShipment).ToList();
            }

            foreach (var shipment in shipments)
            {
                try
                {
                    _orderProcessingService.Deliver(shipment, true);
                }
                catch
                {
                    //ignore any exception
                }
            }

            return Json(new { Result = true });
        }

        //prachi - added new method ReshipOrder - 11/25/2015
        [HttpPost]
        public ActionResult ReshipOrder(int id)
        {
            var order = _orderService.GetOrderById(id);

            foreach (var orderItem in order.OrderItems)
            {
                //reshippedOrder.OrderItems.Add(orderItem);

                _shoppingCartService.AddToCart(order.Customer,
                       orderItem.Product, ShoppingCartType.ShoppingCart, order.StoreId,
                       orderItem.AttributesXml, 0, null, null, orderItem.Quantity);
            }

            ShippingOption shippingOption = new ShippingOption();
            shippingOption.ShippingRateComputationMethodSystemName = order.ShippingRateComputationMethodSystemName;
            shippingOption.Name = order.ShippingMethod;


            _genericAttributeService.SaveAttribute<ShippingOption>(order.Customer,
               SystemCustomerAttributeNames.SelectedShippingOption, shippingOption, order.StoreId);

            ProcessPaymentRequest paymentInfo = new ProcessPaymentRequest();

            paymentInfo.CreditCardCvv2 = order.CardCvv2;
            paymentInfo.CreditCardExpireMonth = 0;
            paymentInfo.CreditCardExpireYear = 0;
            paymentInfo.CreditCardName = order.CardName;
            paymentInfo.CreditCardNumber = order.CardNumber;
            paymentInfo.CreditCardType = order.CardType;
            paymentInfo.CustomerId = order.CustomerId;
            paymentInfo.StoreId = order.StoreId;
            paymentInfo.OrderGuid = order.OrderGuid;
            paymentInfo.OrderTotal = 0;
            paymentInfo.InitialOrderId = order.Id;
            paymentInfo.PaymentMethodSystemName = order.PaymentMethodSystemName;

            var placeOrderResult = _orderProcessingService.PlaceOrder(paymentInfo, true);
            if (placeOrderResult.Success)
            {
                var postProcessPaymentRequest = new PostProcessPaymentRequest
                {
                    Order = placeOrderResult.PlacedOrder
                };
                _paymentService.PostProcessPayment(postProcessPaymentRequest);

                int reshippedId = placeOrderResult.PlacedOrder.Id;

                //if (_webHelper.IsRequestBeingRedirected || _webHelper.IsPostBeingDone)
                //{
                //    //redirection or POST has been done in PostProcessPayment
                //    return Content("Redirected");
                //}

                //ReshipCompletedModel reshipCompleted = new ReshipCompletedModel();
                //reshipCompleted.OrderId = placeOrderResult.PlacedOrder.Id;
                OrderModel orderModel = new OrderModel();

                var reshippedOrder = _orderService.GetOrderById(reshippedId);

                reshippedOrder.OrderNotes.Add(new OrderNote
                {
                    Note = "Re-Shipped Order #" + id,
                    DisplayToCustomer = false,
                    CreatedOnUtc = DateTime.UtcNow
                });

                //Added By Prachi On 23 Jun, 2016
                reshippedOrder.AllowStoringCreditCardNumber = order.AllowStoringCreditCardNumber;
                reshippedOrder.CardType = order.CardType;
                reshippedOrder.CardName = order.CardName;
                reshippedOrder.CardNumber = order.CardNumber;
                reshippedOrder.MaskedCreditCardNumber = order.MaskedCreditCardNumber;
                reshippedOrder.CardCvv2 = order.CardCvv2;
                reshippedOrder.CardExpirationMonth = order.CardExpirationMonth;
                reshippedOrder.CardExpirationYear = order.CardExpirationYear;
                reshippedOrder.PaymentMethodSystemName = order.PaymentMethodSystemName;
                reshippedOrder.AuthorizationTransactionId = order.AuthorizationTransactionId;
                reshippedOrder.AuthorizationTransactionCode = order.AuthorizationTransactionCode;
                reshippedOrder.AuthorizationTransactionResult = order.AuthorizationTransactionResult;
                reshippedOrder.CaptureTransactionId = order.CaptureTransactionId;
                reshippedOrder.CaptureTransactionResult = order.CaptureTransactionResult;
                reshippedOrder.SubscriptionTransactionId = order.SubscriptionTransactionId;
                reshippedOrder.PaymentStatus = order.PaymentStatus;
                reshippedOrder.PaidDateUtc = order.PaidDateUtc;
                reshippedOrder.PaymentStatus = order.PaymentStatus;
                reshippedOrder.PaymentType = order.PaymentType;
                reshippedOrder.PaymentRemarks = order.PaymentRemarks;
                //End

                _orderService.UpdateOrder(reshippedOrder);

                order.OrderNotes.Add(new OrderNote
                {
                    Note = "Re-Shipped order. New Order #" + reshippedId,
                    DisplayToCustomer = false,
                    CreatedOnUtc = DateTime.UtcNow
                });
                _orderService.UpdateOrder(order);

                SaveSelectedTabIndex(4, true);

                return RedirectToAction("Edit", "Order", new { id = id });
            }
            else
                return null;
            //order.OrderShippingExclTax = 0;
            //_orderService.InsertOrder(order);
            //return null;
        }


        #endregion

        #region Order notes

        [HttpPost]
        public ActionResult OrderNotesSelect(int orderId, DataSourceRequest command)
        {
            if (!_permissionService.Authorize(StandardPermissionProvider.ManageOrders))
                return AccessDeniedView();

            var order = _orderService.GetOrderById(orderId);
            if (order == null)
                throw new ArgumentException("No order found with the specified id");

            //a vendor does not have access to this functionality
            if (_workContext.CurrentVendor != null)
                return Content("");

            //order notes
            var orderNoteModels = new List<OrderModel.OrderNote>();
            foreach (var orderNote in order.OrderNotes
                .OrderByDescending(on => on.CreatedOnUtc))
            {
                var download = _downloadService.GetDownloadById(orderNote.DownloadId);
                orderNoteModels.Add(new OrderModel.OrderNote
                {
                    Id = orderNote.Id,
                    OrderId = orderNote.OrderId,
                    DownloadId = orderNote.DownloadId,
                    DownloadGuid = download != null ? download.DownloadGuid : Guid.Empty,
                    DisplayToCustomer = orderNote.DisplayToCustomer,
                    Note = orderNote.FormatOrderNoteText(),
                    CreatedOn = _dateTimeHelper.ConvertToUserTime(orderNote.CreatedOnUtc, DateTimeKind.Utc)
                });
            }

            var gridModel = new DataSourceResult
            {
                Data = orderNoteModels,
                Total = orderNoteModels.Count
            };

            return Json(gridModel);
        }

        [ValidateInput(false)]
        public ActionResult OrderNoteAdd(int orderId, int downloadId, bool displayToCustomer, string message)
        {
            if (!_permissionService.Authorize(StandardPermissionProvider.ManageOrders))
                return AccessDeniedView();

            var order = _orderService.GetOrderById(orderId);
            if (order == null)
                return Json(new { Result = false }, JsonRequestBehavior.AllowGet);

            //a vendor does not have access to this functionality
            if (_workContext.CurrentVendor != null)
                return Json(new { Result = false }, JsonRequestBehavior.AllowGet);

            var orderNote = new OrderNote
            {
                DisplayToCustomer = displayToCustomer,
                Note = message,
                DownloadId = downloadId,
                CreatedOnUtc = DateTime.UtcNow,
            };
            order.OrderNotes.Add(orderNote);
            _orderService.UpdateOrder(order);

            //new order notification
            if (displayToCustomer)
            {
                //email
                _workflowMessageService.SendNewOrderNoteAddedCustomerNotification(
                    orderNote, _workContext.WorkingLanguage.Id);

            }

            return Json(new { Result = true }, JsonRequestBehavior.AllowGet);
        }

        [HttpPost]
        public ActionResult OrderNoteDelete(int id, int orderId)
        {
            if (!_permissionService.Authorize(StandardPermissionProvider.ManageOrders))
                return AccessDeniedView();

            var order = _orderService.GetOrderById(orderId);
            if (order == null)
                throw new ArgumentException("No order found with the specified id");

            //a vendor does not have access to this functionality
            if (_workContext.CurrentVendor != null)
                return RedirectToAction("Edit", "Order", new { id = orderId });

            var orderNote = order.OrderNotes.FirstOrDefault(on => on.Id == id);
            if (orderNote == null)
                throw new ArgumentException("No order note found with the specified id");
            _orderService.DeleteOrderNote(orderNote);

            return new NullJsonResult();
        }

        #endregion

        #region Reports

        [NonAction]
        protected DataSourceResult GetBestsellersBriefReportModel(int pageIndex,
            int pageSize, int orderBy)
        {
            //a vendor should have access only to his products
            int vendorId = 0;
            if (_workContext.CurrentVendor != null)
                vendorId = _workContext.CurrentVendor.Id;

            var items = _orderReportService.BestSellersReport(
                vendorId: vendorId,
                orderBy: orderBy,
                pageIndex: pageIndex,
                pageSize: pageSize,
                showHidden: true);
            var gridModel = new DataSourceResult
            {
                Data = items.Select(x =>
                {
                    var m = new BestsellersReportLineModel
                    {
                        ProductId = x.ProductId,
                        TotalAmount = _priceFormatter.FormatPrice(x.TotalAmount, true, false),
                        TotalQuantity = x.TotalQuantity,
                    };
                    var product = _productService.GetProductById(x.ProductId);
                    if (product != null)
                        m.ProductName = product.Name;
                    return m;
                }),
                Total = items.TotalCount
            };
            return gridModel;
        }
        public ActionResult BestsellersBriefReportByQuantity()
        {
            if (!_permissionService.Authorize(StandardPermissionProvider.ManageOrders))
                return Content("");

            return PartialView();
        }
        [HttpPost]
        public ActionResult BestsellersBriefReportByQuantityList(DataSourceRequest command)
        {
            if (!_permissionService.Authorize(StandardPermissionProvider.ManageOrders))
                return Content("");

            var gridModel = GetBestsellersBriefReportModel(command.Page - 1,
                command.PageSize, 1);

            return Json(gridModel);
        }
        public ActionResult BestsellersBriefReportByAmount()
        {
            if (!_permissionService.Authorize(StandardPermissionProvider.ManageOrders))
                return Content("");

            return PartialView();
        }
        [HttpPost]
        public ActionResult BestsellersBriefReportByAmountList(DataSourceRequest command)
        {
            if (!_permissionService.Authorize(StandardPermissionProvider.ManageOrders))
                return Content("");

            var gridModel = GetBestsellersBriefReportModel(command.Page - 1,
                command.PageSize, 2);

            return Json(gridModel);
        }



        public ActionResult BestsellersReport()
        {
            if (!_permissionService.Authorize(StandardPermissionProvider.ManageOrders))
                return AccessDeniedView();

            var model = new BestsellersReportModel();

            //order statuses
            model.AvailableOrderStatuses = OrderStatus.Pending.ToSelectList(false).ToList();
            model.AvailableOrderStatuses.Insert(0, new SelectListItem { Text = _localizationService.GetResource("Admin.Common.All"), Value = "0" });

            //payment statuses
            model.AvailablePaymentStatuses = PaymentStatus.Pending.ToSelectList(false).ToList();
            model.AvailablePaymentStatuses.Insert(0, new SelectListItem { Text = _localizationService.GetResource("Admin.Common.All"), Value = "0" });

            //categories
            model.AvailableCategories.Add(new SelectListItem { Text = _localizationService.GetResource("Admin.Common.All"), Value = "0" });
            var categories = _categoryService.GetAllCategories(showHidden: true);
            foreach (var c in categories)
                model.AvailableCategories.Add(new SelectListItem { Text = c.GetFormattedBreadCrumb(categories), Value = c.Id.ToString() });

            //manufacturers
            model.AvailableManufacturers.Add(new SelectListItem { Text = _localizationService.GetResource("Admin.Common.All"), Value = "0" });
            foreach (var m in _manufacturerService.GetAllManufacturers(showHidden: true))
                model.AvailableManufacturers.Add(new SelectListItem { Text = m.Name, Value = m.Id.ToString() });

            //billing countries
            foreach (var c in _countryService.GetAllCountriesForBilling(true))
            {
                model.AvailableCountries.Add(new SelectListItem { Text = c.Name, Value = c.Id.ToString() });
            }
            model.AvailableCountries.Insert(0, new SelectListItem { Text = _localizationService.GetResource("Admin.Common.All"), Value = "0" });

            //vendor
            model.IsLoggedInAsVendor = _workContext.CurrentVendor != null;

            return View(model);
        }
        [HttpPost]
        public ActionResult BestsellersReportList(DataSourceRequest command, BestsellersReportModel model)
        {
            if (!_permissionService.Authorize(StandardPermissionProvider.ManageOrders))
                return Content("");

            //a vendor should have access only to his products
            int vendorId = 0;
            if (_workContext.CurrentVendor != null)
                vendorId = _workContext.CurrentVendor.Id;

            DateTime? startDateValue = (model.StartDate == null) ? null
                            : (DateTime?)_dateTimeHelper.ConvertToUtcTime(model.StartDate.Value, _dateTimeHelper.CurrentTimeZone);

            DateTime? endDateValue = (model.EndDate == null) ? null
                            : (DateTime?)_dateTimeHelper.ConvertToUtcTime(model.EndDate.Value, _dateTimeHelper.CurrentTimeZone).AddDays(1);

            OrderStatus? orderStatus = model.OrderStatusId > 0 ? (OrderStatus?)(model.OrderStatusId) : null;
            PaymentStatus? paymentStatus = model.PaymentStatusId > 0 ? (PaymentStatus?)(model.PaymentStatusId) : null;

            var items = _orderReportService.BestSellersReport(
                createdFromUtc: startDateValue,
                createdToUtc: endDateValue,
                os: orderStatus,
                ps: paymentStatus,
                billingCountryId: model.BillingCountryId,
                orderBy: 2,
                vendorId: vendorId,
                categoryId: model.CategoryId,
                manufacturerId: model.ManufacturerId,
                pageIndex: command.Page - 1,
                pageSize: command.PageSize,
                showHidden: true);
            var gridModel = new DataSourceResult
            {
                Data = items.Select(x =>
                {
                    var m = new BestsellersReportLineModel
                    {
                        ProductId = x.ProductId,
                        TotalAmount = _priceFormatter.FormatPrice(x.TotalAmount, true, false),
                        TotalQuantity = x.TotalQuantity,
                    };
                    var product = _productService.GetProductById(x.ProductId);
                    if (product != null)
                        m.ProductName = product.Name;
                    return m;
                }),
                Total = items.TotalCount
            };

            return Json(gridModel);
        }



        public ActionResult NeverSoldReport()
        {
            if (!_permissionService.Authorize(StandardPermissionProvider.ManageOrders))
                return AccessDeniedView();

            var model = new NeverSoldReportModel();
            return View(model);
        }
        [HttpPost]
        public ActionResult NeverSoldReportList(DataSourceRequest command, NeverSoldReportModel model)
        {
            if (!_permissionService.Authorize(StandardPermissionProvider.ManageOrders))
                return Content("");

            //a vendor should have access only to his products
            int vendorId = 0;
            if (_workContext.CurrentVendor != null)
                vendorId = _workContext.CurrentVendor.Id;

            DateTime? startDateValue = (model.StartDate == null) ? null
                            : (DateTime?)_dateTimeHelper.ConvertToUtcTime(model.StartDate.Value, _dateTimeHelper.CurrentTimeZone);

            DateTime? endDateValue = (model.EndDate == null) ? null
                            : (DateTime?)_dateTimeHelper.ConvertToUtcTime(model.EndDate.Value, _dateTimeHelper.CurrentTimeZone).AddDays(1);

            var items = _orderReportService.ProductsNeverSold(vendorId,
                startDateValue, endDateValue,
                command.Page - 1, command.PageSize, true);
            var gridModel = new DataSourceResult
            {
                Data = items.Select(x =>
                    new NeverSoldReportLineModel
                    {
                        ProductId = x.Id,
                        ProductName = x.Name,
                    }),
                Total = items.TotalCount
            };

            return Json(gridModel);
        }


        [ChildActionOnly]
        public ActionResult OrderAverageReport()
        {
            if (!_permissionService.Authorize(StandardPermissionProvider.ManageOrders))
                return Content("");

            return PartialView();
        }
        [HttpPost]
        public ActionResult OrderAverageReportList(DataSourceRequest command)
        {
            if (!_permissionService.Authorize(StandardPermissionProvider.ManageOrders))
                return Content("");

            //a vendor does have access to this report
            if (_workContext.CurrentVendor != null)
                return Content("");


            var report = new List<OrderAverageReportLineSummary>();
            report.Add(_orderReportService.OrderAverageReport(0, OrderStatus.Pending));
            report.Add(_orderReportService.OrderAverageReport(0, OrderStatus.Processing));
            report.Add(_orderReportService.OrderAverageReport(0, OrderStatus.Complete));
            report.Add(_orderReportService.OrderAverageReport(0, OrderStatus.Cancelled));
            var model = report.Select(x => new OrderAverageReportLineSummaryModel
            {
                OrderStatus = x.OrderStatus.GetLocalizedEnum(_localizationService, _workContext),
                SumTodayOrders = _priceFormatter.FormatPrice(x.SumTodayOrders, true, false),
                SumThisWeekOrders = _priceFormatter.FormatPrice(x.SumThisWeekOrders, true, false),
                SumThisMonthOrders = _priceFormatter.FormatPrice(x.SumThisMonthOrders, true, false),
                SumThisYearOrders = _priceFormatter.FormatPrice(x.SumThisYearOrders, true, false),
                SumAllTimeOrders = _priceFormatter.FormatPrice(x.SumAllTimeOrders, true, false),
            }).ToList();

            var gridModel = new DataSourceResult
            {
                Data = model,
                Total = model.Count
            };

            return Json(gridModel);
        }

        [ChildActionOnly]
        public ActionResult OrderIncompleteReport()
        {
            if (!_permissionService.Authorize(StandardPermissionProvider.ManageOrders))
                return Content("");

            return PartialView();
        }
        [HttpPost]
        public ActionResult OrderIncompleteReportList(DataSourceRequest command)
        {
            if (!_permissionService.Authorize(StandardPermissionProvider.ManageOrders))
                return Content("");

            //a vendor does have access to this report
            if (_workContext.CurrentVendor != null)
                return Content("");

            var model = new List<OrderIncompleteReportLineModel>();
            //not paid
            var psPending = _orderReportService.GetOrderAverageReportLine(ps: PaymentStatus.Pending, ignoreCancelledOrders: true);
            model.Add(new OrderIncompleteReportLineModel
            {
                Item = _localizationService.GetResource("Admin.SalesReport.Incomplete.TotalUnpaidOrders"),
                Count = psPending.CountOrders,
                Total = _priceFormatter.FormatPrice(psPending.SumOrders, true, false),
                ViewLink = Url.Action("List", "Order", new { paymentStatusId = ((int)PaymentStatus.Pending).ToString() })
            });
            //not shipped
            var ssPending = _orderReportService.GetOrderAverageReportLine(ss: ShippingStatus.NotYetShipped, ignoreCancelledOrders: true);
            model.Add(new OrderIncompleteReportLineModel
            {
                Item = _localizationService.GetResource("Admin.SalesReport.Incomplete.TotalNotShippedOrders"),
                Count = ssPending.CountOrders,
                Total = _priceFormatter.FormatPrice(ssPending.SumOrders, true, false),
                ViewLink = Url.Action("List", "Order", new { shippingStatusId = ((int)ShippingStatus.NotYetShipped).ToString() })
            });
            //pending
            var osPending = _orderReportService.GetOrderAverageReportLine(os: OrderStatus.Pending, ignoreCancelledOrders: true);
            model.Add(new OrderIncompleteReportLineModel
            {
                Item = _localizationService.GetResource("Admin.SalesReport.Incomplete.TotalIncompleteOrders"),
                Count = osPending.CountOrders,
                Total = _priceFormatter.FormatPrice(osPending.SumOrders, true, false),
                ViewLink = Url.Action("List", "Order", new { orderStatusId = ((int)OrderStatus.Pending).ToString() })
            });

            var gridModel = new DataSourceResult
            {
                Data = model,
                Total = model.Count
            };

            return Json(gridModel);
        }



        public ActionResult CountryReport()
        {
            if (!_permissionService.Authorize(StandardPermissionProvider.OrderCountryReport))
                return AccessDeniedView();

            var model = new CountryReportModel();

            //order statuses
            model.AvailableOrderStatuses = OrderStatus.Pending.ToSelectList(false).ToList();
            model.AvailableOrderStatuses.Insert(0, new SelectListItem { Text = _localizationService.GetResource("Admin.Common.All"), Value = "0" });

            //payment statuses
            model.AvailablePaymentStatuses = PaymentStatus.Pending.ToSelectList(false).ToList();
            model.AvailablePaymentStatuses.Insert(0, new SelectListItem { Text = _localizationService.GetResource("Admin.Common.All"), Value = "0" });

            return View(model);
        }

        [HttpPost]
        public ActionResult CountryReportList(DataSourceRequest command, CountryReportModel model)
        {
            if (!_permissionService.Authorize(StandardPermissionProvider.OrderCountryReport))
                return Content("");

            DateTime? startDateValue = (model.StartDate == null) ? null
                            : (DateTime?)_dateTimeHelper.ConvertToUtcTime(model.StartDate.Value, _dateTimeHelper.CurrentTimeZone);

            DateTime? endDateValue = (model.EndDate == null) ? null
                            : (DateTime?)_dateTimeHelper.ConvertToUtcTime(model.EndDate.Value, _dateTimeHelper.CurrentTimeZone).AddDays(1);

            OrderStatus? orderStatus = model.OrderStatusId > 0 ? (OrderStatus?)(model.OrderStatusId) : null;
            PaymentStatus? paymentStatus = model.PaymentStatusId > 0 ? (PaymentStatus?)(model.PaymentStatusId) : null;

            var items = _orderReportService.GetCountryReport(
                os: orderStatus,
                ps: paymentStatus,
                startTimeUtc: startDateValue,
                endTimeUtc: endDateValue);
            var gridModel = new DataSourceResult
            {
                Data = items.Select(x =>
                {
                    var country = _countryService.GetCountryById(x.CountryId.HasValue ? x.CountryId.Value : 0);
                    var m = new CountryReportLineModel
                    {
                        CountryName = country != null ? country.Name : "Unknown",
                        SumOrders = _priceFormatter.FormatPrice(x.SumOrders, true, false),
                        TotalOrders = x.TotalOrders,
                    };
                    return m;
                }),
                Total = items.Count
            };

            return Json(gridModel);
        }

        #endregion

        //Added By Faisal on 02 Dec, 2015
        [HttpPost]
        public ActionResult OrderAllowStoringCreditCardNumber(int id, string flag)
        {
            var order = _orderService.GetOrderById(id);

            string strFriendlyName = "";
            var paymentMethod = _paymentService.LoadPaymentMethodBySystemName(order.PaymentMethodSystemName);
            if (paymentMethod != null)
                strFriendlyName = paymentMethod.GetLocalizedFriendlyName(_localizationService, _workContext.WorkingLanguage.Id);

            if (strFriendlyName.ToUpper().Trim() != "CREDIT CARD" && strFriendlyName.ToUpper().Trim() != "CREDITCARD")
            {
                order.AllowStoringCreditCardNumber = Convert.ToBoolean(flag);
                _orderService.UpdateOrder(order);
            }

            return Json(new object());
        }

        //Added By Faisal on 04 Dec, 2015

        public void BindDDLs(OrderModel model)
        {
            //CCTypes
            model.CreditCardTypes.Add(new SelectListItem
            {
                Text = "Visa",
                Value = "Visa",
            });
            model.CreditCardTypes.Add(new SelectListItem
            {
                Text = "Master card",
                Value = "MasterCard",
            });
            model.CreditCardTypes.Add(new SelectListItem
            {
                Text = "Discover",
                Value = "Discover",
            });
            //model.CreditCardTypes.Add(new SelectListItem
            //{
            //    Text = "Amex",
            //    Value = "Amex",
            //});

            //months
            for (int i = 1; i <= 12; i++)
            {
                string text = (i < 10) ? "0" + i : i.ToString();
                model.ExpireMonths.Add(new SelectListItem
                {
                    Text = text,
                    Value = i.ToString(),
                });
            }

            //years
            for (int i = 0; i < 15; i++)
            {
                string year = Convert.ToString(DateTime.Now.Year + i);
                model.ExpireYears.Add(new SelectListItem
                {
                    Text = year,
                    Value = year,
                });
            }
        }

        [HttpPost]
        public ActionResult BindCCDetails(int id)
        {
            var order = _orderService.GetOrderById(id);
            var model = new OrderModel();
            PrepareOrderDetailsModel(model, order);

            int filterByCountryId = 0;
            if (_addressSettings.CountryEnabled &&
                _workContext.CurrentCustomer.BillingAddress != null &&
                _workContext.CurrentCustomer.BillingAddress.Country != null)
            {
                filterByCountryId = order.BillingAddress.Country.Id;
            }

            var paymentMethod = _paymentService.LoadPaymentMethodBySystemName("");
            var paymentMethods = _paymentService
                .LoadActivePaymentMethods(_workContext.CurrentCustomer.Id, order.StoreId, filterByCountryId)
                .Where(pm => pm.PaymentMethodType == PaymentMethodType.Standard || pm.PaymentMethodType == PaymentMethodType.Redirection)
                .ToList();

            foreach (IPaymentMethod obj in paymentMethods)
            {
                if (obj.IsPaymentMethodActive(_paymentSettings))
                {
                    string name = obj.GetLocalizedFriendlyName(_localizationService, _workContext.WorkingLanguage.Id);
                    if (name.ToUpper().Trim() == "CREDIT CARD" || name.ToUpper().Trim() == "CREDITCARD")
                    {
                        paymentMethod = _paymentService.LoadPaymentMethodBySystemName(obj.ToModel().SystemName);
                        break;
                    }
                }
            }

            return Json(new object());
        }

        //Added By Prachi on 15 Jan, 2016
        public ActionResult ImportBulkOrders()
        {
            return View();
        }

        //Added By Prachi on 15 Jan, 2016
        [HttpPost]
        [ValidateAntiForgeryToken]
        public ActionResult UploadBulkOrders(HttpPostedFileBase upload)
        {
            SqlConnection sqlCon = new SqlConnection();

            //WriteToLog("function start " + path + Environment.NewLine);
            try
            {
                if (upload != null && upload.ContentLength > 0)
                {
                    var path = Path.Combine(Server.MapPath("~/Content/UploadedFiles/"),
                                     System.IO.Path.GetFileName(upload.FileName));
                    if (System.IO.Path.GetExtension(upload.FileName) != ".csv")
                    {
                        ModelState.AddModelError("", "Uploaded file format is not supported. Please upload the file with '.csv' extension.");
                        ErrorNotification("Uploaded file format is not supported. Please upload the file with '.csv' extension.", false);
                    }
                    if (ModelState.IsValid)
                    {
                        //WriteToLog("ModelState.IsValid:" + ModelState.IsValid + Environment.NewLine);


                        //WriteToLog("function start " + path + Environment.NewLine);)
                        // WriteToLog("saving file " + upload.ContentLength + Environment.NewLine);
                        upload.SaveAs(path);
                        //WriteToLog("1. File Created" + Environment.NewLine);

                        //String strConnection = "Data Source=.\\SQLEXPRESS;AttachDbFilename='C:\\Users\\Hemant\\documents\\visual studio 2010\\Projects\\CRMdata\\CRMdata\\App_Data\\Database1.mdf';Integrated Security=True;User Instance=True";
                        string connString = System.IO.File.ReadAllText(Server.MapPath(ConfigurationManager.AppSettings["ConnectionStringPath"].ToString()));
                        sqlCon.ConnectionString = connString.Substring(connString.LastIndexOf(":") + 1).Trim();
                        sqlCon.Open();
                        //try
                        //{
                        //String excelConnString = String.Format("Provider=Microsoft.ACE.OLEDB.12.0;Data Source={0};Extended Properties='Excel 12.0;HDR=YES;IMEX=1;'", path);
                        //Create Connection to Excel work book 
                        //using (OleDbConnection excelConnection = new OleDbConnection(excelConnString))
                        //{
                        //WriteToLog("2. oledb connection" + Environment.NewLine);
                        //Create OleDbCommand to fetch data from Excel 
                        //using (SqlCommand cmd = new SqlCommand("Select [Customer (RI) #],[Last],[First],[State Association],[Ship to: Address 1],[Address 2],City,[State],Zip,PHONE,[Special Instructions],[2015 FB GUIDE BFBNL15],[GUIDE $],[ACTUAL SHIP],[S&H],[TOTAL ORDER],[PRODUCT CODE] from [Sheet1$]", sqlCon))
                        //{
                        //    //excelConnection.Open();
                        //    WriteToLog("3. excel connection opened" + Environment.NewLine);
                        //using (SqlDataReader dReader = cmd.ExecuteReader())
                        //{

                        DataTable dt = new DataTable();
                        dt.Columns.AddRange(new DataColumn[17] {
	                        new DataColumn("[Customer (RI) #]", typeof(int)),
	                        new DataColumn("[Last]", typeof(string)),
	                        new DataColumn("[First]", typeof(string)),
                            new DataColumn("[State Association]", typeof(string)),
                            new DataColumn("[Ship to: Address 1n]", typeof(string)),
    new DataColumn("[Address 2]", typeof(string)),
        new DataColumn("City", typeof(string)),
            new DataColumn("[State]", typeof(string)),
                new DataColumn("[Zip]", typeof(string)),
                    new DataColumn("[PHONE]", typeof(string)),
                     new DataColumn("[Special Instructions]", typeof(string)),
                      new DataColumn("[2015 FB GUIDE BFBNL15]", typeof(decimal)),
                       new DataColumn("[GUIDE $]", typeof(decimal)),
                      new DataColumn("[ACTUAL SHIP]", typeof(decimal)),
                      new DataColumn("[S&H]", typeof(decimal)),
                      new DataColumn("[TOTAL ORDER]", typeof(decimal)),
                      new DataColumn("[PRODUCT CODE]", typeof(string))
                                });

                        string csvData = System.IO.File.ReadAllText(path);
                        foreach (string row in csvData.Split('\n'))
                        {
                            if (!string.IsNullOrEmpty(row))
                            {
                                dt.Rows.Add();
                                int i = 0;
                                foreach (string cell in row.Split(','))
                                {
                                    string cellData = cell.Trim();
                                    cellData = (cellData.IndexOf("$") != -1 ? cellData.Replace(cellData.Substring(cellData.IndexOf("$"), 1), "") : cellData);
                                    //cellData = (cellData.IndexOf(",") != -1 ? cellData.Replace(cellData.Substring(cellData.IndexOf(","), 1),"") : cellData);
                                    cellData = (cellData.IndexOf("-") != -1 ? cellData.Replace(cellData.Substring(cellData.IndexOf("-"), 1), "") : cellData);
                                    //cellData = (cellData.IndexOf("'\'") != -1 ? cellData.Replace(cellData.Substring(cellData.IndexOf("'\'"), 1), "") : cellData);
                                    dt.Rows[dt.Rows.Count - 1][i] = cellData;
                                    i += 1;
                                }
                            }
                        }
                        using (SqlBulkCopy sqlBulk = new SqlBulkCopy(connString.Substring(connString.LastIndexOf(":") + 1).Trim()))
                        {
                            //WriteToLog("4. writing to sql" + Environment.NewLine);
                            //Give your Destination table name 
                            sqlBulk.BatchSize = 1000000;
                            sqlBulk.DestinationTableName = "FileUpload";
                            sqlBulk.WriteToServer(dt);
                            sqlBulk.Close();
                            //excelConnection.Close();
                        }
                        //}
                        //}
                        //}
                        //}
                        //catch (Exception ex)
                        //{
                        //    WriteToLog("5. excel upload catch" + ex.Message + Environment.NewLine);

                        //    String excelConnString = String.Format("Provider=Microsoft.JET.OLEDB.8.0;Data Source={0};Extended Properties='Excel 8.0;HDR=YES;IMEX=1;'", path);
                        //    //Create Connection to Excel work book 
                        //    using (OleDbConnection excelConnection = new OleDbConnection(excelConnString))
                        //    {
                        //        //Create OleDbCommand to fetch data from Excel 
                        //        using (OleDbCommand cmd = new OleDbCommand("Select [Customer (RI) #],[Last],[First],[State Association],[Ship to: Address 1],[Address 2],City,[State],Zip,PHONE,[Special Instructions],[2015 FB GUIDE BFBNL15],[GUIDE $],[ACTUAL SHIP],[S&H],[TOTAL ORDER],[PRODUCT CODE] from [Officials Liasons$]", excelConnection))
                        //        {
                        //            excelConnection.Open();
                        //            using (OleDbDataReader dReader = cmd.ExecuteReader())
                        //            {
                        //                using (SqlBulkCopy sqlBulk = new SqlBulkCopy(connString.Substring(connString.LastIndexOf(":") + 1).Trim()))
                        //                {
                        //                    //Give your Destination table name 
                        //                    sqlBulk.BatchSize = 1000000;
                        //                    sqlBulk.DestinationTableName = "FileUpload";
                        //                    sqlBulk.WriteToServer(dReader);
                        //                    sqlBulk.Close();
                        //                    //excelConnection.Close();
                        //                }
                        //            }
                        //        }
                        //    }
                        //    ErrorNotification(ex.Message + " " + Convert.ToString(ex.InnerException.Message));
                        //}

                        SqlCommand com = new SqlCommand("sp_UploadOrderData", sqlCon);
                        //{
                        int flag = 0;
                        //com.CommandText = "sp_UploadOrderData";
                        com.CommandType = CommandType.StoredProcedure;
                        //com.CommandTimeout = 180;
                        flag = com.ExecuteNonQuery();
                        if (flag >= 1)
                        {
                            SuccessNotification(_localizationService.GetResource("Admin.Orders.BulkOrders.Updated"));
                        }
                        //

                        //string FileName = System.IO.Path.GetFileName(upload.FileName);
                        //string query =string.Empty;
                        //if (System.IO.Path.GetExtension(upload.FileName) == ".xlsx")
                        //{
                        //    query = " IF OBJECT_ID('dbo.FileUpload', 'U') IS NOT NULL Drop table FileUpload " +
                        //            " SELECT * into FileUpload" + Environment.NewLine +
                        //            " FROM OPENROWSET('Microsoft.ACE.OLEDB.12.0','Excel 12.0;Database="
                        //            + path + ";IMEX=1;HDR=YES'," +
                        //            "'SELECT * FROM [Officials Liasons$]')";
                        //}
                        //else if (System.IO.Path.GetExtension(upload.FileName) == ".xlsx")
                        //{
                        //    query = " IF OBJECT_ID('dbo.FileUpload', 'U') IS NOT NULL " + Environment.NewLine +
                        //            " Drop table FileUpload " + Environment.NewLine +
                        //            " SELECT * into FileUpload " + Environment.NewLine +
                        //            " FROM OPENROWSET('Microsoft.JET.OLEDB.8.0','Excel 8.0;Database="
                        //            + path + ";IMEX=1;HDR=YES'," +
                        //            "'SELECT * FROM [Officials Liasons$]')";
                        //}
                        //com.CommandType = CommandType.Text;
                        //int flag = com.ExecuteNonQuery();
                        //if (flag >= 1)
                        //}
                    }
                }
                else
                {
                    ModelState.AddModelError("", "Please select a file with valid extension to upload.");
                    ErrorNotification("Please select a file with valid extension to upload.", false);
                }
            }
            catch (Exception ex)
            {
                //Log the error (uncomment dex variable name and add a line here to write a log.
                //ModelState.AddModelError("", "Error:" + ex.Message + " " + ex.InnerException.Message);
                ErrorNotification(ex.Message);
                if (ex.InnerException != null)
                {
                    ErrorNotification(ex.InnerException.Message);
                }
                //_customerActivityService.InsertActivity("Orders.ImportOrder", ex.Message);
            }
            finally
            {
                sqlCon.Close();
            }
            return View("ImportBulkOrders");
        }

        public void WriteToLog(string log)
        {
            string fileName = Server.MapPath("/ErrorLog.txt");
            System.IO.File.AppendAllText(fileName, log);
        }

        //Changes by Prachi on 16 Feb, 2016 - Method added to calculate order totals upon add/edit/delete of particular order item
        protected void CalculateOrderTotals(Order order)
        {
            //decimal orderSubtotalTaxable = 0;
            //decimal orderSubtotalNonTaxable = 0;
            //decimal orderItemPriceExclTax = 0;
            decimal orderSubtotalInclTax = 0;
            decimal orderSubtotalExclTax = 0;
            decimal taxRate = 0;

            foreach (var item in order.OrderItems)
            {
                orderSubtotalExclTax += item.PriceExclTax;
                orderSubtotalInclTax += item.PriceInclTax;
            }

            //decimal tax = 0;
            if (order.TaxRates.IndexOf(":") != -1)
                taxRate = Math.Round(Convert.ToDecimal(order.TaxRates.Substring(0, order.TaxRates.IndexOf(":"))), 2);

            decimal tax = Math.Round(orderSubtotalInclTax - orderSubtotalExclTax, 2);

            //Changes by Prachi on 10 March, 2016 - recalculate subtotal including tax 
            //if (orderSubtotalExclTax == orderSubtotalInclTax)
            //    orderSubtotalInclTax = orderSubtotalExclTax + (orderSubtotalExclTax * tax / 100);

            order.OrderSubtotalExclTax = Math.Round(orderSubtotalExclTax, 2);
            order.OrderSubtotalInclTax = Math.Round(orderSubtotalInclTax, 2);

            //if (tax > 0)
            //{
            //    order.OrderSubTotalDiscountInclTax = order.OrderSubTotalDiscountExclTax + (order.OrderSubTotalDiscountExclTax * tax / 100);
            //}
            //else
            order.OrderSubTotalDiscountInclTax = Math.Round(order.OrderSubTotalDiscountExclTax, 2);

            //if (tax > 0)
            //    order.OrderShippingInclTax = order.OrderShippingExclTax + (order.OrderShippingExclTax * tax / 100);
            //else
            order.OrderShippingInclTax = Math.Round(order.OrderShippingExclTax, 2);

            //if (tax > 0)
            //    order.PaymentMethodAdditionalFeeInclTax = order.PaymentMethodAdditionalFeeExclTax + (order.PaymentMethodAdditionalFeeExclTax * tax / 100);
            //else
            order.PaymentMethodAdditionalFeeInclTax = Math.Round(order.PaymentMethodAdditionalFeeExclTax, 2);

            order.TaxRates = taxRate + ":" + tax + ";";

            order.OrderTax = tax;

            //if (_taxSettings.TaxDisplayType == TaxDisplayType.IncludingTax)
            order.OrderTotal = Math.Round(order.OrderSubtotalInclTax + order.OrderShippingInclTax + order.PaymentMethodAdditionalFeeInclTax - order.OrderDiscount
                - order.OrderSubTotalDiscountInclTax, 2);
            //else
            //    order.OrderTotal = orderSubtotalExclTax + order.OrderShippingExclTax + order.PaymentMethodAdditionalFeeExclTax + order.OrderTax
            //        - order.OrderDiscount - order.OrderSubTotalDiscountExclTax;
        }

        //Changes by Prachi on 23 Feb, 2016 - Method added to check whether a customer is NASO member or not while adding new product in his/her order
        protected bool VerifyNASOMember(Customer customer)
        {
            bool isNasoMember = false;
            string AccountNo = string.Empty;
            string Status = string.Empty;
            string ZipCode = string.Empty;
            try
            {
                if (customer != null)
                {
                    string CustomerEmailID = customer.Email;
                    //string emailAddress = "corinna@corinnaluyken.com";

                    XmlNode node = null;
                    string cuid = WebConfigurationManager.AppSettings["PubServiceCUID"];
                    string cpwd = WebConfigurationManager.AppSettings["PubServicePassword"];

                    const string WSReq = "<ws1600request><pubcode>NO</pubcode><emailaddress><![CDATA[{0}]]></emailaddress></ws1600request>";
                    string reqString = string.Format(WSReq, CustomerEmailID);

                    using (var loPubService = new net.espcomp.espdev.Listener01())
                    {
                        node = loPubService.ws1600(cuid, cpwd, reqString);
                    }

                    if (node.OuterXml != "")
                    {
                        string returnCode = node.SelectSingleNode("/returncode").InnerText;

                        if (node.SelectSingleNode("account") != null)
                        {
                            if (node.SelectSingleNode("account").SelectSingleNode("status") != null)
                                Status = node.SelectSingleNode("account").SelectSingleNode("status").InnerText;

                            if (node.SelectSingleNode("account").SelectSingleNode("acctno") != null)
                                AccountNo = node.SelectSingleNode("account").SelectSingleNode("acctno").InnerText;

                            if (node.SelectSingleNode("account").SelectSingleNode("zipcode") != null)
                                ZipCode = node.SelectSingleNode("account").SelectSingleNode("zipcode").InnerText;
                        }
                        if (returnCode == "1" && Status == "A")
                        {
                            //NASOMemberStatus = NASOMemberStatusEnum.Active.ToString();
                            isNasoMember = true;
                            //var customer = _workContext.CurrentCustomer;
                            var role = _customerService.GetCustomerRoleBySystemName("NASO Member");
                            if (role != null)
                            {
                                if (!customer.CustomerRoles.Any(cr => cr.SystemName == "NASO Member"))
                                {
                                    customer.CustomerRoles.Add(role);
                                    _customerService.UpdateCustomer(customer);
                                }
                            }
                        }
                    }
                }
            }
            catch (Exception ex)
            {
                isNasoMember = false;
            }
            return isNasoMember;
        }

        //Changes by Prachi on 23 Feb, 2016 - Method added to get tier prices of product being added/edited
        [HttpGet]
        public string GetTierPriceOfProduct(int productId, int orderId, int quantity)
        {
            //decimal productTierPrice = 0;
            Product product = _productService.GetProductById(productId);
            Order order = _orderService.GetOrderById(orderId);
            decimal discountAmount = 0;
            decimal taxRate = 0;
            Discount appliedDiscount = null;
            VerifyNASOMember(order.Customer);
            decimal productTierPrice = Math.Round(_priceCalculationService.GetFinalPrice(product, order.Customer, 0, true, quantity, out discountAmount, out appliedDiscount), 2);
            decimal productPriceInclTax = Math.Round(_taxService.GetProductPrice(product, productTierPrice, true, order.Customer, out taxRate), 2);
            //Added By Faisal On 12 Apr, 2016
            decimal totalDiscExclTax = Math.Round(discountAmount * quantity, 2);
            decimal totalDiscInclTax = Math.Round(totalDiscExclTax + ((discountAmount * quantity * taxRate) / 100.00M), 2);
            //End 12 Apr, 2016

            return productTierPrice + ":" + productPriceInclTax + ":" + totalDiscExclTax + ":" + totalDiscInclTax;
        }

        //Changes by Prachi on 24 Feb, 2016 - Preparing model for return request
        [NonAction]
        protected virtual SubmitReturnRequestModel PrepareReturnRequestModel(Order order, SubmitReturnRequestModel model = null)
        {
            if (order == null)
                throw new ArgumentNullException("order");

            if (model == null)
                model = new SubmitReturnRequestModel();

            model.OrderId = order.Id;

            //return reasons
            if (_orderSettings.ReturnRequestReasons != null)
                foreach (var rrr in _orderSettings.ReturnRequestReasons)
                {
                    model.AvailableReturnReasons.Add(new SelectListItem
                    {
                        Text = rrr,
                        Value = rrr
                    });
                }

            //return actions
            if (_orderSettings.ReturnRequestActions != null)
                foreach (var rra in _orderSettings.ReturnRequestActions)
                {
                    model.AvailableReturnActions.Add(new SelectListItem
                    {
                        Text = rra,
                        Value = rra
                    });
                }
            //products
            var orderItems = _orderService.GetAllOrderItems(order.Id, null, null, null, null, null, null);
            foreach (var orderItem in orderItems)
            {
                var orderItemModel = new SubmitReturnRequestModel.OrderItemModel
                {
                    Id = orderItem.Id,
                    ProductId = orderItem.Product.Id,
                    ProductName = orderItem.Product.GetLocalized(x => x.Name),
                    AttributeInfo = orderItem.AttributeDescription,
                    Quantity = orderItem.Quantity
                };
                model.Items.Add(orderItemModel);

                //unit price
                if (order.CustomerTaxDisplayType == TaxDisplayType.IncludingTax)
                {
                    //including tax
                    var unitPriceInclTaxInCustomerCurrency = _currencyService.ConvertCurrency(orderItem.UnitPriceInclTax, order.CurrencyRate);
                    orderItemModel.UnitPrice = _priceFormatter.FormatPrice(unitPriceInclTaxInCustomerCurrency, true, order.CustomerCurrencyCode, _workContext.WorkingLanguage, true);
                }
                else
                {
                    //excluding tax
                    var unitPriceExclTaxInCustomerCurrency = _currencyService.ConvertCurrency(orderItem.UnitPriceExclTax, order.CurrencyRate);
                    orderItemModel.UnitPrice = _priceFormatter.FormatPrice(unitPriceExclTaxInCustomerCurrency, true, order.CustomerCurrencyCode, _workContext.WorkingLanguage, false);
                }
            }
            return model;
        }

        //Changes by Prachi on 24 Feb, 2016- Method added to return the product
        [HttpPost, ActionName("Edit")]
        [FormValueRequired(FormValueRequirement.Equal, "submit-return-request")]
        [ValidateInput(false)]
        public ActionResult ReturnRequestSubmit(int id, FormCollection form)
        {
            var order = _orderService.GetOrderById(id);
            if (order == null || order.Deleted || order.Customer.Id != order.CustomerId)
                return new HttpUnauthorizedResult();

            if (!_orderProcessingService.IsReturnRequestAllowed(order))
                return RedirectToRoute("HomePage");

            foreach (var orderItem in order.OrderItems)
            {
                int quantity = 0; //parse quantity
                foreach (string formKey in form.AllKeys)
                    if (formKey.Equals(string.Format("hdnQuantity{0}", orderItem.Id), StringComparison.InvariantCultureIgnoreCase))
                    {
                        int.TryParse(form[formKey], out quantity);
                        break;
                    }
                if (quantity > 0)
                {
                    var rr = new ReturnRequest
                    {
                        StoreId = order.StoreId,
                        OrderItemId = orderItem.Id,
                        Quantity = quantity,
                        CustomerId = order.Customer.Id,
                        ReasonForReturn = form["ReturnRequest.ReturnReason"],
                        RequestedAction = form["ReturnRequest.ReturnAction"],
                        CustomerComments = form["ReturnRequest.Comments"],
                        StaffNotes = string.Empty,
                        ReturnRequestStatus = ReturnRequestStatus.Pending,
                        CreatedOnUtc = DateTime.UtcNow,
                        UpdatedOnUtc = DateTime.UtcNow
                    };
                    order.Customer.ReturnRequests.Add(rr);
                    _customerService.UpdateCustomer(order.Customer);
                    //notify store owner here (email)
                    _workflowMessageService.SendNewReturnRequestStoreOwnerNotification(rr, orderItem, order.CustomerLanguageId);
                }
            }
            //SubmitReturnRequestModel model = new SubmitReturnRequestModel();

            //model = PrepareReturnRequestModel(order);
            //if (count > 0)
            //    model.Result = _localizationService.GetResource("ReturnRequests.Submitted");
            //else
            //    model.Result = _localizationService.GetResource("ReturnRequests.NoItemsSubmitted");

            var orderModel = new OrderModel();
            PrepareOrderDetailsModel(orderModel, order);

            //selected tab
            SaveSelectedTabIndex(persistForTheNextRequest: false);

            return View(orderModel);
        }

        //Added By Faisal On 03 Mar, 2016
        [HttpPost, ActionName("Edit")]
        [FormValueRequired("btnRefund")]
        public ActionResult RefundOrCharge(int id, FormCollection frm)
        {
            var order = _orderService.GetOrderById(id);
            var orderModel = new OrderModel();
            CalculationsForRefundOrCharge(ref order, frm, false, ref orderModel);
            return View(orderModel);

        }

        [HttpPost, ActionName("Edit")]
        [FormValueRequired("btnRefundOffline")]
        public ActionResult RefundOrChargeOffline(int id, FormCollection frm)
        {
            var order = _orderService.GetOrderById(id);
            var orderModel = new OrderModel();
            CalculationsForRefundOrCharge(ref order, frm, true, ref orderModel);
            return View(orderModel);

        }

        public void CalculationsForRefundOrCharge(ref Order order, FormCollection form, bool offline, ref OrderModel orderModel)
        {
            decimal amountToCharge = decimal.Zero;
            decimal amountToRefund = decimal.Zero;
            decimal shippingToCharge = decimal.Zero;
            decimal shippingToRefund = decimal.Zero;
            decimal orderTotalChange = decimal.Zero;
            bool transFailed = false;

            //Added By Faisal on 31 Mar, 2016
            bool chargeDueAmount = false;
            ArrayList arrOrderNotes = new ArrayList();

            //Added By faisal on 04 May, 2016
            decimal prdAmtRefAfterShip = decimal.Zero;
            decimal totaltaxRefunded = decimal.Zero;
            decimal orderSubtotalInclTax = decimal.Zero;
            decimal orderSubtotalExclTax = decimal.Zero;

            foreach (OrderItem item in order.OrderItems)
            {
                foreach (string formKey in form.AllKeys)
                {
                    if (formKey.Equals(string.Format("txtAmount_{0}", item.Id), StringComparison.InvariantCultureIgnoreCase))
                    {
                        string txtAmount = "txtAmount_" + item.Id;
                        //string chkCharge = "hdnCharge_" + item.Id;
                        if (form[txtAmount] != null && form[txtAmount].ToString() != string.Empty)
                        //&& form[chkCharge] != null && form[chkCharge].ToString() != string.Empty)
                        {
                            //if (form[chkCharge] == "R")
                            //{
                            decimal itemRefundAmount = Convert.ToDecimal(form[txtAmount]);
                            item.RefundedProduct += itemRefundAmount;
                            item.RefundAfterShipping += itemRefundAmount;
                            amountToRefund += itemRefundAmount;

                            decimal taxRate = 0;
                            Math.Round(_taxService.GetProductPrice(item.Product, itemRefundAmount, true, order.Customer, out taxRate), 2);

                            //Changed By Faisal on 01 Jul, 2016
                            //arrOrderNotes.Add(_priceFormatter.FormatPrice(itemRefundAmount, true, false) + " has been refunded from " + item.Product.Name + " by " + _workContext.CurrentCustomer.GetFullName() + "~" + itemRefundAmount + "~" + Math.Round(itemRefundAmount * taxRate / 100, 2).ToString() + "~" + item.ProductId);
                            arrOrderNotes.Add(_priceFormatter.FormatPrice(itemRefundAmount, true, false) + " has been refunded from " + item.Product.Name + " by " + _workContext.CurrentCustomer.GetFullName() + "~" + itemRefundAmount + "~" + Math.Round(itemRefundAmount * taxRate / (100 + taxRate), 2).ToString() + "~" + item.ProductId);

                            //Added By faisal on 04 May, 2016
                            prdAmtRefAfterShip += itemRefundAmount;
                            item.TotalChargedAmount -= itemRefundAmount;
                            item.PriceInclTax -= itemRefundAmount;
                            
                            //Changed By Faisal on 01 Jul, 2016
                            //item.PriceExclTax -= (itemRefundAmount - Math.Round(itemRefundAmount * taxRate / 100, 2));
                            //totaltaxRefunded += Math.Round(itemRefundAmount * taxRate / 100, 2);

                            totaltaxRefunded += Math.Round(itemRefundAmount * taxRate / (100 + taxRate), 2);
                            item.PriceExclTax -= itemRefundAmount - Math.Round(itemRefundAmount * taxRate / (100 + taxRate), 2);
                            //End 01 Jul, 2016

                            //Added by Faisal On 01 Jul, 2016
                            if (item.PriceExclTax <= 0 || item.PriceInclTax <= 0)
                                item.PriceExclTax = 0;
                            //End 01 Jul, 2016

                            //}
                            //else
                            //    amountToCharge += Convert.ToDecimal(form[txtAmount]);
                        }

                    }
                }

                //Added By faisal on 04 May, 2016
                orderSubtotalInclTax += item.PriceInclTax;
                orderSubtotalExclTax += item.PriceExclTax;
            }

            foreach (string formKey in form.AllKeys)
            {
                if (formKey.Equals(string.Format("txtRefundShipping"), StringComparison.InvariantCultureIgnoreCase))
                {
                    if (form["txtRefundShipping"] != null && form["txtRefundShipping"].ToString() != string.Empty
                        && form["hdnRefundShipping"] != null && form["hdnRefundShipping"].ToString() != string.Empty)
                    {
                        if (form["hdnRefundShipping"] == "R")
                        {
                            //order.RefundedShipping += Convert.ToDecimal(form["txtRefundShipping"]);
                            //amountToRefund += Convert.ToDecimal(form["txtRefundShipping"]);
                            //shippingToRefund = Convert.ToDecimal(form["txtRefundShipping"]);

                            order.RefundedShipping += Convert.ToDecimal(form["txtRefundShipping"]);
                            shippingToRefund = Convert.ToDecimal(form["txtRefundShipping"]);

                            //02 Apr, 2016
                            //amountToRefund += Convert.ToDecimal(form["txtRefundShipping"]);
                            order.OrderShippingExclTax -= Convert.ToDecimal(form["txtRefundShipping"]);
                            order.OrderShippingInclTax -= Convert.ToDecimal(form["txtRefundShipping"]);
                        }
                        else
                        {
                            amountToCharge += Convert.ToDecimal(form["txtRefundShipping"]);
                            order.OrderShippingExclTax += Convert.ToDecimal(form["txtRefundShipping"]);
                            order.OrderShippingInclTax += Convert.ToDecimal(form["txtRefundShipping"]);
                            shippingToCharge = Convert.ToDecimal(form["txtRefundShipping"]);
                        }
                    }
                    //Added by Prachi on 5 July, 2016 - Code to charge/refund of shipping amount calculated when shipping address is changed
                    else if (order.OrderShippingInclTax != order.TotalShippingCharged)
                    {
                        var diff = order.OrderShippingInclTax - order.TotalShippingCharged;
                        if (diff < 0)
                        {
                            diff *= -1;
                            order.RefundedShipping += diff;
                            order.TotalShippingCharged -= diff;
                            shippingToRefund = diff;
                            order.OrderTotal += shippingToRefund;
                            order.TotalChargedAmount += shippingToRefund;
                        }
                        else {
                            //order.TotalChargedAmount += diff;
                            order.TotalShippingCharged += diff;
                            shippingToCharge = diff;
                        }
                    }
                    //End 5 July, 2016
                }
                
                if (formKey.Equals(string.Format("hdnAmountDue"), StringComparison.InvariantCultureIgnoreCase))
                {
                    if (form["hdnChkAmountDue"] != null && form["hdnChkAmountDue"].ToString() != string.Empty)
                    {
                        chargeDueAmount = true;
                        decimal val = Convert.ToDecimal(form["hdnAmountDue"]);
                        if (val < 0)
                            amountToRefund += (val * -1);
                        //else
                        //    amountToCharge += val;

                        orderTotalChange = val;


                        foreach (OrderItem item in order.OrderItems)
                        {
                            //decimal taxRate = 0;
                            //decimal prdTax = 0;
                            decimal difference = item.TotalChargedAmount - item.PriceInclTax;
                            string strNote = "";
                            if (difference > 0)
                            {
                                strNote = _priceFormatter.FormatPrice(difference, true, false) + " has been refunded from " + item.Product.Name + " by " + _workContext.CurrentCustomer.GetFullName();
                            }
                            else
                            {
                                difference *= -1;
                                strNote = _priceFormatter.FormatPrice(difference, true, false) + " has been charged for " + item.Product.Name + " by " + _workContext.CurrentCustomer.GetFullName();

                                //decimal refundAmt = item.RefundedProduct - difference;
                                //if (refundAmt < 0)
                                //    refundAmt = 0;
                                //item.RefundedProduct = refundAmt;
                            }

                            //decimal modVal = difference / item.UnitPriceInclTax;
                            //decimal taxVal = difference - (item.UnitPriceExclTax * Math.Ceiling(modVal));

                            decimal taxRate = 0;
                            Math.Round(_taxService.GetProductPrice(item.Product, difference, true, order.Customer, out taxRate), 2);
                            decimal taxVal = Math.Round(difference * taxRate / 100, 2);

                            //Math.Round(_taxService.GetProductPrice(item.Product, difference, true, order.Customer, out taxRate), 2);
                            //prdTax = Math.Round(difference * taxRate / 100, 2);

                            if (difference != 0)
                            {
                                order.OrderNotes.Add(new OrderNote
                                {
                                    Note = strNote,
                                    DisplayToCustomer = false,
                                    CreatedOnUtc = DateTime.UtcNow,
                                    Amount = difference,
                                    ProductTax = taxVal, //prdTax,
                                    ProductId = item.ProductId
                                });

                                _orderService.UpdateOrder(order);
                            }
                        }
                    }
                }
            }
           

            //order.RefundedAmount += amountToRefund;

            order.OrderTotal += amountToCharge;
            order.TotalChargedAmount += amountToCharge;
            //02 Apr, 2016
            order.OrderTotal -= shippingToRefund;
            order.TotalChargedAmount -= shippingToRefund;
            //if (amountToCharge > amountToRefund)
            //    order.TotalChargedAmount += (amountToCharge - amountToRefund);

            order.TotalChargedAmount += orderTotalChange;

            //Added By faisal on 04 May, 2016
            order.TotalChargedAmount -= prdAmtRefAfterShip;
            order.OrderTotal -= prdAmtRefAfterShip;
            order.OrderTax -= totaltaxRefunded;
            order.OrderSubtotalExclTax = orderSubtotalExclTax;
            order.OrderSubtotalInclTax = orderSubtotalInclTax;

            //Added By Faisal on 04 Jul, 2016
            if (order.OrderTax < 0)
                order.OrderTax = 0;

            decimal actualChargeAmount = amountToCharge;
            if (orderTotalChange > 0)
                actualChargeAmount += orderTotalChange;

            if (actualChargeAmount != (amountToRefund + shippingToRefund) && !offline)
            {
                decimal amount = 0;
                if (actualChargeAmount > (amountToRefund + shippingToRefund))
                    amount = actualChargeAmount - (amountToRefund + shippingToRefund);
                else
                    amount = (amountToRefund + shippingToRefund) - actualChargeAmount;

                //Process Payment Start
                ProcessPaymentRequest paymentInfo = new ProcessPaymentRequest();
                paymentInfo.CreditCardCvv2 = _encryptionService.DecryptText(order.CardCvv2);
                paymentInfo.CreditCardExpireMonth = Convert.ToInt32(_encryptionService.DecryptText(order.CardExpirationMonth));
                paymentInfo.CreditCardExpireYear = Convert.ToInt32(_encryptionService.DecryptText(order.CardExpirationYear));
                paymentInfo.CreditCardName = _encryptionService.DecryptText(order.CardName);
                paymentInfo.CreditCardNumber = _encryptionService.DecryptText(order.CardNumber);
                paymentInfo.CreditCardType = _encryptionService.DecryptText(order.CardType);
                paymentInfo.CustomerId = order.CustomerId;
                paymentInfo.StoreId = order.StoreId;
                paymentInfo.OrderGuid = order.OrderGuid;
                paymentInfo.OrderTotal = amount;//(amountToCharge - amountToRefund);
                paymentInfo.InitialOrderId = order.Id;
                //Changed By Prachi on 27 June, 2016
                //paymentInfo.PaymentMethodSystemName = order.PaymentMethodSystemName;
                paymentInfo.PaymentMethodSystemName = "Payments.Cybersource";

                try
                {
                    var errors = _orderProcessingService.RefundOrCharge(order, amount, paymentInfo, (actualChargeAmount < (amountToRefund + shippingToRefund)));
                    foreach (var error in errors)
                        ErrorNotification(error, false);
                    if (errors.Count > 0)
                        transFailed = true;
                }
                catch (Exception exc)
                {
                    transFailed = true;
                    ErrorNotification(exc, false);
                }
                //Process Payment End
            }
            else if (offline)
            {
                if (actualChargeAmount < (amountToRefund + shippingToRefund))
                    order.PaymentStatus = PaymentStatus.PartiallyRefunded;
            }

            //For any exception or error, need to rollback all the changes made above for the order
            if (transFailed)
                RollBackOrderDetails(ref order, form, amountToCharge, amountToRefund, shippingToCharge, shippingToRefund, orderTotalChange);
            //Added By Faisal on 31 Mar, 2016
            else if (chargeDueAmount)
            {
                foreach (OrderItem item in order.OrderItems)
                {
                    if (item.PriceInclTax > item.TotalChargedAmount)
                        item.TotalChargedAmount = item.PriceInclTax;

                    if (item.TotalChargedAmount - item.PriceInclTax > 0)
                    {
                        item.RefundedProduct += (item.TotalChargedAmount - item.PriceInclTax);
                        item.TotalChargedAmount = item.PriceInclTax;
                    }
                    //else
                    //    item.RefundedProduct = 0;
                }
            }

            if (!transFailed)
            {
                if (order.OrderTotal == order.TotalChargedAmount)
                    order.PaymentStatus = PaymentStatus.Paid;

                decimal orderRefAmt = order.RefundedAmount + order.RefundedShipping;
                foreach (OrderItem item in order.OrderItems)
                    orderRefAmt += item.RefundedProduct;

                if(orderRefAmt > 0)
                    order.PaymentStatus = PaymentStatus.PartiallyRefunded;

                foreach (string str in arrOrderNotes)
                {
                    order.OrderNotes.Add(new OrderNote
                    {
                        Note = str.Split('~')[0],
                        DisplayToCustomer = false,
                        CreatedOnUtc = DateTime.UtcNow,
                        Amount = Convert.ToDecimal(str.Split('~')[1]),
                        ProductTax = Convert.ToDecimal(str.Split('~')[2]),
                        ProductId = Convert.ToInt32(str.Split('~')[3])
                    });
                    _orderService.UpdateOrder(order);
                }

                string strShipping = "";
                if (shippingToCharge > 0)
                    strShipping = "charged for shipping";
                else if (shippingToRefund > 0)
                    strShipping = "refunded from shipping";

                if (strShipping.Trim() != string.Empty)
                {
                    order.OrderNotes.Add(new OrderNote
                    {
                        Note = _priceFormatter.FormatPrice((shippingToCharge > 0 ? shippingToCharge : shippingToRefund), true, false) + " has been " + strShipping + " by " + _workContext.CurrentCustomer.GetFullName(),
                        DisplayToCustomer = false,
                        CreatedOnUtc = DateTime.UtcNow,
                        Amount = shippingToCharge > 0 ? shippingToCharge : shippingToRefund
                    });
                    _orderService.UpdateOrder(order);
                }
            }

            //Added by Faisal On 26 May, 2016
            if (order.OrderTotal <= 0 && order.TotalChargedAmount <= 0)
                order.PaymentStatus = PaymentStatus.Refunded;
            //End - 26 May, 2016

            _orderService.UpdateOrder(order);
            //End 31 Mar, 2016

            PrepareOrderDetailsModel(orderModel, order);
            SaveSelectedTabIndex(persistForTheNextRequest: false);
        }

        public void RollBackOrderDetails(ref Order order, FormCollection form, decimal amountToCharge, decimal amountToRefund, decimal shippingToCharge, decimal shippingToRefund, decimal orderTotalChange)
        {
            //Added By faisal on 04 May, 2016
            decimal prdAmtRefAfterShip = decimal.Zero;
            decimal totaltaxRefunded = decimal.Zero;
            decimal orderSubtotalInclTax = decimal.Zero;
            decimal orderSubtotalExclTax = decimal.Zero;

            foreach (OrderItem item in order.OrderItems)
            {
                foreach (string formKey in form.AllKeys)
                {
                    if (formKey.Equals(string.Format("txtAmount_{0}", item.Id), StringComparison.InvariantCultureIgnoreCase))
                    {
                        string txtAmount = "txtAmount_" + item.Id;
                        if (form[txtAmount] != null && form[txtAmount].ToString() != string.Empty)
                        {
                            item.RefundedProduct -= Convert.ToDecimal(form[txtAmount]);
                            item.RefundAfterShipping -= Convert.ToDecimal(form[txtAmount]);

                            //Added By faisal on 04 May, 2016
                            decimal taxRate = 0;
                            decimal itemRefundAmount = Convert.ToDecimal(form[txtAmount]);
                            Math.Round(_taxService.GetProductPrice(item.Product, itemRefundAmount, true, order.Customer, out taxRate), 2);

                            prdAmtRefAfterShip += itemRefundAmount;
                            item.TotalChargedAmount += itemRefundAmount;
                            item.PriceInclTax += itemRefundAmount;

                            //Changed By Faisal on 01 Jul, 2016
                            //item.PriceExclTax += (itemRefundAmount - Math.Round(itemRefundAmount * taxRate / 100, 2));
                            //totaltaxRefunded += Math.Round(itemRefundAmount * taxRate / 100, 2);

                            item.PriceExclTax += (itemRefundAmount - Math.Round(itemRefundAmount * taxRate / (100 + taxRate), 2));
                            totaltaxRefunded += Math.Round(itemRefundAmount * taxRate / (100 + taxRate), 2);
                            //End 01 Jul, 2016
                        }
                    }
                }
                //Added By faisal on 04 May, 2016
                orderSubtotalInclTax += item.PriceInclTax;
                orderSubtotalExclTax += item.PriceExclTax;
            }
            //Added By faisal on 04 May, 2016
            order.TotalChargedAmount += prdAmtRefAfterShip;
            order.OrderTotal += prdAmtRefAfterShip;
            order.OrderTax += totaltaxRefunded;
            order.OrderSubtotalExclTax = orderSubtotalExclTax;
            order.OrderSubtotalInclTax = orderSubtotalInclTax;

            order.OrderShippingExclTax -= shippingToCharge;
            order.OrderShippingInclTax -= shippingToCharge;
            order.RefundedShipping -= shippingToRefund;

            order.RefundedAmount -= amountToRefund;
            order.OrderTotal -= amountToCharge;
            order.TotalChargedAmount -= amountToCharge;
            order.TotalChargedAmount -= orderTotalChange;

            //02 Apr, 2016
            order.OrderTotal += shippingToRefund;
            order.TotalChargedAmount += shippingToRefund;
            order.OrderShippingExclTax += shippingToRefund;
            order.OrderShippingInclTax += shippingToRefund;
            //if (amountToCharge > amountToRefund)
            //    order.TotalChargedAmount -= (amountToCharge - amountToRefund);

            _orderService.UpdateOrder(order);
        }
        //End 03 Mar, 2016

        //Changes by Prachi on 4 March, 2016- Method added to split the order
        [HttpPost, ActionName("Edit")]
        [FormValueRequired(FormValueRequirement.Equal, "btnSplitOrder")]
        [ValidateInput(false)]
        public ActionResult SplitOrder(int id, FormCollection form)
        {
            var order = _orderService.GetOrderById(id);
            if (order == null || order.Deleted || order.Customer.Id != order.CustomerId)
                return new HttpUnauthorizedResult();

            OrderModel orderModel = new OrderModel();

            IList<ShoppingCartItem> shoppingCartItems = order.Customer.ShoppingCartItems.ToList();
            for (int i = 0; i < shoppingCartItems.Count; i++)
            {
                _shoppingCartService.DeleteShoppingCartItem(shoppingCartItems[i]);
            }

            Dictionary<string, int> orderItemsRemoved = new Dictionary<string, int>();

            foreach (var orderItem in order.OrderItems)
            {
                bool SplitProductSelected = false;
                int quantity = 0; //parse quantity
                foreach (string formKey in form.AllKeys)
                {

                    if (formKey.Equals(string.Format("chk{0}", orderItem.Id), StringComparison.InvariantCultureIgnoreCase))
                    {
                        if (form[formKey] != null)
                        {
                            SplitProductSelected = true;
                        }
                    }
                    if (formKey.Equals(string.Format("splitQuantity{0}", orderItem.Id), StringComparison.InvariantCultureIgnoreCase))
                    {
                        if (SplitProductSelected == true)
                        {
                            int.TryParse(form[formKey], out quantity);
                            break;
                        }
                    }
                }

                if (quantity > 0)
                {
                    _shoppingCartService.AddToCart(order.Customer,
                        orderItem.Product, ShoppingCartType.ShoppingCart, order.StoreId,
                        orderItem.AttributesXml, 0, null, null, quantity);

                    orderItemsRemoved.Add(orderItem.Product.Name, quantity);
                }
            }

            ShippingOption shippingOption = new ShippingOption();
            shippingOption.ShippingRateComputationMethodSystemName = order.ShippingRateComputationMethodSystemName;
            shippingOption.Name = order.ShippingMethod;

            _genericAttributeService.SaveAttribute<ShippingOption>(order.Customer,
               SystemCustomerAttributeNames.SelectedShippingOption, shippingOption, order.StoreId);           

            ProcessPaymentRequest paymentInfo = new ProcessPaymentRequest();

            paymentInfo.CreditCardCvv2 = _encryptionService.DecryptText(order.CardCvv2);
            paymentInfo.CreditCardExpireMonth = Convert.ToInt32(order.CardExpirationMonth == null || order.CardExpirationMonth == "" ? "0" : _encryptionService.DecryptText(order.CardExpirationMonth));
            paymentInfo.CreditCardExpireYear = Convert.ToInt32(order.CardExpirationYear == null || order.CardExpirationYear == "" ? "0" : _encryptionService.DecryptText(order.CardExpirationYear));
            paymentInfo.CreditCardName = _encryptionService.DecryptText(order.CardName);
            paymentInfo.CreditCardNumber = _encryptionService.DecryptText(order.CardNumber);
            paymentInfo.CreditCardType = _encryptionService.DecryptText(order.CardType);
            paymentInfo.CustomerId = order.CustomerId;
            paymentInfo.StoreId = order.StoreId;
            paymentInfo.OrderGuid = order.OrderGuid;
            paymentInfo.InitialOrderId = order.Id;
            paymentInfo.PaymentMethodSystemName = order.PaymentMethodSystemName;
            paymentInfo.IsRecurringPayment = false;

            var placeOrderResult = _orderProcessingService.PlaceOrder(paymentInfo, false, true);
            if (placeOrderResult.Success)
            {
                var postProcessPaymentRequest = new PostProcessPaymentRequest
                {
                    Order = placeOrderResult.PlacedOrder
                };
                _paymentService.PostProcessPayment(postProcessPaymentRequest);

                int reshippedId = placeOrderResult.PlacedOrder.Id;

                var splittedOrder = _orderService.GetOrderById(reshippedId);
                splittedOrder.OrderShippingExclTax = 0;
                splittedOrder.OrderShippingInclTax = 0;
                splittedOrder.CustomValuesXml = order.CustomValuesXml;
                
                //Added By Faisal On 03 Jun, 2016
                splittedOrder.AllowStoringCreditCardNumber = order.AllowStoringCreditCardNumber;
                splittedOrder.CardType = order.CardType;
                splittedOrder.CardName = order.CardName;
                splittedOrder.CardNumber = order.CardNumber;
                splittedOrder.MaskedCreditCardNumber = order.MaskedCreditCardNumber;
                splittedOrder.CardCvv2 = order.CardCvv2;
                splittedOrder.CardExpirationMonth = order.CardExpirationMonth;
                splittedOrder.CardExpirationYear = order.CardExpirationYear;
                splittedOrder.PaymentMethodSystemName = order.PaymentMethodSystemName;
                splittedOrder.AuthorizationTransactionId = order.AuthorizationTransactionId;
                splittedOrder.AuthorizationTransactionCode = order.AuthorizationTransactionCode;
                splittedOrder.AuthorizationTransactionResult = order.AuthorizationTransactionResult;
                splittedOrder.CaptureTransactionId = order.CaptureTransactionId;
                splittedOrder.CaptureTransactionResult = order.CaptureTransactionResult;
                splittedOrder.SubscriptionTransactionId = order.SubscriptionTransactionId;
                splittedOrder.PaymentStatus = order.PaymentStatus;
                splittedOrder.PaidDateUtc = order.PaidDateUtc;
                splittedOrder.PaymentStatus = order.PaymentStatus;
                splittedOrder.PaymentType = order.PaymentType;
                splittedOrder.PaymentRemarks = order.PaymentRemarks;
                //End

                splittedOrder.OrderNotes.Add(new OrderNote
                {
                    Note = "Order Created by splitting Order #" + id,
                    DisplayToCustomer = false,
                    CreatedOnUtc = DateTime.UtcNow
                });
                CalculateOrderTotals(splittedOrder);

                order.OrderNotes.Add(new OrderNote
                {
                    Note = "Splitted order. New Order #" + reshippedId,
                    DisplayToCustomer = false,
                    CreatedOnUtc = DateTime.UtcNow
                });


                //Added By Faisal on 08 Jun, 2016
                decimal totalCharged = 0;
                foreach (OrderItem splitItem in splittedOrder.OrderItems)
                {
                    foreach (OrderItem parentItem in order.OrderItems)
                    {
                        if (splitItem.ProductId == parentItem.ProductId)
                        {           
                            //discount for single quantity
                            var DiscountAmountExclTax = (parentItem.DiscountAmountExclTax) / (parentItem.Quantity);
                            var DiscountAmountInclTax = (parentItem.DiscountAmountInclTax) / (parentItem.Quantity);

                            //same SelectedWarehouse
                            splitItem.SelectedWarehouse = parentItem.SelectedWarehouse;
                            
                            //var splittedDiscountAmountExclTax = (parentItem.DiscountAmountExclTax) / (splitItem.Quantity);
                            //var splittedDiscountAmountInclTax = (parentItem.DiscountAmountInclTax) / (splitItem.Quantity);
                         
                            if (parentItem.Quantity != splitItem.Quantity)
                            {
                                //Splitted Item calculation
                                splitItem.DiscountAmountExclTax = Math.Round(splitItem.Quantity * DiscountAmountExclTax, 2);
                                splitItem.DiscountAmountInclTax = Math.Round(splitItem.Quantity * DiscountAmountInclTax, 2);

                                splitItem.PriceExclTax = Math.Round(splitItem.UnitPriceExclTax * splitItem.Quantity, 2);
                                splitItem.PriceInclTax = Math.Round(splitItem.UnitPriceInclTax * splitItem.Quantity, 2);
                                
                                if (splitItem.PriceInclTax >= parentItem.TotalChargedAmount)
                                {
                                    splitItem.TotalChargedAmount = parentItem.TotalChargedAmount;
                                    parentItem.TotalChargedAmount = 0;
                                }
                                else if (splitItem.PriceInclTax < parentItem.TotalChargedAmount)
                                {
                                    splitItem.TotalChargedAmount = splitItem.PriceInclTax;
                                    parentItem.TotalChargedAmount -= splitItem.TotalChargedAmount;
                                }

                                //Order Item calculation
                                parentItem.DiscountAmountExclTax -= splitItem.DiscountAmountExclTax;
                                parentItem.DiscountAmountInclTax -= splitItem.DiscountAmountInclTax;

                                parentItem.PriceExclTax -= splitItem.PriceExclTax;
                                parentItem.PriceInclTax -= splitItem.PriceInclTax;
                            }
                            else {
                                //Splitted Item calculation
                                splitItem.DiscountAmountExclTax = parentItem.DiscountAmountExclTax;
                                splitItem.DiscountAmountInclTax = parentItem.DiscountAmountInclTax;
                                splitItem.PriceExclTax = parentItem.PriceExclTax;
                                splitItem.PriceInclTax = parentItem.PriceInclTax;
                                splitItem.TotalChargedAmount = parentItem.TotalChargedAmount;

                                //Order Item calculation
                                parentItem.DiscountAmountExclTax = 0;
                                parentItem.DiscountAmountInclTax = 0;
                                parentItem.PriceExclTax = 0;
                                parentItem.PriceInclTax = 0;
                                parentItem.TotalChargedAmount = 0;
                            }

                            parentItem.Quantity -= splitItem.Quantity;
                            totalCharged += splitItem.TotalChargedAmount;

                            break;
                        }
                    }
                }

                CalculateOrderTotals(splittedOrder);
                CalculateOrderTotals(order);

                //if (order.TotalChargedAmount != totalCharged)
                order.TotalChargedAmount -= totalCharged;
                splittedOrder.TotalChargedAmount = totalCharged;

                _orderService.UpdateOrder(order);
                _orderService.UpdateOrder(splittedOrder);
                //End 08 Jun, 2016

                PrepareOrderDetailsModel(orderModel, order);

                SuccessNotification("Order has been splitted into new order #" + splittedOrder.Id);
            }
            else
            {
                string strError = string.Empty;
                foreach (var error in placeOrderResult.Errors)
                    strError += error;

                ErrorNotification(strError);
            }
            return RedirectToAction("Edit", "Order", new { id = id });
        }

        //Added by Prachi on 22 March, 2016- Code added to update stock back into inventory if quantity of order item is updated
        public int UpdateStock(int orderItemId, int Quantity, bool isDelete = false)
        {
            int WarehouseId = 0;
            int shipmentId = 0;
            //ShipmentItem si = null;
            OrderItem oi = _orderService.GetOrderItemById(orderItemId);
            Product product = oi.Product;
            int orderId = oi.OrderId;
            Order order = oi.Order;
            //Added by Prachi On 28 June, 2016
            var attributesXml = oi.AttributesXml;
            WarehouseId = oi.SelectedWarehouse;
            //End

            //if (order.Shipments.Count > 0)
            //{
            //    foreach (var shipment in order.Shipments)
            //    {
            //        foreach (var item in shipment.ShipmentItems)
            //        {
            //            if (item.OrderItemId == oi.Id && shipment.ShippedDateUtc != null)
            //            {
            //                shipmentId = item.Id;
            //            }
            //        }
            //    }
            //}

            //if (shipmentId > 0)
            //    si = _shipmentService.GetShipmentItemById(shipmentId);
            //else
            //    WarehouseId = product.WarehouseId;

            //Changed by Prachi On 28 June, 2016
            if (WarehouseId > 0)
            {
                var existingPwI = product.ProductWarehouseInventory.FirstOrDefault(x => x.WarehouseId == WarehouseId);
                if (existingPwI != null)
                {
                    //update existing record
                    //if (Quantity > 0)
                    //    existingPwI.ReservedQuantity = existingPwI.ReservedQuantity + Quantity;
                    //else
                    //if (!isDelete && Quantity < 0 && shipmentId > 0)
                    //{
                    //    if (si.Quantity != oi.Quantity)
                    //        existingPwI.ReservedQuantity = existingPwI.ReservedQuantity + Quantity;
                    //    else
                    //    {
                    //        existingPwI.StockQuantity = existingPwI.StockQuantity + ((-1) * Quantity);
                    //        existingPwI.ReservedQuantity = existingPwI.ReservedQuantity + ((-1) * Quantity);
                    //    }
                    //}
                    //else 
                    if (!isDelete && Quantity > 0)
                    {
                        existingPwI.ReservedQuantity = existingPwI.ReservedQuantity + Quantity;
                    }
                    else if (!isDelete && shipmentId == 0 && Quantity < 0)
                    {
                        existingPwI.ReservedQuantity = (existingPwI.ReservedQuantity + Quantity < 0 ? 0 : existingPwI.ReservedQuantity + Quantity);
                    }
                    //else if (isDelete && shipmentId > 0)
                    //{
                    //    existingPwI.StockQuantity = existingPwI.StockQuantity + Quantity;
                    //}
                    else if (isDelete && shipmentId == 0)
                    {
                        existingPwI.ReservedQuantity = (existingPwI.ReservedQuantity - Quantity < 0 ? 0 : existingPwI.ReservedQuantity - Quantity);
                    }

                    _productService.UpdateProduct(product);
                    ////update existing record
                    ////if (Quantity > 0)
                    ////    existingPwI.ReservedQuantity = existingPwI.ReservedQuantity + Quantity;
                    ////else
                    //if (Quantity < 0 && shipmentId > 0)
                    //{
                    //    existingPwI.StockQuantity = existingPwI.StockQuantity + Quantity;
                    //    _productService.UpdateProduct(product);
                    //    //}
                    //    //else if (Quantity < 0 && shipmentId == 0)
                    //    //{
                    //    //    existingPwI.ReservedQuantity = existingPwI.ReservedQuantity - Quantity;
                    //    //}   
                    //}
                    //else if (Quantity > 0 && shipmentId > 0)
                    //{
                    //    existingPwI.ReservedQuantity = existingPwI.ReservedQuantity + Quantity;
                    //    _productService.UpdateProduct(product);
                    //    //}
                    //}
                }
            }
            else if ((WarehouseId == 0 && Quantity < 0) || Quantity > 0 || (Quantity < 0 && shipmentId == 0))
                WarehouseId = _productService.AdjustInventory(product, -(Quantity), attributesXml);

            //Added by Prachi On 28 June, 2016
            if (product.ManageInventoryMethod == ManageInventoryMethod.ManageStockByAttributes)
            {
                var combination = _productAttributeParser.FindProductAttributeCombination(product, attributesXml);
                if (combination != null)
                {
                    combination.StockQuantity -= Quantity;
                    _productAttributeService.UpdateProductAttributeCombination(combination);
                }
            }

            return WarehouseId;
            //End
        }

        public bool IsQuantityAvailable(int Quantity, int ShippedQuantity, Product product, int OrderItemId)
        {
            //Added By Prachi on 22 March, 2016
            //var itemsFromMultipleWarehouses = 0;

            //if (product.UseMultipleWarehouses)
            //{
            //    foreach (var pwi in product.ProductWarehouseInventory)
            //    {
            //        //if (pwi.StockQuantity >= 0)
            //        //    itemsFromMultipleWarehouses += pwi.ReservedQuantity;
            //        //else if (pwi.StockQuantity < 0)
            //        itemsFromMultipleWarehouses += (pwi.StockQuantity - (pwi.ReservedQuantity - Quantity));
            //    }
            //}
            //else
            //{
            //    itemsFromMultipleWarehouses += product.StockQuantity;
            //}

            //if (itemsFromMultipleWarehouses >= 0)
            //{
            //    if (itemsFromMultipleWarehouses >= Quantity)
            //        //    && (ShippedQuantity != 0 || ShippedQuantity != itemsFromMultipleWarehouses))
            //        return true;
            //    else
            //        return false;
            //}
            //else if (product.ManageInventoryMethod == ManageInventoryMethod.DontManageStock)
            //{
            //    return true;
            //}
            //else
            //    return false;
            //End 22 March, 2016

            //Added By Prachi on 30 June, 2016
            bool isQuantityAvailable = false;
            var orderitem = _orderService.GetOrderItemById(OrderItemId);
            int warehouseId = orderitem.SelectedWarehouse;
            int AvailableQty = 0;
            int StockQty = 0;
            int ReservedQty = 0;
            int AdjustedQty = 0;
            var order = orderitem.Order;
            if (product.UseMultipleWarehouses && warehouseId > 0)
            {
                if (product.ProductWarehouseInventory_OpeningAdjustment.FirstOrDefault(pwia => pwia.WarehouseId == warehouseId) != null)
                    AdjustedQty = product.ProductWarehouseInventory_OpeningAdjustment.FirstOrDefault(pwia => pwia.WarehouseId == warehouseId).AdjustmentQuantity;

                StockQty = product.ProductWarehouseInventory.FirstOrDefault(pwia => pwia.WarehouseId == warehouseId).StockQuantity;
                AvailableQty = AdjustedQty + StockQty;

                List<Order> ordList = _orderService.SearchOrders(order.StoreId, 0, 0, orderitem.ProductId).
                    Where(ord => ord.CreatedOnUtc < order.CreatedOnUtc && ord.Id != order.Id).ToList();
                foreach (var ord in ordList)
                {
                    foreach (var ordItem in ord.OrderItems)
                    {
                        if (ordItem.ProductId == product.Id && ordItem.SelectedWarehouse == warehouseId)
                        {
                            ReservedQty += ordItem.Quantity;
                            foreach (var shipment in ord.Shipments)
                            {
                                var si = shipment.ShipmentItems.Where(shpi => shpi.OrderItemId == ordItem.Id).FirstOrDefault();
                                if (ReservedQty > 0 && si != null && shipment.ShippedDateUtc != null)
                                {
                                    ReservedQty -= si.Quantity;
                                }
                            }
                        }
                    }
                }
                AvailableQty -= ReservedQty;
            }
            else
            {
                AvailableQty = product.StockQuantity;
            }

            if (Quantity == ShippedQuantity)
            {
                isQuantityAvailable = true;
            }
            else if (AvailableQty > 0)
            {
                if (AvailableQty >= Quantity)
                    isQuantityAvailable = true;
                else
                    isQuantityAvailable = false;
            }
            else 
            {
                if (product.ManageInventoryMethodId == 0)
                    isQuantityAvailable = true;
                else
                    isQuantityAvailable = false;
            }

            return isQuantityAvailable;
            //End 30 June, 2016
        }

        [HttpPost, ActionName("ShipmentDetails")]
        [FormValueRequired("setactualshipdate")]
        public ActionResult SetActualShipDate(int id, ShipmentModel model)
        {
            Shipment shipment = _shipmentService.GetShipmentById(id);
            if (shipment == null)
                //No order found with the specified id
                return RedirectToAction("List");

            shipment.ActualDateShipped = model.ActualDateShipped;
            //shipment.ActualShipCost = model.ActualShipCost;
            _shipmentService.UpdateShipment(shipment);

            //SuccessNotification("Shipping Data has been successfully updated.");
            return RedirectToAction("ShipmentDetails", new { id = shipment.Id });
        }

        [HttpPost, ActionName("ShipmentDetails")]
        [FormValueRequired("setactualshipcost")]
        public ActionResult SetActualShipCost(int id, ShipmentModel model)
        {
            Shipment shipment = _shipmentService.GetShipmentById(id);
            if (shipment == null)
                //No order found with the specified id
                return RedirectToAction("List");

            //shipment.ActualDateShipped = model.ActualDateShipped;
            shipment.ActualShipCost = model.ActualShipCost;
            _shipmentService.UpdateShipment(shipment);

            //SuccessNotification("Shipping Data has been successfully updated.");
            return RedirectToAction("ShipmentDetails", new { id = shipment.Id });
        }

        public string CheckNullOrEmpty(string val)
        {
            if (string.IsNullOrEmpty(val))
                return string.Empty;
            else
                return ", " + val;
        }
    }
}
