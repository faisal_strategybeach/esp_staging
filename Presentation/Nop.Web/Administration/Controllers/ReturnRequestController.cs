﻿using Nop.Admin.Models.Orders;
using Nop.Core;
using Nop.Core.Domain.Catalog;
using Nop.Core.Domain.Customers;
using Nop.Core.Domain.Localization;
using Nop.Core.Domain.Orders;
using Nop.Services.Customers;
using Nop.Services.Helpers;
using Nop.Services.Localization;
using Nop.Services.Logging;
using Nop.Services.Messages;
using Nop.Services.Orders;
using Nop.Services.Security;
using Nop.Web.Framework.Controllers;
using Nop.Web.Framework.Kendoui;
using System;
using System.Collections.Generic;
using System.Web.Mvc;
using System.Linq;
using Nop.Services.Catalog;
using Nop.Services.Payments;

namespace Nop.Admin.Controllers
{
    public partial class ReturnRequestController : BaseAdminController
    {
        #region Fields

        private readonly IOrderService _orderService;
        private readonly IDateTimeHelper _dateTimeHelper;
        private readonly ICustomerService _customerService;
        private readonly ILocalizationService _localizationService;
        private readonly IWorkContext _workContext;
        private readonly IWorkflowMessageService _workflowMessageService;
        private readonly LocalizationSettings _localizationSettings;
        private readonly ICustomerActivityService _customerActivityService;
        private readonly IPermissionService _permissionService;
        //Added by Prachi on 18 March, 2016
        private readonly IProductService _productService;
        //Added by Prachi on 11 April, 2016
        private readonly IEncryptionService _encryptionService;
        private readonly IOrderProcessingService _orderProcessingService;
		
		//Added By faisal on 28 Apr, 2016
        private readonly IPriceFormatter _priceFormatter;

        //Added By Prachi on 27 June, 2016
        private readonly IProductAttributeParser _productAttributeParser;
        private readonly IProductAttributeService _productAttributeService;

        #endregion Fields

        #region Constructors

        public ReturnRequestController(IOrderService orderService,
            ICustomerService customerService, IDateTimeHelper dateTimeHelper,
            ILocalizationService localizationService, IWorkContext workContext,
            IWorkflowMessageService workflowMessageService, LocalizationSettings localizationSettings,
            ICustomerActivityService customerActivityService, IPermissionService permissionService,
            //Added by Prachi on 18 March, 2016
            IProductService productService,
            //Added by Prachi on 11 April, 2016
            IEncryptionService encryptionService,
            IOrderProcessingService orderProcessingService
            
            //Added By Faisal On 28 Apr, 2016
            , IPriceFormatter priceFormatter,
            //Added By Prachi on 27 June, 2016
            IProductAttributeParser productAttributeParser,
            IProductAttributeService productAttributeService)
        {
            this._orderService = orderService;
            this._customerService = customerService;
            this._dateTimeHelper = dateTimeHelper;
            this._localizationService = localizationService;
            this._workContext = workContext;
            this._workflowMessageService = workflowMessageService;
            this._localizationSettings = localizationSettings;
            this._customerActivityService = customerActivityService;
            this._permissionService = permissionService;
            //Added by Prachi on 18 March, 2016
            this._productService = productService;
            //Added by Prachi on 11 April, 2016
            this._encryptionService = encryptionService;
            this._orderProcessingService = orderProcessingService;
            
            //Added By Faisal On 28 Apr, 2016
            this._priceFormatter = priceFormatter;

            //Added By Prachi on 27 June, 2016
            this._productAttributeParser = productAttributeParser;
            this._productAttributeService = productAttributeService;
        }

        #endregion

        #region Utilities

        [NonAction]
        protected virtual bool PrepareReturnRequestModel(ReturnRequestModel model,
            ReturnRequest returnRequest, bool excludeProperties)
        {
            if (model == null)
                throw new ArgumentNullException("model");

            if (returnRequest == null)
                throw new ArgumentNullException("returnRequest");

            var orderItem = _orderService.GetOrderItemById(returnRequest.OrderItemId);
            if (orderItem == null)
                return false;

            model.Id = returnRequest.Id;
            model.ProductId = orderItem.ProductId;
            model.ProductName = orderItem.Product.Name;
            model.OrderId = orderItem.OrderId;
            model.CustomerId = returnRequest.CustomerId;
            var customer = returnRequest.Customer;
            model.CustomerInfo = customer.IsRegistered() ? customer.Email : _localizationService.GetResource("Admin.Customers.Guest");
            model.Quantity = returnRequest.Quantity;
            model.ReturnRequestStatusStr = returnRequest.ReturnRequestStatus.GetLocalizedEnum(_localizationService, _workContext);
            model.CreatedOn = _dateTimeHelper.ConvertToUserTime(returnRequest.CreatedOnUtc, DateTimeKind.Utc);
            //Added by Prachi on 13 April, 2016
            model.ReturnStatus = returnRequest.ReturnStatus;
            if (!excludeProperties)
            {
                model.ReasonForReturn = returnRequest.ReasonForReturn;
                model.RequestedAction = returnRequest.RequestedAction;
                model.CustomerComments = returnRequest.CustomerComments;
                model.StaffNotes = returnRequest.StaffNotes;
                model.ReturnRequestStatusId = returnRequest.ReturnRequestStatusId;
            }
         
            //model is successfully prepared
            return true;
        }

        #endregion

        #region Methods

        //list
        public ActionResult Index()
        {
            return RedirectToAction("List");
        }

        public ActionResult List()
        {
            if (!_permissionService.Authorize(StandardPermissionProvider.ManageReturnRequests))
                return AccessDeniedView();

            return View();
        }

        [HttpPost]
        public ActionResult List(DataSourceRequest command)
        {
            if (!_permissionService.Authorize(StandardPermissionProvider.ManageReturnRequests))
                return AccessDeniedView();

            var returnRequests = _orderService.SearchReturnRequests(0, 0, 0, null, command.Page - 1, command.PageSize);
            var returnRequestModels = new List<ReturnRequestModel>();
            foreach (var rr in returnRequests)
            {
                var m = new ReturnRequestModel();
                if (PrepareReturnRequestModel(m, rr, false))
                    returnRequestModels.Add(m);
            }
            var gridModel = new DataSourceResult
            {
                Data = returnRequestModels,
                Total = returnRequests.TotalCount,
            };

            return Json(gridModel);
        }

        //edit
        public ActionResult Edit(int id)
        {
            if (!_permissionService.Authorize(StandardPermissionProvider.ManageReturnRequests))
                return AccessDeniedView();

            var returnRequest = _orderService.GetReturnRequestById(id);
            if (returnRequest == null)
                //No return request found with the specified id
                return RedirectToAction("List");

            var model = new ReturnRequestModel();
            PrepareReturnRequestModel(model, returnRequest, false);
            return View(model);
        }

        [HttpPost, ParameterBasedOnFormName("save-continue", "continueEditing")]
        [FormValueRequired("save", "save-continue")]
        public ActionResult Edit(ReturnRequestModel model, bool continueEditing)
        {
            if (!_permissionService.Authorize(StandardPermissionProvider.ManageReturnRequests))
                return AccessDeniedView();

            var returnRequest = _orderService.GetReturnRequestById(model.Id);
            if (returnRequest == null)
                //No return request found with the specified id
                return RedirectToAction("List");

            if (ModelState.IsValid)
            {
                returnRequest.Quantity = model.Quantity;
                returnRequest.ReasonForReturn = model.ReasonForReturn;
                returnRequest.RequestedAction = model.RequestedAction;
                returnRequest.CustomerComments = model.CustomerComments;
                returnRequest.StaffNotes = model.StaffNotes;
                returnRequest.ReturnRequestStatusId = (model.ReturnRequestStatusId == 0 && returnRequest.ReturnRequestStatusId == 10 ?
                        returnRequest.ReturnRequestStatusId : model.ReturnRequestStatusId);
                returnRequest.UpdatedOnUtc = DateTime.UtcNow;

                //Added by Prachi on 18 March, 2016- Code added to update stock back into inventory if return status is "Received" (ReturnRequestStatusId==10)
                
                Order order = _orderService.GetOrderById(model.OrderId);
                OrderItem oi = _orderService.GetOrderItemById(returnRequest.OrderItemId);
                decimal totalRefundedeAmount = Math.Round(oi.UnitPriceInclTax * model.Quantity, 2);

                if (model.ReturnRequestStatusId == 10)
                {
                    UpdateStockQuantity(returnRequest.OrderItemId, model);
                }//End
                else if (model.ReturnRequestStatusId == 40)
                {
                    if (returnRequest.ReturnStatus != 10)
                    {
                        UpdateStockQuantity(returnRequest.OrderItemId, model);
                    }

                    oi.Quantity -= model.Quantity;
                    oi.PriceExclTax = (oi.UnitPriceExclTax * oi.Quantity);
                    oi.PriceInclTax = (oi.UnitPriceInclTax * oi.Quantity);
                    
                    //Changed By Faisal on 05 Jul, 2016 -- change charge columns & add notes only when order is paid
                    if (order.PaidDateUtc != null)
                    {
                        //_orderService.UpdateOrder(order);
                        oi.RefundedProduct += totalRefundedeAmount;
                        //Added by Faisal on 29 Apr, 2016
                        oi.RefundAfterShipping += totalRefundedeAmount;
                        
                        oi.TotalChargedAmount -= totalRefundedeAmount;
                    }
                    CalculateOrderTotals(order, totalRefundedeAmount);

                    //Changed By Faisal on 05 Jul, 2016 -- change charge columns & add notes only when order is paid
                    if (order.PaidDateUtc != null)
                    {
                        //Added By faisal on 28 Apr, 2016
                        order.OrderNotes.Add(new OrderNote
                        {
                            Note = _priceFormatter.FormatPrice(totalRefundedeAmount, true, false) + " has been refunded from " + oi.Product.Name + " by " + _workContext.CurrentCustomer.GetFullName(),
                            DisplayToCustomer = false,
                            CreatedOnUtc = DateTime.UtcNow,
                            Amount = totalRefundedeAmount,
                            ProductId = oi.ProductId,
                            ProductTax = Math.Round((oi.UnitPriceInclTax - oi.UnitPriceExclTax) * model.Quantity, 2)
                        });

                        _orderService.UpdateOrder(order);
                    }
                }
                else if (model.ReturnRequestStatusId == 70)
                {
                    //Changed By Faisal on 05 Jul, 2016 -- change charge columns & add notes only when order is paid
                    if (order.PaidDateUtc != null)
                    {
                        //order.TotalChargedAmount -= totalRefundedeAmount;
                        oi.RefundedProduct += totalRefundedeAmount;

                        //Added by Faisal on 29 Apr, 2016
                        oi.RefundAfterShipping += totalRefundedeAmount;
                    }

                    RefundOrder(order, totalRefundedeAmount);

                    //Changed By Faisal on 05 Jul, 2016 -- change charge columns & add notes only when order is paid
                    if (order.PaidDateUtc != null)
                    {
                        //Added By faisal on 28 Apr, 2016
                        order.OrderNotes.Add(new OrderNote
                        {
                            Note = _priceFormatter.FormatPrice(totalRefundedeAmount, true, false) + " has been refunded from " + oi.Product.Name + " by " + _workContext.CurrentCustomer.GetFullName(),
                            DisplayToCustomer = false,
                            CreatedOnUtc = DateTime.UtcNow,
                            Amount = totalRefundedeAmount,
                            ProductId = oi.ProductId,
                            ProductTax = Math.Round((oi.UnitPriceInclTax - oi.UnitPriceExclTax) * model.Quantity, 2)
                        });
                        _orderService.UpdateOrder(order);
                    }
                }

                if (model.ReturnRequestStatusId == 10 || model.ReturnRequestStatusId == 40 ||
                    model.ReturnRequestStatusId == 70)
                    returnRequest.ReturnStatus = model.ReturnRequestStatusId;

                _customerService.UpdateCustomer(returnRequest.Customer);

                //activity log
                _customerActivityService.InsertActivity("EditReturnRequest", _localizationService.GetResource("ActivityLog.EditReturnRequest"), returnRequest.Id);

                SuccessNotification(_localizationService.GetResource("Admin.ReturnRequests.Updated"));
                return continueEditing ? RedirectToAction("Edit", new { id = returnRequest.Id }) : RedirectToAction("List");
            }

            //If we got this far, something failed, redisplay form
            PrepareReturnRequestModel(model, returnRequest, true);
            return View(model);
        }

        [HttpPost, ActionName("Edit")]
        [FormValueRequired("notify-customer")]
        public ActionResult NotifyCustomer(ReturnRequestModel model)
        {
            if (!_permissionService.Authorize(StandardPermissionProvider.ManageReturnRequests))
                return AccessDeniedView();

            var returnRequest = _orderService.GetReturnRequestById(model.Id);
            if (returnRequest == null)
                //No return request found with the specified id
                return RedirectToAction("List");

            //var customer = returnRequest.Customer;
            var orderItem = _orderService.GetOrderItemById(returnRequest.OrderItemId);
            int queuedEmailId = _workflowMessageService.SendReturnRequestStatusChangedCustomerNotification(returnRequest, orderItem, _localizationSettings.DefaultAdminLanguageId);
            if (queuedEmailId > 0)
                SuccessNotification(_localizationService.GetResource("Admin.ReturnRequests.Notified"));
            return RedirectToAction("Edit", new { id = returnRequest.Id });
        }

        //delete
        [HttpPost]
        public ActionResult Delete(int id)
        {
            if (!_permissionService.Authorize(StandardPermissionProvider.ManageReturnRequests))
                return AccessDeniedView();

            var returnRequest = _orderService.GetReturnRequestById(id);
            if (returnRequest == null)
                //No return request found with the specified id
                return RedirectToAction("List");

            _orderService.DeleteReturnRequest(returnRequest);

            //activity log
            _customerActivityService.InsertActivity("DeleteReturnRequest", _localizationService.GetResource("ActivityLog.DeleteReturnRequest"), returnRequest.Id);

            SuccessNotification(_localizationService.GetResource("Admin.ReturnRequests.Deleted"));
            return RedirectToAction("List");
        }

        #endregion

        public void UpdateStockQuantity(int OrderItemId, ReturnRequestModel model)
        {
            int WarehouseId = 0;
            Product product = _orderService.GetOrderItemById(OrderItemId).Product;
            Order order = _orderService.GetOrderById(model.OrderId);

            if (product.ManageInventoryMethod == ManageInventoryMethod.ManageStock)
            {
                if (product.UseMultipleWarehouses || product.WarehouseId > 0)
                {
                    if (order.Shipments.Count > 0)
                    {
                        foreach (var shipment in order.Shipments)
                        {
                            foreach (var item in shipment.ShipmentItems)
                            {
                                if (_orderService.GetOrderItemById(item.OrderItemId).ProductId == model.ProductId)
                                {
                                    WarehouseId = item.WarehouseId;
                                }
                            }
                        }
                    }
                    else
                        WarehouseId = product.WarehouseId;
                }

                if (WarehouseId > 0)
                {
                    var existingPwI = product.ProductWarehouseInventory.FirstOrDefault(x => x.WarehouseId == WarehouseId);
                    if (existingPwI != null)
                    {
                        //update existing record
                        existingPwI.StockQuantity = existingPwI.StockQuantity + model.Quantity;
                    }
                }
                else
                    product.StockQuantity = product.StockQuantity + model.Quantity;

                _productService.UpdateProduct(product);
            }

     
            //if (product.ManageInventoryMethod == ManageInventoryMethod.ManageStockByAttributes)
            //{
            //    var attributesXml = _orderService.GetOrderItemById(OrderItemId).AttributesXml;

            //    var combination = _productAttributeParser.FindProductAttributeCombination(product, attributesXml);
            //    if (combination != null)
            //    {
            //        combination.StockQuantity += model.Quantity;
            //        _productAttributeService.UpdateProductAttributeCombination(combination);
            //    }
            //}
        }


        protected void CalculateOrderTotals(Order order, decimal totalRefundedeAmount)
        {
            decimal orderSubtotalInclTax = 0;
            decimal orderSubtotalExclTax = 0;
            decimal taxRate = 0;

            foreach (var item in order.OrderItems)
            {
                orderSubtotalExclTax += item.PriceExclTax;
                orderSubtotalInclTax += item.PriceInclTax;
            }

            //decimal tax = 0;
            if (order.TaxRates.IndexOf(":") != -1)
                taxRate = Math.Round(Convert.ToDecimal(order.TaxRates.Substring(0, order.TaxRates.IndexOf(":"))), 2);

            decimal tax = Math.Round(orderSubtotalInclTax - orderSubtotalExclTax, 2);

            order.OrderSubtotalExclTax = Math.Round(orderSubtotalExclTax, 2);
            order.OrderSubtotalInclTax = Math.Round(orderSubtotalInclTax, 2);

            order.OrderSubTotalDiscountInclTax = Math.Round(order.OrderSubTotalDiscountExclTax, 2);

            order.OrderShippingInclTax = Math.Round(order.OrderShippingExclTax, 2);

            order.PaymentMethodAdditionalFeeInclTax = Math.Round(order.PaymentMethodAdditionalFeeExclTax, 2);

            order.TaxRates = taxRate + ":" + tax + ";";

            order.OrderTax = tax;

            order.OrderTotal = Math.Round(order.OrderSubtotalInclTax + order.OrderShippingInclTax + order.PaymentMethodAdditionalFeeInclTax - 
                order.OrderDiscount - order.OrderSubTotalDiscountInclTax, 2);

            //Changed By Faisal on 05 Jul, 2016 -- change charge columns & add notes only when order is paid
            if (order.PaidDateUtc != null)
                order.TotalChargedAmount -= totalRefundedeAmount;

            RefundOrder(order, totalRefundedeAmount);
            _orderService.UpdateOrder(order);
        }

        public void RefundOrder(Order order, decimal totalRefundedeAmount)
        {
            bool transFailed = false;
            if (order.CardNumber != null && order.CardNumber != "")
            {
                //Process Payment Start
                ProcessPaymentRequest paymentInfo = new ProcessPaymentRequest();
                paymentInfo.CreditCardCvv2 = _encryptionService.DecryptText(order.CardCvv2);
                paymentInfo.CreditCardExpireMonth = Convert.ToInt32(_encryptionService.DecryptText(order.CardExpirationMonth));
                paymentInfo.CreditCardExpireYear = Convert.ToInt32(_encryptionService.DecryptText(order.CardExpirationYear));
                paymentInfo.CreditCardName = _encryptionService.DecryptText(order.CardName);
                paymentInfo.CreditCardNumber = _encryptionService.DecryptText(order.CardNumber);
                paymentInfo.CreditCardType = _encryptionService.DecryptText(order.CardType);
                paymentInfo.CustomerId = order.CustomerId;
                paymentInfo.StoreId = order.StoreId;
                paymentInfo.OrderGuid = order.OrderGuid;
                paymentInfo.OrderTotal = totalRefundedeAmount;//(amountToCharge - amountToRefund);
                paymentInfo.InitialOrderId = order.Id;
                paymentInfo.PaymentMethodSystemName = order.PaymentMethodSystemName;

                try
                {
                    var errors = _orderProcessingService.RefundOrCharge(order, totalRefundedeAmount, paymentInfo, true);
                    foreach (var error in errors)
                        ErrorNotification(error, false);
                    if (errors.Count > 0)
                        transFailed = true;
                }
                catch (Exception exc)
                {
                    transFailed = true;
                    ErrorNotification(exc, false);
                }
            }
            
            //For any exception or error, need to rollback all the changes made above for the order
            //Changed By Faisal On 05 Jul, 2016
            //if (transFailed)
            if (transFailed && order.PaidDateUtc != null)
                order.TotalChargedAmount += totalRefundedeAmount;
        }

    }
}
