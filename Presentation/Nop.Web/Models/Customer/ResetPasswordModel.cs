﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace Nop.Web.Models.Customer
{
    public class ResetPasswordModel
    {
        public string Email { get; set; }
        public string Message { get; set; }
    }
}