﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.Mvc;

namespace Nop.Web.Models.Checkout
{
    public class CheckoutDiscountModel
    {
        //Added by Faisal on 08 Jan, 2016
        public CheckoutDiscountModel()
        {
            AvailableCouponCodes = new List<SelectListItem>();
        }

        public bool Display { get; set; }
        public string Message { get; set; }
        public string CurrentCode { get; set; }
        public bool IsApplied { get; set; }

        //Added by Faisal on 08 Jan, 2016
        public IList<SelectListItem> AvailableCouponCodes { get; set; }
    }
}