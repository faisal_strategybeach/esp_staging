﻿using System.Collections.Generic;
using Nop.Web.Framework.Mvc;

namespace Nop.Web.Models.Checkout
{
    public partial class CheckoutConfirmModel : BaseNopModel
    {
        public CheckoutConfirmModel()
        {
            Warnings = new List<string>();
        }

        public bool TermsOfServiceOnOrderConfirmPage { get; set; }
        public string MinOrderTotalWarning { get; set; }
        public List<AccountNoPubcodeInfoModel> accountNoPubcodeInfo { get; set; }

        public IList<string> Warnings { get; set; }
    }

    public partial class AccountNoPubcodeInfoModel : BaseNopModel
    {
        public string AccountNumber { get; set; }
        public string PubCode { get; set; }
    }
}