﻿using Nop.Web.Framework.Mvc;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace Nop.Web.Models.VerifyNASOMember
{
    public partial class VerifyNASOMemberModel : BaseNopEntityModel
    {
        public string NASOMemberStatus { get; set; }
        public string AccountNumber { get; set; }
        public string ZipCode { get; set; }
    }
}