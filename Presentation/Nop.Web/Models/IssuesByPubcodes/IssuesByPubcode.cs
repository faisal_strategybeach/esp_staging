﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace Nop.Web.Models.IssuesByPubcodes
{
    public partial class IssuesByPubcode
    {
        public List<PubCode> pubCodeList { get; set; }
        public List<int> IssueYear { get; set; }
        public int IssueMonth { get; set; }
        public List<IssuesList> issueList { get; set; }
        public Nop.Web.Models.Catalog.ProductDetailsModel.AddToCartModel AddToCart { get; set; }
    }

    public class PubCode
    {
        public string PublicationCode { get; set; }
        public string CompanyCode { get; set; }
        public string PubShortDesc { get; set; }
    }

    public class IssuesList
    {
        public string ImageUrl { get; set; }
        public string ProductName { get; set; }
        public Dictionary<int, string> IssueDates { get; set; }
        public string IssueDate { get; set; }
        public int AttributeID { get; set; }
        public int ProductAttributeId { get; set; }
        public string OrderedIssueDate { get; set; }
        public int ProductID { get; set; }
        public string ProductSeName { get; set; }
        public int OrderID { get; set; }
        public List<Nop.Web.Models.Catalog.ProductDetailsModel.ProductAttributeValueModel> attributeValueModel { get; set; }
    }
}