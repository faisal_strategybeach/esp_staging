﻿using System;
using System.Collections.Generic;
using System.Data;
using System.Data.SqlClient;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;
using Nop.Core.Domain.Customers;
using Nop.Services.Customers;
using Nop.Core;
using Nop.Core.Infrastructure;
using Nop.Core.Data;

namespace RefereeStoreQuery
{
    public partial class WHInventory : System.Web.UI.Page
    {
        #region Declarations
        string connectionString = "Data Source=.;Initial Catalog=RefreeStoreNew;Integrated Security=True";
        #endregion

        #region Page Events
        protected void Page_Load(object sender, EventArgs e)
        {
            connectionString = new DataSettingsManager().LoadSettings().DataConnectionString;
            if (!IsPostBack)
            {
                bool hasRoleAdmin = EngineContext.Current.Resolve<IWorkContext>().CurrentCustomer.IsInCustomerRole("Administrators");

                if (HttpContext.Current.User.Identity.IsAuthenticated == true && hasRoleAdmin == true)
                {
                    //Bind product name
                    SetProductName();

                    int prodID = ToInt(hdnProductID.Value);

                    //Bind warehouse grid
                    BindWarehouseList(prodID);

                    //string hostAddress = Request.Url.Scheme + System.Uri.SchemeDelimiter + Request.Url.Host + (Request.Url.IsDefaultPort ? "" : ":" + Request.Url.Port) + siteName;

                    string hostAddress = Convert.ToString(System.Configuration.ConfigurationManager.AppSettings["WebURL"]);
                    hlAdminHome.NavigateUrl = hostAddress + "/admin";
                    hlProduct.NavigateUrl = hostAddress + "/Admin/Product/Edit/" + hdnProductID.Value;
                }
                else
                {
                    mainDiv.Visible = false;
                    lbMessage.Text = "Please login to continue.";
                }
            }
        }


        #endregion

        #region Control Events
        protected void btnSubmit_Click(object sender, EventArgs e)
        {
            //Validate the entries
            StringBuilder sbMessage = new StringBuilder();
            int editedQty = 0;
            foreach (GridViewRow row in grdWarehouse.Rows)
            {
                int qty = ToInt(((TextBox)row.Cells[2].FindControl("txtQty")).Text);
                string remarks = ((TextBox)row.Cells[3].FindControl("txtRemarks")).Text;

                editedQty += qty;
                //if (qty > 0 && remarks.Trim().Length <= 0)
                //{
                //    sbMessage.Append("Please enter remarks for warehouse <b>").Append(row.Cells[0].Text).Append("</b><br/>");
                //}
            }

            //if invalid entry, return from here and show the message
            if (sbMessage.Length > 0)
            {
                lbMessage.Text = sbMessage.ToString();
                lbMessage.ForeColor = Color.Red;
                return;
            }

            //if user has not edited any quantity
            if (editedQty <= 0)
            {
                lbMessage.Text = "Please enter Quantity and Remarks to edit the Stock.";
                lbMessage.ForeColor = Color.Red;
                return;
            }

            int prodID = ToInt(hdnProductID.Value);
            int userID = EngineContext.Current.Resolve<IWorkContext>().CurrentCustomer.Id;
            //Save the entry in database
            foreach (GridViewRow row in grdWarehouse.Rows)
            {
                int qty = ToInt(((TextBox)row.Cells[2].FindControl("txtQty")).Text);
                if (qty > 0)
                {
                    int warehouseID = ToInt(((HiddenField)row.Cells[1].FindControl("hdnWHID")).Value);
                    string adjType = ((DropDownList)row.Cells[1].FindControl("ddlAdjType")).SelectedValue;
                    string remarks = ((TextBox)row.Cells[3].FindControl("txtRemarks")).Text;

                    if (adjType == "Subtract")
                    {
                        if (GetCurrentStockQty(prodID, warehouseID) - qty >= 0)
                        {
                            //insert in database ProductTransactionHistory
                            InsertInProductTransactionHistory(prodID, warehouseID, qty, adjType, remarks, userID);

                            //Update updated qty in DataBase
                            UpdateProductQuantity(prodID, warehouseID, qty, adjType);

                            sbMessage.Append(qty).Append(" Quantity(s) updated for warehouse <b>").Append(row.Cells[0].Text).Append("</b><br/>");
                        }
                        else
                        {
                            sbMessage.Append("Quantity is less than the required quantity to subtract.");
                        }
                    }
                    else if (adjType == "Add")
                    {
                        InsertInProductTransactionHistory(prodID, warehouseID, qty, adjType, remarks, userID);

                        //Update updated qty in DataBase
                        UpdateProductQuantity(prodID, warehouseID, qty, adjType);

                        sbMessage.Append(qty).Append(" Quantity(s) updated for warehouse <b>").Append(row.Cells[0].Text).Append("</b><br/>");
                    }
                    BindWarehouseList(prodID);
                }
            }
            lbMessage.Text = sbMessage.ToString();
            lbMessage.ForeColor = Color.Green;
        }
        protected void lnkBack_Click(object sender, EventArgs e)
        {
            historyDiv.Visible = false;
            adjustDiv.Visible = true;
        }
        protected void grdWarehouse_RowCommand(object sender, GridViewCommandEventArgs e)
        {
            if (e.CommandName == "history")
            {
                int warehouseID = ToInt(e.CommandArgument);
                BindProducTransactionHistoryGrid(ToInt(hdnProductID.Value), warehouseID);
                adjustDiv.Visible = false;
                historyDiv.Visible = true;
            }
        }
        #endregion

        #region Page Methods
        private void SetProductName()
        {
            hdnProductID.Value = "0";
            int productID = 0;
            if (Request.QueryString["Id"] != null)
            {
                //get Product ID from query string
                productID = ToInt(Request.QueryString["Id"]);

                //find whether the product exists or not
                DataTable dtProduct = GetDBResult(@"Select Name,Sku,ManageInventoryMethodId,UseMultipleWarehouses from Product where Product.Id=" + productID);

                //if product is exists
                if (dtProduct.Rows.Count > 0
                    && ToInt(dtProduct.Rows[0]["ManageInventoryMethodId"]) == 1
                    && Convert.ToBoolean(dtProduct.Rows[0]["UseMultipleWarehouses"]) == true)
                {
                    lbProductName.Text = dtProduct.Rows[0]["Name"].ToString();
                    if (!String.IsNullOrEmpty(dtProduct.Rows[0]["Sku"].ToString()))
                        lbProductName.Text += " (" + dtProduct.Rows[0]["Sku"] + ")";
                    hdnProductID.Value = productID.ToString();
                }
                else
                {
                    btnSubmit.Enabled = false;
                    btnSubmit.Text = "Invalid Product or Product Stock is not managed Warehouse wise.";
                }
            }

            //if product is not found
            if (productID == 0)
            {
                btnSubmit.Enabled = false;
                btnSubmit.Text = "Invalid Product or Product Stock is not managed Warehouse wise.";
            }
        }
        private void BindWarehouseList(int ProductId)
        {
            if (ProductId > 0)
            {
                //fetch the warehouse list
                //DataTable dtWH = GetDBResult("Select Warehouse.Id,Name,StockQuantity from Warehouse inner join ProductWarehouseInventory PWI on PWI.WarehouseId=Warehouse.id where ProductId=" + ProductId + " order by Name");

                DataTable dtWH = GetDBResult(@"SELECT Warehouse.Id,NAME, StockQuantity + dbo.[Uf_GetProductBeginningBalancebyWarehouse_Quantity](PWI.ProductId,Warehouse.id) as 'StockQuantity' 
                                                    FROM Warehouse INNER JOIN ProductWarehouseInventory PWI ON PWI.WarehouseId = Warehouse.id 
                                                    WHERE PWI.ProductId =" + ProductId + " ORDER BY NAME");

                if (dtWH.Rows.Count > 0)
                {
                    //bind the data to grid
                    grdWarehouse.DataSource = dtWH;
                    grdWarehouse.DataBind();
                }
                else
                {
                    lbMessage.Text = "No Warehouse found. Please select one or more warehouses to manage inventory.";
                    lbMessage.ForeColor = Color.Red;
                    btnSubmit.Visible = false;
                }
            }
            else
            {
                lbMessage.Text = "Select Appropriate product.";
                lbMessage.ForeColor = Color.Red;
                btnSubmit.Visible = false;
            }


        }
        private void BindProducTransactionHistoryGrid(int prodID, int warehouseID)
        {
            //Fetch Product Transaction History
            DataTable dtHistory = GetProductTransctionHistory(prodID, warehouseID);

            //Sort the history, most recent first
            dtHistory.DefaultView.Sort = "AdjustmentDateUtc Desc";

            //Store the sorted history in DataTable
            DataTable dtSorted = dtHistory.DefaultView.ToTable();

            //Add RunningTotal columns for showing Closing Quantity
            dtSorted.Columns.Add("RunningTotal", typeof(double));
            dtSorted.Columns.Add("OpeningBalance", typeof(double));
            dtSorted.Columns.Add("Adjustment_Date", typeof(DateTime));

            DataColumn dc = dtSorted.Columns["AdjustementType"];
            dc.MaxLength = 100;

            dtSorted.AcceptChanges();

            DataTable dt_ProductInvoiceSales = GetProductInvoiceSales(prodID, warehouseID);
            double NetSoldQuantity = 0;
            if (dt_ProductInvoiceSales.Rows.Count > 0)
            {
                NetSoldQuantity = Convert.ToDouble(dt_ProductInvoiceSales.Rows[0]["NetSoldQuantity"]);
            }


            DataTable dt_GetProductWarehouseInventory_OpeningAdjustment = GetProductWarehouseInventory_OpeningAdjustment(prodID, warehouseID);
            double AdjustmentQuantity = 0;
            DateTime AdjustmentDate = new DateTime(2016, 01, 01);

            if (dt_GetProductWarehouseInventory_OpeningAdjustment.Rows.Count > 0)
            {
                AdjustmentQuantity = Convert.ToDouble(dt_GetProductWarehouseInventory_OpeningAdjustment.Rows[0]["AdjustmentQuantity"]);
                AdjustmentDate = Convert.ToDateTime(dt_GetProductWarehouseInventory_OpeningAdjustment.Rows[0]["AdjustmentDate"]);
            }


            DataRow dr_new_1 = dtSorted.NewRow();
            dr_new_1["Qty"] = AdjustmentQuantity;
            dr_new_1["AdjustementType"] = "Initial quantity reset";
            dr_new_1["AdjustmentDateUtc"] = AdjustmentDate;
            dr_new_1["UserName"] = "";
            dr_new_1["Remarks"] = "";
            dr_new_1["RunningTotal"] = 0;
            dr_new_1["OpeningBalance"] = 0;
            dtSorted.Rows.Add(dr_new_1);


            //Fetch current Quantity
            double curQty = GetCurrentStockQty(prodID, warehouseID);
            double runningTotal = curQty;
            ///double runningTotal = curQty + AdjustmentQuantity;



            double openingBalance = 0;

            DataRow dr_new = dtSorted.NewRow();
            dr_new["Qty"] = NetSoldQuantity * -1;
            dr_new["AdjustementType"] = "Shipped Till Date";
            dr_new["AdjustmentDateUtc"] = DateTime.Now;
            dr_new["UserName"] = "";
            dr_new["Remarks"] = "";
            dr_new["RunningTotal"] = 0;
            dr_new["OpeningBalance"] = 0;
            dtSorted.Rows.InsertAt(dr_new, 0);

            //Iterate row by row, calculate and set the Closing Quantity
            foreach (DataRow dr in dtSorted.Rows)
            {
                openingBalance = runningTotal - ToDouble(dr["Qty"]);
                dr["RunningTotal"] = runningTotal;
                dr["OpeningBalance"] = openingBalance;


                if (dr["AdjustementType"].ToString() == "Shipped Till Date")
                {
                    dr["Adjustment_Date"] = DBNull.Value;
                }
                else
                {
                    dr["Adjustment_Date"] = dr["AdjustmentDateUtc"];
                }

                //openingBalance = runningTotal + ToDouble(dr["Qty"]);
                runningTotal = runningTotal - ToDouble(dr["Qty"]);
            }

            dtSorted.AcceptChanges();

            //Again sort the DataTable, most recent in last
            dtSorted.DefaultView.Sort = "AdjustmentDateUtc";

            //Bind the GridView
            grdHistory.DataSource = dtSorted;
            grdHistory.DataBind();

            //Set the Current Quantity in label
            lbClosingQty.Text = "Current Stock Quantity : <b>" + GetCurrentStockQty(prodID, warehouseID).ToString() + "</b>";
        }
        #endregion

        #region Database Methods

        private DataTable GetProductInvoiceSales(int prodID, int warehouseID)
        {


            using (SqlConnection con = new SqlConnection(connectionString))
            {
                using (SqlCommand cmd = new SqlCommand("usp_GetProductInventoryDetailsTillDate", con))
                {
                    cmd.CommandType = CommandType.StoredProcedure;

                    cmd.Parameters.Add("@ProductId", SqlDbType.Int).Value = prodID;
                    cmd.Parameters.Add("@WarehouseId", SqlDbType.Int).Value = warehouseID;
                    cmd.Parameters.Add("@FromDate", SqlDbType.DateTime).Value = Convert.ToDateTime("01-Jan-1991");
                    cmd.Parameters.Add("@ToDate", SqlDbType.DateTime).Value = DateTime.Now;

                    con.Open();

                    DataTable dt = new DataTable();
                    dt.Load(cmd.ExecuteReader());

                    return dt;
                }
            }

        }

        private DataTable GetProductTransctionHistory(int prodID, int warehouseID)
        {
            string query = @"select case PTH.AdjustmentType when 'Add' then '+' else '-' end +CONVERT(varchar,PTH.Quantity) as Qty,
                case PTH.AdjustmentType when 'Add' then 'Received' else 'Subtract' end as AdjustementType,
                DATEADD(mi, DATEDIFF(mi, GETUTCDATE(), GETDATE()), PTH.AdjustmentDateUtc) as AdjustmentDateUtc
                ,isnull(C.Username,C.Email)Username,PTH.Remarks from ProductTransactionHistory PTH
                inner join Customer C ON C.Id=PTH.UserId
                 where ProductId={0} and WarehouseId={1}";

            //removed "Shipped" AdjustementType -- prachi - 10032015
            //                UNION ALL 
            //
            //                select '-'+Convert(varchar,SHPITM.Quantity) as Qty,
            //                'Shipped' as AdjustementType,
            //                DATEADD(mi, DATEDIFF(mi, GETUTCDATE(), GETDATE()), SHP.ShippedDateUtc) as AdjustmentDateUtc
            //                ,'' as ByUser,'' as Remarks  from Shipment SHP
            //                inner join ShipmentItem SHPITM ON SHP.Id=SHPITM.ShipmentId
            //                inner join OrderItem OI ON SHPITM.OrderItemId=OI.Id
            //                where OI.ProductId={0} and SHPITM.WarehouseId={1}";
            DataTable dt = GetDBResult(String.Format(query, prodID, warehouseID));
            return dt;
        }


        private DataTable GetProductWarehouseInventory_OpeningAdjustment(int prodID, int warehouseID)
        {
            string query = @"select * from [Uf_GetProductWarehouseInventory_OpeningAdjustment]({0},{1})";

            //removed "Shipped" AdjustementType -- prachi - 10032015
            //                UNION ALL 
            //
            //                select '-'+Convert(varchar,SHPITM.Quantity) as Qty,
            //                'Shipped' as AdjustementType,
            //                DATEADD(mi, DATEDIFF(mi, GETUTCDATE(), GETDATE()), SHP.ShippedDateUtc) as AdjustmentDateUtc
            //                ,'' as ByUser,'' as Remarks  from Shipment SHP
            //                inner join ShipmentItem SHPITM ON SHP.Id=SHPITM.ShipmentId
            //                inner join OrderItem OI ON SHPITM.OrderItemId=OI.Id
            //                where OI.ProductId={0} and SHPITM.WarehouseId={1}";
            DataTable dt = GetDBResult(String.Format(query, prodID, warehouseID));
            return dt;
        }


        private void UpdateProductQuantity(int prodID, int warehouseID, int qty, string adujstmentType)
        {
            DataTable dtCurrentQty = GetDBResult(String.Format(@"Select P.Id as ProdID,WH.Id as WHID,PWI.StockQuantity,PWI.Id as PWI_ID  
from Product P cross join Warehouse WH
left outer join ProductWarehouseInventory PWI ON P.Id=PWI.ProductId and PWI.WarehouseId=WH.Id
where P.Id={0} and WH.Id={1}", prodID, warehouseID));
            if (dtCurrentQty.Rows.Count > 0)
            {
                double curQty = ToDouble(dtCurrentQty.Rows[0]["StockQuantity"]);
                int pwiID = ToInt(dtCurrentQty.Rows[0]["PWI_ID"]);

                //Calculate new updated qunatity based on the adujstment method
                if (adujstmentType == "Add")
                {
                    curQty = curQty + qty;
                }
                else if (adujstmentType == "Subtract")
                {
                    curQty = curQty - qty;
                }

                //set qty 0 if less than 0
                //if (curQty <= 0)
                //    curQty = 0;

                //update in database
                InsertUpdateProductWarehouseInventory(prodID, warehouseID, curQty, pwiID);
            }
            else
            {
                //update in database
                InsertUpdateProductWarehouseInventory(prodID, warehouseID, qty, 0);
            }
        }
        private void InsertUpdateProductWarehouseInventory(int prodID, int warehouseID, double qty, int pwiID)
        {
            SqlConnection con = new SqlConnection(connectionString);
            SqlCommand cmd = new SqlCommand();
            cmd.Connection = con;

            //If already exists in database
            if (pwiID > 0)
            {
                cmd.CommandText = "Update ProductWarehouseInventory set StockQuantity=@StockQuantity where Id=@Id";
                cmd.Parameters.Add("@StockQuantity", SqlDbType.Int).Value = qty;
                cmd.Parameters.Add("@Id", SqlDbType.Int).Value = pwiID;
            }
            else
            {
                //create a new record in database
                cmd.CommandText = @"Insert into ProductWarehouseInventory(ProductID,WarehouseID,StockQuantity,ReservedQuantity) values
                (@ProductID,@WarehouseID,@StockQuantity,0)";
                cmd.Parameters.Add("@ProductID", SqlDbType.Int).Value = prodID;
                cmd.Parameters.Add("@WarehouseID", SqlDbType.Int).Value = warehouseID;
                cmd.Parameters.Add("@StockQuantity", SqlDbType.Int).Value = qty;
            }

            con.Open();
            using (con)
            {
                cmd.ExecuteNonQuery();
            }
        }
        private void InsertInProductTransactionHistory(int prodID, int warehouseID, int qty, string adjType, string remark, int userID)
        {
            SqlConnection con = new SqlConnection(connectionString);
            SqlCommand cmd = new SqlCommand(@"Insert into [ProductTransactionHistory]
    ([ProductId],[WarehouseId],[Quantity],[UserId],[AdjustmentType],[Remarks],[AdjustmentDateUtc])
    values(@ProductId,@WarehouseId,@Quantity,@UserId,@AdjustmentType,@Remarks,@AdjustmentDateUtc)", con);
            cmd.Parameters.Add("@ProductId", SqlDbType.Int).Value = prodID;
            cmd.Parameters.Add("@WarehouseId", SqlDbType.Int).Value = warehouseID;
            cmd.Parameters.Add("@Quantity", SqlDbType.Int).Value = qty;
            cmd.Parameters.Add("@UserId", SqlDbType.Int).Value = userID;
            cmd.Parameters.Add("@AdjustmentType", SqlDbType.VarChar, 50).Value = adjType;
            cmd.Parameters.Add("@Remarks", SqlDbType.VarChar, 200).Value = remark;
            cmd.Parameters.Add("@AdjustmentDateUtc", SqlDbType.DateTime, 20).Value = DateTime.UtcNow;

            con.Open();
            using (con)
            {
                cmd.ExecuteNonQuery();
            }
        }
        private double GetCurrentStockQty(int productID, int warehouseID)
        {
            DataTable dt = GetDBResult(String.Format("select StockQuantity + dbo.[Uf_GetProductBeginningBalancebyWarehouse_Quantity]({0},{1}) as 'StockQuantity'  from ProductWarehouseInventory where ProductId={0} and WarehouseId={1}", productID, warehouseID));
            double currentQty = 0;
            if (dt.Rows.Count > 0)
            {
                currentQty = ToDouble(dt.Rows[0]["StockQuantity"]);
            }
            return currentQty;
        }
        #endregion

        #region Utility Functions
        private DataTable GetDBResult(string query)
        {
            SqlConnection con = new SqlConnection(connectionString);
            SqlCommand cmd = new SqlCommand(query, con);
            DataTable dt = new DataTable();
            con.Open();
            using (con)
            {
                dt.Load(cmd.ExecuteReader());
            }
            return dt;
        }
        public static int ToInt(object theValue)
        {
            int theResult = 0;
            if (theValue == null || Convert.IsDBNull(theValue) || !int.TryParse(theValue.ToString(), out theResult))
                theResult = 0;
            return theResult;
        }
        public static double ToDouble(object theValue)
        {
            double theResult = 0;
            if (theValue == null || Convert.IsDBNull(theValue) || !double.TryParse(theValue.ToString(), out theResult))
                theResult = 0;
            return theResult;
        }
        protected void grdHistory_RowDataBound(Object sender, GridViewRowEventArgs e)
        {
            if (e.Row.RowType == DataControlRowType.DataRow)
            {
              
                if (e.Row.Cells[2].Text.ToString() == "Initial quantity reset")
                {
                    e.Row.Cells[2].Text = "<i>" + e.Row.Cells[2].Text + "</i>";
                }


            }
        }

        #endregion
    }



}