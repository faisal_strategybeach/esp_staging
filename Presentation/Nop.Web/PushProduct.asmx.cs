﻿using Nop.Core.Data;
using Nop.Core.Domain.Catalog;
using Nop.Core.Infrastructure;
using Nop.Services.Catalog;
using System;
using System.Collections.Generic;
using System.Configuration;
using System.Data;
using System.Data.SqlClient;
using System.Linq;
using System.Web;
using System.Web.Services;

namespace Nop.Web
{
    /// <summary>
    /// Summary description for PushProduct
    /// </summary>
    [WebService(Namespace = "http://az.espproductstore.com/")]
    [WebServiceBinding(ConformsTo = WsiProfiles.BasicProfile1_1)]
    [System.ComponentModel.ToolboxItem(false)]
    // To allow this Web Service to be called from script, using ASP.NET AJAX, uncomment the following line. 
    // [System.Web.Script.Services.ScriptService]
    public class PushProduct : System.Web.Services.WebService
    {
        //private readonly IProductService _productService;
        
        //public PushProduct() { }

        //public PushProduct(IProductService productService)
        //{
        //    this._productService = productService;
        //}

        [WebMethod]
        public string HelloWorld()
        {
            return "Hello World";
        }

        [WebMethod]
        public int PushProductToNop(string productName, string pubCode, string issueDate, string PublicationURL, string StoreCode)
        {
            SqlConnection con = new SqlConnection();
            string connString = System.IO.File.ReadAllText(Server.MapPath("/App_Data/Settings.txt"));
            con.ConnectionString = connString.Substring(connString.LastIndexOf(":") + 1).Trim();
           
            con.Open();

            SqlCommand com = new SqlCommand("sp_AddProduct", con);
            com.CommandType = CommandType.StoredProcedure;
            com.Parameters.AddWithValue("@Name", productName);
            com.Parameters.AddWithValue("@PubCode", pubCode);
            com.Parameters.AddWithValue("@Issue", issueDate);
            com.Parameters.AddWithValue("@PublicationURL", PublicationURL);
            com.Parameters.AddWithValue("@StoreCode", StoreCode);
            SqlParameter outputParam = com.Parameters.Add("@ID", SqlDbType.Int);
            outputParam.Direction = ParameterDirection.Output;
            com.ExecuteNonQuery();
            int productID = (int)outputParam.Value;
            con.Close();
            //Product product = new Product();
            //product.Name = productName;
            //product.Price = productPrice;
            //product.CreatedOnUtc = DateTime.UtcNow;
            //product.UpdatedOnUtc = DateTime.UtcNow;

            //productService.InsertProduct(product);
            //string id = product.Id.ToString();
            //return id;
            return productID;
        }
    }
}
