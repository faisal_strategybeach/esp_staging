﻿using Nop.Core;
using Nop.Core.Caching;
using Nop.Core.Domain.Catalog;
using Nop.Core.Domain.Customers;
using Nop.Core.Domain.Media;
using Nop.Services.Catalog;
using Nop.Services.Localization;
using Nop.Services.Media;
using Nop.Services.Orders;
using Nop.Services.Seo;
using Nop.Web.Framework.Security;
using Nop.Web.Infrastructure.Cache;
using Nop.Web.Models.Catalog;
using Nop.Web.Models.IssuesByPubcodes;
using System;
using System.Collections.Generic;
using System.Data;
using System.Linq;
using System.Web.Configuration;
using System.Web.Mvc;
using System.Web.Services;

namespace Nop.Web.Controllers
{
    public class IssuesByPubcodeController : BasePublicController
    {
        private readonly IWorkContext _workContext;
        private readonly IProductService _productService;
        private readonly IOrderService _orderService;
        private readonly IStoreContext _storeContext;
        private readonly IPictureService _pictureService;
        private readonly IProductAttributeService _productAttributeService;
        private readonly MediaSettings _mediaSettings;
        private readonly ISpecificationAttributeService _specificationAttributeService;
        private readonly ICacheManager _cacheManager;
        private readonly ILocalizationService _localizationService;

        public IssuesByPubcodeController(IWorkContext workContext,
                                        IProductService productService,
                                        IOrderService orderService,
                                        IStoreContext storeContext,
                                        IPictureService pictureService,
                                        IProductAttributeService productAttributeService,
                                        MediaSettings mediaSettings,
                                        ISpecificationAttributeService specificationAttributeService,
                                        ICacheManager cacheManager,
                                        ILocalizationService localizationService)
        {
            this._workContext = workContext;
            this._orderService = orderService;
            this._productService = productService;
            this._storeContext = storeContext;
            this._pictureService = pictureService;
            this._productAttributeService = productAttributeService;
            this._mediaSettings = mediaSettings;
            this._specificationAttributeService = specificationAttributeService;
            this._cacheManager = cacheManager;
            this._localizationService = localizationService;
        }

        [HttpGet]
        public ActionResult IssuesByPubcode()
        {
            DataTable dt = new DataTable();
            IssuesByPubcode issuesByPubcode = new IssuesByPubcode();
            List<PubCode> pubList = new List<PubCode>();
            List<int> lstYear = new List<int>();
            try
            {
                string cuid = WebConfigurationManager.AppSettings["PubServiceCUID"];

                using (var loPubService = new ESPSubPublication.ESPSubPublication())
                {
                    dt = loPubService.GetPublicationsByCompany(_storeContext.CurrentStore.StoreCode, cuid);
                }

                if (dt != null)
                {
                    if (dt.Rows.Count > 0)
                    {
                        foreach (DataRow dr in dt.Rows)
                        {
                            PubCode pubCode = new PubCode();
                            pubCode.PublicationCode = dr["SMFZPub"].ToString();
                            pubCode.PubShortDesc = dr["SMFZPubShortName"].ToString();
                            pubCode.CompanyCode = dr["SMFZCompanyCode"].ToString();
                            pubList.Add(pubCode);
                        }
                    }
                }
                if (_storeContext.CurrentStore.StoreCode.ToUpper() == "PJ")
                {
                    PubCode pubCode1 = new PubCode();
                    pubCode1.PublicationCode = "HR";
                    pubCode1.PubShortDesc = "HR";
                    pubCode1.CompanyCode = "PJ";
                    pubList.Add(pubCode1);
                }
                
                issuesByPubcode.pubCodeList = pubList;
                issuesByPubcode.IssueMonth = 0;
                Session["PubCodeList"] = pubList;

                var products = _productService.SearchProducts(storeId: _storeContext.CurrentStore.Id);

                List<IssuesList> lstIssueList = new List<IssuesList>();
                bool checkAttribute = false;
                string publicationCode = string.Empty;
                List<Nop.Web.Models.Catalog.ProductDetailsModel.ProductAttributeModel> lstAttributeModel = new List<ProductDetailsModel.ProductAttributeModel>();

                foreach (var product in products)
                {
                    ICollection<ProductSpecificationAttribute> productSpecificationAttribute = product.ProductSpecificationAttributes;
                    if (productSpecificationAttribute.Count > 0)
                    {
                        foreach (var attribute in productSpecificationAttribute)
                        {
                            if (attribute.SpecificationAttributeOption.SpecificationAttribute.Name.ToUpper() == "PUBCODE" && pubList.FindIndex(item => item.PublicationCode.ToUpper() == attribute.CustomValue.ToUpper()) >= 0)
                            {
                                checkAttribute = true;
                                publicationCode = attribute.CustomValue.ToUpper();
                                break;
                            }
                            else
                                checkAttribute = false;
                        }
                    }

                    if (checkAttribute == true)
                    {
                        IList<ProductAttributeMapping> productAttributeMapping = null;
                        if (product.ProductAttributeMappings.Count > 0)
                        {
                            productAttributeMapping = _productAttributeService.GetProductAttributeMappingsByProductId(product.Id);
                            foreach (var attribute in productAttributeMapping)
                            {
                                string attributeName = attribute.ProductAttribute.GetLocalized(x => x.Name);
                                var pictures = _pictureService.GetPicturesByProductId(product.Id);
                                var defaultPicture = pictures.FirstOrDefault();

                                if (attributeName.ToUpper() == "ISSUE" && attribute.ProductAttributeValues.Count > 0)
                                {
                                    IssuesList issuesList = new IssuesList();
                                    issuesList.ImageUrl = _pictureService.GetPictureUrl(defaultPicture);
                                    issuesList.ProductID = product.Id;
                                    issuesList.ProductName = product.Name;
                                    issuesList.ProductSeName = product.GetSeName();
                                    issuesList.AttributeID = attribute.Id;
                                    issuesList.ProductAttributeId = attribute.ProductAttributeId;

                                    if (attribute.ShouldHaveValues())
                                    {
                                        Dictionary<int, string> lstIssueDates = new Dictionary<int, string>();
                                        var attributeValues = _productAttributeService.GetProductAttributeValues(attribute.Id);
                                        foreach (var attributeValue in attributeValues)
                                        {
                                            string issueDate = Convert.ToDateTime(attributeValue.Name.Substring(attributeValue.Name.IndexOf(":") + 1).Trim()).ToLongDateString();
                                            lstIssueDates.Add(attributeValue.Id, issueDate);

                                            if (!(lstYear.Contains(Convert.ToDateTime(issueDate).Year)))
                                                lstYear.Add(Convert.ToDateTime(issueDate).Year);
                                        }
                                        issuesList.IssueDates = lstIssueDates;
                                    }
                                    lstIssueList.Add(issuesList);
                                }
                            }
                        }
                    }
                }
                
                issuesByPubcode.IssueYear = lstYear.OrderByDescending(item => item).ToList();
                issuesByPubcode.issueList = lstIssueList;

                return View(issuesByPubcode);
            }
            catch (Exception ex)
            {
                logger.Error(ex);
                return Content("");
            }
        }

        [HttpPost]
        public ActionResult FilteredIssuesByPubcode(string publicationCode = "", int year = 0, int month = 0)
        {
            IssuesByPubcode issuesByPubcode = new IssuesByPubcode();
            List<int> lstYear = new List<int>();
            try
            {
              
                bool checkAttribute = false;

                var products = _productService.SearchProducts(storeId: _storeContext.CurrentStore.Id);

                List<IssuesList> lstIssueList = new List<IssuesList>();
                List<PubCode> pubList = new List<PubCode>();
                pubList = (List<PubCode>)Session["PubCodeList"];
                List<Nop.Web.Models.Catalog.ProductDetailsModel.ProductAttributeModel> lstAttributeModel = new List<ProductDetailsModel.ProductAttributeModel>();

                foreach (var product in products)
                {
                    ICollection<ProductSpecificationAttribute> productSpecificationAttribute = product.ProductSpecificationAttributes;
                    if (productSpecificationAttribute.Count > 0)
                    {
                        foreach (var attribute in productSpecificationAttribute)
                        {
                            if (attribute.SpecificationAttributeOption.SpecificationAttribute.Name.ToUpper() == "PUBCODE" && (!string.IsNullOrEmpty(publicationCode)) && attribute.CustomValue.ToUpper() == publicationCode.ToUpper())
                            {
                                checkAttribute = true;
                                break;
                            }
                            else if (attribute.SpecificationAttributeOption.SpecificationAttribute.Name.ToUpper() == "PUBCODE" && (string.IsNullOrEmpty(publicationCode)) && pubList.FindIndex(item => item.PublicationCode.ToUpper() == attribute.CustomValue.ToUpper()) >= 0)
                            {
                                checkAttribute = true;
                                publicationCode = attribute.CustomValue;
                                break;
                            }
                            else
                                checkAttribute = false;
                        }
                    }

                    if (checkAttribute == true)
                    {
                        IList<ProductAttributeMapping> productAttributeMapping = null;
                        if (product.ProductAttributeMappings.Count > 0)
                        {
                            productAttributeMapping = _productAttributeService.GetProductAttributeMappingsByProductId(product.Id);
                            foreach (var attribute in productAttributeMapping)
                            {
                                string attributeName = attribute.ProductAttribute.GetLocalized(x => x.Name);
                                var pictures = _pictureService.GetPicturesByProductId(product.Id);
                                var defaultPicture = pictures.FirstOrDefault();

                                if (attributeName.ToUpper() == "ISSUE" && attribute.ProductAttributeValues.Count > 0)
                                {
                                    IssuesList issuesList = new IssuesList();
                                    issuesList.ImageUrl = _pictureService.GetPictureUrl(defaultPicture);
                                    issuesList.ProductID = product.Id;
                                    issuesList.ProductName = product.Name;
                                    issuesList.ProductSeName = product.GetSeName();
                                    issuesList.AttributeID = attribute.Id;
                                    issuesList.ProductAttributeId = attribute.ProductAttributeId;

                                    if (attribute.ShouldHaveValues())
                                    {
                                        //values
                                        Dictionary<int,string> lstIssueDates = new Dictionary<int,string>();
                                        var attributeValues = _productAttributeService.GetProductAttributeValues(attribute.Id);
                                        foreach (var attributeValue in attributeValues)
                                        {
                                            string issueDate = Convert.ToDateTime(attributeValue.Name.Substring(attributeValue.Name.IndexOf(":") + 1).Trim()).ToLongDateString();

                                            if (year > 0)
                                            {
                                                if (Convert.ToDateTime(issueDate).Year == year)
                                                {
                                                    if (month > 0)
                                                    {
                                                        if (Convert.ToDateTime(issueDate).Month == month)
                                                            lstIssueDates.Add(attributeValue.Id, issueDate);
                                                    }
                                                    else
                                                        lstIssueDates.Add(attributeValue.Id, issueDate);
                                                }
                                            }
                                            else if (month > 0)
                                            {
                                                if (Convert.ToDateTime(issueDate).Month == month)
                                                    lstIssueDates.Add(attributeValue.Id, issueDate);
                                            }
                                            else
                                                lstIssueDates.Add(attributeValue.Id, issueDate);
                                        }
                                        issuesList.IssueDates = lstIssueDates;
                                    }

                                    lstIssueList.Add(issuesList);
                                }
                            }
                        }
                    }
                    else
                        continue;
                }
             
                issuesByPubcode.issueList = lstIssueList;

                return PartialView("_FilteredIssues", issuesByPubcode);
            }
            catch (Exception ex)
            {
                logger.Error(ex);
                return Content("");
            }
        }

        [NopHttpsRequirement(SslRequirement.Yes)]
        public void ViewDigitalVersionOfProduct(int orderID)
        {
            
            string storeCode = _storeContext.CurrentStore.StoreCode;
            var orderItem = _orderService.GetOrderItemById(orderID);
            string PublicationURL = string.Empty;
            //ProductAttributeMapping issue = product.ProductAttributeMappings.FirstOrDefault(p => p.ProductAttribute.Name == "Issue");
            string issue = string.Format("{0:yyyyMMdd}", Convert.ToDateTime(orderItem.AttributeDescription.Substring(orderItem.AttributeDescription.IndexOf(":") + 1).Trim()));

            foreach (var attributes in orderItem.Product.ProductAttributeMappings)
            {
                PublicationURL = attributes.ProductAttributeValues.Where(i => i.Name == orderItem.AttributeDescription.Substring(orderItem.AttributeDescription.IndexOf(":") + 1).Trim() && orderItem.AttributeDescription.Substring(0, orderItem.AttributeDescription.IndexOf(":")).ToUpper() == "ISSUE").Select(i => i.PublicationURL).FirstOrDefault();
            }

            string url = ((PublicationURL == string.Empty || PublicationURL == null) ? WebConfigurationManager.AppSettings["PublicationURL"].ToString() + storeCode + "/" + orderItem.Product.ProductSpecificationAttributes
                            .Where(w => w.SpecificationAttributeOption.SpecificationAttribute.Name.ToUpper() == "PUBCODE")
                            .FirstOrDefault().CustomValue + "/" + issue + "/index.html" : PublicationURL);
            Response.Write("<script language='javascript'> window.open('" + url + "','_self');</script>");

            //string url = ((PublicationURL == string.Empty || PublicationURL == null) ? "http://localhost:22735/publications/" + storeCode + "/" + orderItem.Product.ProductSpecificationAttributes
            //                .Where(w => w.SpecificationAttributeOption.SpecificationAttribute.Name.ToUpper() == "PUBCODE")
            //                .FirstOrDefault().CustomValue + "/" + issue + "/index.html" : PublicationURL);

            //Response.Redirect(url);
            //Response.Write("<script language='javascript'> window.open('" + url + "');</script>");
            //Response.Write("<script language='javascript'> window.open('" + url + "','_blank'); window.location.href = '/DigitalPublications'</script>");
            //JavaScript("window.open('" + url + "');");
        }

        public ActionResult DigitalPublications()
        {
            if (!_workContext.CurrentCustomer.IsRegistered())
                return new HttpUnauthorizedResult();

            //PushProduct push = new PushProduct();
            //push.PushProductToNop("testabc123", "ZQ", "2015/01/01");

            var orders = _orderService.SearchOrders(storeId: _storeContext.CurrentStore.Id,
                 customerId: _workContext.CurrentCustomer.Id);

            IssuesByPubcode issuesByPubcode = new IssuesByPubcode();
           
            List<IssuesList> lstIssueList = new List<IssuesList>();
            foreach (var order in orders)
            {
                foreach (var item in order.OrderItems)
                {
                    IssuesList lstIssue = new IssuesList();
                    if (item.Product.ProductAttributeMappings.Count > 0)
                    {
                        if (item.AttributeDescription != "" && item.AttributeDescription.Substring(0, item.AttributeDescription.IndexOf(":")).ToUpper().Trim() == "ISSUE")
                        {
                            lstIssue.OrderID = item.Id;
                            lstIssue.ProductID = item.ProductId;
                            lstIssue.ProductName = item.Product.Name;
                            ICollection<ProductSpecificationAttribute> productSpecificationAttribute = item.Product.ProductSpecificationAttributes;
                            string publicationCode = string.Empty;
                            if (productSpecificationAttribute.Count > 0)
                            {
                                foreach (var attribute in productSpecificationAttribute)
                                {
                                    if (attribute.SpecificationAttributeOption.SpecificationAttribute.Name.ToUpper() == "PUBCODE" && attribute.CustomValue.ToUpper() == "HR")
                                    {
                                        publicationCode = attribute.CustomValue.ToUpper();
                                        break;
                                    }
                                }
                            }
                           
                                lstIssue.OrderedIssueDate = Convert.ToDateTime(item.AttributeDescription.Substring(item.AttributeDescription.IndexOf(":") + 1).Trim()).ToLongDateString();
                           
                            lstIssue.ProductSeName = item.Product.GetSeName();

                            var pictures = _pictureService.GetPicturesByProductId(item.ProductId);
                            if (pictures != null && pictures.Count > 0)
                            {
                                lstIssue.ImageUrl = _pictureService.GetPictureUrl(pictures[0]);
                            }
                            lstIssueList.Add(lstIssue);
                        }
                    }
                }
            }
            issuesByPubcode.issueList = lstIssueList;
            return View(issuesByPubcode);
        }
    }
}