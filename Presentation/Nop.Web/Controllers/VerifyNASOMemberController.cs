﻿using Nop.Core;
using Nop.Services.Customers;
using System;
using System.Web.Configuration;
using System.Web.Mvc;
using System.Xml;
using System.Linq;
using Nop.Web.Models.VerifyNASOMember;
using Nop.Core.Domain.Customers;
using Nop.Core.Domain.Orders;

namespace Nop.Web.Controllers 
{
    public partial class VerifyNASOMemberController : BasePublicController
    {
        private IWorkContext _workContext;
        private ICustomerService _customerService;
        // GET: VerifyNASOMember

        public VerifyNASOMemberController(IWorkContext workContext,
                                        ICustomerService customerService)
        {
            this._workContext = workContext;
            this._customerService = customerService;
        }

        public enum NASOMemberStatusEnum
        {
            Active,
            NotActive,
            NotLoggedIn,
            NotNASOMember
        };

        [ChildActionOnly]
        public ActionResult VerifyNASOMember1()
        {
            string NASOMemberStatus = string.Empty;
            string AccountNo = string.Empty;
            string Status = string.Empty;
            string ZipCode = string.Empty;
            VerifyNASOMemberModel verifyNASOMember = new VerifyNASOMemberModel();
            try
            {
                if (_workContext.CurrentCustomer.Email != null)
                {
                    if (Convert.ToString(_workContext.CurrentCustomer.Email) != "")
                    {
                        string emailAddress = _workContext.CurrentCustomer.Email;
                        //string emailAddress = "corinna@corinnaluyken.com";

                        XmlNode node = null;
                        string cuid = WebConfigurationManager.AppSettings["PubServiceCUID"];
                        string cpwd = WebConfigurationManager.AppSettings["PubServicePassword"];

                        const string WSReq = "<ws1600request><pubcode>NO</pubcode><emailaddress><![CDATA[{0}]]></emailaddress></ws1600request>";
                        string reqString = string.Format(WSReq, emailAddress);

                        using (var loPubService = new net.espcomp.espdev.Listener01())
                        {
                            node = loPubService.ws1600(cuid, cpwd, reqString);
                        }

                        if (node.OuterXml != "")
                        {
                            string returnCode = node.SelectSingleNode("/returncode").InnerText;

                            if (node.SelectSingleNode("account") != null)
                            {
                                if (node.SelectSingleNode("account").SelectSingleNode("status") != null)
                                    Status = node.SelectSingleNode("account").SelectSingleNode("status").InnerText;

                                if (node.SelectSingleNode("account").SelectSingleNode("acctno") != null)
                                    AccountNo = node.SelectSingleNode("account").SelectSingleNode("acctno").InnerText;

                                if (node.SelectSingleNode("account").SelectSingleNode("zipcode") != null)
                                    ZipCode = node.SelectSingleNode("account").SelectSingleNode("zipcode").InnerText;
                            }
                            if (returnCode == "1" && Status == "A")
                            {
                                NASOMemberStatus = NASOMemberStatusEnum.Active.ToString();

                                var customer = _workContext.CurrentCustomer;
                                var role = _customerService.GetCustomerRoleBySystemName("NASO Member");
                                if (role != null)
                                {
                                    if (!customer.CustomerRoles.Any(cr => cr.SystemName == "NASO Member"))
                                    {
                                        customer.CustomerRoles.Add(role);
                                        _customerService.UpdateCustomer(customer);
                                    }
                                }
                            }
                            else if (returnCode == "1" && Status != "A")
                            {
                                NASOMemberStatus = NASOMemberStatusEnum.NotActive.ToString();
                            }
                            else if (returnCode != "1" && Status != "A")
                            {
                                NASOMemberStatus = NASOMemberStatusEnum.NotNASOMember.ToString();
                            }
                        }
                    }
                    else
                    {
                        NASOMemberStatus = NASOMemberStatusEnum.NotLoggedIn.ToString();
                    }
                }
                else
                {
                    NASOMemberStatus = NASOMemberStatusEnum.NotLoggedIn.ToString();
                }
                verifyNASOMember.NASOMemberStatus = NASOMemberStatus;
                verifyNASOMember.AccountNumber = AccountNo;
                verifyNASOMember.ZipCode = ZipCode;
            }
            catch (Exception ex)
            {
                logger.Error(ex);
                verifyNASOMember.AccountNumber = _workContext.CurrentCustomer.Email;
                verifyNASOMember.NASOMemberStatus = ex.Message;
            }
            return PartialView(verifyNASOMember);
        }
    }
}