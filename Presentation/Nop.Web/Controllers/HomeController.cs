﻿using System.Web.Mvc;
using Nop.Web.Framework.Security;

namespace Nop.Web.Controllers
{
    public partial class HomeController : BasePublicController
    {
        //Changed / Added By Faisal On 25 Nov, 2015
        private readonly Nop.Core.IWorkContext _workContext;
        private readonly System.Web.HttpContextBase _httpContext;

        public HomeController(Nop.Core.IWorkContext workContext, System.Web.HttpContextBase httpContext)
        {
            this._workContext = workContext;
            this._httpContext = httpContext;

            TempData["IsImpersonated"] = (_workContext.OriginalCustomerIfImpersonated == null) ? "N" : "Y";
            TempData.Keep("IsImpersonated");
            ViewBag.IsImpersonated = (_workContext.OriginalCustomerIfImpersonated == null) ? "N" : "Y";

            if (_workContext.OriginalCustomerIfImpersonated == null)
                _httpContext.Session["IsImpersonated"] = "N";
            else
                _httpContext.Session["IsImpersonated"] = "Y";
        }


        [NopHttpsRequirement(SslRequirement.No)]
        public ActionResult Index()
        {
            return View();
        }
    }
}
