﻿using System.Web.Mvc;
using System.Web.Routing;
using Nop.Web.Framework.Mvc.Routes;

namespace Nop.Plugin.DiscountRules.OrderTotal
{
    public partial class RouteProvider : IRouteProvider
    {
        public void RegisterRoutes(RouteCollection routes)
        {
            routes.MapRoute("Plugin.DiscountRules.OrderTotal.Configure",
                 "Plugins/DiscountRulesOrderTotal/Configure",
                 new { controller = "DiscountRulesOrderTotal", action = "Configure" },
                 new[] { "Nop.Plugin.DiscountRules.OrderTotal.Controllers" }
            );
        }
        public int Priority
        {
            get
            {
                return 0;
            }
        }
    }
}
