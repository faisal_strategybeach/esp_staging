﻿using System;
using System.Linq;
using System.Web.Mvc;
using Nop.Core.Domain.Discounts;
using Nop.Plugin.DiscountRules.OrderTotal.Models;
using Nop.Services.Discounts;
using Nop.Web.Framework.Controllers;
using Nop.Services.Orders;
using Nop.Services.Configuration;
using Nop.Services.Security;
using System.Globalization;
using System.Threading;
using Nop.Core;

namespace Nop.Plugin.DiscountRules.OrderTotal.Controllers
{
    [AdminAuthorize]
    public class DiscountRulesOrderTotalController : Controller
    {
        private readonly IDiscountService _discountService;
        private readonly IOrderTotalCalculationService OrderTotalCalculationService;
        private readonly ISettingService _settingService;
        private readonly IPermissionService _permissionService;

        public DiscountRulesOrderTotalController(IDiscountService discountService,IOrderTotalCalculationService OrderTotalCalculationService, ISettingService SettingService, IPermissionService permissionService)
        {
            this._discountService = discountService;
            this.OrderTotalCalculationService = OrderTotalCalculationService;
            this._settingService = SettingService;
            this._permissionService = permissionService;
        }

        protected override void Initialize(System.Web.Routing.RequestContext requestContext)
        {
            //little hack here
            //always set culture to 'en-US' (Telerik has a bug related to editing decimal values in other cultures). Like currently it's done for admin area in Global.asax.cs
            CommonHelper.SetTelerikCulture();
            base.Initialize(requestContext);
        }

        public ActionResult Configure(int discountId, int? discountRequirementId)
        {

            if (!_permissionService.Authorize(StandardPermissionProvider.ManageDiscounts))
                return Content("Access denied");

            var discount = _discountService.GetDiscountById(discountId);
            if (discount == null)
                throw new ArgumentException("Discount could not be loaded");

            DiscountRequirement discountRequirement = null;
            if (discountRequirementId.HasValue)
            {
                discountRequirement = discount.DiscountRequirements.FirstOrDefault(dr => dr.Id == discountRequirementId.Value);
                if (discountRequirement == null)
                    return Content("Failed to load requirement.");
            }

            var model = new RequirementModel();
            model.RequirementId = discountRequirementId.HasValue ? discountRequirementId.Value : 0;
            model.DiscountId = discountId;
            model.OrderTotal = _settingService.GetSettingByKey<decimal>(string.Format("DiscountRequirement.OrderTotal-{0}", discountRequirementId.HasValue ? discountRequirementId.Value : 0));

            //add a prefix
            ViewData.TemplateInfo.HtmlFieldPrefix = string.Format("DiscountRulesOrderTotal{0}", discountRequirementId.HasValue ? discountRequirementId.Value.ToString() : "0");

            return View("~/Plugins/DiscountRules.OrderTotal/Views/DiscountRulesOrderTotal/Configure.cshtml", model);
        }

        [HttpPost]
        public ActionResult Configure(int discountId, int? discountRequirementId, decimal ordertotal)
        {
            var discount = _discountService.GetDiscountById(discountId);
            if (discount == null)
                throw new ArgumentException("Discount could not be loaded");

            DiscountRequirement discountRequirement = null;
            if (discountRequirementId.HasValue)
                discountRequirement = discount.DiscountRequirements.Where(dr => dr.Id == discountRequirementId.Value).FirstOrDefault();

            if (discountRequirement != null)
            {
                //update existing rule
                _settingService.SetSetting(string.Format("DiscountRequirement.OrderTotal-{0}", discountRequirement.Id), ordertotal);
            }
            else
            {
                //save new rule
                discountRequirement = new DiscountRequirement()
                {
                    DiscountRequirementRuleSystemName = "DiscountRequirement.OrderTotal", 
                };
                discount.DiscountRequirements.Add(discountRequirement);
                _discountService.UpdateDiscount(discount);
                _settingService.SetSetting(string.Format("DiscountRequirement.OrderTotal-{0}", discountRequirement.Id), ordertotal);
            }
            return Json(new { Result = true, NewRequirementId = discountRequirement.Id }, JsonRequestBehavior.AllowGet);
        }
        
    }
}