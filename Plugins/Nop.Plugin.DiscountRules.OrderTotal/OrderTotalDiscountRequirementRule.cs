using System;
using System.Linq;
using Nop.Core;
using Nop.Core.Domain.Orders;
using Nop.Core.Plugins;
using Nop.Services.Customers;
using Nop.Services.Discounts;
using Nop.Services.Localization;
using Nop.Services.Orders;
using Nop.Services.Catalog;
using Nop.Services.Tax;
using System.Collections.Generic;
using Nop.Core.Domain.Discounts;
using Nop.Core.Domain.Customers;
using Nop.Services.Common;
using Nop.Services.Configuration;

namespace Nop.Plugin.DiscountRules.OrderTotal
{
    public partial class OrderTotalDiscountRequirementRule : BasePlugin, IDiscountRequirementRule
    {

        #region init services
        private readonly IPriceCalculationService _priceCalculationService;
        private readonly ITaxService _taxService;
        private readonly ICheckoutAttributeParser _checkoutAttributeParser;
        private readonly ShoppingCartSettings _shoppingCartSettings;
        private readonly IGenericAttributeService _genericAttributeService;
        private readonly ISettingService _settingService;
        private readonly IStoreContext _storeContext;

        public OrderTotalDiscountRequirementRule(IPriceCalculationService PriceCalculationService, ITaxService TaxService, ICheckoutAttributeParser CheckoutAttributeParser, ShoppingCartSettings ShoppingCartSettings, IGenericAttributeService GenericAttributeService, ISettingService SettingService, IStoreContext StoreContext) {
        this._priceCalculationService = PriceCalculationService;
        this._taxService = TaxService;
        this._checkoutAttributeParser = CheckoutAttributeParser;
        this._shoppingCartSettings = ShoppingCartSettings;
        this._genericAttributeService = GenericAttributeService;
        this._settingService = SettingService;
        this._storeContext = StoreContext;
        }
        #endregion

        // copy of GetShoppingCartSubTotal from IOrderTotalCalculationService but without calls to discounts
        public virtual void GetShoppingCartSubTotalWithoutDiscount(IList<ShoppingCartItem> cart, out decimal subTotalWithoutDiscount)
        {
            SortedDictionary<decimal, decimal> taxRates;
            subTotalWithoutDiscount = decimal.Zero;
            taxRates = new SortedDictionary<decimal, decimal>();

            if (cart.Count == 0)
                return;

            //get the customer 
            Customer customer = cart.GetCustomer();

            //sub totals
            decimal subTotalInclTaxWithoutDiscount = decimal.Zero;
            foreach (var shoppingCartItem in cart)
            {
                decimal sciSubTotal = _priceCalculationService.GetSubTotal(shoppingCartItem, false);
                decimal taxRate;
                decimal sciInclTax = _taxService.GetProductPrice(shoppingCartItem.Product, sciSubTotal, true, customer, out taxRate);
                subTotalInclTaxWithoutDiscount += sciInclTax;
            }

            //checkout attributes
            if (customer != null)
            {
                var checkoutAttributesXml = customer.GetAttribute<string>(SystemCustomerAttributeNames.CheckoutAttributes, _genericAttributeService, _storeContext.CurrentStore.Id);
                var attributeValues = _checkoutAttributeParser.ParseCheckoutAttributeValues(checkoutAttributesXml);
                if (attributeValues != null)
                {
                    foreach (var attributeValue in attributeValues)
                    {
                        decimal taxRate;
                        decimal caInclTax = _taxService.GetCheckoutAttributePrice(attributeValue, true, customer, out taxRate);
                        subTotalInclTaxWithoutDiscount += caInclTax;
                    }
                }
            }

            //subtotal without discount
                subTotalWithoutDiscount = subTotalInclTaxWithoutDiscount;
            if (subTotalWithoutDiscount < decimal.Zero)
                subTotalWithoutDiscount = decimal.Zero;
            if (_shoppingCartSettings.RoundPricesDuringCalculation)
                subTotalWithoutDiscount = RoundingHelper.RoundPrice(subTotalWithoutDiscount);
        }



        /// <summary>
        /// Check discount requirement
        /// </summary>
        /// <param name="request">Object that contains all information required to check the requirement (Current customer, discount, etc)</param>
        /// <returns>true - requirement is met; otherwise, false</returns>
        public bool CheckRequirement(CheckDiscountRequirementRequest request)
        {
            if (request == null)
                throw new ArgumentNullException("request");

            if (request.DiscountRequirement == null)
                throw new NopException("Discount requirement is not set");

            //if (request.Customer == null || request.Customer.IsGuest())
            //    return false;

            //Prachi 11/19/2015
            if (request.Customer == null)
                return false;

            var items = request.Customer.ShoppingCartItems.ToList();

            //SortedDictionary<decimal, decimal> taxRates;
            decimal ordertotal;
            
            GetShoppingCartSubTotalWithoutDiscount(items, out ordertotal);

            if (ordertotal <= request.DiscountRequirement.Discount.DiscountAmount)
                return false;



            return ordertotal >= _settingService.GetSettingByKey<decimal>(string.Format("DiscountRequirement.OrderTotal-{0}", request.DiscountRequirement.Id));
       }

        /// <summary>
        /// Get URL for rule configuration
        /// </summary>
        /// <param name="discountId">Discount identifier</param>
        /// <param name="discountRequirementId">Discount requirement identifier (if editing)</param>
        /// <returns>URL</returns>
        public string GetConfigurationUrl(int discountId, int? discountRequirementId)
        {
            //configured in RouteProvider.cs
            string result = "Plugins/DiscountRulesOrderTotal/Configure/?discountId=" + discountId;
            if (discountRequirementId.HasValue)
                result += string.Format("&discountRequirementId={0}", discountRequirementId.Value);
            return result;
        }

        public override void Install()
        {
            //locales
            this.AddOrUpdatePluginLocaleResource("Plugins.DiscountRules.OrderTotal.Fields.Total", "Required minimum order total");
            this.AddOrUpdatePluginLocaleResource("Plugins.DiscountRules.OrderTotal.Fields.Total.Hint", "Discount will be applied if order total is greater or equal to specified.");
            base.Install();
        }

        public override void Uninstall()
        {
            //locales
            this.DeletePluginLocaleResource("Plugins.DiscountRules.OrderTotal.Fields.Total");
            this.DeletePluginLocaleResource("Plugins.DiscountRules.OrderTotal.Fields.Total.Hint");
            base.Uninstall();
        }
    }
}