﻿using Nop.Web.Framework;

namespace Nop.Plugin.DiscountRules.OrderTotal.Models
{
    public class RequirementModel
    {
        [NopResourceDisplayName("Plugins.DiscountRules.OrderTotal.Fields.Total")]
        public decimal OrderTotal { get; set; }

        public int DiscountId { get; set; }

        public int RequirementId { get; set; }
    }
}