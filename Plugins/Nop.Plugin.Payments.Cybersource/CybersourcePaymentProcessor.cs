using System;
using System.Collections;
using System.Collections.Generic;
using System.Collections.Specialized;
using System.Globalization;
using System.Net;
using System.Text;
using System.Web.Routing;
using Nop.Core;
using Nop.Core.Domain.Catalog;
using Nop.Core.Domain.Directory;
using Nop.Core.Domain.Orders;
using Nop.Core.Domain.Payments;
using Nop.Core.Plugins;
using Nop.Plugin.Payments.Cybersource.Controllers;
using Nop.Plugin.Payments.Cybersource.net.authorize.api;
using Nop.Services.Configuration;
using Nop.Services.Customers;
using Nop.Services.Directory;
using Nop.Services.Localization;
using Nop.Services.Orders;
using Nop.Services.Payments;
using Nop.Services.Security;
using CyberSource.Clients;
using System.Security.Cryptography;

namespace Nop.Plugin.Payments.Cybersource
{
    /// <summary>
    /// Cybersource payment processor
    /// </summary>
    public class CybersourcePaymentProcessor : BasePlugin, IPaymentMethod
    {
        #region Fields

        private readonly CybersourcePaymentSettings _cybersourcePaymentSettings;
        private readonly ISettingService _settingService;
        private readonly ICurrencyService _currencyService;
        private readonly ICustomerService _customerService;
        private readonly CurrencySettings _currencySettings;
        private readonly IWebHelper _webHelper;
        private readonly IOrderTotalCalculationService _orderTotalCalculationService;
        private readonly IEncryptionService _encryptionService;

        #endregion

        #region Ctor

        public CybersourcePaymentProcessor(CybersourcePaymentSettings cybersourcePaymentSettings,
            ISettingService settingService,
            ICurrencyService currencyService,
            ICustomerService customerService,
            CurrencySettings currencySettings, IWebHelper webHelper,
            IOrderTotalCalculationService orderTotalCalculationService, IEncryptionService encryptionService)
        {
            this._cybersourcePaymentSettings = cybersourcePaymentSettings;
            this._settingService = settingService;
            this._currencyService = currencyService;
            this._customerService = customerService;
            this._currencySettings = currencySettings;
            this._webHelper = webHelper;
            this._orderTotalCalculationService = orderTotalCalculationService;
            this._encryptionService = encryptionService;
        }

        #endregion

        #region Utilities


        /// <summary>
        /// Gets Authorize.NET URL
        /// </summary>
        /// <returns></returns>
        private string GetCybersourceUrl()
        {
            return _cybersourcePaymentSettings.UseSandbox ? "https://test.authorize.net/gateway/transact.dll" :
                "https://secure.authorize.net/gateway/transact.dll";
        }

        /// <summary>
        /// Gets Authorize.NET API version
        /// </summary>
        private string GetApiVersion()
        {
            return "3.1";
        }

        // Populate merchant authentication (ARB Support)
        private MerchantAuthenticationType PopulateMerchantAuthentication()
        {
            var authentication = new MerchantAuthenticationType();
            authentication.name = _cybersourcePaymentSettings.LoginId;
            authentication.transactionKey = _cybersourcePaymentSettings.TransactionKey;
            return authentication;
        }
        /// <summary>
        ///  Get errors (ARB Support)
        /// </summary>
        /// <param name="response"></param>
        private static string GetErrors(ANetApiResponseType response)
        {
            var sb = new StringBuilder();
            sb.AppendLine("The API request failed with the following errors:");
            foreach (var message in response.messages)
                sb.AppendLine("[" + message.code + "] " + message.text);
            return sb.ToString();
        }
        #endregion

        #region Methods

        /// <summary>
        /// Process a payment
        /// </summary>
        /// <param name="processPaymentRequest">Payment info required for an order processing</param>
        /// <returns>Process payment result</returns>
        public ProcessPaymentResult ProcessPayment(ProcessPaymentRequest processPaymentRequest)
        {
            var result = new ProcessPaymentResult();

            result.AllowStoringCreditCardNumber = true;
            // Get customer information based on the unique CustomerId
            var customer = _customerService.GetCustomerById(processPaymentRequest.CustomerId);

            Hashtable request = new Hashtable();
            // Credit Card Authorization
            request.Add("ccAuthService_run", "true");

            // Credit Card Capture
            request.Add("ccCaptureService_run", "true");

            request.Add("merchantReferenceCode", string.Format("Full order #{0}", processPaymentRequest.OrderGuid));
            // Billign Address info
            request.Add("billTo_firstName", customer.BillingAddress.FirstName);
            request.Add("billTo_lastName", customer.BillingAddress.LastName );
            request.Add("billTo_street1", customer.BillingAddress.Address1 );
            request.Add("billTo_city", customer.BillingAddress.City );
            request.Add("billTo_postalCode", customer.BillingAddress.ZipPostalCode );
            
            if (customer.BillingAddress.StateProvince != null)
                request.Add("billTo_state", customer.BillingAddress.StateProvince.Abbreviation );
            if (customer.BillingAddress.Country != null)
                request.Add("billTo_country", customer.BillingAddress.Country.ThreeLetterIsoCode);
           
            request.Add("billTo_email", customer.BillingAddress.Email);
            request.Add("billTo_ipAddress", _webHelper.GetCurrentIpAddress());
            
            // set up credit card details
            request.Add("card_accountNumber", processPaymentRequest.CreditCardNumber);
            request.Add("card_expirationMonth", processPaymentRequest.CreditCardExpireMonth.ToString());
            request.Add("card_expirationYear", processPaymentRequest.CreditCardExpireYear.ToString());
            request.Add("card_cvNumber", processPaymentRequest.CreditCardCvv2);
            request.Add("purchaseTotals_currency", _currencyService.GetCurrencyById(_currencySettings.PrimaryStoreCurrencyId).CurrencyCode);

            var orderTotal = Math.Round(processPaymentRequest.OrderTotal, 2);
            request.Add("purchaseTotals_grandTotalAmount", orderTotal.ToString("0.00", CultureInfo.InvariantCulture));

            // add adidtional optional fields here

            try
            {
                Hashtable reply = NVPClient.RunTransaction(request);
                
                // Parse through the response to get message codes.
                int reasonCode = int.Parse((string)reply["reasonCode"]);
                switch (reasonCode)
                {
                    // Success
                    case 100:
                        result.AuthorizationTransactionCode = string.Format("{0}", reply["ccAuthReply_authorizationCode"]);
                        result.AuthorizationTransactionResult = string.Format("Approved ({0}: {1})", reply["requestID"], reply["ccCaptureReply_amount"]);
                        result.NewPaymentStatus = PaymentStatus.Paid;
                        break;
                    // Missing field(s)
                    case 101:
                        result.AddError(string.Format("Declined ({0})", "The following required field(s) are missing: " + 
                            EnumerateValues( reply, "missingField" )));
                        break;

                    // Invalid field(s)
                    case 102:
                        result.AddError(string.Format("Declined ({0})", "The following field(s) are invalid: " + 
                            EnumerateValues(reply, "invalidField")));
                        break;

                    // Insufficient funds
                    case 204:
                        result.AddError(string.Format("Declined ({0})", "Insufficient funds in the account.  Please use a different card or select another form of payment."));
                        break;

                    // add additional reason codes here that you need to handle
                    // specifically.

                    default:
                        result.AddError(string.Format("Error: {0}", string.Format("{0}", reply["decision"])));
                        break;
                }
            }

            //System.Net exception    
            catch (WebException we)
            {
                //The WebException class is thrown by classes descended from WebRequest and WebResponse that implement pluggable protocols for accessing the Internet.
                string errMesg = we.Message.ToString();

                result.AddError(errMesg);
            }
            catch (CryptographicException ce)
            {
                result.AddError(ce.Message);
            }
            //Any other exception!
            catch (Exception e)
            {
                string errMesg = e.InnerException.ToString() ; 
                //SaveOrderState();
                //HandleException(e);
                result.AddError(errMesg);
            } 
            return result;
        }

        private static string EnumerateValues(Hashtable reply, string fieldName)
        {
            System.Text.StringBuilder sb = new System.Text.StringBuilder();

            string val = "";
            for (int i = 0; val != null; ++i)
            {
                val = (string)reply[fieldName + "_" + i];
                if (val != null)
                {
                    sb.Append(val + "\n");
                }
            }

            return (sb.ToString());
        }


         /// <summary>
        /// Post process payment (used by payment gateways that require redirecting to a third-party URL)
        /// </summary>
        /// <param name="postProcessPaymentRequest">Payment info required for an order processing</param>
        public void PostProcessPayment(PostProcessPaymentRequest postProcessPaymentRequest)
        {
            //nothing
        }

        /// <summary>
        /// Gets additional handling fee
        /// </summary>
        /// <param name="cart">Shoping cart</param>
        /// <returns>Additional handling fee</returns>
        public decimal GetAdditionalHandlingFee(IList<ShoppingCartItem> cart)
        {
            var result = this.CalculateAdditionalFee(_orderTotalCalculationService, cart,
                _cybersourcePaymentSettings.AdditionalFee, _cybersourcePaymentSettings.AdditionalFeePercentage);
            return result;
        }

        /// <summary>
        /// Returns a value indicating whether payment method should be hidden during checkout
        /// </summary>
        /// <param name="cart">Shoping cart</param>
        /// <returns>true - hide; false - display.</returns>
        public bool HidePaymentMethod(IList<ShoppingCartItem> cart)
        {
            //you can put any logic here
            //for example, hide this payment method if all products in the cart are downloadable
            //or hide this payment method if current customer is from certain country
            return false;
        }

        /// <summary>
        /// Captures payment
        /// </summary>
        /// <param name="capturePaymentRequest">Capture payment request</param>
        /// <returns>Capture payment result</returns>
        public CapturePaymentResult Capture(CapturePaymentRequest capturePaymentRequest)
        {
            var result = new CapturePaymentResult();
            result.AddError("Capture method not supported");
            return result;
        }

        /// <summary>
        /// Refunds a payment
        /// </summary>
        /// <param name="refundPaymentRequest">Request</param>
        /// <returns>Result</returns>
        public RefundPaymentResult Refund(RefundPaymentRequest refundPaymentRequest)
        {
            var result = new RefundPaymentResult();

            var customer = _customerService.GetCustomerById(refundPaymentRequest.Order.CustomerId);
            
            Hashtable request = new Hashtable();
            // Credit Card credit
            request.Add("ccCreditService_run", "true");

            request.Add("merchantReferenceCode", string.Format("Full order #{0}", refundPaymentRequest.Order.OrderGuid));
            request.Add("billTo_firstName", customer.BillingAddress.FirstName);
            request.Add("billTo_lastName", customer.BillingAddress.LastName);
            request.Add("billTo_street1", customer.BillingAddress.Address1);
            request.Add("billTo_city", customer.BillingAddress.City);
            request.Add("billTo_postalCode", customer.BillingAddress.ZipPostalCode);

            if (customer.BillingAddress.StateProvince != null)
                request.Add("billTo_state", customer.BillingAddress.StateProvince.Abbreviation);
            if (customer.BillingAddress.Country != null)
                request.Add("billTo_country", customer.BillingAddress.Country.ThreeLetterIsoCode);

            request.Add("billTo_email", customer.BillingAddress.Email);
            request.Add("billTo_ipAddress", _webHelper.GetCurrentIpAddress());

            request.Add("card_accountNumber", _encryptionService.DecryptText(refundPaymentRequest.Order.CardNumber));
            request.Add("card_expirationMonth", _encryptionService.DecryptText(refundPaymentRequest.Order.CardExpirationMonth.ToString()));
            request.Add("card_expirationYear", _encryptionService.DecryptText(refundPaymentRequest.Order.CardExpirationYear.ToString()));
            request.Add("card_cvNumber", _encryptionService.DecryptText(refundPaymentRequest.Order.CardCvv2));
            request.Add("purchaseTotals_currency", _currencyService.GetCurrencyById(_currencySettings.PrimaryStoreCurrencyId).CurrencyCode);

            // there are two items in this sample
            //request.Add("item_0_unitPrice", "12.34");
            //request.Add("item_1_unitPrice", "56.78");
            var orderTotal = Math.Round(refundPaymentRequest.AmountToRefund , 2);
            request.Add("purchaseTotals_grandTotalAmount", orderTotal.ToString("0.00", CultureInfo.InvariantCulture));

            // add optional fields here per your business needs

            try
            {
                Hashtable reply = NVPClient.RunTransaction(request);

                int reasonCode = int.Parse((string)reply["reasonCode"]);
                switch (reasonCode)
                {
                    // Success
                    case 100:
                        var isOrderFullyRefunded = (refundPaymentRequest.AmountToRefund + refundPaymentRequest.Order.RefundedAmount == refundPaymentRequest.Order.OrderTotal);
                        result.NewPaymentStatus = isOrderFullyRefunded ? PaymentStatus.Refunded : PaymentStatus.PartiallyRefunded;

                        break;
                    // Missing field(s)
                    case 101:
                        result.AddError(string.Format("Declined ({0})", "The following required field(s) are missing: " +
                            EnumerateValues(reply, "missingField")));
                        break;

                    // Invalid field(s)
                    case 102:
                        result.AddError(string.Format("Declined ({0})", "The following field(s) are invalid: " +
                            EnumerateValues(reply, "invalidField")));
                        break;

                    // Insufficient funds
                    case 204:
                        result.AddError(string.Format("Declined ({0})", "Insufficient funds in the account.  Please use a different card or select another form of payment."));
                        break;

                    // add additional reason codes here that you need to handle
                    // specifically.

                    default:
                        result.AddError(string.Format("Error: {0}", string.Format("{0}", reply["decision"])));
                        break;
                }
            }

            //System.Net exception    
            catch (WebException we)
            {
                //The WebException class is thrown by classes descended from WebRequest and WebResponse that implement pluggable protocols for accessing the Internet.
                //SaveOrderState();
                //HandleWebException(we);
                result.AddError("Cybersource unknown error");
            }
            //Any other exception!
            catch (Exception e)
            {
                var errMesg = e.InnerException;
                //SaveOrderState();
                //HandleException(e);
                result.AddError("Cybersource unknown error");
            }

            //var responseData = webClient.UploadValues(GetCybersourceUrl(), form);

            return result;


            //var webClient = new WebClient();
            //var form = new NameValueCollection();
            //form.Add("x_login", _cybersourcePaymentSettings.LoginId);
            //form.Add("x_tran_key", _cybersourcePaymentSettings.TransactionKey);

            //form.Add("x_delim_data", "TRUE");
            //form.Add("x_delim_char", "|");
            //form.Add("x_encap_char", "");
            //form.Add("x_version", GetApiVersion());
            //form.Add("x_relay_response", "FALSE");

            //form.Add("x_method", "CC");
            //form.Add("x_currency_code", _currencyService.GetCurrencyById(_currencySettings.PrimaryStoreCurrencyId).CurrencyCode);

            //string[] codes = refundPaymentRequest.Order.CaptureTransactionId == null ?
            //    refundPaymentRequest.Order.AuthorizationTransactionCode.Split(',') : refundPaymentRequest.Order.CaptureTransactionId.Split(',');
            ////x_trans_id. When x_test_request (sandbox) is set to a positive response, 
            ////or when Test mode is enabled on the payment gateway, this value will be "0".
            //form.Add("x_trans_id", codes[0]);

            //string maskedCreditCardNumberDecrypted = _encryptionService.DecryptText(refundPaymentRequest.Order.MaskedCreditCardNumber);
            //if (String.IsNullOrEmpty(maskedCreditCardNumberDecrypted) || maskedCreditCardNumberDecrypted.Length < 4)
            //{
            //    result.AddError("Last four digits of Credit Card Not Available");
            //    return result;
            //}
            //var lastFourDigitsCardNumber = maskedCreditCardNumberDecrypted.Substring(maskedCreditCardNumberDecrypted.Length - 4);
            //form.Add("x_card_num", lastFourDigitsCardNumber); // only last four digits are required for doing a credit
            //form.Add("x_amount", refundPaymentRequest.AmountToRefund.ToString("0.00", CultureInfo.InvariantCulture));
            ////x_invoice_num is 20 chars maximum. hece we also pass x_description
            //form.Add("x_invoice_num", refundPaymentRequest.Order.OrderGuid.ToString().Substring(0, 20));
            //form.Add("x_description", string.Format("Full order #{0}", refundPaymentRequest.Order.OrderGuid));
            //form.Add("x_type", "CREDIT");
            
            //// Send Request to Authorize and Get Response
            //var responseData = webClient.UploadValues(GetCybersourceUrl(), form);
            //var reply = Encoding.ASCII.GetString(responseData);

            //if (!String.IsNullOrEmpty(reply))
            //{
            //    string[] responseFields = reply.Split('|');
            //    switch (responseFields[0])
            //    {
            //        case "1":                         
            //            var isOrderFullyRefunded = (refundPaymentRequest.AmountToRefund + refundPaymentRequest.Order.RefundedAmount == refundPaymentRequest.Order.OrderTotal);
            //            result.NewPaymentStatus = isOrderFullyRefunded ? PaymentStatus.Refunded : PaymentStatus.PartiallyRefunded;
            //            break;
            //        case "2":
            //            result.AddError(string.Format("Declined ({0}: {1})", responseFields[2], responseFields[3]));
            //            break;
            //        case "3":
            //            result.AddError(string.Format("Error: {0}", reply));
            //            break;
            //    }
            //}
            //else
            //{
            //    result.AddError("Authorize.NET unknown error");
            //}
            //return result;
        }

        /// <summary>
        /// Voids a payment
        /// </summary>
        /// <param name="voidPaymentRequest">Request</param>
        /// <returns>Result</returns>
        public VoidPaymentResult Void(VoidPaymentRequest voidPaymentRequest)
        {
            var result = new VoidPaymentResult();
            result.AddError("Void method not supported");
            return result;
        }

        /// <summary>
        /// Process recurring payment
        /// </summary>
        /// <param name="processPaymentRequest">Payment info required for an order processing</param>
        /// <returns>Process payment result</returns>
        public ProcessPaymentResult ProcessRecurringPayment(ProcessPaymentRequest processPaymentRequest)
        {
            var result = new ProcessPaymentResult();

            if (!processPaymentRequest.IsRecurringPayment)
            {
                var customer = _customerService.GetCustomerById(processPaymentRequest.CustomerId);

                var subscription = new ARBSubscriptionType();
                var creditCard = new net.authorize.api.CreditCardType();

                subscription.name = processPaymentRequest.OrderGuid.ToString();

                creditCard.cardNumber = processPaymentRequest.CreditCardNumber;
                creditCard.expirationDate = processPaymentRequest.CreditCardExpireYear + "-" + processPaymentRequest.CreditCardExpireMonth; // required format for API is YYYY-MM
                creditCard.cardCode = processPaymentRequest.CreditCardCvv2;

                subscription.payment = new PaymentType();
                subscription.payment.Item = creditCard;

                subscription.billTo = new NameAndAddressType();
                subscription.billTo.firstName = customer.BillingAddress.FirstName;
                subscription.billTo.lastName = customer.BillingAddress.LastName;
                subscription.billTo.address = customer.BillingAddress.Address1 + " " + customer.BillingAddress.Address2;
                subscription.billTo.city = customer.BillingAddress.City;
                if (customer.BillingAddress.StateProvince != null)
                {
                    subscription.billTo.state = customer.BillingAddress.StateProvince.Abbreviation;
                }
                subscription.billTo.zip = customer.BillingAddress.ZipPostalCode;

                if (customer.ShippingAddress != null)
                {
                    subscription.shipTo = new NameAndAddressType();
                    subscription.shipTo.firstName = customer.ShippingAddress.FirstName;
                    subscription.shipTo.lastName = customer.ShippingAddress.LastName;
                    subscription.shipTo.address = customer.ShippingAddress.Address1 + " " + customer.ShippingAddress.Address2;
                    subscription.shipTo.city = customer.ShippingAddress.City;
                    if (customer.ShippingAddress.StateProvince != null)
                    {
                        subscription.shipTo.state = customer.ShippingAddress.StateProvince.Abbreviation;
                    }
                    subscription.shipTo.zip = customer.ShippingAddress.ZipPostalCode;

                }

                subscription.customer = new CustomerType();
                subscription.customer.email = customer.BillingAddress.Email;
                subscription.customer.phoneNumber = customer.BillingAddress.PhoneNumber;

                subscription.order = new OrderType();
                subscription.order.description = "Recurring payment";

                // Create a subscription that is leng of specified occurrences and interval is amount of days ad runs

                subscription.paymentSchedule = new PaymentScheduleType();
                DateTime dtNow = DateTime.UtcNow;
                subscription.paymentSchedule.startDate = new DateTime(dtNow.Year, dtNow.Month, dtNow.Day);
                subscription.paymentSchedule.startDateSpecified = true;

                subscription.paymentSchedule.totalOccurrences = Convert.ToInt16(processPaymentRequest.RecurringTotalCycles);
                subscription.paymentSchedule.totalOccurrencesSpecified = true;

                var orderTotal = Math.Round(processPaymentRequest.OrderTotal, 2);
                subscription.amount = orderTotal;
                subscription.amountSpecified = true;

                // Interval can't be updated once a subscription is created.
                subscription.paymentSchedule.interval = new PaymentScheduleTypeInterval();
                switch (processPaymentRequest.RecurringCyclePeriod)
                {
                    case RecurringProductCyclePeriod.Days:
                        subscription.paymentSchedule.interval.length = Convert.ToInt16(processPaymentRequest.RecurringCycleLength);
                        subscription.paymentSchedule.interval.unit = ARBSubscriptionUnitEnum.days;
                        break;
                    case RecurringProductCyclePeriod.Weeks:
                        subscription.paymentSchedule.interval.length = Convert.ToInt16(processPaymentRequest.RecurringCycleLength * 7);
                        subscription.paymentSchedule.interval.unit = ARBSubscriptionUnitEnum.days;
                        break;
                    case RecurringProductCyclePeriod.Months:
                        subscription.paymentSchedule.interval.length = Convert.ToInt16(processPaymentRequest.RecurringCycleLength);
                        subscription.paymentSchedule.interval.unit = ARBSubscriptionUnitEnum.months;
                        break;
                    case RecurringProductCyclePeriod.Years:
                        subscription.paymentSchedule.interval.length = Convert.ToInt16(processPaymentRequest.RecurringCycleLength * 12);
                        subscription.paymentSchedule.interval.unit = ARBSubscriptionUnitEnum.months;
                        break;
                    default:
                        throw new NopException("Not supported cycle period");
                }

                using (var webService = new net.authorize.api.Service())
                {
                    if (_cybersourcePaymentSettings.UseSandbox)
                        webService.Url = "https://apitest.authorize.net/soap/v1/Service.asmx";
                    else
                        webService.Url = "https://api.authorize.net/soap/v1/Service.asmx";

                    var authentication = PopulateMerchantAuthentication();
                    var response = webService.ARBCreateSubscription(authentication, subscription);

                    if (response.resultCode == MessageTypeEnum.Ok)
                    {
                        result.SubscriptionTransactionId = response.subscriptionId.ToString();
                        result.AuthorizationTransactionCode = response.resultCode.ToString();
                        result.AuthorizationTransactionResult = string.Format("Approved ({0}: {1})", response.resultCode.ToString(), response.subscriptionId.ToString());

                        if (_cybersourcePaymentSettings.TransactMode == TransactMode.Authorize)
                        {
                            result.NewPaymentStatus = PaymentStatus.Authorized;
                        }
                        else
                        {
                            result.NewPaymentStatus = PaymentStatus.Paid;
                        }
                    }
                    else
                    {
                        result.AddError(string.Format("Error processing recurring payment. {0}", GetErrors(response)));
                    }
                }
            }

            return result;
        }

        /// <summary>
        /// Cancels a recurring payment
        /// </summary>
        /// <param name="cancelPaymentRequest">Request</param>
        /// <returns>Result</returns>
        public CancelRecurringPaymentResult CancelRecurringPayment(CancelRecurringPaymentRequest cancelPaymentRequest)
        {
            var result = new CancelRecurringPaymentResult();
            long subscriptionId;
            long.TryParse(cancelPaymentRequest.Order.SubscriptionTransactionId, out subscriptionId);


            using (var webService = new net.authorize.api.Service())
            {
                if (_cybersourcePaymentSettings.UseSandbox)
                    webService.Url = "https://apitest.authorize.net/soap/v1/Service.asmx";
                else
                    webService.Url = "https://api.authorize.net/soap/v1/Service.asmx";

                var authentication = PopulateMerchantAuthentication();
                var response = webService.ARBCancelSubscription(authentication, subscriptionId);

                if (response.resultCode == MessageTypeEnum.Ok)
                {
                    //ok
                }
                else
                {
                    result.AddError("Error cancelling subscription, please contact customer support. " + GetErrors(response));
                }
            }
            return result;
        }

        /// <summary>
        /// Gets a value indicating whether customers can complete a payment after order is placed but not completed (for redirection payment methods)
        /// </summary>
        /// <param name="order">Order</param>
        /// <returns>Result</returns>
        public bool CanRePostProcessPayment(Order order)
        {
            if (order == null)
                throw new ArgumentNullException("order");
            
            //it's not a redirection payment method. So we always return false
            return false;
        }

        /// <summary>
        /// Gets a route for provider configuration
        /// </summary>
        /// <param name="actionName">Action name</param>
        /// <param name="controllerName">Controller name</param>
        /// <param name="routeValues">Route values</param>
        public void GetConfigurationRoute(out string actionName, out string controllerName, out RouteValueDictionary routeValues)
        {
            actionName = "Configure";
            controllerName = "PaymentCybersource";
            routeValues = new RouteValueDictionary { { "Namespaces", "Nop.Plugin.Payments.Cybersource.Controllers" }, { "area", null } };
        }

        /// <summary>
        /// Gets a route for payment info
        /// </summary>
        /// <param name="actionName">Action name</param>
        /// <param name="controllerName">Controller name</param>
        /// <param name="routeValues">Route values</param>
        public void GetPaymentInfoRoute(out string actionName, out string controllerName, out RouteValueDictionary routeValues)
        {
            actionName = "PaymentInfo";
            controllerName = "PaymentCybersource";
            routeValues = new RouteValueDictionary { { "Namespaces", "Nop.Plugin.Payments.Cybersource.Controllers" }, { "area", null } };
        }

        public Type GetControllerType()
        {
            return typeof(PaymentCybersourceController);
        }

        public override void Install()
        {
            //settings
            var settings = new CybersourcePaymentSettings
            {
                UseSandbox = true,
                TransactMode = TransactMode.Authorize,
                TransactionKey = "123",
                LoginId = "456"
            };
            _settingService.SaveSetting(settings);

            //locales
            this.AddOrUpdatePluginLocaleResource("Plugins.Payments.Cybersource.Notes", "If you're using this gateway, ensure that your primary store currency is supported by Authorize.NET.");
            this.AddOrUpdatePluginLocaleResource("Plugins.Payments.Cybersource.Fields.UseSandbox", "Use Sandbox");
            this.AddOrUpdatePluginLocaleResource("Plugins.Payments.Cybersource.Fields.UseSandbox.Hint", "Check to enable Sandbox (testing environment).");
            this.AddOrUpdatePluginLocaleResource("Plugins.Payments.Cybersource.Fields.TransactModeValues", "Transaction mode");
            this.AddOrUpdatePluginLocaleResource("Plugins.Payments.Cybersource.Fields.TransactModeValues.Hint", "Choose transaction mode");
            this.AddOrUpdatePluginLocaleResource("Plugins.Payments.Cybersource.Fields.TransactionKey", "Transaction key");
            this.AddOrUpdatePluginLocaleResource("Plugins.Payments.Cybersource.Fields.TransactionKey.Hint", "Specify transaction key");
            this.AddOrUpdatePluginLocaleResource("Plugins.Payments.Cybersource.Fields.LoginId", "Login ID");
            this.AddOrUpdatePluginLocaleResource("Plugins.Payments.Cybersource.Fields.LoginId.Hint", "Specify login identifier.");
            this.AddOrUpdatePluginLocaleResource("Plugins.Payments.Cybersource.Fields.AdditionalFee", "Additional fee");
            this.AddOrUpdatePluginLocaleResource("Plugins.Payments.Cybersource.Fields.AdditionalFee.Hint", "Enter additional fee to charge your customers.");
            this.AddOrUpdatePluginLocaleResource("Plugins.Payments.Cybersource.Fields.AdditionalFeePercentage", "Additional fee. Use percentage");
            this.AddOrUpdatePluginLocaleResource("Plugins.Payments.Cybersource.Fields.AdditionalFeePercentage.Hint", "Determines whether to apply a percentage additional fee to the order total. If not enabled, a fixed value is used.");

            
            base.Install();
        }

        public override void Uninstall()
        {
            //settings
            _settingService.DeleteSetting<CybersourcePaymentSettings>();

            //locales
            this.DeletePluginLocaleResource("Plugins.Payments.Cybersource.Notes");
            this.DeletePluginLocaleResource("Plugins.Payments.Cybersource.Fields.UseSandbox");
            this.DeletePluginLocaleResource("Plugins.Payments.Cybersource.Fields.UseSandbox.Hint");
            this.DeletePluginLocaleResource("Plugins.Payments.Cybersource.Fields.TransactModeValues");
            this.DeletePluginLocaleResource("Plugins.Payments.Cybersource.Fields.TransactModeValues.Hint");
            this.DeletePluginLocaleResource("Plugins.Payments.Cybersource.Fields.TransactionKey");
            this.DeletePluginLocaleResource("Plugins.Payments.Cybersource.Fields.TransactionKey.Hint");
            this.DeletePluginLocaleResource("Plugins.Payments.Cybersource.Fields.LoginId");
            this.DeletePluginLocaleResource("Plugins.Payments.Cybersource.Fields.LoginId.Hint");
            this.DeletePluginLocaleResource("Plugins.Payments.Cybersource.Fields.AdditionalFee");
            this.DeletePluginLocaleResource("Plugins.Payments.Cybersource.Fields.AdditionalFee.Hint");
            this.DeletePluginLocaleResource("Plugins.Payments.Cybersource.Fields.AdditionalFeePercentage");
            this.DeletePluginLocaleResource("Plugins.Payments.Cybersource.Fields.AdditionalFeePercentage.Hint");
            
            base.Uninstall();
        }

        #endregion

        #region Properies

        /// <summary>
        /// Gets a value indicating whether capture is supported
        /// </summary>
        public bool SupportCapture
        {
            get
            {
                return true;
            }
        }

        /// <summary>
        /// Gets a value indicating whether partial refund is supported
        /// </summary>
        public bool SupportPartiallyRefund
        {
            get
            {
                return true;
            }
        }

        /// <summary>
        /// Gets a value indicating whether refund is supported
        /// </summary>
        public bool SupportRefund
        {
            get
            {
                return true;
            }
        }

        /// <summary>
        /// Gets a value indicating whether void is supported
        /// </summary>
        public bool SupportVoid
        {
            get
            {
                return true;
            }
        }

        /// <summary>
        /// Gets a recurring payment type of payment method
        /// </summary>
        public RecurringPaymentType RecurringPaymentType
        {
            get
            {
                return RecurringPaymentType.Manual;
            }
        }

        /// <summary>
        /// Gets a payment method type
        /// </summary>
        public PaymentMethodType PaymentMethodType
        {
            get
            {
                return PaymentMethodType.Standard;
            }
        }

        /// <summary>
        /// Gets a value indicating whether we should display a payment information page for this plugin
        /// </summary>
        public bool SkipPaymentInfo
        {
            get
            {
                return false;
            }
        }

        #endregion
    }
}

