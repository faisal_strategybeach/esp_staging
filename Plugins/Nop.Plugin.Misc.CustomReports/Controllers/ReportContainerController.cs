﻿using System;
using System.Linq;
using System.Web.Mvc;
using Nop.Core.Domain.Discounts;
using Nop.Services.Configuration;
using Nop.Services.Customers;
using Nop.Services.Discounts;
using Nop.Services.Security;
using Nop.Web.Framework.Controllers;
using Nop.Web.Framework.Security;

namespace Nop.Plugin.Misc.CustomReports.Controllers
{
    [AdminAuthorize]
    public class ReportContainerController : BasePluginController
    {
      
        public ActionResult Show()
        {
            return View("Show");
        }
    }
}