﻿using System.Web.Mvc;
using System.Web.Routing;
using Nop.Web.Framework.Mvc.Routes;

namespace Nop.Plugin.Misc.CustomReports
{
    public partial class RouteProvider : IRouteProvider
    {
        public void RegisterRoutes(RouteCollection routes)
        {
            routes.MapRoute("Nop.Plugin.Misc.CustomReports.ReportContainer",
                 "Admin/ReportContainer/Show",
                 new { controller = "ReportContainer", action = "Show" },
                 new[] { "Nop.Plugin.Misc.CustomReports.ReportContainer.Controllers" }
            );
        }
        public int Priority
        {
            get
            {
                return 0;
            }
        }
    }
}
