﻿using Nop.Core;
using Nop.Plugin.Widgets.CheckSubscriber.Domain;
using Nop.Plugin.Widgets.CheckSubscriber.Models;
using Nop.Services.Configuration;
using Nop.Services.Localization;
using Nop.Services.Stores;
using Nop.Web.Framework.Controllers;
using System;
using System.Text;
using System.Threading.Tasks;
using System.Web.Mvc;
using System.Net.Http;
using System.Net.Http.Headers;
using Nop.Services.Customers;
using System.Linq;
using System.Data.SqlClient;
using System.Data;
using System.Web.Configuration;
using Nop.Services.Orders; 

namespace Nop.Plugin.Widgets.CheckSubscriber.Controllers
{
    class subscriberDetails
    {
        public string accountNumber { get; set; }
        public string zipCode { get; set; }
    }

    public class WidgetsCheckSubscriberController : BasePluginController
    {
        private IWorkContext _workContext;
        private IStoreContext _storeContext;
        private IStoreService _storeService;
        private ISettingService _settingService;
        private ILocalizationService _localizationService;
        private ICustomerService _customerService;

        //static public string _acctStatus; 
        private string sqlConnectionString;

        public WidgetsCheckSubscriberController(IWorkContext workContext,
            IStoreContext storeContext,
            IStoreService storeService,
            ISettingService settingService,
            ILocalizationService localizationService,
            ICustomerService customerService)
        {
            this._workContext = workContext;
            this._storeContext = storeContext;
            this._storeService = storeService;
            this._settingService = settingService;
            this._localizationService = localizationService;
            this._customerService = customerService;
        }

        [AdminAuthorize]
        [ChildActionOnly]
        public ActionResult Configure()
        {
            ConfigurationModel model = new ConfigurationModel();

            return View("~/Plugins/Widgets.CheckSubscriber/Views/WidgetsCheckSubscriber/Configure.cshtml", model);
        }

        [HttpPost]
        [AdminAuthorize]
        [ChildActionOnly]
        public ActionResult Configure(ConfigurationModel model)
        {
            //now clear settings cache
            _settingService.ClearCache();

            SuccessNotification(_localizationService.GetResource("Admin.Plugins.Saved"));
            return Configure();
        }

        [ChildActionOnly]
        public ActionResult PublicInfo(string widgetZone, object additionalData = null)
        {
            CheckSubscriberRecord checkSubModel = new CheckSubscriberRecord();

            checkSubModel.Display = true;

            var customer = _workContext.CurrentCustomer;
            var role = _customerService.GetCustomerRoleBySystemName("NASO Member");
            if (role != null)
            {
                if (customer.CustomerRoles.Any(cr => cr.SystemName == "NASO Member"))
                    checkSubModel.Display = false;  
            }
           
            //return View("~/Plugins/Widgets.CheckSubscriber/Views/WidgetsCheckSubscriber/PublicInfo.cshtml", model);
            return View("~/Plugins/Widgets.CheckSubscriber/Views/WidgetsCheckSubscriber/PublicInfo.cshtml", checkSubModel);
        }

        [HttpPost]
        public ActionResult ValidateSub([Bind(Include = "AccountNumber, AccountZipCode")] CheckSubscriberRecord vm)
        {
            //string returnText = validateSubscriber("NO", "000500", "91606");
            string returnText = validateSubscriber("NO", vm.AccountNumber, vm.AccountZipCode);
            //ValidateAsync(vm.AccountNumber, vm.AccountZipCode).Wait();

            bool someCondition = true;

            if (returnText != "A")
            {
                someCondition = false;
                return Json(new { error = "true", message = "Sorry – but that membership number/Postal Code combination is not valid. Please try again or contact customer service at: <a href='mailto:cservice@referee.com'>cservice@referee.com</a>" });
            }

            if (someCondition)
            {
                
                //Sanjay 07 Jul 2015 (Commented below code for saving "NASO Member" role of customer
                //We are now asking every time MemberID and ZipCode before checkout.                
                var customer = _workContext.CurrentCustomer;
                var role = _customerService.GetCustomerRoleBySystemName("NASO Member");
                if (role == null)
                    throw new NopException("'Your' role could not be loaded");
                if (!customer.CustomerRoles.Any(cr => cr.SystemName == "NASO Member")) 
                {
                    customer.CustomerRoles.Add(role);
                    _customerService.UpdateCustomer(customer);
                }
               
            }

            return View("~/Plugins/Widgets.CheckSubscriber/Views/WidgetsCheckSubscriber/ValidatorMesg.cshtml", vm);
        }

        static async Task ValidateAsync(string accountNumber, string zipCode)
        {
            using (var client = new HttpClient())
            {
                //client.BaseAddress = new Uri("http://localhost/");
                //client.DefaultRequestHeaders.Accept.Clear();
                //client.DefaultRequestHeaders.Accept.Add(new MediaTypeWithQualityHeaderValue("application/json"));

                HttpResponseMessage response = await client.GetAsync("http://localhost/TestApi/api/AccountStatus?pubcode=NO&acctNo=000500&zipCode=91606");
                if (response.IsSuccessStatusCode)
                {
                    string acctStatus = await response.Content.ReadAsStringAsync();
                    //_acctStatus = acctStatus; 
                    //return await Task.Run(() > JsonObject.Parse(acctStatus));
                }
                else
                {
                    // show the response status code
                    String failureMsg = "HTTP Status: " + response.StatusCode.ToString() + " - Reason: " + response.ReasonPhrase;
                }
                
            }

        }

        public string validateSubscriber(string pubCode, string acctNo, string zipCode)
        {
            
           
           // sqlConnectionString = "Data source=ESPDEV-VM1;Initial Catalog=Fulfillment;User ID=pubservdbu;Password=Z23^BZTek";

            string acctStatus = string.Empty;

            if (string.IsNullOrEmpty(pubCode)) return "No Pub Code";
            if (string.IsNullOrEmpty(acctNo)) return "No acct no";
            if (string.IsNullOrEmpty(zipCode)) return "No zipcode";

            if (acctNo.Length < 6) return "Incorrect Account number";
            if (zipCode.Length < 5) return "Incorrect ZipCode";

            string tempPubCode = acctNo.Substring(0, 2).ToUpper();

            if (pubCode == tempPubCode)
            { acctNo = acctNo.Substring(2); }
            else
            {
                acctNo = acctNo.Trim().Substring(0, 6);
            }

            zipCode = zipCode.Trim().Substring(0, 5);


            const string WSReq = "<ws1000request><pubcode>{0}</pubcode><uid>{1}</uid><upwd>{2}</upwd></ws1000request>";
            string cuid = WebConfigurationManager.AppSettings["PubServiceCUID"];
            string pswd = WebConfigurationManager.AppSettings["PubServicePassword"];

            string reqString = string.Format(WSReq, pubCode, acctNo, zipCode);
            string serviceUrl = WebConfigurationManager.AppSettings["PubServiceUrl"];
            var remoteAddress = new System.ServiceModel.EndpointAddress(serviceUrl);
            System.Xml.XmlNode loXMLNode = null;
            using (var loPubService = new pubservice.ListenerSoapClient(new System.ServiceModel.BasicHttpBinding(), remoteAddress))
            {
                //set timeout
                loPubService.Endpoint.Binding.SendTimeout = new TimeSpan(0, 0, 0, 90);

                //call web service method
                loXMLNode = loPubService.ws1000(cuid, pswd, reqString);
            } 

          
           
            System.Xml.XmlNode loXMLNode1 = loXMLNode.SelectSingleNode("/returncode");
            if (loXMLNode1.InnerText == "1") acctStatus = "A";
            else acctStatus = "Account Not Found";
            
            //using (SqlConnection connection = new SqlConnection(sqlConnectionString))
            //{
            //    SqlCommand command = new SqlCommand(
            //      "Select SMVAStatus From SMVA WITH (NOLOCK) Where SMVAPub = @pubcode AND SMVAAcctNo = @acctno AND LEFT(SMVAZip,5) = @zipcode;",
            //      connection);
            //    connection.Open();
            //    command.Parameters.Add("@pubcode", SqlDbType.Char, 2);
            //    command.Parameters["@pubcode"].Value = pubCode;
            //    command.Parameters.Add("@acctno", SqlDbType.Char, 6);
            //    command.Parameters["@acctno"].Value = acctNo;
            //    command.Parameters.Add("@zipcode", SqlDbType.VarChar, 10);
            //    command.Parameters["@zipcode"].Value = zipCode;
            //    SqlDataReader reader = command.ExecuteReader();
            //    bool hasRows = false;
            //    try
            //    {
            //        hasRows = reader.HasRows;
            //        if (hasRows)
            //        {
            //            while (reader.Read())
            //            {
            //                acctStatus = reader.GetString(0);
            //                return acctStatus;
            //            }
            //        }
            //        else
            //        {
            //            return "Account Not Found";
            //        }
            //    }
            //    catch (Exception ex)
            //    {
            //        //TODO
            //        //send out error email
            //    }
            //    finally
            //    {
            //        reader.Close();
            //    }
            //}
            return acctStatus;
        }


    }
}
