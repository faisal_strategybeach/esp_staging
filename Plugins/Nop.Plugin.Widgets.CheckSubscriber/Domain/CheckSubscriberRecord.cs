﻿using FluentValidation.Attributes;
using Nop.Core;
using Nop.Web.Framework;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Nop.Plugin.Widgets.CheckSubscriber.Domain
{
    [Validator(typeof(CheckSubscriberValidator))]
   public class CheckSubscriberRecord: BaseEntity
    {
        public int CheckSubscriberId { get; set; }
        [NopResourceDisplayName("Membership Number: ")]
        public string AccountNumber { get; set; }
        [NopResourceDisplayName("ZIP/Postal Code: ")]
        public string AccountZipCode { get; set; }
        public int CustomerId { get; set; }
        public bool Display { get; set; }
    }
}
