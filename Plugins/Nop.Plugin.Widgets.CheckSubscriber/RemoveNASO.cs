﻿using Nop.Core;
using Nop.Core.Domain.Orders;
using Nop.Core.Events;
using Nop.Core.Plugins;
using Nop.Services.Customers;
using Nop.Services.Events;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Nop.Plugin.Widgets.CheckSubscriber
{
    public class RemoveNASOPlugin : IConsumer<OrderPlacedEvent>
    {
        private readonly ICustomerService _customerService;
        private readonly IPluginFinder _pluginFinder;

        public RemoveNASOPlugin(ICustomerService customerService,
            IPluginFinder pluginFinder)
        {
            this._customerService = customerService;
            this._pluginFinder = pluginFinder;
        }
        /// <summary>
        /// Handles the event.
        /// </summary>
        /// <param name="eventMessage">The event message.</param>
        public void HandleEvent(OrderPlacedEvent eventMessage)
        {

            var customer = eventMessage.Order.Customer;
            var role = _customerService.GetCustomerRoleBySystemName("NASO Member");
            if (role == null)
                throw new NopException("'Your' role could not be loaded");
            if (customer.CustomerRoles.Any(cr => cr.SystemName == "NASO Member"))
            {
                customer.CustomerRoles.Remove(role);
                _customerService.UpdateCustomer(customer);
            }
        }
    }
}
