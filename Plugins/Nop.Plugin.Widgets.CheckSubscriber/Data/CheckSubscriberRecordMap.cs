﻿using Nop.Plugin.Widgets.CheckSubscriber.Domain;
using System;
using System.Collections.Generic;
using System.Data.Entity.ModelConfiguration;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Nop.Plugin.Widgets.CheckSubscriber.Data
{
    class CheckSubscriberRecordMap : EntityTypeConfiguration<CheckSubscriberRecord>
    {
        public CheckSubscriberRecordMap()
        {
            ToTable("CheckSubscriber");
            HasKey(m => m.CheckSubscriberId);

            Property(m => m.AccountNumber);
            Property(m => m.AccountZipCode);
            Property(m => m.CustomerId);

        }
    }
}
