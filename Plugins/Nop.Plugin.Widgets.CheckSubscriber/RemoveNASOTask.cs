﻿using Nop.Core.Data;
using System;
using System.Collections.Generic;
using System.Data.SqlClient;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Nop.Plugin.Widgets.CheckSubscriber
{
    public class RemoveNASOTask: Nop.Services.Tasks.ITask
    {
        
        string connectionString;
        public RemoveNASOTask()
        {
            connectionString = new DataSettingsManager().LoadSettings().DataConnectionString;
        }

        public void Execute()
        {
            SqlConnection con = new SqlConnection(connectionString);
            SqlCommand cmd = new SqlCommand("delete from Customer_CustomerRole_Mapping where CustomerRole_Id=9", con);
            con.Open();
            using(con)
            {
                cmd.ExecuteNonQuery();
            }
        }

    }
}
