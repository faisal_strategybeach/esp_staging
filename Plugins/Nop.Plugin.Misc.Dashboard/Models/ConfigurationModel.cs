﻿using System.Collections.Generic;
using System.Web.Mvc;
using Nop.Web.Framework;
using Nop.Web.Framework.Localization;
using Nop.Web.Framework.Mvc;

namespace Nop.Plugin.Misc.Dashboard.Models
{
    public class ConfigurationModel : BaseNopModel
    {
        public string DescriptionText { get; set; }
        public string Skin { get; set; }
        public string TopN { get; set; }
        public string TimeZone { get; set; }
        public string PriceWithTax { get; set; }
        public string Active { get; set; }
        public string AvailableZones { get; set; }
        public string DashboardAdminChartWeek { get; set; }
        public string DashboardAdminChartMonth { get; set; }
        public string DashboardAdminChartYear { get; set; }
        public string DashboardAdminChartMYear { get; set; }
        public string DashboardAdminChartProduct { get; set; }
        public string DashboardAdminChartProductLastMonth { get; set; }
        public string LastMonth { get; set; }
        public string DashboardAdminChartCategory { get; set; }
        public string DashboardAdminChartManufacturer { get; set; }
        public string DashboardAdminChartVendor { get; set; }
        public string OrderStatus { get; set; }
        public string OrderStatuses { get; set; }
        public string PaymentStatus { get; set; }
        public string PaymentStatuses { get; set; }


        public partial class ConfigurationLocalizedModel : ILocalizedModelLocal
        {
            public int LanguageId { get; set; }

            [AllowHtml]
            public string DescriptionText { get; set; }
        }
    }
}
