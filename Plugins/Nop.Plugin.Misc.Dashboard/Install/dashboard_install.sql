IF EXISTS (
		SELECT *
		FROM sys.objects
		WHERE object_id = OBJECT_ID(N'[dbo].[UDTToLocalTime]') AND OBJECTPROPERTY(object_id,N'IsScalarFunction') = 1)
DROP FUNCTION [dbo].[UDTToLocalTime]
GO
CREATE FUNCTION [dbo].[UDTToLocalTime](@UDT AS DATETIME)  
RETURNS DATETIME
AS
BEGIN 
--====================================================
--Set the Timezone Offset (NOT During DST [Daylight Saving Time])
--====================================================
DECLARE @Offset AS SMALLINT
SET @Offset = isnull((select top 1 Value from [dbo].[Setting] where name = 'dashboardsettings.timezone') ,0)

--====================================================
--Figure out the Offset Datetime
--====================================================
DECLARE @LocalDate AS DATETIME
SET @LocalDate = DATEADD(hh, @Offset, @UDT)

--====================================================
--Figure out the DST Offset for the UDT Datetime
--====================================================
DECLARE @DaylightSavingOffset AS SMALLINT
DECLARE @Year as SMALLINT
DECLARE @DSTStartDate AS DATETIME
DECLARE @DSTEndDate AS DATETIME
--Get Year
SET @Year = YEAR(@LocalDate)

--Get First Possible DST StartDay
IF (@Year > 2006) SET @DSTStartDate = CAST(@Year AS CHAR(4)) + '-03-08 02:00:00'
ELSE              SET @DSTStartDate = CAST(@Year AS CHAR(4)) + '-04-01 02:00:00'
--Get DST StartDate 
WHILE (DATENAME(dw, @DSTStartDate) <> 'sunday') SET @DSTStartDate = DATEADD(day, 1,@DSTStartDate)


--Get First Possible DST EndDate
IF (@Year > 2006) SET @DSTEndDate = CAST(@Year AS CHAR(4)) + '-11-01 02:00:00'
ELSE              SET @DSTEndDate = CAST(@Year AS CHAR(4)) + '-10-25 02:00:00'
--Get DST EndDate 
WHILE (DATENAME(dw, @DSTEndDate) <> 'sunday') SET @DSTEndDate = DATEADD(day,1,@DSTEndDate)

--Get DaylightSavingOffset
SET @DaylightSavingOffset = CASE WHEN @LocalDate BETWEEN @DSTStartDate AND @DSTEndDate THEN 1 ELSE 0 END

--====================================================
--Finally add the DST Offset 
--====================================================
RETURN DATEADD(hh, @DaylightSavingOffset, @LocalDate)
END
GO
IF EXISTS (
		SELECT *
		FROM sys.objects
		WHERE object_id = OBJECT_ID(N'[dbo].[nop4you_dashboard_categories]') AND OBJECTPROPERTY(object_id,N'IsProcedure') = 1)
DROP PROCEDURE [dbo].[nop4you_dashboard_categories]
GO
CREATE PROCEDURE [dbo].[nop4you_dashboard_categories]
(
	@CategoryId			int,
	@ControlId			int,
	@GroupId			int = 3,
	@StartDate			Datetime = '19000101',
	@EndDate			Datetime = '20991231',
	@OrderStatusId		int = 0,
	@PaymentStatusId	int = 0	
)
AS
BEGIN

	set @GroupId = isnull(@GroupId,3)
	set @OrderStatusId = isnull(@OrderStatusId,0)
	set @PaymentStatusId = isnull(@PaymentStatusId,0)
	

	DECLARE @topN INT
	declare @priceTax bit
	SET @topN = (select Value from Setting where Name = 'dashboardsettings.topn')
	SET @priceTax = (select case when [Value]='true' then '1' else '0' end from Setting where Name = 'dashboardsettings.PriceWithTax')

	IF ((@StartDate is null) OR (@StartDate = '')) SET @StartDate = '19000101'
	IF ((@EndDate is null) OR (@EndDate = '')) SET @EndDate = '20991231'

	if @ControlId in (1,2)
	Begin
		SELECT CASE 
				WHEN @GroupId = 1 THEN CONVERT(NVARCHAR(10),dbo.UDTToLocalTime(T1.CreatedOnUtc),120) 
				WHEN @GroupId = 2 THEN CONVERT(NVARCHAR(4),DATEPART(YEAR, dbo.UDTToLocalTime(T1.CreatedOnUtc))) + '-' + CASE WHEN LEN(CONVERT(NVARCHAR(2),DATEPART(WEEK, dbo.UDTToLocalTime(T1.CreatedOnUtc)))) = 1 THEN '0' + CONVERT(NVARCHAR(2),DATEPART(WEEK, dbo.UDTToLocalTime(T1.CreatedOnUtc))) ELSE CONVERT(NVARCHAR(2),DATEPART(WEEK, dbo.UDTToLocalTime(T1.CreatedOnUtc))) END
				WHEN @GroupId = 3 THEN CONVERT(NVARCHAR(4),DATEPART(YEAR, dbo.UDTToLocalTime(T1.CreatedOnUtc))) + '-' + CASE WHEN LEN(CONVERT(NVARCHAR(20),DATEPART(MONTH, dbo.UDTToLocalTime(T1.CreatedOnUtc)))) = 1 THEN '0' + CONVERT(NVARCHAR(20),DATEPART(MONTH, dbo.UDTToLocalTime(T1.CreatedOnUtc))) ELSE CONVERT(NVARCHAR(20),DATEPART(MONTH, dbo.UDTToLocalTime(T1.CreatedOnUtc))) END
				WHEN @GroupId = 4 THEN CONVERT(NVARCHAR(4),DATEPART(YEAR, dbo.UDTToLocalTime(T1.CreatedOnUtc))) + '-' + CASE WHEN LEN(CONVERT(NVARCHAR(2),DATEPART(QUARTER, dbo.UDTToLocalTime(T1.CreatedOnUtc)))) = 1 THEN '0' + CONVERT(NVARCHAR(2),DATEPART(QUARTER, dbo.UDTToLocalTime(T1.CreatedOnUtc))) ELSE CONVERT(NVARCHAR(2),DATEPART(QUARTER, dbo.UDTToLocalTime(T1.CreatedOnUtc))) END
				WHEN @GroupId = 5 THEN CONVERT(NVARCHAR(4),DATEPART(YEAR, dbo.UDTToLocalTime(T1.CreatedOnUtc)))
				ELSE CONVERT(NVARCHAR(4),DATEPART(YEAR, dbo.UDTToLocalTime(T1.CreatedOnUtc))) + '-' + CASE WHEN LEN(CONVERT(NVARCHAR(20),DATEPART(MONTH, dbo.UDTToLocalTime(T1.CreatedOnUtc)))) = 1 THEN '0' + CONVERT(NVARCHAR(20),DATEPART(MONTH, dbo.UDTToLocalTime(T1.CreatedOnUtc))) ELSE CONVERT(NVARCHAR(20),DATEPART(MONTH, dbo.UDTToLocalTime(T1.CreatedOnUtc))) END
			END AS DimName,
			case when @priceTax = 1 then SUM(T2.PriceInclTax) else SUM(T2.PriceExclTax) end AS TotalSales FROM [Order] T1
			INNER JOIN [OrderItem] T2 ON T1.Id = T2.OrderId
			INNER JOIN [Product_Category_Mapping] T3 ON T2.ProductId = T3.ProductId
			WHERE (cast(dbo.UDTToLocalTime(T1.[CreatedOnUtc]) as Date) BETWEEN @StartDate AND @EndDate) 
				AND (T3.CategoryId in (select Id from dbo.nop4you_function_category(@CategoryId,0))) 
				AND T1.OrderStatusId like (CASE WHEN @OrderStatusId = 0 then '[1234]0' Else convert(nvarchar(2),@OrderStatusId) end)
				AND T1.PaymentStatusId like (CASE WHEN @PaymentStatusId = 0 then '[1234]%' Else convert(nvarchar(2),@PaymentStatusId) end)
				AND T1.Deleted = 0
			GROUP BY 
				CASE 
					WHEN @GroupId = 1 THEN CONVERT(NVARCHAR(10),dbo.UDTToLocalTime(T1.CreatedOnUtc),120) 
					WHEN @GroupId = 2 THEN CONVERT(NVARCHAR(4),DATEPART(YEAR, dbo.UDTToLocalTime(T1.CreatedOnUtc))) + '-' + CASE WHEN LEN(CONVERT(NVARCHAR(2),DATEPART(WEEK, dbo.UDTToLocalTime(T1.CreatedOnUtc)))) = 1 THEN '0' + CONVERT(NVARCHAR(2),DATEPART(WEEK, dbo.UDTToLocalTime(T1.CreatedOnUtc))) ELSE CONVERT(NVARCHAR(2),DATEPART(WEEK, dbo.UDTToLocalTime(T1.CreatedOnUtc))) END
					WHEN @GroupId = 3 THEN CONVERT(NVARCHAR(4),DATEPART(YEAR, dbo.UDTToLocalTime(T1.CreatedOnUtc))) + '-' + CASE WHEN LEN(CONVERT(NVARCHAR(20),DATEPART(MONTH, dbo.UDTToLocalTime(T1.CreatedOnUtc)))) = 1 THEN '0' + CONVERT(NVARCHAR(20),DATEPART(MONTH, dbo.UDTToLocalTime(T1.CreatedOnUtc))) ELSE CONVERT(NVARCHAR(20),DATEPART(MONTH, dbo.UDTToLocalTime(T1.CreatedOnUtc))) END
					WHEN @GroupId = 4 THEN CONVERT(NVARCHAR(4),DATEPART(YEAR, dbo.UDTToLocalTime(T1.CreatedOnUtc))) + '-' + CASE WHEN LEN(CONVERT(NVARCHAR(2),DATEPART(QUARTER, dbo.UDTToLocalTime(T1.CreatedOnUtc)))) = 1 THEN '0' + CONVERT(NVARCHAR(2),DATEPART(QUARTER, dbo.UDTToLocalTime(T1.CreatedOnUtc))) ELSE CONVERT(NVARCHAR(2),DATEPART(QUARTER, dbo.UDTToLocalTime(T1.CreatedOnUtc))) END
					WHEN @GroupId = 5 THEN CONVERT(NVARCHAR(4),DATEPART(YEAR, dbo.UDTToLocalTime(T1.CreatedOnUtc)))
					ELSE CONVERT(NVARCHAR(4),DATEPART(YEAR, dbo.UDTToLocalTime(T1.CreatedOnUtc))) + '-' + CASE WHEN LEN(CONVERT(NVARCHAR(20),DATEPART(MONTH, dbo.UDTToLocalTime(T1.CreatedOnUtc)))) = 1 THEN '0' + CONVERT(NVARCHAR(20),DATEPART(MONTH, dbo.UDTToLocalTime(T1.CreatedOnUtc))) ELSE CONVERT(NVARCHAR(20),DATEPART(MONTH, dbo.UDTToLocalTime(T1.CreatedOnUtc))) END
				END
	END



	if @ControlId = 3
	Begin
			SELECT TOP (@topN) T3.Name AS DimName,	SUM(T2.Quantity) AS Quantity, 
			case when @priceTax = 1 then SUM(T2.PriceInclTax) else SUM(T2.PriceExclTax) end AS TotalSales FROM [Order] T1
			INNER JOIN [OrderItem] T2 ON T1.Id = T2.OrderId
			INNER JOIN [Product] T3 ON T2.ProductId = T3.Id
			INNER JOIN [Product_Category_Mapping] T4 ON T3.Id = T4.ProductId
			WHERE (cast(dbo.UDTToLocalTime(T1.[CreatedOnUtc]) as Date) BETWEEN @StartDate AND @EndDate) 
			AND (T4.CategoryId in (select Id from dbo.nop4you_function_category(@CategoryId,0)))
			AND T1.OrderStatusId like (CASE WHEN @OrderStatusId = 0 then '[1234]0' Else convert(nvarchar(2),@OrderStatusId) end)
			AND T1.PaymentStatusId like (CASE WHEN @PaymentStatusId = 0 then '[1234]%' Else convert(nvarchar(2),@PaymentStatusId) end)
			AND T1.Deleted = 0
			GROUP BY T3.Name
			ORDER BY SUM(T1.OrderSubtotalExclTax) DESC
	end


	if @ControlId = 4
	Begin
			SELECT T3.Name AS DimName,	SUM(T2.Quantity) AS Quantity, 
			case when @priceTax = 1 then SUM(T2.PriceInclTax) else SUM(T2.PriceExclTax) end AS TotalSales FROM [Order] T1
			INNER JOIN [OrderItem] T2 ON T1.Id = T2.OrderId
			INNER JOIN [Product] T3 ON T2.ProductId = T3.Id
			INNER JOIN [Product_Category_Mapping] T4 ON T3.Id = T4.ProductId
			WHERE (cast(dbo.UDTToLocalTime(T1.[CreatedOnUtc]) as Date) BETWEEN @StartDate AND @EndDate) 
			AND (T4.CategoryId in (select Id from dbo.nop4you_function_category(@CategoryId,0))) 
			AND T1.OrderStatusId like (CASE WHEN @OrderStatusId = 0 then '[1234]0' Else convert(nvarchar(2),@OrderStatusId) end)
			AND T1.PaymentStatusId like (CASE WHEN @PaymentStatusId = 0 then '[1234]%' Else convert(nvarchar(2),@PaymentStatusId) end)
			AND T1.Deleted = 0
			GROUP BY T3.Name	
			ORDER BY SUM(T1.OrderSubtotalExclTax) DESC
	end
	
	if @ControlId in (5,6)
	BEGIN
	
		WITH tree (Id, ParentCategoryId, level, name) as 
		(
		   SELECT Id, ParentCategoryId, 0 as level, name
		   FROM Category
		   WHERE ParentCategoryId = 0 

		   UNION ALL

		   SELECT c2.Id, c2.ParentCategoryId, tree.level + 1, c2.name
		   FROM Category c2 
			 INNER JOIN tree ON tree.Id = c2.ParentCategoryId
	 
		)

		SELECT *
		into #GLOWNA
		FROM  tree  where ParentCategoryId = (select ParentCategoryId from Category where Id = @categoryId)

		SELECT T0.Name as DimName, isnull(sum(T1.Quantity),0) as Quantity, isnull(sum(T1.TotalSales),0) as TotalSales FROM
		(
		select T0.Id, T0.Name, T3.Id as ProductId, T3.Name as ProductName from #GLOWNA T0
		cross apply nop4you_function_category(T0.Id,0) T1
		LEFT JOIN [Product_Category_Mapping] T2 on T2.CategoryId = T1.Id
		LEFT JOIN [Product] T3 on T3.Id = T2.ProductId
		)T0
		LEFT JOIN 
		( 
			SELECT T3.Id,T3.Name,
			SUM(T2.Quantity) AS Quantity,
			SUM(T2.PriceInclTax) AS TotalSales 
			FROM [Order] T1
			INNER JOIN [OrderItem] T2 ON T1.Id = T2.OrderId
			INNER JOIN [Product] T3 ON T2.ProductId = T3.Id
			WHERE 
			(
				cast(dbo.UDTToLocalTime(T1.[CreatedOnUtc]) as Date) BETWEEN @StartDate AND @EndDate
				AND T1.OrderStatusId like (CASE WHEN @OrderStatusId = 0 then '[1234]0' Else convert(nvarchar(2),@OrderStatusId) end)
				AND T1.PaymentStatusId like (CASE WHEN @PaymentStatusId = 0 then '[1234]%' Else convert(nvarchar(2),@PaymentStatusId) end)
				AND T1.Deleted = 0
			) 
			GROUP BY T3.Id, T3.Name
		)T1 on T1.Id = T0.ProductId
		group by T0.Name
		having isnull(sum(T1.TotalSales),0) != 0
		drop table #GLOWNA
		
	END


END
GO
IF EXISTS (
		SELECT *
		FROM sys.objects
		WHERE object_id = OBJECT_ID(N'[dbo].[nop4you_dashboard_customer]') AND OBJECTPROPERTY(object_id,N'IsProcedure') = 1)
DROP PROCEDURE [dbo].[nop4you_dashboard_customer]
GO
CREATE PROCEDURE [dbo].[nop4you_dashboard_customer]
(
	@CustomerId			int = null,
	@ControlId			int = null,
	@GroupId			int = 3,
	@StartDate			Datetime = '19000101',
	@EndDate			Datetime = '20991231',
	@OrderStatusId		int = null,
	@PaymentStatusId	int = null	
)
AS
BEGIN

	DECLARE @topN INT
	DECLARE @priceTax bit

	set @GroupId = isnull(@GroupId,3)
	set @OrderStatusId = isnull(@OrderStatusId,0)
	set @PaymentStatusId = isnull(@PaymentStatusId,0)

	SET @topN = (select Value from Setting where Name = 'dashboardsettings.topn')
	SET @priceTax = (select case when [Value]='true' then '1' else '0' end from Setting where Name = 'dashboardsettings.PriceWithTax')

	IF ((@StartDate is null) OR (@StartDate = '')) SET @StartDate = '19000101'
	IF ((@EndDate is null) OR (@EndDate = '')) SET @EndDate = '20991231'

	if @ControlId in (1,2)
	Begin

		SELECT CASE 
				WHEN @GroupId = 1 THEN CONVERT(NVARCHAR(10),dbo.UDTToLocalTime(T1.CreatedOnUtc),120) 
				WHEN @GroupId = 2 THEN CONVERT(NVARCHAR(4),DATEPART(YEAR, dbo.UDTToLocalTime(T1.CreatedOnUtc))) + '-' + CASE WHEN LEN(CONVERT(NVARCHAR(2),DATEPART(WEEK, dbo.UDTToLocalTime(T1.CreatedOnUtc)))) = 1 THEN '0' + CONVERT(NVARCHAR(2),DATEPART(WEEK, dbo.UDTToLocalTime(T1.CreatedOnUtc))) ELSE CONVERT(NVARCHAR(2),DATEPART(WEEK, dbo.UDTToLocalTime(T1.CreatedOnUtc))) END
				WHEN @GroupId = 3 THEN CONVERT(NVARCHAR(4),DATEPART(YEAR, dbo.UDTToLocalTime(T1.CreatedOnUtc))) + '-' + CASE WHEN LEN(CONVERT(NVARCHAR(20),DATEPART(MONTH, dbo.UDTToLocalTime(T1.CreatedOnUtc)))) = 1 THEN '0' + CONVERT(NVARCHAR(20),DATEPART(MONTH, dbo.UDTToLocalTime(T1.CreatedOnUtc))) ELSE CONVERT(NVARCHAR(20),DATEPART(MONTH, dbo.UDTToLocalTime(T1.CreatedOnUtc))) END
				WHEN @GroupId = 4 THEN CONVERT(NVARCHAR(4),DATEPART(YEAR, dbo.UDTToLocalTime(T1.CreatedOnUtc))) + '-' + CASE WHEN LEN(CONVERT(NVARCHAR(2),DATEPART(QUARTER, dbo.UDTToLocalTime(T1.CreatedOnUtc)))) = 1 THEN '0' + CONVERT(NVARCHAR(2),DATEPART(QUARTER, dbo.UDTToLocalTime(T1.CreatedOnUtc))) ELSE CONVERT(NVARCHAR(2),DATEPART(QUARTER, dbo.UDTToLocalTime(T1.CreatedOnUtc))) END
				WHEN @GroupId = 5 THEN CONVERT(NVARCHAR(4),DATEPART(YEAR, dbo.UDTToLocalTime(T1.CreatedOnUtc)))
				ELSE CONVERT(NVARCHAR(4),DATEPART(YEAR, dbo.UDTToLocalTime(T1.CreatedOnUtc))) + '-' + CASE WHEN LEN(CONVERT(NVARCHAR(20),DATEPART(MONTH, dbo.UDTToLocalTime(T1.CreatedOnUtc)))) = 1 THEN '0' + CONVERT(NVARCHAR(20),DATEPART(MONTH, dbo.UDTToLocalTime(T1.CreatedOnUtc))) ELSE CONVERT(NVARCHAR(20),DATEPART(MONTH, dbo.UDTToLocalTime(T1.CreatedOnUtc))) END
			END AS DimName,
				case when @priceTax = 1 then SUM(T1.OrderSubtotalInclTax) else SUM(T1.OrderSubtotalExclTax) end AS TotalSales,
				count(*) as Quantity
		FROM [Order] T1
		WHERE 	(cast(dbo.UDTToLocalTime(T1.[CreatedOnUtc]) as Date) BETWEEN @StartDate AND @EndDate) 
			AND (T1.CustomerId = @CustomerId) 
			AND T1.OrderStatusId like (CASE WHEN @OrderStatusId = 0 then '[1234]0' Else convert(nvarchar(2),@OrderStatusId) end)
			AND T1.PaymentStatusId like (CASE WHEN @PaymentStatusId = 0 then '[1234]%' Else convert(nvarchar(2),@PaymentStatusId) end)
			AND T1.Deleted = 0
		GROUP BY 
				CASE 
					WHEN @GroupId = 1 THEN CONVERT(NVARCHAR(10),dbo.UDTToLocalTime(T1.CreatedOnUtc),120) 
					WHEN @GroupId = 2 THEN CONVERT(NVARCHAR(4),DATEPART(YEAR, dbo.UDTToLocalTime(T1.CreatedOnUtc))) + '-' + CASE WHEN LEN(CONVERT(NVARCHAR(2),DATEPART(WEEK, dbo.UDTToLocalTime(T1.CreatedOnUtc)))) = 1 THEN '0' + CONVERT(NVARCHAR(2),DATEPART(WEEK, dbo.UDTToLocalTime(T1.CreatedOnUtc))) ELSE CONVERT(NVARCHAR(2),DATEPART(WEEK, dbo.UDTToLocalTime(T1.CreatedOnUtc))) END
					WHEN @GroupId = 3 THEN CONVERT(NVARCHAR(4),DATEPART(YEAR, dbo.UDTToLocalTime(T1.CreatedOnUtc))) + '-' + CASE WHEN LEN(CONVERT(NVARCHAR(20),DATEPART(MONTH, dbo.UDTToLocalTime(T1.CreatedOnUtc)))) = 1 THEN '0' + CONVERT(NVARCHAR(20),DATEPART(MONTH, dbo.UDTToLocalTime(T1.CreatedOnUtc))) ELSE CONVERT(NVARCHAR(20),DATEPART(MONTH, dbo.UDTToLocalTime(T1.CreatedOnUtc))) END
					WHEN @GroupId = 4 THEN CONVERT(NVARCHAR(4),DATEPART(YEAR, dbo.UDTToLocalTime(T1.CreatedOnUtc))) + '-' + CASE WHEN LEN(CONVERT(NVARCHAR(2),DATEPART(QUARTER, dbo.UDTToLocalTime(T1.CreatedOnUtc)))) = 1 THEN '0' + CONVERT(NVARCHAR(2),DATEPART(QUARTER, dbo.UDTToLocalTime(T1.CreatedOnUtc))) ELSE CONVERT(NVARCHAR(2),DATEPART(QUARTER, dbo.UDTToLocalTime(T1.CreatedOnUtc))) END
					WHEN @GroupId = 5 THEN CONVERT(NVARCHAR(4),DATEPART(YEAR, dbo.UDTToLocalTime(T1.CreatedOnUtc)))
					ELSE CONVERT(NVARCHAR(4),DATEPART(YEAR, dbo.UDTToLocalTime(T1.CreatedOnUtc))) + '-' + CASE WHEN LEN(CONVERT(NVARCHAR(20),DATEPART(MONTH, dbo.UDTToLocalTime(T1.CreatedOnUtc)))) = 1 THEN '0' + CONVERT(NVARCHAR(20),DATEPART(MONTH, dbo.UDTToLocalTime(T1.CreatedOnUtc))) ELSE CONVERT(NVARCHAR(20),DATEPART(MONTH, dbo.UDTToLocalTime(T1.CreatedOnUtc))) END
				END
	end

	if @ControlId = 3
	Begin
			SELECT TOP (@topN) T3.Name AS DimName,	SUM(T2.Quantity) AS Quantity, 
			case when @priceTax = 1 then SUM(T2.PriceInclTax) else SUM(T2.PriceExclTax) end AS TotalSales
			FROM [Order] T1
			INNER JOIN [OrderItem] T2 ON T1.Id = T2.OrderId
			INNER JOIN [Product] T3 ON T2.ProductId = T3.Id			
			WHERE (cast(dbo.UDTToLocalTime(T1.[CreatedOnUtc]) as Date) BETWEEN @StartDate AND @EndDate) 
			AND (T1.CustomerId = @CustomerId) 
			AND T1.OrderStatusId like (CASE WHEN @OrderStatusId = 0 then '[1234]0' Else convert(nvarchar(2),@OrderStatusId) end)
			AND T1.PaymentStatusId like (CASE WHEN @PaymentStatusId = 0 then '[1234]%' Else convert(nvarchar(2),@PaymentStatusId) end)
			AND T1.Deleted = 0
			GROUP BY T3.Name
			ORDER BY SUM(T1.OrderSubtotalExclTax) DESC
	end

	if @ControlId = 4
	Begin
			SELECT T3.Name AS DimName,	SUM(T2.Quantity) AS Quantity, 
			case when @priceTax = 1 then SUM(T2.PriceInclTax) else SUM(T2.PriceExclTax) end AS TotalSales
			FROM [Order] T1
			INNER JOIN [OrderItem] T2 ON T1.Id = T2.OrderId
			INNER JOIN [Product] T3 ON T2.ProductId = T3.Id			
			WHERE (cast(dbo.UDTToLocalTime(T1.[CreatedOnUtc]) as Date) BETWEEN @StartDate AND @EndDate) 
			AND (T1.CustomerId = @CustomerId) 
			AND T1.OrderStatusId like (CASE WHEN @OrderStatusId = 0 then '[1234]0' Else convert(nvarchar(2),@OrderStatusId) end)
			AND T1.PaymentStatusId like (CASE WHEN @PaymentStatusId = 0 then '[1234]%' Else convert(nvarchar(2),@PaymentStatusId) end)
			AND T1.Deleted = 0
			GROUP BY T3.Name
			ORDER BY SUM(T1.OrderSubtotalExclTax) DESC
	end
	if @ControlId in (5,6)
	Begin
	
			SELECT T5.Name AS DimName,	SUM(T2.Quantity) AS Quantity, 
			case when @priceTax = 1 then SUM(T2.PriceInclTax) else SUM(T2.PriceExclTax) end AS TotalSales
			FROM [Order] T1
			INNER JOIN [OrderItem] T2 ON T1.Id = T2.OrderId
			INNER JOIN [Product] T3 ON T2.ProductId = T3.Id			
			INNER JOIN [Product_Category_Mapping] T4 on T4.ProductId = T3.Id
			INNER JOIN [Category] T5 ON T5.Id = T4.CategoryId
			WHERE (cast(dbo.UDTToLocalTime(T1.[CreatedOnUtc]) as Date) BETWEEN @StartDate AND @EndDate) 
			AND (T1.CustomerId = @CustomerId) 
			AND T1.OrderStatusId like (CASE WHEN @OrderStatusId = 0 then '[1234]0' Else convert(nvarchar(2),@OrderStatusId) end)
			AND T1.PaymentStatusId like (CASE WHEN @PaymentStatusId = 0 then '[1234]%' Else convert(nvarchar(2),@PaymentStatusId) end) 
			AND T1.Deleted = 0
			GROUP BY T5.Name
			ORDER BY SUM(T1.OrderSubtotalExclTax) DESC
	End
	if @ControlId in (7,8)
	Begin
	
			SELECT T5.Name AS DimName,	SUM(T2.Quantity) AS Quantity, 
			case when @priceTax = 1 then SUM(T2.PriceInclTax) else SUM(T2.PriceExclTax) end AS TotalSales
			FROM [Order] T1
			INNER JOIN [OrderItem] T2 ON T1.Id = T2.OrderId
			INNER JOIN [Product] T3 ON T2.ProductId = T3.Id			
			INNER JOIN [Product_Manufacturer_Mapping] T4 on T4.ProductId = T3.Id
			INNER JOIN [Manufacturer] T5 ON T5.Id = T4.ManufacturerId
			WHERE (cast(dbo.UDTToLocalTime(T1.[CreatedOnUtc]) as Date) BETWEEN @StartDate AND @EndDate) 
			AND (T1.CustomerId = @CustomerId) 
			AND T1.OrderStatusId like (CASE WHEN @OrderStatusId = 0 then '[1234]0' Else convert(nvarchar(2),@OrderStatusId) end)
			AND T1.PaymentStatusId like (CASE WHEN @PaymentStatusId = 0 then '[1234]%' Else convert(nvarchar(2),@PaymentStatusId) end) 
			AND T1.Deleted = 0
			GROUP BY T5.Name
			ORDER BY SUM(T1.OrderSubtotalExclTax) DESC

	End


END

GO
IF EXISTS (
		SELECT *
		FROM sys.objects
		WHERE object_id = OBJECT_ID(N'[dbo].[nop4you_dashboard_manufacturer]') AND OBJECTPROPERTY(object_id,N'IsProcedure') = 1)
DROP PROCEDURE [dbo].[nop4you_dashboard_manufacturer]
GO
CREATE PROCEDURE [dbo].[nop4you_dashboard_manufacturer]
(
	@ManufacturerId			int = null,
	@ControlId			int = null,
	@GroupId			int = null,
	@StartDate			Datetime = null,
	@EndDate			Datetime = null,
	@OrderStatusId		int = null,
	@PaymentStatusId	int = null	
)
AS
BEGIN

	set @GroupId = isnull(@GroupId,3)
	set @OrderStatusId = isnull(@OrderStatusId,0)
	set @PaymentStatusId = isnull(@PaymentStatusId,0)

	DECLARE @topN INT
	DECLARE @priceTax bit

	SET @topN = (select Value from Setting where Name = 'dashboardsettings.topn')

	IF ((@StartDate is null) OR (@StartDate = '')) SET @StartDate = '19000101'
	IF ((@EndDate is null) OR (@EndDate = '')) SET @EndDate = '20991231'
	SET @priceTax = (select case when [Value]='true' then '1' else '0' end from Setting where Name = 'dashboardsettings.PriceWithTax')

	if @ControlId in (1,2)
	Begin
		SELECT CASE 
				WHEN @GroupId = 1 THEN CONVERT(NVARCHAR(10),dbo.UDTToLocalTime(T1.CreatedOnUtc),120) 
				WHEN @GroupId = 2 THEN CONVERT(NVARCHAR(4),DATEPART(YEAR, dbo.UDTToLocalTime(T1.CreatedOnUtc))) + '-' + CASE WHEN LEN(CONVERT(NVARCHAR(2),DATEPART(WEEK, dbo.UDTToLocalTime(T1.CreatedOnUtc)))) = 1 THEN '0' + CONVERT(NVARCHAR(2),DATEPART(WEEK, dbo.UDTToLocalTime(T1.CreatedOnUtc))) ELSE CONVERT(NVARCHAR(2),DATEPART(WEEK, dbo.UDTToLocalTime(T1.CreatedOnUtc))) END
				WHEN @GroupId = 3 THEN CONVERT(NVARCHAR(4),DATEPART(YEAR, dbo.UDTToLocalTime(T1.CreatedOnUtc))) + '-' + CASE WHEN LEN(CONVERT(NVARCHAR(20),DATEPART(MONTH, dbo.UDTToLocalTime(T1.CreatedOnUtc)))) = 1 THEN '0' + CONVERT(NVARCHAR(20),DATEPART(MONTH, dbo.UDTToLocalTime(T1.CreatedOnUtc))) ELSE CONVERT(NVARCHAR(20),DATEPART(MONTH, dbo.UDTToLocalTime(T1.CreatedOnUtc))) END
				WHEN @GroupId = 4 THEN CONVERT(NVARCHAR(4),DATEPART(YEAR, dbo.UDTToLocalTime(T1.CreatedOnUtc))) + '-' + CASE WHEN LEN(CONVERT(NVARCHAR(2),DATEPART(QUARTER, dbo.UDTToLocalTime(T1.CreatedOnUtc)))) = 1 THEN '0' + CONVERT(NVARCHAR(2),DATEPART(QUARTER, dbo.UDTToLocalTime(T1.CreatedOnUtc))) ELSE CONVERT(NVARCHAR(2),DATEPART(QUARTER, dbo.UDTToLocalTime(T1.CreatedOnUtc))) END
				WHEN @GroupId = 5 THEN CONVERT(NVARCHAR(4),DATEPART(YEAR, dbo.UDTToLocalTime(T1.CreatedOnUtc)))
				ELSE CONVERT(NVARCHAR(4),DATEPART(YEAR, dbo.UDTToLocalTime(T1.CreatedOnUtc))) + '-' + CASE WHEN LEN(CONVERT(NVARCHAR(20),DATEPART(MONTH, dbo.UDTToLocalTime(T1.CreatedOnUtc)))) = 1 THEN '0' + CONVERT(NVARCHAR(20),DATEPART(MONTH, dbo.UDTToLocalTime(T1.CreatedOnUtc))) ELSE CONVERT(NVARCHAR(20),DATEPART(MONTH, dbo.UDTToLocalTime(T1.CreatedOnUtc))) END
			END AS DimName,
			case when @priceTax = 1 then SUM(T2.PriceInclTax) else SUM(T2.PriceExclTax) end AS TotalSales FROM [Order] T1
			INNER JOIN [OrderItem] T2 ON T1.Id = T2.OrderId
			INNER JOIN [Product_Manufacturer_Mapping] T3 ON T2.ProductId = T3.ProductId
			WHERE (cast(dbo.UDTToLocalTime(T1.[CreatedOnUtc]) as Date) BETWEEN @StartDate AND @EndDate) 
				AND (T3.ManufacturerId = @ManufacturerId) 
				AND T1.OrderStatusId like (CASE WHEN @OrderStatusId = 0 then '[1234]0' Else convert(nvarchar(2),@OrderStatusId) end)
				AND T1.PaymentStatusId like (CASE WHEN @PaymentStatusId = 0 then '[1234]%' Else convert(nvarchar(2),@PaymentStatusId) end)
				AND T1.Deleted = 0
			GROUP BY 
				CASE 
					WHEN @GroupId = 1 THEN CONVERT(NVARCHAR(10),dbo.UDTToLocalTime(T1.CreatedOnUtc),120) 
					WHEN @GroupId = 2 THEN CONVERT(NVARCHAR(4),DATEPART(YEAR, dbo.UDTToLocalTime(T1.CreatedOnUtc))) + '-' + CASE WHEN LEN(CONVERT(NVARCHAR(2),DATEPART(WEEK, dbo.UDTToLocalTime(T1.CreatedOnUtc)))) = 1 THEN '0' + CONVERT(NVARCHAR(2),DATEPART(WEEK, dbo.UDTToLocalTime(T1.CreatedOnUtc))) ELSE CONVERT(NVARCHAR(2),DATEPART(WEEK, dbo.UDTToLocalTime(T1.CreatedOnUtc))) END
					WHEN @GroupId = 3 THEN CONVERT(NVARCHAR(4),DATEPART(YEAR, dbo.UDTToLocalTime(T1.CreatedOnUtc))) + '-' + CASE WHEN LEN(CONVERT(NVARCHAR(20),DATEPART(MONTH, dbo.UDTToLocalTime(T1.CreatedOnUtc)))) = 1 THEN '0' + CONVERT(NVARCHAR(20),DATEPART(MONTH, dbo.UDTToLocalTime(T1.CreatedOnUtc))) ELSE CONVERT(NVARCHAR(20),DATEPART(MONTH, dbo.UDTToLocalTime(T1.CreatedOnUtc))) END
					WHEN @GroupId = 4 THEN CONVERT(NVARCHAR(4),DATEPART(YEAR, dbo.UDTToLocalTime(T1.CreatedOnUtc))) + '-' + CASE WHEN LEN(CONVERT(NVARCHAR(2),DATEPART(QUARTER, dbo.UDTToLocalTime(T1.CreatedOnUtc)))) = 1 THEN '0' + CONVERT(NVARCHAR(2),DATEPART(QUARTER, dbo.UDTToLocalTime(T1.CreatedOnUtc))) ELSE CONVERT(NVARCHAR(2),DATEPART(QUARTER, dbo.UDTToLocalTime(T1.CreatedOnUtc))) END
					WHEN @GroupId = 5 THEN CONVERT(NVARCHAR(4),DATEPART(YEAR, dbo.UDTToLocalTime(T1.CreatedOnUtc)))
					ELSE CONVERT(NVARCHAR(4),DATEPART(YEAR, dbo.UDTToLocalTime(T1.CreatedOnUtc))) + '-' + CASE WHEN LEN(CONVERT(NVARCHAR(20),DATEPART(MONTH, dbo.UDTToLocalTime(T1.CreatedOnUtc)))) = 1 THEN '0' + CONVERT(NVARCHAR(20),DATEPART(MONTH, dbo.UDTToLocalTime(T1.CreatedOnUtc))) ELSE CONVERT(NVARCHAR(20),DATEPART(MONTH, dbo.UDTToLocalTime(T1.CreatedOnUtc))) END
				END
	END


	if @ControlId = 3
	Begin
			SELECT TOP (@topN) T3.Name AS DimName,	SUM(T2.Quantity) AS Quantity, 
			case when @priceTax = 1 then SUM(T2.PriceInclTax) else SUM(T2.PriceExclTax) end AS TotalSales FROM [Order] T1
			INNER JOIN [OrderItem] T2 ON T1.Id = T2.OrderId
			INNER JOIN [Product] T3 ON T2.ProductId = T3.Id
			INNER JOIN [Product_Manufacturer_Mapping] T4 ON T3.Id = T4.ProductId
			WHERE (cast(dbo.UDTToLocalTime(T1.[CreatedOnUtc]) as Date) BETWEEN @StartDate AND @EndDate) 
			AND (T4.ManufacturerId = @ManufacturerId)
			AND T1.OrderStatusId like (CASE WHEN @OrderStatusId = 0 then '[1234]0' Else convert(nvarchar(2),@OrderStatusId) end)
			AND T1.PaymentStatusId like (CASE WHEN @PaymentStatusId = 0 then '[1234]%' Else convert(nvarchar(2),@PaymentStatusId) end)
			AND T1.Deleted = 0
			GROUP BY T3.Name
			ORDER BY SUM(T1.OrderSubtotalExclTax) DESC
	end

	if @ControlId = 4
	Begin
			SELECT T3.Name AS DimName,	SUM(T2.Quantity) AS Quantity, 
			case when @priceTax = 1 then SUM(T2.PriceInclTax) else SUM(T2.PriceExclTax) end AS TotalSales FROM [Order] T1
			INNER JOIN [OrderItem] T2 ON T1.Id = T2.OrderId
			INNER JOIN [Product] T3 ON T2.ProductId = T3.Id
			INNER JOIN [Product_Manufacturer_Mapping] T4 ON T3.Id = T4.ProductId
			WHERE (cast(dbo.UDTToLocalTime(T1.[CreatedOnUtc]) as Date) BETWEEN @StartDate AND @EndDate) 
			AND (T4.ManufacturerId = @ManufacturerId)
			AND T1.OrderStatusId like (CASE WHEN @OrderStatusId = 0 then '[1234]0' Else convert(nvarchar(2),@OrderStatusId) end)
			AND T1.PaymentStatusId like (CASE WHEN @PaymentStatusId = 0 then '[1234]%' Else convert(nvarchar(2),@PaymentStatusId) end)
			AND T1.Deleted = 0 
			GROUP BY T3.Name
			ORDER BY SUM(T1.OrderSubtotalExclTax) DESC
	end

	if @ControlId in (5,6)
	Begin
			SELECT T5.Name AS DimName,	SUM(T2.Quantity) AS Quantity, 
			case when @priceTax = 1 then SUM(T2.PriceInclTax) else SUM(T2.PriceExclTax) end AS TotalSales FROM [Order] T1
			INNER JOIN [OrderItem] T2 ON T1.Id = T2.OrderId
			INNER JOIN [Product] T3 ON T2.ProductId = T3.Id
			INNER JOIN [Product_Manufacturer_Mapping] T4 ON T3.Id = T4.ProductId
			INNER JOIN [Manufacturer] T5 ON T5.Id = T4.ManufacturerId
			WHERE (cast(dbo.UDTToLocalTime(T1.[CreatedOnUtc]) as Date) BETWEEN @StartDate AND @EndDate) 
			--AND (T4.ManufacturerId = @ManufacturerId)
			AND T1.OrderStatusId like (CASE WHEN @OrderStatusId = 0 then '[1234]0' Else convert(nvarchar(2),@OrderStatusId) end)
			AND T1.PaymentStatusId like (CASE WHEN @PaymentStatusId = 0 then '[1234]%' Else convert(nvarchar(2),@PaymentStatusId) end)
			AND T1.Deleted = 0 
			GROUP BY T5.Name
			ORDER BY SUM(T2.PriceExclTax) DESC
	end

END
GO
IF EXISTS (
		SELECT *
		FROM sys.objects
		WHERE object_id = OBJECT_ID(N'[dbo].[nop4you_dashboard_mcustomers]') AND OBJECTPROPERTY(object_id,N'IsProcedure') = 1)
DROP PROCEDURE [dbo].[nop4you_dashboard_mcustomers]
GO
CREATE PROCEDURE [dbo].[nop4you_dashboard_mcustomers]
(
	@ControlId			int = null,
	@GroupId			int = 3,
	@StartDate			Datetime = '19000101',
	@EndDate			Datetime = '20991231',
	@OrderStatusId		int = 0,
	@PaymentStatusId	int = 0
)
AS
BEGIN

	declare @tab table(
	DimName nvarchar(100),
	TotalSales numeric(19,2),
	Quantity int
	)

	set @GroupId = isnull(@GroupId,3)
	set @OrderStatusId = isnull(@OrderStatusId,0)
	set @PaymentStatusId = isnull(@PaymentStatusId,0)


	DECLARE @topN INT
	DECLARE @priceTax bit

	SET @topN = (select Value from Setting where Name = 'dashboardsettings.topn')
	SET @priceTax = (select case when [Value]='true' then '1' else '0' end from Setting where Name = 'dashboardsettings.PriceWithTax')

	IF ((@StartDate is null) OR (@StartDate = '')) SET @StartDate = '19000101'
	IF ((@EndDate is null) OR (@EndDate = '')) SET @EndDate = '20991231'



	if @ControlId = 1
	Begin
			SELECT TOP (@topN) isnull(T2.Email, 'Guest') AS DimName,	COUNT(*) AS Quantity, 
			case when @priceTax = 1 then SUM(T1.OrderSubtotalInclTax) else SUM(T1.OrderSubtotalExclTax) end AS TotalSales FROM [Order] T1
			INNER JOIN Customer T2 on T2.Id = T1.CustomerId
			WHERE (cast(dbo.UDTToLocalTime(T1.[CreatedOnUtc]) as Date) BETWEEN @StartDate AND @EndDate) 			
			AND T1.OrderStatusId like (CASE WHEN @OrderStatusId = 0 then '[1234]0' Else convert(nvarchar(2),@OrderStatusId) end)
			AND T1.PaymentStatusId like (CASE WHEN @PaymentStatusId = 0 then '[1234]%' Else convert(nvarchar(2),@PaymentStatusId) end) 
			AND T1.Deleted = 0
			GROUP BY T2.Email
			ORDER BY SUM(T1.OrderTotal) DESC
	end
	if @ControlId = 2
	Begin
			SELECT isnull(T2.Email, 'Guest') AS DimName,	COUNT(*) AS Quantity, 
			case when @priceTax = 1 then SUM(T1.OrderSubtotalInclTax) else SUM(T1.OrderSubtotalExclTax) end AS TotalSales FROM [Order] T1
			INNER JOIN Customer T2 on T2.Id = T1.CustomerId
			WHERE (cast(dbo.UDTToLocalTime(T1.[CreatedOnUtc]) as Date) BETWEEN @StartDate AND @EndDate) 			
			AND T1.OrderStatusId like (CASE WHEN @OrderStatusId = 0 then '[1234]0' Else convert(nvarchar(2),@OrderStatusId) end)
			AND T1.PaymentStatusId like (CASE WHEN @PaymentStatusId = 0 then '[1234]%' Else convert(nvarchar(2),@PaymentStatusId) end) 
			AND T1.Deleted = 0
			GROUP BY T2.Email
			ORDER BY SUM(T1.OrderTotal) DESC
	end

	
	if @ControlId in (3,4)
	Begin
		
		insert @tab
		SELECT CASE 
				WHEN @GroupId = 1 THEN CONVERT(NVARCHAR(10),dbo.UDTToLocalTime(T1.CreatedOnUtc),120) 
				WHEN @GroupId = 2 THEN CONVERT(NVARCHAR(4),DATEPART(YEAR, dbo.UDTToLocalTime(T1.CreatedOnUtc))) + '-' + CASE WHEN LEN(CONVERT(NVARCHAR(2),DATEPART(WEEK, dbo.UDTToLocalTime(T1.CreatedOnUtc)))) = 1 THEN '0' + CONVERT(NVARCHAR(2),DATEPART(WEEK, dbo.UDTToLocalTime(T1.CreatedOnUtc))) ELSE CONVERT(NVARCHAR(2),DATEPART(WEEK, dbo.UDTToLocalTime(T1.CreatedOnUtc))) END
				WHEN @GroupId = 3 THEN CONVERT(NVARCHAR(4),DATEPART(YEAR, dbo.UDTToLocalTime(T1.CreatedOnUtc))) + '-' + CASE WHEN LEN(CONVERT(NVARCHAR(20),DATEPART(MONTH, dbo.UDTToLocalTime(T1.CreatedOnUtc)))) = 1 THEN '0' + CONVERT(NVARCHAR(20),DATEPART(MONTH, dbo.UDTToLocalTime(T1.CreatedOnUtc))) ELSE CONVERT(NVARCHAR(20),DATEPART(MONTH, dbo.UDTToLocalTime(T1.CreatedOnUtc))) END
				WHEN @GroupId = 4 THEN CONVERT(NVARCHAR(4),DATEPART(YEAR, dbo.UDTToLocalTime(T1.CreatedOnUtc))) + '-' + CASE WHEN LEN(CONVERT(NVARCHAR(2),DATEPART(QUARTER, dbo.UDTToLocalTime(T1.CreatedOnUtc)))) = 1 THEN '0' + CONVERT(NVARCHAR(2),DATEPART(QUARTER, dbo.UDTToLocalTime(T1.CreatedOnUtc))) ELSE CONVERT(NVARCHAR(2),DATEPART(QUARTER, dbo.UDTToLocalTime(T1.CreatedOnUtc))) END
				WHEN @GroupId = 5 THEN CONVERT(NVARCHAR(4),DATEPART(YEAR, dbo.UDTToLocalTime(T1.CreatedOnUtc)))
				ELSE CONVERT(NVARCHAR(4),DATEPART(YEAR, dbo.UDTToLocalTime(T1.CreatedOnUtc))) + '-' + CASE WHEN LEN(CONVERT(NVARCHAR(20),DATEPART(MONTH, dbo.UDTToLocalTime(T1.CreatedOnUtc)))) = 1 THEN '0' + CONVERT(NVARCHAR(20),DATEPART(MONTH, dbo.UDTToLocalTime(T1.CreatedOnUtc))) ELSE CONVERT(NVARCHAR(20),DATEPART(MONTH, dbo.UDTToLocalTime(T1.CreatedOnUtc))) END
			END AS DimName,
			case when @priceTax = 1 then SUM(T1.OrderSubtotalInclTax) else SUM(T1.OrderSubtotalExclTax) end AS TotalSales, 
			0 as Quantity
			FROM [Order] T1			
			WHERE (cast(dbo.UDTToLocalTime(T1.[CreatedOnUtc]) as Date) BETWEEN @StartDate AND @EndDate) 				
				AND T1.OrderStatusId like (CASE WHEN @OrderStatusId = 0 then '[1234]0' Else convert(nvarchar(2),@OrderStatusId) end)
				AND T1.PaymentStatusId like (CASE WHEN @PaymentStatusId = 0 then '[1234]%' Else convert(nvarchar(2),@PaymentStatusId) end)
				AND T1.Deleted = 0
			GROUP BY 
				CASE 
					WHEN @GroupId = 1 THEN CONVERT(NVARCHAR(10),dbo.UDTToLocalTime(T1.CreatedOnUtc),120) 
					WHEN @GroupId = 2 THEN CONVERT(NVARCHAR(4),DATEPART(YEAR, dbo.UDTToLocalTime(T1.CreatedOnUtc))) + '-' + CASE WHEN LEN(CONVERT(NVARCHAR(2),DATEPART(WEEK, dbo.UDTToLocalTime(T1.CreatedOnUtc)))) = 1 THEN '0' + CONVERT(NVARCHAR(2),DATEPART(WEEK, dbo.UDTToLocalTime(T1.CreatedOnUtc))) ELSE CONVERT(NVARCHAR(2),DATEPART(WEEK, dbo.UDTToLocalTime(T1.CreatedOnUtc))) END
					WHEN @GroupId = 3 THEN CONVERT(NVARCHAR(4),DATEPART(YEAR, dbo.UDTToLocalTime(T1.CreatedOnUtc))) + '-' + CASE WHEN LEN(CONVERT(NVARCHAR(20),DATEPART(MONTH, dbo.UDTToLocalTime(T1.CreatedOnUtc)))) = 1 THEN '0' + CONVERT(NVARCHAR(20),DATEPART(MONTH, dbo.UDTToLocalTime(T1.CreatedOnUtc))) ELSE CONVERT(NVARCHAR(20),DATEPART(MONTH, dbo.UDTToLocalTime(T1.CreatedOnUtc))) END
					WHEN @GroupId = 4 THEN CONVERT(NVARCHAR(4),DATEPART(YEAR, dbo.UDTToLocalTime(T1.CreatedOnUtc))) + '-' + CASE WHEN LEN(CONVERT(NVARCHAR(2),DATEPART(QUARTER, dbo.UDTToLocalTime(T1.CreatedOnUtc)))) = 1 THEN '0' + CONVERT(NVARCHAR(2),DATEPART(QUARTER, dbo.UDTToLocalTime(T1.CreatedOnUtc))) ELSE CONVERT(NVARCHAR(2),DATEPART(QUARTER, dbo.UDTToLocalTime(T1.CreatedOnUtc))) END
					WHEN @GroupId = 5 THEN CONVERT(NVARCHAR(4),DATEPART(YEAR, dbo.UDTToLocalTime(T1.CreatedOnUtc)))
					ELSE CONVERT(NVARCHAR(4),DATEPART(YEAR, dbo.UDTToLocalTime(T1.CreatedOnUtc))) + '-' + CASE WHEN LEN(CONVERT(NVARCHAR(20),DATEPART(MONTH, dbo.UDTToLocalTime(T1.CreatedOnUtc)))) = 1 THEN '0' + CONVERT(NVARCHAR(20),DATEPART(MONTH, dbo.UDTToLocalTime(T1.CreatedOnUtc))) ELSE CONVERT(NVARCHAR(20),DATEPART(MONTH, dbo.UDTToLocalTime(T1.CreatedOnUtc))) END
				END

		insert @tab
		SELECT CASE 
				WHEN @GroupId = 1 THEN CONVERT(NVARCHAR(10),dbo.UDTToLocalTime(T1.CreatedOnUtc),120) 
				WHEN @GroupId = 2 THEN CONVERT(NVARCHAR(4),DATEPART(YEAR, dbo.UDTToLocalTime(T1.CreatedOnUtc))) + '-' + CASE WHEN LEN(CONVERT(NVARCHAR(2),DATEPART(WEEK, dbo.UDTToLocalTime(T1.CreatedOnUtc)))) = 1 THEN '0' + CONVERT(NVARCHAR(2),DATEPART(WEEK, dbo.UDTToLocalTime(T1.CreatedOnUtc))) ELSE CONVERT(NVARCHAR(2),DATEPART(WEEK, dbo.UDTToLocalTime(T1.CreatedOnUtc))) END
				WHEN @GroupId = 3 THEN CONVERT(NVARCHAR(4),DATEPART(YEAR, dbo.UDTToLocalTime(T1.CreatedOnUtc))) + '-' + CASE WHEN LEN(CONVERT(NVARCHAR(20),DATEPART(MONTH, dbo.UDTToLocalTime(T1.CreatedOnUtc)))) = 1 THEN '0' + CONVERT(NVARCHAR(20),DATEPART(MONTH, dbo.UDTToLocalTime(T1.CreatedOnUtc))) ELSE CONVERT(NVARCHAR(20),DATEPART(MONTH, dbo.UDTToLocalTime(T1.CreatedOnUtc))) END
				WHEN @GroupId = 4 THEN CONVERT(NVARCHAR(4),DATEPART(YEAR, dbo.UDTToLocalTime(T1.CreatedOnUtc))) + '-' + CASE WHEN LEN(CONVERT(NVARCHAR(2),DATEPART(QUARTER, dbo.UDTToLocalTime(T1.CreatedOnUtc)))) = 1 THEN '0' + CONVERT(NVARCHAR(2),DATEPART(QUARTER, dbo.UDTToLocalTime(T1.CreatedOnUtc))) ELSE CONVERT(NVARCHAR(2),DATEPART(QUARTER, dbo.UDTToLocalTime(T1.CreatedOnUtc))) END
				WHEN @GroupId = 5 THEN CONVERT(NVARCHAR(4),DATEPART(YEAR, dbo.UDTToLocalTime(T1.CreatedOnUtc)))
				ELSE CONVERT(NVARCHAR(4),DATEPART(YEAR, dbo.UDTToLocalTime(T1.CreatedOnUtc))) + '-' + CASE WHEN LEN(CONVERT(NVARCHAR(20),DATEPART(MONTH, dbo.UDTToLocalTime(T1.CreatedOnUtc)))) = 1 THEN '0' + CONVERT(NVARCHAR(20),DATEPART(MONTH, dbo.UDTToLocalTime(T1.CreatedOnUtc))) ELSE CONVERT(NVARCHAR(20),DATEPART(MONTH, dbo.UDTToLocalTime(T1.CreatedOnUtc))) END
			END AS DimName,
			0 AS TotalSales, 
			count(*) as Quantity
			FROM [Customer] T1			
			WHERE (cast(dbo.UDTToLocalTime(T1.[CreatedOnUtc]) as Date) BETWEEN @StartDate AND @EndDate AND ISNULL(T1.Email,'') <> '' AND T1.IsSystemAccount = 0) 				
			GROUP BY 
				CASE 
					WHEN @GroupId = 1 THEN CONVERT(NVARCHAR(10),dbo.UDTToLocalTime(T1.CreatedOnUtc),120) 
					WHEN @GroupId = 2 THEN CONVERT(NVARCHAR(4),DATEPART(YEAR, dbo.UDTToLocalTime(T1.CreatedOnUtc))) + '-' + CASE WHEN LEN(CONVERT(NVARCHAR(2),DATEPART(WEEK, dbo.UDTToLocalTime(T1.CreatedOnUtc)))) = 1 THEN '0' + CONVERT(NVARCHAR(2),DATEPART(WEEK, dbo.UDTToLocalTime(T1.CreatedOnUtc))) ELSE CONVERT(NVARCHAR(2),DATEPART(WEEK, dbo.UDTToLocalTime(T1.CreatedOnUtc))) END
					WHEN @GroupId = 3 THEN CONVERT(NVARCHAR(4),DATEPART(YEAR, dbo.UDTToLocalTime(T1.CreatedOnUtc))) + '-' + CASE WHEN LEN(CONVERT(NVARCHAR(20),DATEPART(MONTH, dbo.UDTToLocalTime(T1.CreatedOnUtc)))) = 1 THEN '0' + CONVERT(NVARCHAR(20),DATEPART(MONTH, dbo.UDTToLocalTime(T1.CreatedOnUtc))) ELSE CONVERT(NVARCHAR(20),DATEPART(MONTH, dbo.UDTToLocalTime(T1.CreatedOnUtc))) END
					WHEN @GroupId = 4 THEN CONVERT(NVARCHAR(4),DATEPART(YEAR, dbo.UDTToLocalTime(T1.CreatedOnUtc))) + '-' + CASE WHEN LEN(CONVERT(NVARCHAR(2),DATEPART(QUARTER, dbo.UDTToLocalTime(T1.CreatedOnUtc)))) = 1 THEN '0' + CONVERT(NVARCHAR(2),DATEPART(QUARTER, dbo.UDTToLocalTime(T1.CreatedOnUtc))) ELSE CONVERT(NVARCHAR(2),DATEPART(QUARTER, dbo.UDTToLocalTime(T1.CreatedOnUtc))) END
					WHEN @GroupId = 5 THEN CONVERT(NVARCHAR(4),DATEPART(YEAR, dbo.UDTToLocalTime(T1.CreatedOnUtc)))
					ELSE CONVERT(NVARCHAR(4),DATEPART(YEAR, dbo.UDTToLocalTime(T1.CreatedOnUtc))) + '-' + CASE WHEN LEN(CONVERT(NVARCHAR(20),DATEPART(MONTH, dbo.UDTToLocalTime(T1.CreatedOnUtc)))) = 1 THEN '0' + CONVERT(NVARCHAR(20),DATEPART(MONTH, dbo.UDTToLocalTime(T1.CreatedOnUtc))) ELSE CONVERT(NVARCHAR(20),DATEPART(MONTH, dbo.UDTToLocalTime(T1.CreatedOnUtc))) END
				END

		select DimName, SUM(TotalSales) as TotalSales, sum(Quantity) as Quantity from @tab
		group by DimName


	END


	if @ControlId in (5,6)
	Begin
			SELECT isnull(T3.Value,'(empty)') AS DimName,	COUNT(*) AS Quantity, 
			case when @priceTax = 1 then SUM(T1.OrderSubtotalInclTax) else SUM(T1.OrderSubtotalExclTax) end AS TotalSales FROM [Order] T1
			INNER JOIN Customer T2 on T2.Id = T1.CustomerId
			LEFT JOIN GenericAttribute T3 on T3.EntityId = T2.Id and T3.KeyGroup = 'Customer' and T3.[Key] = 'Gender'
			WHERE (cast(dbo.UDTToLocalTime(T1.[CreatedOnUtc]) as Date) BETWEEN @StartDate AND @EndDate) 			
			AND T1.OrderStatusId like (CASE WHEN @OrderStatusId = 0 then '[1234]0' Else convert(nvarchar(2),@OrderStatusId) end)
			AND T1.PaymentStatusId like (CASE WHEN @PaymentStatusId = 0 then '[1234]%' Else convert(nvarchar(2),@PaymentStatusId) end) 
			AND T1.Deleted = 0
			GROUP BY isnull(T3.Value,'(empty)')
			ORDER BY SUM(T1.OrderTotal) DESC
	end

END
GO
IF EXISTS (
		SELECT *
		FROM sys.objects
		WHERE object_id = OBJECT_ID(N'[dbo].[nop4you_dashboard_morders]') AND OBJECTPROPERTY(object_id,N'IsProcedure') = 1)
DROP PROCEDURE [dbo].[nop4you_dashboard_morders]
GO
CREATE PROCEDURE [dbo].[nop4you_dashboard_morders]
(
	@ControlId			int = null,
	@GroupId			int = 3,
	@StartDate			Datetime = '19000101',
	@EndDate			Datetime = '20991231',
	@OrderStatusId		int = 0,
	@PaymentStatusId	int = 0	
)
AS
BEGIN

	DECLARE @topN INT
	DECLARE @priceTax bit
	set @GroupId = isnull(@GroupId,3)
	set @OrderStatusId = isnull(@OrderStatusId,0)
	set @PaymentStatusId = isnull(@PaymentStatusId,0)

	SET @topN = (select Value from Setting where Name = 'dashboardsettings.topn')
	SET @priceTax = (select case when [Value]='true' then '1' else '0' end from Setting where Name = 'dashboardsettings.PriceWithTax')

	IF ((@StartDate is null) OR (@StartDate = '')) SET @StartDate = '19000101'
	IF ((@EndDate is null) OR (@EndDate = '')) SET @EndDate = '20991231'


	if @ControlId in (1,2)
	Begin

		SELECT CASE 
				WHEN @GroupId = 1 THEN CONVERT(NVARCHAR(10),dbo.UDTToLocalTime(T1.CreatedOnUtc),120) 
				WHEN @GroupId = 2 THEN CONVERT(NVARCHAR(4),DATEPART(YEAR, dbo.UDTToLocalTime(T1.CreatedOnUtc))) + '-' + CASE WHEN LEN(CONVERT(NVARCHAR(2),DATEPART(WEEK, dbo.UDTToLocalTime(T1.CreatedOnUtc)))) = 1 THEN '0' + CONVERT(NVARCHAR(2),DATEPART(WEEK, dbo.UDTToLocalTime(T1.CreatedOnUtc))) ELSE CONVERT(NVARCHAR(2),DATEPART(WEEK, dbo.UDTToLocalTime(T1.CreatedOnUtc))) END
				WHEN @GroupId = 3 THEN CONVERT(NVARCHAR(4),DATEPART(YEAR, dbo.UDTToLocalTime(T1.CreatedOnUtc))) + '-' + CASE WHEN LEN(CONVERT(NVARCHAR(20),DATEPART(MONTH, dbo.UDTToLocalTime(T1.CreatedOnUtc)))) = 1 THEN '0' + CONVERT(NVARCHAR(20),DATEPART(MONTH, dbo.UDTToLocalTime(T1.CreatedOnUtc))) ELSE CONVERT(NVARCHAR(20),DATEPART(MONTH, dbo.UDTToLocalTime(T1.CreatedOnUtc))) END
				WHEN @GroupId = 4 THEN CONVERT(NVARCHAR(4),DATEPART(YEAR, dbo.UDTToLocalTime(T1.CreatedOnUtc))) + '-' + CASE WHEN LEN(CONVERT(NVARCHAR(2),DATEPART(QUARTER, dbo.UDTToLocalTime(T1.CreatedOnUtc)))) = 1 THEN '0' + CONVERT(NVARCHAR(2),DATEPART(QUARTER, dbo.UDTToLocalTime(T1.CreatedOnUtc))) ELSE CONVERT(NVARCHAR(2),DATEPART(QUARTER, dbo.UDTToLocalTime(T1.CreatedOnUtc))) END
				WHEN @GroupId = 5 THEN CONVERT(NVARCHAR(4),DATEPART(YEAR, dbo.UDTToLocalTime(T1.CreatedOnUtc)))
				ELSE CONVERT(NVARCHAR(4),DATEPART(YEAR, dbo.UDTToLocalTime(T1.CreatedOnUtc))) + '-' + CASE WHEN LEN(CONVERT(NVARCHAR(20),DATEPART(MONTH, dbo.UDTToLocalTime(T1.CreatedOnUtc)))) = 1 THEN '0' + CONVERT(NVARCHAR(20),DATEPART(MONTH, dbo.UDTToLocalTime(T1.CreatedOnUtc))) ELSE CONVERT(NVARCHAR(20),DATEPART(MONTH, dbo.UDTToLocalTime(T1.CreatedOnUtc))) END
			END AS DimName,
			case when @priceTax = 1 then SUM(T1.OrderTotal) else SUM(T1.OrderTotal-T1.OrderTax) end AS TotalSales, 
			count(*) as Quantity
			FROM [Order] T1			
			WHERE (cast(dbo.UDTToLocalTime(T1.[CreatedOnUtc]) as Date) BETWEEN @StartDate AND @EndDate) 				
				AND T1.OrderStatusId like (CASE WHEN @OrderStatusId = 0 then '[1234]0' Else convert(nvarchar(2),@OrderStatusId) end)
				AND T1.PaymentStatusId like (CASE WHEN @PaymentStatusId = 0 then '[1234]%' Else convert(nvarchar(2),@PaymentStatusId) end)
				AND T1.Deleted = 0
			GROUP BY 
				CASE 
					WHEN @GroupId = 1 THEN CONVERT(NVARCHAR(10),dbo.UDTToLocalTime(T1.CreatedOnUtc),120) 
					WHEN @GroupId = 2 THEN CONVERT(NVARCHAR(4),DATEPART(YEAR, dbo.UDTToLocalTime(T1.CreatedOnUtc))) + '-' + CASE WHEN LEN(CONVERT(NVARCHAR(2),DATEPART(WEEK, dbo.UDTToLocalTime(T1.CreatedOnUtc)))) = 1 THEN '0' + CONVERT(NVARCHAR(2),DATEPART(WEEK, dbo.UDTToLocalTime(T1.CreatedOnUtc))) ELSE CONVERT(NVARCHAR(2),DATEPART(WEEK, dbo.UDTToLocalTime(T1.CreatedOnUtc))) END
					WHEN @GroupId = 3 THEN CONVERT(NVARCHAR(4),DATEPART(YEAR, dbo.UDTToLocalTime(T1.CreatedOnUtc))) + '-' + CASE WHEN LEN(CONVERT(NVARCHAR(20),DATEPART(MONTH, dbo.UDTToLocalTime(T1.CreatedOnUtc)))) = 1 THEN '0' + CONVERT(NVARCHAR(20),DATEPART(MONTH, dbo.UDTToLocalTime(T1.CreatedOnUtc))) ELSE CONVERT(NVARCHAR(20),DATEPART(MONTH, dbo.UDTToLocalTime(T1.CreatedOnUtc))) END
					WHEN @GroupId = 4 THEN CONVERT(NVARCHAR(4),DATEPART(YEAR, dbo.UDTToLocalTime(T1.CreatedOnUtc))) + '-' + CASE WHEN LEN(CONVERT(NVARCHAR(2),DATEPART(QUARTER, dbo.UDTToLocalTime(T1.CreatedOnUtc)))) = 1 THEN '0' + CONVERT(NVARCHAR(2),DATEPART(QUARTER, dbo.UDTToLocalTime(T1.CreatedOnUtc))) ELSE CONVERT(NVARCHAR(2),DATEPART(QUARTER, dbo.UDTToLocalTime(T1.CreatedOnUtc))) END
					WHEN @GroupId = 5 THEN CONVERT(NVARCHAR(4),DATEPART(YEAR, dbo.UDTToLocalTime(T1.CreatedOnUtc)))
					ELSE CONVERT(NVARCHAR(4),DATEPART(YEAR, dbo.UDTToLocalTime(T1.CreatedOnUtc))) + '-' + CASE WHEN LEN(CONVERT(NVARCHAR(20),DATEPART(MONTH, dbo.UDTToLocalTime(T1.CreatedOnUtc)))) = 1 THEN '0' + CONVERT(NVARCHAR(20),DATEPART(MONTH, dbo.UDTToLocalTime(T1.CreatedOnUtc))) ELSE CONVERT(NVARCHAR(20),DATEPART(MONTH, dbo.UDTToLocalTime(T1.CreatedOnUtc))) END
				END

	END

	if @ControlId in (3,4)
	Begin
			SELECT T1.ShippingMethod AS DimName,	COUNT(*) AS Quantity, 
			case when @priceTax = 1 then SUM(T1.OrderTotal) else SUM(T1.OrderTotal-T1.OrderTax) end AS TotalSales FROM [Order] T1
			WHERE (cast(dbo.UDTToLocalTime(T1.[CreatedOnUtc]) as Date) BETWEEN @StartDate AND @EndDate) 			
			AND T1.OrderStatusId like (CASE WHEN @OrderStatusId = 0 then '[1234]0' Else convert(nvarchar(2),@OrderStatusId) end)
			AND T1.PaymentStatusId like (CASE WHEN @PaymentStatusId = 0 then '[1234]%' Else convert(nvarchar(2),@PaymentStatusId) end) 
			AND T1.Deleted = 0
			GROUP BY T1.ShippingMethod
			ORDER BY SUM(T1.OrderTotal) DESC
	end
	if @ControlId in (5,6)
	Begin
			SELECT replace(T1.PaymentMethodSystemName, 'Payments.', '') AS DimName,	COUNT(*) AS Quantity, 
			case when @priceTax = 1 then SUM(T1.OrderTotal) else SUM(T1.OrderTotal-T1.OrderTax) end AS TotalSales FROM [Order] T1
			WHERE (cast(dbo.UDTToLocalTime(T1.[CreatedOnUtc]) as Date) BETWEEN @StartDate AND @EndDate) 			
			AND T1.OrderStatusId like (CASE WHEN @OrderStatusId = 0 then '[1234]0' Else convert(nvarchar(2),@OrderStatusId) end)
			AND T1.PaymentStatusId like (CASE WHEN @PaymentStatusId = 0 then '[1234]%' Else convert(nvarchar(2),@PaymentStatusId) end) 
			AND T1.Deleted = 0
			GROUP BY T1.PaymentMethodSystemName
			ORDER BY SUM(T1.OrderTotal) DESC
	end


END
GO
IF EXISTS (
		SELECT *
		FROM sys.objects
		WHERE object_id = OBJECT_ID(N'[dbo].[nop4you_dashboard_mproducts]') AND OBJECTPROPERTY(object_id,N'IsProcedure') = 1)
DROP PROCEDURE [dbo].[nop4you_dashboard_mproducts]
GO
CREATE PROCEDURE [dbo].[nop4you_dashboard_mproducts]
(
	@ControlId			int = null,
	@GroupId			int = 3,
	@StartDate			Datetime = '19000101',
	@EndDate			Datetime = '20991231',
	@OrderStatusId		int = 0,
	@PaymentStatusId	int = 0
)
AS
BEGIN

	DECLARE @topN INT
	DECLARE @priceTax bit

	set @GroupId = isnull(@GroupId,3)
	set @OrderStatusId = isnull(@OrderStatusId,0)
	set @PaymentStatusId = isnull(@PaymentStatusId,0)

	SET @topN = (select Value from Setting where Name = 'dashboardsettings.topn')
	SET @priceTax = (select case when [Value]='true' then '1' else '0' end from Setting where Name = 'dashboardsettings.PriceWithTax')

	IF ((@StartDate is null) OR (@StartDate = '')) SET @StartDate = '19000101'
	IF ((@EndDate is null) OR (@EndDate = '')) SET @EndDate = '20991231'

	if @ControlId = 1
	Begin
			SELECT TOP (@topN) T3.Name AS DimName,	SUM(T2.Quantity) AS Quantity, 
			case when @priceTax = 1 then SUM(T2.PriceInclTax) else SUM(T2.PriceExclTax) end AS TotalSales FROM [Order] T1
			INNER JOIN [OrderItem] T2 ON T1.Id = T2.OrderId
			INNER JOIN [Product] T3 ON T2.ProductId = T3.Id
			INNER JOIN [Product_Category_Mapping] T4 ON T3.Id = T4.ProductId
			WHERE (cast(dbo.UDTToLocalTime(T1.[CreatedOnUtc]) as Date) BETWEEN @StartDate AND @EndDate) 			
			AND T1.OrderStatusId like (CASE WHEN @OrderStatusId = 0 then '[1234]0' Else convert(nvarchar(2),@OrderStatusId) end)
			AND T1.PaymentStatusId like (CASE WHEN @PaymentStatusId = 0 then '[1234]%' Else convert(nvarchar(2),@PaymentStatusId) end)
			AND T1.Deleted = 0
			GROUP BY T3.Name
			ORDER BY SUM(T2.PriceExclTax) DESC
	end
	if @ControlId = 2
	Begin
			SELECT T3.Name AS DimName,	SUM(T2.Quantity) AS Quantity, 
			case when @priceTax = 1 then SUM(T2.PriceInclTax) else SUM(T2.PriceExclTax) end AS TotalSales FROM [Order] T1
			INNER JOIN [OrderItem] T2 ON T1.Id = T2.OrderId
			INNER JOIN [Product] T3 ON T2.ProductId = T3.Id			
			WHERE (cast(dbo.UDTToLocalTime(T1.[CreatedOnUtc]) as Date) BETWEEN @StartDate AND @EndDate) 			
			AND T1.OrderStatusId like (CASE WHEN @OrderStatusId = 0 then '[1234]0' Else convert(nvarchar(2),@OrderStatusId) end)
			AND T1.PaymentStatusId like (CASE WHEN @PaymentStatusId = 0 then '[1234]%' Else convert(nvarchar(2),@PaymentStatusId) end)
			AND T1.Deleted = 0
			GROUP BY T3.Name
			ORDER BY SUM(T2.PriceExclTax) DESC
	end
	if @ControlId = 3
	Begin
			SELECT TOP (@topN) T5.Name AS DimName,	SUM(T2.Quantity) AS Quantity, 
			case when @priceTax = 1 then SUM(T2.PriceInclTax) else SUM(T2.PriceExclTax) end AS TotalSales FROM [Order] T1
			INNER JOIN [OrderItem] T2 ON T1.Id = T2.OrderId
			INNER JOIN [Product] T3 ON T2.ProductId = T3.Id
			INNER JOIN [Product_Category_Mapping] T4 ON T3.Id = T4.ProductId
			INNER JOIN [Category] T5 on T5.Id = T4.CategoryId
			WHERE (cast(dbo.UDTToLocalTime(T1.[CreatedOnUtc]) as Date) BETWEEN @StartDate AND @EndDate) 			
			AND T1.OrderStatusId like (CASE WHEN @OrderStatusId = 0 then '[1234]0' Else convert(nvarchar(2),@OrderStatusId) end)
			AND T1.PaymentStatusId like (CASE WHEN @PaymentStatusId = 0 then '[1234]%' Else convert(nvarchar(2),@PaymentStatusId) end)
			AND T1.Deleted = 0
			GROUP BY T5.Name
			ORDER BY SUM(T2.PriceExclTax) DESC
	end
	if @ControlId = 4
	Begin
			SELECT T5.Name AS DimName,	SUM(T2.Quantity) AS Quantity, 
			case when @priceTax = 1 then SUM(T2.PriceInclTax) else SUM(T2.PriceExclTax) end AS TotalSales FROM [Order] T1
			INNER JOIN [OrderItem] T2 ON T1.Id = T2.OrderId
			INNER JOIN [Product] T3 ON T2.ProductId = T3.Id
			INNER JOIN [Product_Category_Mapping] T4 ON T3.Id = T4.ProductId
			INNER JOIN [Category] T5 on T5.Id = T4.CategoryId
			WHERE (cast(dbo.UDTToLocalTime(T1.[CreatedOnUtc]) as Date) BETWEEN @StartDate AND @EndDate) 			
			AND T1.OrderStatusId like (CASE WHEN @OrderStatusId = 0 then '[1234]0' Else convert(nvarchar(2),@OrderStatusId) end)
			AND T1.PaymentStatusId like (CASE WHEN @PaymentStatusId = 0 then '[1234]%' Else convert(nvarchar(2),@PaymentStatusId) end)
			AND T1.Deleted = 0
			GROUP BY T5.Name
			ORDER BY SUM(T2.PriceExclTax) DESC
	end
	if @ControlId in (5,6)
	Begin
			SELECT TOP (@topN) T5.Name AS DimName,	SUM(T2.Quantity) AS Quantity, 
			case when @priceTax = 1 then SUM(T2.PriceInclTax) else SUM(T2.PriceExclTax) end AS TotalSales FROM [Order] T1
			INNER JOIN [OrderItem] T2 ON T1.Id = T2.OrderId
			INNER JOIN [Product] T3 ON T2.ProductId = T3.Id
			INNER JOIN [Product_Manufacturer_Mapping] T4 ON T3.Id = T4.ProductId
			INNER JOIN [Manufacturer] T5 on T5.Id = T4.ManufacturerId
			WHERE (cast(dbo.UDTToLocalTime(T1.[CreatedOnUtc]) as Date) BETWEEN @StartDate AND @EndDate) 			
			AND T1.OrderStatusId like (CASE WHEN @OrderStatusId = 0 then '[1234]0' Else convert(nvarchar(2),@OrderStatusId) end)
			AND T1.PaymentStatusId like (CASE WHEN @PaymentStatusId = 0 then '[1234]%' Else convert(nvarchar(2),@PaymentStatusId) end)
			AND T1.Deleted = 0
			GROUP BY T5.Name
			ORDER BY SUM(T2.PriceExclTax) DESC
	end

	if @ControlId = 7
	Begin
			SELECT TOP (@topN) T3.Name+ ' - '+ replace(T2.AttributeDescription, '<br />', char(13))  AS DimName,	SUM(T2.Quantity) AS Quantity, 
			case when @priceTax = 1 then SUM(T2.PriceInclTax) else SUM(T2.PriceExclTax) end AS TotalSales FROM [Order] T1
			INNER JOIN [OrderItem] T2 ON T1.Id = T2.OrderId
			INNER JOIN [Product] T3 ON T2.ProductId = T3.Id
			WHERE (cast(dbo.UDTToLocalTime(T1.[CreatedOnUtc]) as Date) BETWEEN @StartDate AND @EndDate) 			
			AND T1.OrderStatusId like (CASE WHEN @OrderStatusId = 0 then '[1234]0' Else convert(nvarchar(2),@OrderStatusId) end)
			AND T1.PaymentStatusId like (CASE WHEN @PaymentStatusId = 0 then '[1234]%' Else convert(nvarchar(2),@PaymentStatusId) end)
			AND isnull(T2.AttributeDescription,'') != ''
			AND T1.Deleted = 0
			GROUP BY T3.Name+ ' - '+ replace(T2.AttributeDescription, '<br />', char(13))
			ORDER BY SUM(T2.PriceExclTax) DESC
	end
	if @ControlId = 8
	Begin
			SELECT  T3.Name+ ' - '+ replace(T2.AttributeDescription, '<br />', char(13)) AS DimName,	SUM(T2.Quantity) AS Quantity, 
			case when @priceTax = 1 then SUM(T2.PriceInclTax) else SUM(T2.PriceExclTax) end AS TotalSales FROM [Order] T1
			INNER JOIN [OrderItem] T2 ON T1.Id = T2.OrderId
			INNER JOIN [Product] T3 ON T2.ProductId = T3.Id			
			WHERE (cast(dbo.UDTToLocalTime(T1.[CreatedOnUtc]) as Date) BETWEEN @StartDate AND @EndDate) 			
			AND T1.OrderStatusId like (CASE WHEN @OrderStatusId = 0 then '[1234]0' Else convert(nvarchar(2),@OrderStatusId) end)
			AND T1.PaymentStatusId like (CASE WHEN @PaymentStatusId = 0 then '[1234]%' Else convert(nvarchar(2),@PaymentStatusId) end)
			AND isnull(T2.AttributeDescription,'') != ''
			AND T1.Deleted = 0
			GROUP BY T3.Name+ ' - '+ replace(T2.AttributeDescription, '<br />', char(13))
			ORDER BY SUM(T2.PriceExclTax) DESC
	end


END

GO
IF EXISTS (
		SELECT *
		FROM sys.objects
		WHERE object_id = OBJECT_ID(N'[dbo].[nop4you_dashboard_product]') AND OBJECTPROPERTY(object_id,N'IsProcedure') = 1)
DROP PROCEDURE [dbo].[nop4you_dashboard_product]
GO
CREATE PROCEDURE [dbo].[nop4you_dashboard_product]
(
	@ProductId			int = null,
	@ControlId			int = null,
	@GroupId			int = 3,
	@StartDate			Datetime = '19000101',
	@EndDate			Datetime = '20991231',
	@OrderStatusId		int = null,
	@PaymentStatusId	int = null	
)
AS
BEGIN

	set @GroupId = isnull(@GroupId,3)
	set @OrderStatusId = isnull(@OrderStatusId,0)
	set @PaymentStatusId = isnull(@PaymentStatusId,0)


	DECLARE @topN INT
	DECLARE @priceTax bit

	SET @topN = (select Value from Setting where Name = 'dashboardsettings.topn')
	SET @priceTax = (select case when [Value]='true' then '1' else '0' end from Setting where Name = 'dashboardsettings.PriceWithTax')

	IF ((@StartDate is null) OR (@StartDate = '')) SET @StartDate = '19000101'
	IF ((@EndDate is null) OR (@EndDate = '')) SET @EndDate = '20991231'


	if @ControlId in (1,2)
	Begin

		SELECT CASE 
				WHEN @GroupId = 1 THEN CONVERT(NVARCHAR(10),dbo.UDTToLocalTime(T1.CreatedOnUtc),120) 
				WHEN @GroupId = 2 THEN CONVERT(NVARCHAR(4),DATEPART(YEAR, dbo.UDTToLocalTime(T1.CreatedOnUtc))) + '-' + CASE WHEN LEN(CONVERT(NVARCHAR(2),DATEPART(WEEK, dbo.UDTToLocalTime(T1.CreatedOnUtc)))) = 1 THEN '0' + CONVERT(NVARCHAR(2),DATEPART(WEEK, dbo.UDTToLocalTime(T1.CreatedOnUtc))) ELSE CONVERT(NVARCHAR(2),DATEPART(WEEK, dbo.UDTToLocalTime(T1.CreatedOnUtc))) END
				WHEN @GroupId = 3 THEN CONVERT(NVARCHAR(4),DATEPART(YEAR, dbo.UDTToLocalTime(T1.CreatedOnUtc))) + '-' + CASE WHEN LEN(CONVERT(NVARCHAR(20),DATEPART(MONTH, dbo.UDTToLocalTime(T1.CreatedOnUtc)))) = 1 THEN '0' + CONVERT(NVARCHAR(20),DATEPART(MONTH, dbo.UDTToLocalTime(T1.CreatedOnUtc))) ELSE CONVERT(NVARCHAR(20),DATEPART(MONTH, dbo.UDTToLocalTime(T1.CreatedOnUtc))) END
				WHEN @GroupId = 4 THEN CONVERT(NVARCHAR(4),DATEPART(YEAR, dbo.UDTToLocalTime(T1.CreatedOnUtc))) + '-' + CASE WHEN LEN(CONVERT(NVARCHAR(2),DATEPART(QUARTER, dbo.UDTToLocalTime(T1.CreatedOnUtc)))) = 1 THEN '0' + CONVERT(NVARCHAR(2),DATEPART(QUARTER, dbo.UDTToLocalTime(T1.CreatedOnUtc))) ELSE CONVERT(NVARCHAR(2),DATEPART(QUARTER, dbo.UDTToLocalTime(T1.CreatedOnUtc))) END
				WHEN @GroupId = 5 THEN CONVERT(NVARCHAR(4),DATEPART(YEAR, dbo.UDTToLocalTime(T1.CreatedOnUtc)))
				ELSE CONVERT(NVARCHAR(4),DATEPART(YEAR, dbo.UDTToLocalTime(T1.CreatedOnUtc))) + '-' + CASE WHEN LEN(CONVERT(NVARCHAR(20),DATEPART(MONTH, dbo.UDTToLocalTime(T1.CreatedOnUtc)))) = 1 THEN '0' + CONVERT(NVARCHAR(20),DATEPART(MONTH, dbo.UDTToLocalTime(T1.CreatedOnUtc))) ELSE CONVERT(NVARCHAR(20),DATEPART(MONTH, dbo.UDTToLocalTime(T1.CreatedOnUtc))) END
			END AS DimName,
				case when @priceTax = 1 then SUM(T2.PriceInclTax) else SUM(T2.PriceExclTax) end AS TotalSales,
				count(*) as Quantity
		FROM [Order] T1
		INNER JOIN [OrderItem] T2 on T2.OrderId = T1.Id
		WHERE 	(cast(dbo.UDTToLocalTime(T1.[CreatedOnUtc]) as Date) BETWEEN @StartDate AND @EndDate) 
			AND (T2.ProductId = @ProductId) 
			AND T1.OrderStatusId like (CASE WHEN @OrderStatusId = 0 then '[1234]0' Else convert(nvarchar(2),@OrderStatusId) end)
			AND T1.PaymentStatusId like (CASE WHEN @PaymentStatusId = 0 then '[1234]%' Else convert(nvarchar(2),@PaymentStatusId) end)
			AND T1.Deleted = 0
		GROUP BY 
				CASE 
					WHEN @GroupId = 1 THEN CONVERT(NVARCHAR(10),dbo.UDTToLocalTime(T1.CreatedOnUtc),120) 
					WHEN @GroupId = 2 THEN CONVERT(NVARCHAR(4),DATEPART(YEAR, dbo.UDTToLocalTime(T1.CreatedOnUtc))) + '-' + CASE WHEN LEN(CONVERT(NVARCHAR(2),DATEPART(WEEK, dbo.UDTToLocalTime(T1.CreatedOnUtc)))) = 1 THEN '0' + CONVERT(NVARCHAR(2),DATEPART(WEEK, dbo.UDTToLocalTime(T1.CreatedOnUtc))) ELSE CONVERT(NVARCHAR(2),DATEPART(WEEK, dbo.UDTToLocalTime(T1.CreatedOnUtc))) END
					WHEN @GroupId = 3 THEN CONVERT(NVARCHAR(4),DATEPART(YEAR, dbo.UDTToLocalTime(T1.CreatedOnUtc))) + '-' + CASE WHEN LEN(CONVERT(NVARCHAR(20),DATEPART(MONTH, dbo.UDTToLocalTime(T1.CreatedOnUtc)))) = 1 THEN '0' + CONVERT(NVARCHAR(20),DATEPART(MONTH, dbo.UDTToLocalTime(T1.CreatedOnUtc))) ELSE CONVERT(NVARCHAR(20),DATEPART(MONTH, dbo.UDTToLocalTime(T1.CreatedOnUtc))) END
					WHEN @GroupId = 4 THEN CONVERT(NVARCHAR(4),DATEPART(YEAR, dbo.UDTToLocalTime(T1.CreatedOnUtc))) + '-' + CASE WHEN LEN(CONVERT(NVARCHAR(2),DATEPART(QUARTER, dbo.UDTToLocalTime(T1.CreatedOnUtc)))) = 1 THEN '0' + CONVERT(NVARCHAR(2),DATEPART(QUARTER, dbo.UDTToLocalTime(T1.CreatedOnUtc))) ELSE CONVERT(NVARCHAR(2),DATEPART(QUARTER, dbo.UDTToLocalTime(T1.CreatedOnUtc))) END
					WHEN @GroupId = 5 THEN CONVERT(NVARCHAR(4),DATEPART(YEAR, dbo.UDTToLocalTime(T1.CreatedOnUtc)))
					ELSE CONVERT(NVARCHAR(4),DATEPART(YEAR, dbo.UDTToLocalTime(T1.CreatedOnUtc))) + '-' + CASE WHEN LEN(CONVERT(NVARCHAR(20),DATEPART(MONTH, dbo.UDTToLocalTime(T1.CreatedOnUtc)))) = 1 THEN '0' + CONVERT(NVARCHAR(20),DATEPART(MONTH, dbo.UDTToLocalTime(T1.CreatedOnUtc))) ELSE CONVERT(NVARCHAR(20),DATEPART(MONTH, dbo.UDTToLocalTime(T1.CreatedOnUtc))) END
				END
	end

	if @ControlId in (3,4)
	Begin

			SELECT T4.Name + case when isnull(T5.Name,'') != '' then ' >> '+isnull(T5.Name,'') else '' end AS DimName,	
			SUM(T2.Quantity) AS Quantity, 
			case when @priceTax = 1 then SUM(T2.PriceInclTax) else SUM(T2.PriceExclTax) end AS TotalSales FROM [Order] T1
			INNER JOIN [OrderItem] T2 ON T1.Id = T2.OrderId
			INNER JOIN [Address] T3 on T3.Id = T1.BillingAddressId
			INNER JOIN [Country] T4 on T4.Id = T3.CountryId
			LEFT JOIN [StateProvince] T5 on T5.Id = T3.StateProvinceId and T5.CountryId = T4.Id 
			WHERE (cast(dbo.UDTToLocalTime(T1.[CreatedOnUtc]) as Date) BETWEEN @StartDate AND @EndDate) 
			AND (T2.ProductId = @ProductId)
			AND T1.OrderStatusId like (CASE WHEN @OrderStatusId = 0 then '[1234]0' Else convert(nvarchar(2),@OrderStatusId) end)
			AND T1.PaymentStatusId like (CASE WHEN @PaymentStatusId = 0 then '[1234]%' Else convert(nvarchar(2),@PaymentStatusId) end) 
			AND T1.Deleted = 0
			GROUP BY T4.Name, T5.Name
			ORDER BY SUM(T1.OrderSubtotalExclTax) DESC


	End

	if @ControlId = 5
	Begin

			SELECT T3.Email as DimName,	
			SUM(T2.Quantity) AS Quantity, 
			case when @priceTax = 1 then SUM(T2.PriceInclTax) else SUM(T2.PriceExclTax) end AS TotalSales FROM [Order] T1
			INNER JOIN [OrderItem] T2 ON T1.Id = T2.OrderId
			INNER JOIN [Customer] T3 ON T3.Id = T1.CustomerId
			WHERE (cast(dbo.UDTToLocalTime(T1.[CreatedOnUtc]) as Date) BETWEEN @StartDate AND @EndDate) 
			AND (T2.ProductId = @ProductId)
			AND T1.OrderStatusId like (CASE WHEN @OrderStatusId = 0 then '[1234]0' Else convert(nvarchar(2),@OrderStatusId) end)
			AND T1.PaymentStatusId like (CASE WHEN @PaymentStatusId = 0 then '[1234]%' Else convert(nvarchar(2),@PaymentStatusId) end) 
			AND T1.Deleted = 0
			GROUP BY T3.Email
			ORDER BY SUM(T1.OrderSubtotalExclTax) DESC

	End
	if @ControlId = 6
	Begin

			SELECT 'Total Sales' as DimName,
			case when @priceTax = 1 then isnull(SUM(T2.PriceInclTax),0) else isnull(SUM(T2.PriceExclTax),0) end AS TotalSales FROM [Order] T1
			INNER JOIN [OrderItem] T2 ON T1.Id = T2.OrderId
			INNER JOIN [Customer] T3 ON T3.Id = T1.CustomerId
			WHERE (cast(dbo.UDTToLocalTime(T1.[CreatedOnUtc]) as Date) BETWEEN @StartDate AND @EndDate) 
			AND (T2.ProductId = @ProductId)
			AND T1.OrderStatusId like (CASE WHEN @OrderStatusId = 0 then '[1234]0' Else convert(nvarchar(2),@OrderStatusId) end)
			AND T1.PaymentStatusId like (CASE WHEN @PaymentStatusId = 0 then '[1234]%' Else convert(nvarchar(2),@PaymentStatusId) end) 			
			AND T1.Deleted = 0

			union all

			SELECT 'Total Quantity' as DimName,
			isnull(SUM(T2.Quantity),0) AS TotalSales FROM [Order] T1
			INNER JOIN [OrderItem] T2 ON T1.Id = T2.OrderId
			INNER JOIN [Customer] T3 ON T3.Id = T1.CustomerId
			WHERE (cast(dbo.UDTToLocalTime(T1.[CreatedOnUtc]) as Date) BETWEEN @StartDate AND @EndDate) 
			AND (T2.ProductId = @ProductId)
			AND T1.OrderStatusId like (CASE WHEN @OrderStatusId = 0 then '[1234]0' Else convert(nvarchar(2),@OrderStatusId) end)
			AND T1.PaymentStatusId like (CASE WHEN @PaymentStatusId = 0 then '[1234]%' Else convert(nvarchar(2),@PaymentStatusId) end) 			
			AND T1.Deleted = 0
			union all

			SELECT 'Quantity Order' as DimName,
			count(*) AS TotalSales FROM [Order] T1
			INNER JOIN [OrderItem] T2 ON T1.Id = T2.OrderId
			INNER JOIN [Customer] T3 ON T3.Id = T1.CustomerId
			WHERE (cast(dbo.UDTToLocalTime(T1.[CreatedOnUtc]) as Date) BETWEEN @StartDate AND @EndDate) 
			AND (T2.ProductId = @ProductId)
			AND T1.OrderStatusId like (CASE WHEN @OrderStatusId = 0 then '[1234]0' Else convert(nvarchar(2),@OrderStatusId) end)
			AND T1.PaymentStatusId like (CASE WHEN @PaymentStatusId = 0 then '[1234]%' Else convert(nvarchar(2),@PaymentStatusId) end) 			
			AND T1.Deleted = 0
	End

	if @ControlId in (7,8)
	Begin

			SELECT isnull(T4.Sku,'') +' ' + replace(T2.AttributeDescription,'<br />', char(13)) AS DimName,	
			SUM(T2.Quantity) AS Quantity, 
			case when @priceTax = 1 then SUM(T2.PriceInclTax) else SUM(T2.PriceExclTax) end AS TotalSales FROM [Order] T1
			INNER JOIN [OrderItem] T2 ON T1.Id = T2.OrderId
			INNER JOIN [Product] T3 on T3.Id = T2.ProductId
			LEFT JOIN [ProductAttributeCombination] T4 on T4.ProductId = T3.Id and T4.AttributesXml = T2.AttributesXml
			WHERE (cast(dbo.UDTToLocalTime(T1.[CreatedOnUtc]) as Date) BETWEEN @StartDate AND @EndDate) 
			AND (T2.ProductId = @ProductId)
			AND T1.OrderStatusId like (CASE WHEN @OrderStatusId = 0 then '[1234]0' Else convert(nvarchar(2),@OrderStatusId) end)
			AND T1.PaymentStatusId like (CASE WHEN @PaymentStatusId = 0 then '[1234]%' Else convert(nvarchar(2),@PaymentStatusId) end) 
			AND T1.Deleted = 0
			GROUP BY isnull(T4.Sku,'') +' ' +  replace(T2.AttributeDescription,'<br />', char(13))
			ORDER BY SUM(T1.OrderSubtotalExclTax) DESC


	End


END


GO
IF EXISTS (
		SELECT *
		FROM sys.objects
		WHERE object_id = OBJECT_ID(N'[dbo].[nop4you_function_category]') AND OBJECTPROPERTY(object_id,N'IsTableFunction') = 1)
DROP FUNCTION [dbo].[nop4you_function_category]
GO
CREATE FUNCTION [dbo].[nop4you_function_category] ( @idcategory INT, @parent bit)
RETURNS TABLE
AS

RETURN
(
	WITH tree (Id, parentid, level, name) as 
	(
	   SELECT Id, ParentCategoryId, 0 as level, name
	   FROM [dbo].[Category]
	   WHERE (case when @parent = 1 then ParentCategoryId else Id end) = @idcategory

	   UNION ALL

	   SELECT c2.Id, c2.ParentCategoryId, tree.level + 1, c2.name
	   FROM [dbo].[Category] c2 
		 INNER JOIN tree ON tree.Id = c2.ParentCategoryId
	)
	SELECT * FROM tree
)
GO
IF EXISTS (
		SELECT *
		FROM sys.objects
		WHERE object_id = OBJECT_ID(N'[dbo].[nop4you_dashboard_mtable]') AND OBJECTPROPERTY(object_id,N'IsProcedure') = 1)
DROP PROCEDURE [dbo].[nop4you_dashboard_mtable]
GO
CREATE PROCEDURE [dbo].[nop4you_dashboard_mtable]
(

	@StartDate			Datetime = '19000101',
	@EndDate			Datetime = '20991231',
	@OrderStatusId		int = null,
	@PaymentStatusId	int = null	

)
AS
BEGIN

	set @OrderStatusId = isnull(@OrderStatusId,0)
	set @PaymentStatusId = isnull(@PaymentStatusId,0)

	IF ((@StartDate is null) OR (@StartDate = '')) SET @StartDate = '19000101'
	IF ((@EndDate is null) OR (@EndDate = '')) SET @EndDate = '20991231'


	select isnull(T3.Email, 'Guest-'+isnull(T4.Email,'')) as [EmailCustomer],
	T0.PaymentMethodSystemName as PaymentMethod, T0.ShippingMethod,
	T5.Name as Billing_Country, T4.City as Billing_City,
	T2.Name as ProductName, sum(T1.Quantity) as Quantity, T1.UnitPriceExclTax, 
	sum(T1.PriceExclTax) as LineTotalExclTax,
	convert(nvarchar(4), year(T0.CreatedOnUtc)) as [Year],
	convert(nvarchar(4), month(T0.CreatedOnUtc)) as [Month],
	replace(AttributeDescription, '<br />', char(13)) as Attributes,
	isnull(T6.SKu, T2.SKU)  as SKU, T0.Id as OrderId
	 from [Order] T0
	inner join [OrderItem] T1 on T0.Id = T1.OrderId
	inner join [Product] T2 on T2.Id = T1.ProductId
	inner join [Customer] T3 on T3.Id = T0.CustomerId
	inner join [Address] T4 on T4.Id = T0.BillingAddressId
	left join [Country] T5 on T5.Id = T4.CountryId
	LEFT JOIN [ProductAttributeCombination] T6 on T6.ProductId = T2.Id and T6.AttributesXml = T1.AttributesXml
	where 
	(cast(dbo.UDTToLocalTime(T0.[CreatedOnUtc]) as Date) BETWEEN @StartDate AND @EndDate) 			
	AND T0.OrderStatusId like (CASE WHEN @OrderStatusId = 0 then '[1234]0' Else convert(nvarchar(2),@OrderStatusId) end)
	AND T0.PaymentStatusId like (CASE WHEN @PaymentStatusId = 0 then '[1234]%' Else convert(nvarchar(2),@PaymentStatusId) end)
	AND T0.Deleted = 0
	group by  isnull(T3.Email, 'Guest-'+isnull(T4.Email,'')),
	T0.PaymentMethodSystemName, T0.ShippingMethod, T5.Name, T4.City, T2.Name, T1.UnitPriceExclTax,
	year(T0.CreatedOnUtc), month(T0.CreatedOnUtc), replace(AttributeDescription, '<br />', char(13)), T2.SKU, T0.Id, T6.SKu

END
GO
IF EXISTS (
		SELECT *
		FROM sys.objects
		WHERE object_id = OBJECT_ID(N'[dbo].[nop4you_dashboard_admin]') AND OBJECTPROPERTY(object_id,N'IsProcedure') = 1)
DROP PROCEDURE [dbo].[nop4you_dashboard_admin]
GO
CREATE PROCEDURE [dbo].[nop4you_dashboard_admin]
(
	@ControlId			int = null
)
AS
BEGIN

	declare @StartDate datetime
	declare @EndDate datetime
	declare @priceTax bit
	declare @OrderStatusId int
	declare @PaymentStatusId int
	declare @topN INT
	declare @lastMonth INT

	set @OrderStatusId = isnull(((select Value from Setting where Name = 'dashboardsettings.orderstatus')),0)
	set @PaymentStatusId = isnull(((select Value from Setting where Name = 'dashboardsettings.paymentstatus')),0)

	SET @topN = (select Value from Setting where Name = 'dashboardsettings.topn')
	SET @lastMonth = (select Value from Setting where Name = 'dashboardsettings.lastmonth')
	SET @priceTax = (select case when [Value]='true' then '1' else '0' end from Setting where Name = 'dashboardsettings.PriceWithTax')


	if @ControlId in (1)
	Begin
		set @StartDate = getDate();

		SELECT 'Previous' AS DimName,
			isnull(case when @priceTax = 1 then SUM(T1.OrderTotal) else SUM(T1.OrderTotal-T1.OrderTax) end,0) AS TotalSales 
			FROM [Order] T1			
			WHERE 
				DatePart(wk, cast(dbo.UDTToLocalTime(T1.[CreatedOnUtc]) as Date)) = DatePart(wk, @StartDate)-1 and YEAR(T1.[CreatedOnUtc]) = year(@StartDate)
				AND T1.OrderStatusId like (CASE WHEN @OrderStatusId = 0 then '[1234]0' Else convert(nvarchar(2),@OrderStatusId) end)
				AND T1.PaymentStatusId like (CASE WHEN @PaymentStatusId = 0 then '[1234]%' Else convert(nvarchar(2),@PaymentStatusId) end)
				AND T1.Deleted = 0
		union all
		SELECT 'Current' AS DimName,
			isnull(case when @priceTax = 1 then SUM(T1.OrderTotal) else SUM(T1.OrderTotal-T1.OrderTax) end,0) AS TotalSales 
			FROM [Order] T1			
			WHERE 
				DatePart(wk, cast(dbo.UDTToLocalTime(T1.[CreatedOnUtc]) as Date)) = DatePart(wk, @StartDate) and YEAR(T1.[CreatedOnUtc]) = year(@StartDate)
				AND T1.OrderStatusId like (CASE WHEN @OrderStatusId = 0 then '[1234]0' Else convert(nvarchar(2),@OrderStatusId) end)
				AND T1.PaymentStatusId like (CASE WHEN @PaymentStatusId = 0 then '[1234]%' Else convert(nvarchar(2),@PaymentStatusId) end)
				AND T1.Deleted = 0
		
		



	END

	if @ControlId in (2)
	Begin
		set @StartDate = getDate();


		SELECT 'Previous' AS DimName,
			isnull(case when @priceTax = 1 then SUM(T1.OrderTotal) else SUM(T1.OrderTotal-T1.OrderTax) end,0) AS TotalSales 
			FROM [Order] T1			
			WHERE 
				DatePart(mm, cast(dbo.UDTToLocalTime(T1.[CreatedOnUtc]) as Date)) = DatePart(mm, @StartDate)-1 and YEAR(T1.[CreatedOnUtc]) = year(@StartDate)
				AND T1.OrderStatusId like (CASE WHEN @OrderStatusId = 0 then '[1234]0' Else convert(nvarchar(2),@OrderStatusId) end)
				AND T1.PaymentStatusId like (CASE WHEN @PaymentStatusId = 0 then '[1234]%' Else convert(nvarchar(2),@PaymentStatusId) end)
				AND T1.Deleted = 0
		union all
		SELECT 'Current' AS DimName,
			isnull(case when @priceTax = 1 then SUM(T1.OrderTotal) else SUM(T1.OrderTotal-T1.OrderTax) end,0) AS TotalSales 
			FROM [Order] T1			
			WHERE 
				DatePart(mm, cast(dbo.UDTToLocalTime(T1.[CreatedOnUtc]) as Date)) = DatePart(mm, @StartDate) and YEAR(T1.[CreatedOnUtc]) = year(@StartDate)
				AND T1.OrderStatusId like (CASE WHEN @OrderStatusId = 0 then '[1234]0' Else convert(nvarchar(2),@OrderStatusId) end)
				AND T1.PaymentStatusId like (CASE WHEN @PaymentStatusId = 0 then '[1234]%' Else convert(nvarchar(2),@PaymentStatusId) end)
				AND T1.Deleted = 0
		

	END
	if @ControlId in (3)
	Begin
		set @StartDate = getDate();

		SELECT 'Previous' AS DimName,
			isnull(case when @priceTax = 1 then SUM(T1.OrderTotal) else SUM(T1.OrderTotal-T1.OrderTax) end,0) AS TotalSales 
			FROM [Order] T1			
			WHERE 
				DatePart(yy, cast(dbo.UDTToLocalTime(T1.[CreatedOnUtc]) as Date)) = DatePart(yy, @StartDate)-1 
				AND T1.OrderStatusId like (CASE WHEN @OrderStatusId = 0 then '[1234]0' Else convert(nvarchar(2),@OrderStatusId) end)
				AND T1.PaymentStatusId like (CASE WHEN @PaymentStatusId = 0 then '[1234]%' Else convert(nvarchar(2),@PaymentStatusId) end)
				AND T1.Deleted = 0
		union all
		SELECT 'Current' AS DimName,
			isnull(case when @priceTax = 1 then SUM(T1.OrderTotal) else SUM(T1.OrderTotal-T1.OrderTax) end,0) AS TotalSales 
			FROM [Order] T1			
			WHERE 
				YEAR(T1.[CreatedOnUtc]) = year(@StartDate)
				AND T1.OrderStatusId like (CASE WHEN @OrderStatusId = 0 then '[1234]0' Else convert(nvarchar(2),@OrderStatusId) end)
				AND T1.PaymentStatusId like (CASE WHEN @PaymentStatusId = 0 then '[1234]%' Else convert(nvarchar(2),@PaymentStatusId) end)
				AND T1.Deleted = 0
		


	END
	if @ControlId in (4)
	Begin
		set @StartDate = getDate();


		SELECT convert(nvarchar(8), T1.[CreatedOnUtc], 120)+'01' AS DimName, year(T1.[CreatedOnUtc]) as Quantity,
			isnull(case when @priceTax = 1 then SUM(T1.OrderTotal) else SUM(T1.OrderTotal-T1.OrderTax) end,0) AS TotalSales 
			FROM [Order] T1			
			WHERE 
				T1.OrderStatusId like (CASE WHEN @OrderStatusId = 0 then '[1234]0' Else convert(nvarchar(2),@OrderStatusId) end)
				AND T1.PaymentStatusId like (CASE WHEN @PaymentStatusId = 0 then '[1234]%' Else convert(nvarchar(2),@PaymentStatusId) end)
				AND T1.Deleted = 0
			Group by convert(nvarchar(8), T1.[CreatedOnUtc], 120), year(T1.[CreatedOnUtc]) 
			order by convert(nvarchar(8), T1.[CreatedOnUtc], 120)

	END

	if @ControlId in (5)
	Begin
		set @StartDate = getDate();

		SELECT TOP (@topN) T4.Name AS DimName, 
			isnull(case when @priceTax = 1 then SUM(T2.PriceInclTax) else SUM(T2.PriceExclTax) end,0) AS TotalSales 
			FROM [Order] T1			
			inner join [OrderItem] T2 on T2.OrderId = T1.Id
			inner join [Product_Category_Mapping] T3 on T3.ProductId = T2.ProductId
			inner join [Category] T4 on T4.Id = T3.CategoryId
			WHERE 
				year(T1.[CreatedOnUtc]) = year(@StartDate) and 
				T1.OrderStatusId like (CASE WHEN @OrderStatusId = 0 then '[1234]0' Else convert(nvarchar(2),@OrderStatusId) end)
				AND T1.PaymentStatusId like (CASE WHEN @PaymentStatusId = 0 then '[1234]%' Else convert(nvarchar(2),@PaymentStatusId) end)
				AND T1.Deleted = 0
			Group by T4.Name
			order by SUM(T2.PriceInclTax) desc


	END
	if @ControlId in (6)
	Begin
		set @StartDate = getDate();

		SELECT TOP (@topN) T4.Name AS DimName, 
			isnull(case when @priceTax = 1 then SUM(T2.PriceInclTax) else SUM(T2.PriceExclTax) end,0) AS TotalSales 
			FROM [Order] T1			
			inner join [OrderItem] T2 on T2.OrderId = T1.Id
			inner join [Product_Manufacturer_Mapping] T3 on T3.ProductId = T2.ProductId
			inner join [Manufacturer] T4 on T4.Id = T3.ManufacturerId
			WHERE 
				year(T1.[CreatedOnUtc]) = year(@StartDate) and 
				T1.OrderStatusId like (CASE WHEN @OrderStatusId = 0 then '[1234]0' Else convert(nvarchar(2),@OrderStatusId) end)
				AND T1.PaymentStatusId like (CASE WHEN @PaymentStatusId = 0 then '[1234]%' Else convert(nvarchar(2),@PaymentStatusId) end)
				AND T1.Deleted = 0
			Group by T4.Name
			order by SUM(T2.PriceInclTax) desc


	END

	if @ControlId in (7)
	Begin
		set @StartDate = getDate();

		SELECT TOP (@topN) T4.Name AS DimName, 
			isnull(case when @priceTax = 1 then SUM(T2.PriceInclTax) else SUM(T2.PriceExclTax) end,0) AS TotalSales  
			FROM [Order] T1			
			inner join [OrderItem] T2 on T2.OrderId = T1.Id
			inner join [Product] T3 on T3.Id = T2.ProductId
			inner join [Vendor] T4 on T4.Id = T3.VendorId
			WHERE 
				year(T1.[CreatedOnUtc]) = year(@StartDate) and 
				T1.OrderStatusId like (CASE WHEN @OrderStatusId = 0 then '[1234]0' Else convert(nvarchar(2),@OrderStatusId) end)
				AND T1.PaymentStatusId like (CASE WHEN @PaymentStatusId = 0 then '[1234]%' Else convert(nvarchar(2),@PaymentStatusId) end)
				AND T1.Deleted = 0
			Group by T4.Name
			order by SUM(T2.PriceInclTax) desc


	END

	if @ControlId in (8)
	Begin
		set @StartDate = getDate();

		SELECT TOP (@topN) isnull(T4.Name, T3.Name) AS DimName, 
			isnull(case when @priceTax = 1 then SUM(T2.PriceInclTax) else SUM(T2.PriceExclTax) end,0) AS TotalSales  
			FROM [Order] T1			
			inner join [OrderItem] T2 on T2.OrderId = T1.Id
			inner join [Product] T3 on T3.Id = T2.ProductId	
			left join [Product] T4 on T4.Id = T3.ParentGroupedProductId		
			WHERE 
				year(T1.[CreatedOnUtc]) = year(@StartDate) and 
				T1.OrderStatusId like (CASE WHEN @OrderStatusId = 0 then '[1234]0' Else convert(nvarchar(2),@OrderStatusId) end)
				AND T1.PaymentStatusId like (CASE WHEN @PaymentStatusId = 0 then '[1234]%' Else convert(nvarchar(2),@PaymentStatusId) end)
				AND T1.Deleted = 0
			Group by isnull(T4.Name, T3.Name)
			order by SUM(T2.PriceInclTax) desc

	END
	if @ControlId in (9)
	Begin
		set @StartDate = getDate();

		SELECT TOP (@topN) isnull(T4.Name, T3.Name) AS DimName, 
			isnull(case when @priceTax = 1 then SUM(T2.PriceInclTax) else SUM(T2.PriceExclTax) end,0) AS TotalSales  
			FROM [Order] T1			
			inner join [OrderItem] T2 on T2.OrderId = T1.Id
			inner join [Product] T3 on T3.Id = T2.ProductId	
			left join [Product] T4 on T4.Id = T3.ParentGroupedProductId		
			WHERE 
				T1.CreatedOnUtc >= Dateadd(mm, -(@lastMonth), GETDATE())
				AND T1.OrderStatusId like (CASE WHEN @OrderStatusId = 0 then '[1234]0' Else convert(nvarchar(2),@OrderStatusId) end)
				AND T1.PaymentStatusId like (CASE WHEN @PaymentStatusId = 0 then '[1234]%' Else convert(nvarchar(2),@PaymentStatusId) end)
				AND T1.Deleted = 0
			Group by isnull(T4.Name, T3.Name)
			order by SUM(T2.PriceInclTax) desc

	END
END
GO