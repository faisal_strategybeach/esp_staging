﻿using Nop.Core;
using Nop.Core.Domain;
using Nop.Services.Localization;
using Nop.Services;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Web.Mvc;
using Nop.Services.Security;
using Nop.Services.Stores;
using Nop.Services.Vendors;
using Nop.Services.Shipping;
using Nop.Core.Domain.Payments;
using Nop.Core.Domain.Shipping;
using Nop.Web.Framework.Kendoui;
using Nop.Services.Helpers;
using Nop.Admin.Models.Orders;
using Nop.Services.Catalog;
using Nop.Services.Orders;
using Nop.Services.Directory;
using Nop.Core.Domain.Directory;
using Nop.Core.Domain.Catalog;
using Nop.Web.Framework;
using Nop.Web.Framework.Controllers;
using Nop.Core.Plugins;

namespace Nop.Plugin.Misc.OrderStatus.Controllers
{
    public class OrderStatusController : BasePluginController
    {
        private readonly IPluginFinder _pluginFinder;
        private readonly IPermissionService _permissionService;
        private readonly ILocalizationService _localizationService;
        private readonly IWorkContext _workContext;
        private readonly IShippingService _shippingService;
        private readonly IStoreService _storeService;
        private readonly IVendorService _vendorService;
        private readonly IDateTimeHelper _dateTimeHelper;
        private readonly IPriceFormatter _priceFormatter;
        private readonly IOrderService _orderService;
        private readonly IProductService _productService;
        private readonly ICurrencyService _currencyService;
        private readonly CurrencySettings _currencySettings;




        public OrderStatusController(IPermissionService permissionService,
            ILocalizationService localizationService,
            IWorkContext workContext,
            IShippingService shippingService,
            IStoreService storeService,
            IVendorService vendorService,
            IDateTimeHelper dateTimeHelper,
            IPriceFormatter priceFormatter,
            IOrderService orderService,
            IProductService productService,
            ICurrencyService currencyService,
        CurrencySettings currencySettings,
            IPluginFinder pluginFinder
            )
        {
            this._permissionService = permissionService;
            this._localizationService = localizationService;
            this._workContext = workContext;
            this._vendorService = vendorService;
            this._shippingService = shippingService;
            this._storeService = storeService;
            this._dateTimeHelper = dateTimeHelper;
            this._priceFormatter = priceFormatter;
            this._orderService = orderService;
            this._productService = productService;
            this._currencyService = currencyService;
            this._currencySettings = currencySettings;
            this._pluginFinder = pluginFinder;
        }

        public ActionResult Index()
        {
            return RedirectToAction("List");
        }

        #region Order list
        public ActionResult List(int? orderStatusId = null,
            int? paymentStatusId = null, int? shippingStatusId = null)
        {
            var pluginDescriptor = _pluginFinder.GetPluginDescriptorBySystemName("Misc.OrderStatus");
            if (pluginDescriptor == null || pluginDescriptor.Installed == false)
                return RedirectToAction("PageNotFound","Common");
            
              if (!_permissionService.Authorize(StandardPermissionProvider.ManageOrders))
                  return RedirectToAction("AccessDenied", "Security", new { pageUrl = this.Request.RawUrl });
              
            //order statuses
            var model = new Nop.Admin.Models.Orders.OrderListModel();
            model.AvailableOrderStatuses = Nop.Core.Domain.Orders.OrderStatus.Pending.ToSelectList(false).ToList();
            model.AvailableOrderStatuses.Insert(0, new SelectListItem { Text = _localizationService.GetResource("Admin.Common.All"), Value = "0" });
            if (orderStatusId.HasValue)
            {
                //pre-select value?
                var item = model.AvailableOrderStatuses.FirstOrDefault(x => x.Value == orderStatusId.Value.ToString());
                if (item != null)
                    item.Selected = true;
            }

            //payment statuses
            model.AvailablePaymentStatuses = PaymentStatus.Pending.ToSelectList(false).ToList();
            model.AvailablePaymentStatuses.Insert(0, new SelectListItem { Text = _localizationService.GetResource("Admin.Common.All"), Value = "0" });
            if (paymentStatusId.HasValue)
            {
                //pre-select value?
                var item = model.AvailablePaymentStatuses.FirstOrDefault(x => x.Value == paymentStatusId.Value.ToString());
                if (item != null)
                    item.Selected = true;
            }

            //shipping statuses
            model.AvailableShippingStatuses = ShippingStatus.NotYetShipped.ToSelectList(false).ToList();
            model.AvailableShippingStatuses.Insert(0, new SelectListItem { Text = _localizationService.GetResource("Admin.Common.All"), Value = "0" });
            if (shippingStatusId.HasValue)
            {
                //pre-select value?
                var item = model.AvailableShippingStatuses.FirstOrDefault(x => x.Value == shippingStatusId.Value.ToString());
                if (item != null)
                    item.Selected = true;
            }

            //stores
            model.AvailableStores.Add(new SelectListItem { Text = _localizationService.GetResource("Admin.Common.All"), Value = "0" });
            foreach (var s in _storeService.GetAllStores())
                model.AvailableStores.Add(new SelectListItem { Text = s.Name, Value = s.Id.ToString() });

            //vendors
            model.AvailableVendors.Add(new SelectListItem { Text = _localizationService.GetResource("Admin.Common.All"), Value = "0" });
            foreach (var v in _vendorService.GetAllVendors(showHidden: true))
                model.AvailableVendors.Add(new SelectListItem { Text = v.Name, Value = v.Id.ToString() });

            //warehouses
            model.AvailableWarehouses.Add(new SelectListItem { Text = _localizationService.GetResource("Admin.Common.All"), Value = "0" });
            foreach (var w in _shippingService.GetAllWarehouses())
                model.AvailableWarehouses.Add(new SelectListItem { Text = w.Name, Value = w.Id.ToString() });

            //a vendor should have access only to orders with his products
            model.IsLoggedInAsVendor = _workContext.CurrentVendor != null;

            return View("~/Plugins/Misc.OrderStatus/Views/MiscOrderStatus/List.cshtml", model);
        }

        [HttpPost]
        public ActionResult OrderList(DataSourceRequest command, Nop.Admin.Models.Orders.OrderListModel model)
        {
            if (!_permissionService.Authorize(StandardPermissionProvider.ManageOrders))
                return RedirectToAction("AccessDenied", "Security", new { pageUrl = this.Request.RawUrl });
              
              
            //a vendor should have access only to his products
            if (_workContext.CurrentVendor != null)
            {
                model.VendorId = _workContext.CurrentVendor.Id;
            }

            DateTime? startDateValue = (model.StartDate == null) ? null
                            : (DateTime?)_dateTimeHelper.ConvertToUtcTime(model.StartDate.Value, _dateTimeHelper.CurrentTimeZone);

            DateTime? endDateValue = (model.EndDate == null) ? null
                            : (DateTime?)_dateTimeHelper.ConvertToUtcTime(model.EndDate.Value, _dateTimeHelper.CurrentTimeZone).AddDays(1);

            Nop.Core.Domain.Orders.OrderStatus? orderStatus = model.OrderStatusId > 0 ? (Nop.Core.Domain.Orders.OrderStatus?)(model.OrderStatusId) : null;
            PaymentStatus? paymentStatus = model.PaymentStatusId > 0 ? (PaymentStatus?)(model.PaymentStatusId) : null;
            ShippingStatus? shippingStatus = model.ShippingStatusId > 0 ? (ShippingStatus?)(model.ShippingStatusId) : null;

            //load orders
            var orders = _orderService.SearchOrders(storeId: model.StoreId,
                vendorId: model.VendorId,
                //productId: filterByProductId,
                warehouseId: model.WarehouseId,
                createdFromUtc: startDateValue,
                createdToUtc: endDateValue,
                os: orderStatus,
                ps: paymentStatus,
                ss: shippingStatus,
                billingEmail: model.CustomerEmail,
                orderGuid: model.OrderGuid,
                pageIndex: command.Page - 1,
                pageSize: command.PageSize);
            var gridModel = new DataSourceResult
            {
                Data = orders.Select(x =>
                {
                    var store = _storeService.GetStoreById(x.StoreId);
                    return new OrderModel
                    {
                        Id = x.Id,
                        StoreName = store != null ? store.Name : "Unknown",
                        OrderTotal = _priceFormatter.FormatPrice(x.OrderTotal, true, false),
                        OrderStatus = x.OrderStatus.GetLocalizedEnum(_localizationService, _workContext),
                        PaymentStatus = x.PaymentStatus.GetLocalizedEnum(_localizationService, _workContext),
                        ShippingStatus = x.ShippingStatus.GetLocalizedEnum(_localizationService, _workContext),
                        CustomerEmail = x.BillingAddress.Email,
                        CustomerFullName = string.Format("{0} {1}", x.BillingAddress.FirstName, x.BillingAddress.LastName),
                        CreatedOn = _dateTimeHelper.ConvertToUserTime(x.CreatedOnUtc, DateTimeKind.Utc)
                    };
                }),
                Total = orders.TotalCount
            };
            
            return new JsonResult
            {
                Data = gridModel
            };
        }

      
        #endregion
        [HttpPost]
        public ActionResult Update(string selectedIds, int OrderStatusIdUpdate)
        {
            
            if (selectedIds != null && OrderStatusIdUpdate > 0)
            {
                //return ids order
                var ids = selectedIds
                    .Split(new[] { ',' }, StringSplitOptions.RemoveEmptyEntries)
                    .Select(x => Convert.ToInt32(x))
                    .ToArray();

                foreach (int idOrder in ids)
                {
                    var order = _orderService.GetOrderById(idOrder);
                    if (order == null)
                        //No order found with the specified id
                        return RedirectToAction("List");
                    order.OrderStatusId =OrderStatusIdUpdate;
                    _orderService.UpdateOrder(order);
                }
            }

            return base.RedirectToAction("List");
        }

    }
}
