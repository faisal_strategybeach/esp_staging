﻿using Nop.Core;
using Nop.Core.Plugins;
using Nop.Web;
using Nop.Web.Framework.Menu;
using System.Web.Routing;
using System.Linq;

namespace Nop.Plugin.Misc.OrderStatus
{
    public class OrderStatus : BasePlugin, IAdminMenuPlugin, IPlugin
    {
        public override void Install()
        {
                   base.Install();
        }
        public override void Uninstall()
        {
                        base.Uninstall();
        }
        public bool Authenticate()
        {
            return true;

        }

        public SiteMapNode BuildMenuItem()
        {
         
            var SubMenuItem = new SiteMapNode()   
            {
                Title = "Update order status",
                ControllerName = "OrderStatus", 
                ActionName = "List", 
                Visible = true,
                RouteValues = new RouteValueDictionary() { { "Area", null } },
            };
            return SubMenuItem;
        }

        public void ManageSiteMap(SiteMapNode rootNode)
        {
            var menuItem = new SiteMapNode()
            {
                SystemName = "Order status",
                Title = "Update order status",
                ControllerName = "OrderStatus",
                ActionName = "List",
                Visible = true,
                RouteValues = new RouteValueDictionary() { { "Area", null } },
            };
            var pluginNode = rootNode.ChildNodes.FirstOrDefault(x => x.SystemName == "Third party plugins");
            if (pluginNode != null)
                pluginNode.ChildNodes.Add(menuItem);
            else
                rootNode.ChildNodes.Add(menuItem);
        }
    }
}
