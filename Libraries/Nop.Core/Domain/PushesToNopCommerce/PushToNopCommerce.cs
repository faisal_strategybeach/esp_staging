﻿using Nop.Core.Domain.Localization;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Nop.Core.Domain.PushesToNopCommerce
{
    public partial class PushToNopCommerce : BaseEntity, ILocalizedEntity
    {
        public string CompanyCode { get; set; }
        public string PubCode { get; set; }
        public DateTime PublicationDate { get; set; }
        public string LCInitials { get; set; }
        public DateTime LCDate { get; set; }
        public DateTime AddDate { get; set; }
        public string PublicationURL { get; set; }
        public string Description { get; set; }
        public string EditionCode { get; set; }
        public string Active { get; set; }
        public string PublicationID { get; set; }
        public string PublicationDateDescription { get; set; }

    }
}
