﻿Imports Nop.Core
Imports Nop.Web
Imports System
Imports System.Text
Imports System.Threading.Tasks
Imports System.Web.Mvc
Imports Nop.Services.Customers
Imports System.Linq
Imports System.Data.SqlClient
Imports System.Data
Imports Nop.Services.Orders
Imports Nop.Core.Domain.Common
Imports Nop.Web.Framework.Controllers

Public Class CreateSubscriberController
    Inherits BasePluginController


    Private _workContext As IWorkContext
    Private _storeContext As IStoreContext
    Private _customerService As ICustomerService

    'static public string _acctStatus; 
    Private sqlConnectionString As String

    Public Sub New(workContext As IWorkContext, storeContext As IStoreContext, customerService As ICustomerService)
        Me._workContext = workContext
        Me._storeContext = storeContext
        Me._customerService = customerService
    End Sub

    Public Sub CreateSubscriber()

        Dim shippingAddress As Address

        shippingAddress = _workContext.CurrentCustomer.ShippingAddress

        Dim strQuery As String
        strQuery = "<?xml version='1.0' encoding='utf-8'?>" &
                    "<ac:subscribers xmlns:ac='http://schema.highwire.org/Subscriber'>" &
                    "<ac:subscriber subscriberType='N' userType='INDV' password='secret' login='talanindv2' membershipNo='TALANINDV2'>" &
                    "<rp:person xmlns:rp='http://schema.highwire.org/Person'>" &
                    "   <rp:name><rp:firstname>" & shippingAddress.FirstName & " </rp:firstname><rp:lastname>" & shippingAddress.LastName & " </rp:lastname></rp:name>" &
                    "  <rp:email>" & shippingAddress.Email & " </rp:email>" &
                    " <rp:affiliation></rp:affiliation><rp:title></rp:title>" &
                    "<rp:contact><rp:phone>" & shippingAddress.PhoneNumber & " </rp:phone><rp:fax>" & shippingAddress.FaxNumber & " </rp:fax></rp:contact>" &
                    "<rp:address><rp:street>" & shippingAddress.Address1 & " </rp:street><rp:street2/><rp:city>" & shippingAddress.City & " </rp:city><rp:state></rp:state>" &
                    "   <rp:zip>" & shippingAddress.ZipPostalCode & " </rp:zip><rp:country>" & Convert.ToString((If(shippingAddress.Country Is Nothing, "", shippingAddress.Country.ThreeLetterIsoCode))) &
                    " </rp:country></rp:address></rp:person>" &
                    "<sub:subscriptions xmlns:sub='http://schema.highwire.org/Subscription'>" &
                    "   <sub:subscription expires='2015-12-31T08:00:00Z' status='ACTIVE' productCode=' HAFF'/>" &
                    "</sub:subscriptions></ac:subscriber></ac:subscribers>"
    End Sub

End Class
